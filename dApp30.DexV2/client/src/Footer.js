import React from 'react';

const Footer = () => {
  return (
    <footer>
      <p className="text-center">
        DApp30 DexV2
      </p>
    </footer>
  );
};

export default Footer;
