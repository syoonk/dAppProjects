import React, { useState, useEffect } from "react";
import ethers from "ethers";
import Header from "./Header.js";
import Footer from './Footer.js';
import Wallet from "./Wallet.js";
import NewOrder from "./NewOrder.js";
import AllOrders from "./AllOrder.js";
import MyOrders from "./MyOrders.js";
import AllTrades from "./AllTrades";

const SIDE = {
  BUY: 0,
  SELL: 1
};

function App({web3, accounts, contracts}) {
  const [tokens, setTokens] = useState([]);
  const [user, setUser] = useState({
    accounts: [],
    balances: {
      tokenDex: 0,
      tokenWallet: 0
    },
    selectedToken: undefined });
  const [orders, setOrders] = useState({
    buy: [],
    sell: [],
  });
  const [trades, setTrades] = useState([]);
  const [listener, setListener] = useState(undefined);
  
  const selectToken = _token => {
    setUser({...user, selectedToken: _token});
  };

  const getBalances = async (_account, _token) => {
    const tokenDex = await contracts.dex.methods
      .traderBalances(_account, web3.utils.fromAscii(_token.ticker))
      .call();
    const tokenWallet = await contracts[_token.ticker].methods
      .balanceOf(_account)
      .call();
    return {tokenDex, tokenWallet};
  }

  const getOrders = async _token => {
    const orders = await Promise.all([
      contracts.dex.methods
        .getOrders(web3.utils.fromAscii(_token.ticker), SIDE.BUY)
        .call(),
      contracts.dex.methods
        .getOrders(web3.utils.fromAscii(_token.ticker), SIDE.SELL)
        .call()
    ]);

    return {buy: orders[0], sell: orders[1]};
  }

  const listenToTrades = _token => {
    // it prevents duplicate trade ids
    const tradeIds = new Set();
    // it prevents other tokens added to same trade list
    setTrades([]);
    const listener = contracts.dex.events.NewTrade({
      filter: {ticker: web3.utils.fromAscii(_token.ticker)},
      fromBlock: 0 }) // in production it should be specified to deployed block number
      .on('data', _newTrade => {
        if(!tradeIds.has(_newTrade.returnValues.tradeId)) {
          tradeIds.add(_newTrade.returnValues.tradeId);
          setTrades(trades => ([...trades, _newTrade.returnValues]));
        }
      });
    setListener(listener);
  }

  const deposit = async _amount => {
    await contracts[user.selectedToken.ticker].methods
      .approve(contracts.dex.options.address, _amount)
      .send({from: user.accounts[0]});
    await contracts.dex.methods
    .deposit(_amount, web3.utils.fromAscii(user.selectedToken.ticker))
    .send({from: user.accounts[0]});

    const balances = await getBalances(user.accounts[0], user.selectedToken);
    setUser(user => ({...user, balances}));
  };

  const withdraw = async _amount => {
    await contracts.dex.methods
    .withdraw(_amount, web3.utils.fromAscii(user.selectedToken.ticker))
    .send({from: user.accounts[0]});

    const balances = await getBalances(user.accounts[0], user.selectedToken);
    setUser(user => ({...user, balances}));
  };

  const createMarketOrder = async (_amount, _side) => {
    await contracts.dex.methods
      .createMarketOrder(web3.utils.fromAscii(user.selectedToken.ticker), _amount, _side)
      .send({from: user.accounts[0]});

    const orders = await getOrders(user.selectedToken.ticker);
    setOrders(orders);
  };

  const createLimitOrder = async (_amount, _price, _side) => {
    await contracts.dex.methods
      .createLimitOrder(web3.utils.fromAscii(
        user.selectedToken.ticker),
        _amount,
        _price,
        _side)
      .send({from: user.accounts[0]});

    const orders = await getOrders(user.selectedToken.ticker);
    setOrders(orders);
  };

  useEffect(() => {
    const init = async () => {
      const rawTokens = await contracts.dex.methods.getTokens().call();
      const tokens = rawTokens.map(token => ({
        ...token,
        ticker: web3.utils.hexToUtf8(token.ticker)
      }));

      const balances = await getBalances(accounts[0], tokens[0]);
      const orders = await getOrders(tokens[0]);
      listenToTrades(tokens[0]);
      setTokens(tokens);
      setUser({accounts, balances, selectedToken: tokens[0]});
      setOrders(orders);
    }
    init();
  }, [accounts]);

  useEffect(() => {
    const init = async () => {
      const balances = await getBalances(accounts[0], user.selectedToken);
      const orders = await getOrders(user.selectedToken);
      listenToTrades(user.selectedToken);
      setUser(user => ({...user, balances}));
      setOrders(orders);
    }
    if(typeof user.selectedToken !== 'undefined') { init(); } 
  }, [user.selectedToken], () => { listener.unsubscribe(); } );

  if(typeof user.selectedToken == 'undefined') {
    return <div>Loading...</div>;
  }

  return (
    <div id="app">
      <Header
        contracts={contracts}
        tokens={tokens}
        user={user}
        selectToken={selectToken}></Header>
      <main className="container-fluid">
        <div className="row">
          <div className="col-sm-4 first-col">
            <Wallet
              user={user}
              deposit={deposit}
              withdraw={withdraw}
            ></Wallet>
            {user.selectedToken.ticker !== 'DAI' ? (
              <NewOrder
                createMarketOrder={createMarketOrder}
                createLimitOrder={createLimitOrder}
              ></NewOrder>
            ) : null }
          </div>
          {user.selectedToken.ticker !== 'DAI' ? (
            <div className="col-sm-8">
              <AllTrades
                trades={trades}
              ></AllTrades>
              <AllOrders
                orders={orders}
              ></AllOrders>
              <MyOrders
                orders={ {
                  buy: orders.buy.filter(
                    order => order.trader.toLowerCase() === user.accounts[0].toLowerCase()
                  ),
                  sell: orders.sell.filter(
                    order => order.trader.toLowerCase() === user.accounts[0].toLowerCase()
                  )
                } }
              ></MyOrders>
            </div>
          ) : null }
        </div>
      </main>
      <Footer />
    </div>
  );
}

export default App;
