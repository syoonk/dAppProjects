import React, { useState, useEffect } from "react";
import Web3 from 'web3';
import Dex from './contracts/Dex.json';
import ERC20Abi from './ERC20Abi.json';
import App from './App.js';

function LoadingContainer() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState([]);
  const [contracts, setContracts] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      // Searching for modern dapp browser
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } 
      // Legacy dapp browser
      else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } 
      // Develop console host
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Dex.networks[networkId];
      const dex = new web3.eth.Contract(
        Dex.abi,
        deployedNetwork && deployedNetwork.address,
      );
      const tokens = await dex.methods.getTokens().call();
      const tokenContracts = tokens.reduce((acc, token) => ({
        ...acc,
        [web3.utils.hexToUtf8(token.ticker)]: new web3.eth.Contract(
          ERC20Abi,
          token.tokenAddress
        )
      }), {});
      const contracts = { dex, ...tokenContracts }

      setWeb3(web3);
      setContracts(contracts);
    }
    init();
    window.ethereum.on('accountsChanged', accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof web3 !== 'undefined' 
      && typeof contracts !== 'undefined'
      && accounts.length > 0
    );
  }

  if (!isReady()) {
    return <div>Loading...</div>;
  }

  return (
    <App
      web3={web3}
      accounts={accounts}
      contracts={contracts}
    />
  );
}

export default LoadingContainer;
