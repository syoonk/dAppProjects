pragma solidity ^0.6.6;
pragma experimental "ABIEncoderV2";
    
// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";
// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

contract Dex {
    using SafeMath for uint;
    
    enum Side {
        BUY,
        SELL
    }
    struct Token {
        bytes32 ticker;
        address tokenAddress;
    }
    struct Order {
        uint id;
        address trader;
        Side side;
        bytes32 ticker;
        uint amount;
        uint filled;
        uint price;
        uint date;
    }
    mapping(bytes32 => Token) public tokens;
    mapping(address => mapping(bytes32 => uint)) public traderBalances;
    mapping(bytes32 => mapping(uint => Order[])) orderBook;
    bytes32[] public tokenList;
    address public admin;
    uint nextOrderId;
    uint nextTradeId;
    bytes32 constant DAI = bytes32('DAI');
    
    event NewTrade(
        uint traderId,
        uint orderId,
        bytes32 indexed ticker,
        address indexed trader1,
        address indexed trader2,
        uint amount,
        uint price,
        uint date);
    
    constructor() public {
        admin = msg.sender;
    }
    
    function addToken(bytes32 _ticker, address _address)
    external 
    onlyAdmin() {
        tokens[_ticker] = Token(_ticker, _address);
        tokenList.push(_ticker);
    }
    
    function deposit(uint _amount, bytes32 _ticker)
    external 
    tokenExist(_ticker) {
        IERC20(tokens[_ticker].tokenAddress).transferFrom(msg.sender, address(this), _amount);
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(_amount);
    }
    
    function withdraw(uint _amount, bytes32 _ticker)
    external 
    tokenExist(_ticker) {
        require(traderBalances[msg.sender][_ticker] >= _amount,
            "not enough balance");
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
    }
    
    function createLimitOrder(
        bytes32 _ticker,
        uint _amount,
        uint _price,
        Side _side)
    external 
    tokenExist(_ticker) tokenIsNotDai(_ticker) {
        if(_side == Side.SELL) {
            require(traderBalances[msg.sender][_ticker] >= _amount,
                "not enough token balance to sell");
            traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        } else {
            uint daiAmount = _amount.mul(_price);
            require(traderBalances[msg.sender][DAI] >= daiAmount,
                "not enough dai to buy");
            traderBalances[msg.sender][DAI] = traderBalances[msg.sender][DAI].sub(daiAmount);
        }
        
        Order[] storage orders = orderBook[_ticker][uint(_side)];
        orders.push(Order(
            nextOrderId,
            msg.sender,
            _side,
            _ticker,
            _amount,
            0,
            _price,
            now));
        
        uint i = orders.length > 0 ? orders.length - 1 : 0;
        while(i > 0) {
            if(_side == Side.BUY && orders[i - 1].price > orders[i].price) {
                break;
            }
            if(_side == Side.SELL && orders[i - 1].price < orders[i].price) {
                break;
            }
            Order memory order = orders[i - 1];
            orders[i - 1] = orders[i];
            orders[i] = order;
            i = i.sub(1);
        }
        
        nextOrderId = nextOrderId.add(1);
    }
    
    function createMarketOrder(
        bytes32 _ticker,
        uint _amount,
        Side _side)
    external
    tokenExist(_ticker) tokenIsNotDai(_ticker) {
        if(_side == Side.SELL) {
            require(traderBalances[msg.sender][_ticker] >= _amount,
                "not enough token balance to sell");
        }
        
        Order[] storage orders = orderBook[_ticker][uint(_side == Side.BUY ? Side.SELL : Side.BUY)];
        uint i;
        uint remaining = _amount;
        while(i < orders.length && remaining > 0) {
            uint available = orders[i].amount.sub(orders[i].filled);
            uint matched = (remaining > available) ? available : remaining;
            remaining = remaining.sub(matched);
            orders[i].filled = orders[i].filled.add(matched);
            
            emit NewTrade(
                nextTradeId,
                orders[i].id,
                _ticker,
                orders[i].trader,
                msg.sender,
                matched,
                orders[i].price,
                now);
                
            uint daiAmount = matched.mul(orders[i].price);
            if(_side == Side.SELL) {
                traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(matched);
                traderBalances[msg.sender][DAI] = traderBalances[msg.sender][DAI].add(daiAmount);
                traderBalances[orders[i].trader][_ticker] = traderBalances[orders[i].trader][_ticker].add(matched);
            }
            if(_side == Side.BUY) {
                require(traderBalances[msg.sender][DAI] >= daiAmount,
                    "not enough balance to buy token");
                traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(matched);
                traderBalances[msg.sender][DAI] = traderBalances[msg.sender][DAI].sub(daiAmount);
                traderBalances[orders[i].trader][DAI] = traderBalances[orders[i].trader][DAI].add(daiAmount);
            }
            
            nextTradeId = nextTradeId.add(1);
            i = i.add(1);
        }
        
        while(orders[0].filled == orders[0].amount) {
            for(uint j = 0; j < orders.length - 1; j++) {
                orders[j] = orders[j + 1];
            }
            orders.pop();
            
            if(orders.length == 0)  break;
        }
    }
    
    function getOrders(bytes32 _ticker, Side _side) 
    external view
    returns(Order[] memory) {
        return orderBook[_ticker][uint(_side)];
    }
    
    function getTokens()
    external view
    returns(Token[] memory) {
        Token[] memory tokensCopy = new Token[](tokenList.length);
        for (uint i = 0; i < tokenList.length; i++) {
            tokensCopy[i] = Token(tokens[tokenList[i]].ticker, tokens[tokenList[i]].tokenAddress);
      }
      return tokensCopy;
    }
    
    modifier tokenIsNotDai(bytes32 _ticker) {
        require(_ticker != DAI,
            "cannot trade DAI");
        _;
    }
    
    modifier tokenExist(bytes32 _ticker) {
        require(tokens[_ticker].tokenAddress != address(0),
            "token does not exists");
        _;
    }
        
    modifier onlyAdmin() {
        require(msg.sender == admin,
            "only admin");
        _;
    }
}