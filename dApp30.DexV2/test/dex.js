const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const ethers = require("ethers");
const Dai = artifacts.require('./mocks/Dai.sol');
const Rep = artifacts.require('./mocks/Rep.sol');
const Bat = artifacts.require('./mocks/Bat.sol');
const Zrx = artifacts.require('./mocks/Zrx.sol');
const Dex = artifacts.require('./Dex.sol');

contract('Dex', accounts => {
  let dai, rep, bat, zrx, dex;
  // const [DAI, REP, BAT, ZRX] = [
  //   web3.utils.fromAscii('DAI'),
  //   web3.utils.fromAscii('REP'),
  //   web3.utils.fromAscii('BAT'),
  //   web3.utils.fromAscii('ZRX')];
  const [DAI, REP, BAT, ZRX] = [
    ethers.utils.formatBytes32String('DAI'),
    ethers.utils.formatBytes32String('REP'),
    ethers.utils.formatBytes32String('BAT'),
    ethers.utils.formatBytes32String('ZRX')];
  const admin = accounts[0];
  const [trader1, trader2] = [accounts[1], accounts[2]];
  const bnWei = (val) => {
    return web3.utils.toBN(web3.utils.toWei(val));
  }
  const Side = {
    BUY: 0,
    SELL: 1
  }

  beforeEach(async () => {
    [dai, rep, bat, zrx] = await Promise.all([
      Dai.new(),
      Rep.new(),
      Bat.new(),
      Zrx.new()]);
    dex = await Dex.new();
    // add token list on dex contract
    await Promise.all([
      dex.addToken(DAI, dai.address, { from: admin }),
      dex.addToken(REP, rep.address, { from: admin }),
      dex.addToken(BAT, bat.address, { from: admin }),
      dex.addToken(ZRX, zrx.address, { from: admin })]);
    // minting each new tokens to traders
    const amount = bnWei("1000");
    const seedTokenBalance = async (_token, _trader) => {
      await _token.faucet(_trader, amount);
      await _token.approve(dex.address, amount, {from: _trader});
    };
    await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader1)));
    await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader2)));
  });

  it("should deposit tokens", async () => {
    const amount = bnWei("100");
    await dex.deposit(amount, DAI, {from: trader1});
    const balance = await dex.traderBalances(trader1, DAI, {from: trader1});
    const daiContractBalance = await dai.balanceOf(trader1, {from: trader1});

    assert(amount.eq(balance));
    assert(daiContractBalance.eq(bnWei("900")));
  });

  it("should NOT deposit tokens if token does not exist", async () => {
    await expectRevert(
      dex.deposit(bnWei("100"), web3.utils.fromAscii("Kong"), {from: trader1}),
      "token does not exists");
  });

  it("should withdraw tokens", async () => {
    const amount = bnWei("100");
    const withdrawAmount = bnWei("10");
    await dex.deposit(amount, DAI, {from: trader1});
    const balanceBefore = await dex.traderBalances(trader1, DAI, {from: trader1});
    await dex.withdraw(withdrawAmount, DAI, {from: trader1});
    const balanceAfter = await dex.traderBalances(trader1, DAI, {from :trader1});
    const daiContractBalance = await dai.balanceOf(trader1, {from: trader1});

    assert(balanceBefore.sub(balanceAfter).eq(withdrawAmount));
    assert(daiContractBalance.eq(bnWei("910")));
  });

  it("should NOT withdraw tokens if token does not exist", async () => {
    await dex.deposit(bnWei("100"), DAI, {from: trader1});

    await expectRevert(
      dex.withdraw(bnWei("10"), web3.utils.fromAscii("Kong"), {from: trader1}),
      "token does not exists");
  });

  it("should NOT withdraw tokens if balance is too low", async () => {
    await dex.deposit(bnWei("100"), DAI, {from: trader1});

    await expectRevert(
      dex.withdraw(bnWei("101"), DAI, {from: trader1}),
      "not enough balance");
  });

  it("should create limit order", async () => {
    // trader1 selling 100 Rep token with 10 Dai
    // trader1 buying 100 Bat token with 1 Dai
    // trader2 selling 100 Rep token with 5 Dai
    // trader2 buying 100 Bat token with 5 Dai
    // ==> both buy, sell orders should be reordered
    await dex.deposit(bnWei("100"), REP, { from: trader1 });
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });
    await dex.deposit(bnWei("100"), REP, { from: trader2 });
    await dex.deposit(bnWei("500"), DAI, { from: trader2 });
    const beforeRepBalanceTrader1 = await dex.traderBalances(trader1, REP);
    const beforeDaiBalanceTrader1 = await dex.traderBalances(trader1, DAI);
    const beforeRepBalanceTrader2 = await dex.traderBalances(trader2, REP);
    const beforeDaiBalanceTrader2 = await dex.traderBalances(trader2, DAI);
    await dex.createLimitOrder(
      REP,
      bnWei("100"),
      10,
      Side.SELL,
      { from: trader1 });
    await dex.createLimitOrder(
      BAT,
      bnWei("100"),
      1,
      Side.BUY,
      { from: trader1 });
    await dex.createLimitOrder(
      REP,
      bnWei("100"),
      5,
      Side.SELL,
      { from: trader2 });
    await dex.createLimitOrder(
      BAT,
      bnWei("100"),
      5,
      Side.BUY,
      { from: trader2 });
    const afterRepBalanceTrader1 = await dex.traderBalances(trader1, REP);
    const afterDaiBalanceTrader1 = await dex.traderBalances(trader1, DAI);
    const afterRepBalanceTrader2 = await dex.traderBalances(trader2, REP);
    const afterDaiBalanceTrader2 = await dex.traderBalances(trader2, DAI);
    
    assert(beforeRepBalanceTrader1.sub(afterRepBalanceTrader1).eq(bnWei("100")));
    assert(beforeDaiBalanceTrader1.sub(afterDaiBalanceTrader1).eq(bnWei("100")));
    assert(beforeRepBalanceTrader2.sub(afterRepBalanceTrader2).eq(bnWei("100")));
    assert(beforeDaiBalanceTrader2.sub(afterDaiBalanceTrader2).eq(bnWei("500")));
    
    const sellOrders = await dex.getOrders(REP, Side.SELL);
    const buyOrders = await dex.getOrders(BAT, Side.BUY);
    
    assert.equal(sellOrders.length, 2);
    assert.equal(buyOrders.length, 2);
    assert.equal(sellOrders[0].id, 2);
    assert.equal(sellOrders[0].trader, trader2);
    assert.equal(sellOrders[0].side, Side.SELL);
    assert.equal(sellOrders[0].ticker, REP);
    assert(web3.utils.toBN(sellOrders[0].amount).eq(bnWei("100")));
    assert.equal(sellOrders[0].filled, 0);
    assert.equal(sellOrders[0].price, 5);
    assert.equal(sellOrders[1].id, 0);
    assert.equal(sellOrders[1].trader, trader1);
    assert.equal(sellOrders[1].side, Side.SELL);
    assert.equal(sellOrders[1].ticker, REP);
    assert(web3.utils.toBN(sellOrders[1].amount).eq(bnWei("100")));
    assert.equal(sellOrders[1].filled, 0);
    assert.equal(sellOrders[1].price, 10);
    assert.equal(buyOrders[0].id, 3);
    assert.equal(buyOrders[0].trader, trader2);
    assert.equal(buyOrders[0].side, Side.BUY);
    assert.equal(buyOrders[0].ticker, BAT);
    assert(web3.utils.toBN(buyOrders[0].amount).eq(bnWei("100")));
    assert.equal(buyOrders[0].filled, 0);
    assert.equal(buyOrders[0].price, 5);
    assert.equal(buyOrders[1].id, 1);
    assert.equal(buyOrders[1].trader, trader1);
    assert.equal(buyOrders[1].side, Side.BUY);
    assert.equal(buyOrders[1].ticker, BAT);
    assert(web3.utils.toBN(buyOrders[1].amount).eq(bnWei("100")));
    assert.equal(buyOrders[1].filled, 0);
    assert.equal(buyOrders[1].price, 1);
  });

  it("should NOT create limit order if balance too low", async () => {
    await dex.deposit(bnWei("100"), REP, { from: trader1 });
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });

    await expectRevert(
      dex.createLimitOrder(REP, bnWei("101"), 1, Side.SELL),
      "not enough token balance to sell");
    await expectRevert(
      dex.createLimitOrder(REP, bnWei("101"), 1, Side.BUY),
      "not enough dai to buy");
  });

  it("should NOT create limit order if token is Dai", async () => {
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });

    await expectRevert(
      dex.createLimitOrder(DAI, bnWei("100"), 1, Side.SELL),
      "cannot trade DAI");
    await expectRevert(
      dex.createLimitOrder(DAI, bnWei("100"), 1, Side.BUY),
      "cannot trade DAI");
  });

  it("should NOT create limit order if token does not exists", async () => {
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });
    
    await expectRevert(
      dex.createLimitOrder(ethers.utils.formatBytes32String("Kong"), bnWei("100"), 1, Side.SELL),
      "token does not exists");
    await expectRevert(
      dex.createLimitOrder(ethers.utils.formatBytes32String("Kong"), bnWei("100"), 1, Side.BUY),
      "token does not exists");
  });

  it("should create market order & match against existing limit order", async () => {
    await dex.deposit(bnWei("100"), REP, { from: trader1 });
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });
    await dex.deposit(bnWei("100"), REP, { from: trader2 });
    await dex.deposit(bnWei("500"), DAI, { from: trader2 });
    await dex.createLimitOrder(
      REP,
      bnWei("100"),
      10,
      Side.SELL,
      { from: trader1 });
    await dex.createLimitOrder(
      BAT,
      bnWei("100"),
      1,
      Side.BUY,
      { from: trader1 });
    await dex.createLimitOrder(
      REP,
      bnWei("100"),
      5,
      Side.SELL,
      { from: trader2 });
    await dex.createLimitOrder(
      BAT,
      bnWei("100"),
      5,
      Side.BUY,
      { from: trader2 });
    // trader1 DAI balance 1000 - 100, trader2 DAI balance 1000 - 500
    // Sell Orders: REP 100@5(trader2), 100@10(trader1)
    // Buy Orders: BAT 100@5(trader2), 100@1(trader1)
    // accounts[3] will buy 120 Rep, Spending Dai 500 + 200
    // trader2 will get 500 Dai, trader1 will get 200 Dai.
    // Sell Order length should be 1 and filled 20
    // accounts[3] will sell 120 Bat, Recieving 520 Dai.
    // trader2 will get 100 Bat, trader1 will get 20 Bat.
    // Buy Order length should be 1 and filled 20
    await dai.faucet(accounts[3], bnWei("1000"));
    await bat.faucet(accounts[3], bnWei("1000"));
    await rep.faucet(accounts[3], bnWei("1000"));
    await dai.approve(dex.address, bnWei("1000"), { from: accounts[3] });
    await bat.approve(dex.address, bnWei("1000"), { from: accounts[3] });
    await rep.approve(dex.address, bnWei("1000"), { from: accounts[3] });
    await dex.deposit(bnWei("1000"), DAI, { from: accounts[3] });
    await dex.deposit(bnWei("1000"), BAT, { from: accounts[3] });
    await dex.deposit(bnWei("1000"), REP, { from: accounts[3] });
    const beforeDaiBalanceAccounts3 = await dex.traderBalances(accounts[3], DAI);
    const beforeBatBalanceAccounts3 = await dex.traderBalances(accounts[3], BAT);
    const beforeRepBalanceAccounts3 = await dex.traderBalances(accounts[3], REP);
    const beforeDaiBalanceTrader1 = await dex.traderBalances(trader1, DAI);
    const beforeDaiBalanceTrader2 = await dex.traderBalances(trader2, DAI);
    const beforeBatBalanceTrader1 = await dex.traderBalances(trader1, BAT);
    const beforeBatBalanceTrader2 = await dex.traderBalances(trader2, BAT);
    await dex.createMarketOrder(
      REP,
      bnWei("120"),
      Side.BUY,
      { from: accounts[3] });
    await dex.createMarketOrder(
      BAT,
      bnWei("120"),
      Side.SELL,
      { from: accounts[3] });
    const afterDaiBalanceAccounts3 = await dex.traderBalances(accounts[3], DAI);
    const afterBatBalanceAccounts3 = await dex.traderBalances(accounts[3], BAT);
    const afterRepBalanceAccounts3 = await dex.traderBalances(accounts[3], REP);
    const afterDaiBalanceTrader1 = await dex.traderBalances(trader1, DAI);
    const afterDaiBalanceTrader2 = await dex.traderBalances(trader2, DAI);
    const afterBatBalanceTrader1 = await dex.traderBalances(trader1, BAT);
    const afterBatBalanceTrader2 = await dex.traderBalances(trader2, BAT);
    const repSellOrders = await dex.getOrders(REP, Side.SELL);
    const batBuyOrders = await dex.getOrders(BAT, Side.BUY);

    assert(beforeDaiBalanceAccounts3.sub(bnWei("180")).eq(afterDaiBalanceAccounts3));
    assert(beforeDaiBalanceTrader1.add(bnWei("200")).eq(afterDaiBalanceTrader1));
    assert(beforeDaiBalanceTrader2.add(bnWei("500")).eq(afterDaiBalanceTrader2));
    assert(beforeBatBalanceAccounts3.sub(bnWei("120")).eq(afterBatBalanceAccounts3));
    assert(beforeBatBalanceTrader1.add(bnWei("20")).eq(afterBatBalanceTrader1));
    assert(beforeBatBalanceTrader2.add(bnWei("100")).eq(afterBatBalanceTrader2));
    assert(beforeRepBalanceAccounts3.add(bnWei("120")).eq(afterRepBalanceAccounts3));
    assert.equal(repSellOrders.length, 1);
    assert.equal(batBuyOrders.length, 1);
    assert.equal(repSellOrders[0].trader, trader1);
    assert.equal(repSellOrders[0].side, Side.SELL);
    assert.equal(repSellOrders[0].ticker, REP);
    assert(web3.utils.toBN(repSellOrders[0].amount).eq(bnWei("100")));
    assert(web3.utils.toBN(repSellOrders[0].filled).eq(bnWei("20")));
    assert.equal(repSellOrders[0].price, 10);
    assert.equal(batBuyOrders[0].trader, trader1);
    assert.equal(batBuyOrders[0].side, Side.BUY);
    assert.equal(batBuyOrders[0].ticker, BAT);
    assert(web3.utils.toBN(batBuyOrders[0].amount).eq(bnWei("100")));
    assert(web3.utils.toBN(batBuyOrders[0].filled).eq(bnWei("20")));
    assert.equal(batBuyOrders[0].price, 1);
  });

  it("should NOT create market order if balance is too low", async () => {
    await dex.deposit(bnWei("100"), REP, { from: trader1 });
    await dex.deposit(bnWei("100"), DAI, { from: trader1 });
    await dex.createLimitOrder(
      REP,
      bnWei("100"),
      10,
      Side.SELL,
      { from: trader1 });
    await dex.createLimitOrder(
      BAT,
      bnWei("100"),
      1,
      Side.BUY,
      { from: trader1 });
    await dex.deposit(bnWei("20"), DAI, { from: trader2 });
    await dex.deposit(bnWei("20"), BAT, { from: trader2 });

    await expectRevert(
      dex.createMarketOrder(
        REP,
        bnWei("3"), // it needs 30 Dai and trader2 only have 20 dai 
        Side.BUY,
        { from: trader2 }),
      "not enough balance to buy token");
    await expectRevert(
      dex.createMarketOrder(
        BAT,
        bnWei("21"),  // trader2 only has 20 bat 
        Side.SELL,
        { from: trader2 }),
      "not enough token balance to sell");
  });

  it("should NOT create market order if token is Dai", async () => {
    await expectRevert(
      dex.createMarketOrder(
        DAI,
        bnWei("10"),
        Side.SELL,
        { from: trader2 }),
      "cannot trade DAI");
  });

  it("should NOT create market order if token does not exists", async () => {
    await expectRevert(
      dex.createMarketOrder(
        ethers.utils.formatBytes32String("Kong"),
        bnWei("10"),
        Side.SELL,
        { from: trader1 }),
      "token does not exists");
  });
});