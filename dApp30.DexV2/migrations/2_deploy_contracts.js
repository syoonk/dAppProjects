const ethers = require("ethers");
const Dai = artifacts.require('./mocks/Dai.sol');
const Rep = artifacts.require('./mocks/Rep.sol');
const Bat = artifacts.require('./mocks/Bat.sol');
const Zrx = artifacts.require('./mocks/Zrx.sol');
const Dex = artifacts.require('./Dex.sol');

const [DAI, BAT, REP, ZRX] = 
  ['DAI', 'BAT', 'REP', 'ZRX'].map(_ticker => ethers.utils.formatBytes32String(_ticker));
const SIDE = {
  BUY: 0,
  SELL: 1
}

  
module.exports = async (deployer, _network, _accounts) => {
  const [trader1, trader2, trader3, trader4, _] = _accounts;
  
  // Deploy each contracts
  await Promise.all( [Dai, Bat, Rep, Zrx, Dex].map(_contract => deployer.deploy(_contract)) );
  const [dai, bat, rep, zrx, dex] = 
    await Promise.all( [Dai, Bat, Rep, Zrx, Dex].map(_contract => _contract.deployed()) );

  /** 
   * From this part is pre initializaion
   */

  // Assign token address on Dex contract
  await Promise.all( [
    dex.addToken(DAI, dai.address),
    dex.addToken(REP, rep.address),
    dex.addToken(BAT, bat.address),
    dex.addToken(ZRX, zrx.address)] );

  // Give them 1000 ethers amount to each token
  const amount = web3.utils.toWei("1000");
  const seedTokenBalance = async (_token, _trader) => {
    await _token.faucet(_trader, amount);
    // It allows dex can use their token
    await _token.approve(dex.address, amount, {from: _trader});
    // Deposit tokens to dex contract
    const ticker = await _token.name();
    await dex.deposit(amount, ethers.utils.formatBytes32String(ticker), {from: _trader});
  };

  await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader1)));
  await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader2)));
  await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader3)));
  await Promise.all([dai, rep, bat, zrx].map(token => seedTokenBalance(token, trader4)));

  // It can be also replaced by openzeppelin testhelpers but it sometimes does not work properly
  const increaseTime = async (seconds) => {
    await web3.currentProvider.send({
      jsonrpc: '2.0',
      method: 'evm_increaseTime',
      params: [seconds],
      id: 0,
    }, () => {});
    await web3.currentProvider.send({
      jsonrpc: '2.0',
      method: 'evm_mine',
      params: [],
      id: 0,
    }, () => {});
  }

  // create trades
  // Every limit orders are matched with market order so it makes trade
  // The reason why of increasing time is it can reflets real world market places
  await dex.createLimitOrder(BAT, 1000, 10, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(BAT, 1000, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(BAT, 1200, 11, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(BAT, 1200, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(BAT, 1200, 15, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(BAT, 1200, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(BAT, 1500, 14, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(BAT, 1500, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(BAT, 2000, 12, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(BAT, 2000, SIDE.SELL, {from: trader2});

  await dex.createLimitOrder(REP, 1000, 2, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(REP, 1000, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(REP, 500, 4, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(REP, 500, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(REP, 800, 2, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(REP, 800, SIDE.SELL, {from: trader2});
  await increaseTime(1);
  await dex.createLimitOrder(REP, 1200, 6, SIDE.BUY, {from: trader1});
  await dex.createMarketOrder(REP, 1200, SIDE.SELL, {from: trader2});

  //create orders
  await Promise.all([
    dex.createLimitOrder(BAT, 1400, 10, SIDE.BUY, {from: trader1}),
    dex.createLimitOrder(BAT, 1200, 11, SIDE.BUY, {from: trader2}),
    dex.createLimitOrder(BAT, 1000, 12, SIDE.BUY, {from: trader2}),

    dex.createLimitOrder(REP, 3000, 4, SIDE.BUY, {from: trader1}),
    dex.createLimitOrder(REP, 2000, 5, SIDE.BUY, {from: trader1}),
    dex.createLimitOrder(REP, 500, 6, SIDE.BUY, {from: trader2}),

    dex.createLimitOrder(ZRX, 4000, 12, SIDE.BUY, {from: trader1}),
    dex.createLimitOrder(ZRX, 3000, 13, SIDE.BUY, {from: trader1}),
    dex.createLimitOrder(ZRX, 500, 14, SIDE.BUY, {from: trader2}),

    dex.createLimitOrder(BAT, 2000, 16, SIDE.SELL, {from: trader3}),
    dex.createLimitOrder(BAT, 3000, 15, SIDE.SELL, {from: trader4}),
    dex.createLimitOrder(BAT, 500, 14, SIDE.SELL, {from: trader4}),

    dex.createLimitOrder(REP, 4000, 10, SIDE.SELL, {from: trader3}),
    dex.createLimitOrder(REP, 2000, 9, SIDE.SELL, {from: trader3}),
    dex.createLimitOrder(REP, 800, 8, SIDE.SELL, {from: trader4}),

    dex.createLimitOrder(ZRX, 1500, 23, SIDE.SELL, {from: trader3}),
    dex.createLimitOrder(ZRX, 1200, 22, SIDE.SELL, {from: trader3}),
    dex.createLimitOrder(ZRX, 900, 21, SIDE.SELL, {from: trader4}),
  ]);
}