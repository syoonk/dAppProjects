# About DEX Smart Contract
Dex is Decentralized Exchange. It transfers tokens. This is sample so it provides only several mocked tokens.
Frontend is now available. Please refer this [README](https://gitlab.com/syoonk/dAppProjects/-/blob/master/README.md).  
**Page**: http://syoonk.gitlab.io/dex


## Used Tools
Solidity, Truffle, NodeJS, React, Metamask

----
## How to Run
It uses metamask.

| EndPoints | |
---|---|
Truffle | http://localhost:9545  
Ganache | http://localhost:8545

### Deploy Contract  
`./npm install`  
`./truffle develop`  
`truffle> migrate --reset`  

### Run Frontend
`./client/npm install`  
`./client/npm start`

----
## Features
### 1. Limit Orders
User can create orders of limit price.
### 2. Market Orders
User can trade coins of market price
### 3. Coins
DAI, ZRX, REP, BAT

----
## Pages
<img src="./images/dai.png" width="50%" height="50%"></img>
<img src="./images/rep.png" width="50%" height="50%"></img>

----
## Issues
- Event is not caught properly