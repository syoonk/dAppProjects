pragma solidity ^0.6.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";
import "./Wallet.sol";

contract Delivery {
    using SafeMath for uint;
    Wallet wallet;
    
    enum OrderState {
        ORDERED,
        PREPARING,
        DELIVERING,
        COMPLETED,
        CANCELLED
    }
    struct User {
        address userAddress;
        uint userId;
        uint phoneNumber;
        uint reputation;
        string homeAddress;
        string userPublicKey;
        uint[] shopId;
        uint riderId;
    }
    struct Rider {
        uint id;
        address userAddress;
        string area;
        uint currentOrderId;
        uint nextOrderId;
        bool activate;
    }
    struct Shop {
        uint shopId;
        address owner;
        string shopAddress;
    }
    struct Order {
        uint id;
        // users part
        address buyer;
        // shop part
        uint shopId;
        uint goodsId;
        uint amount;
        uint totalPrice;
        uint riderId;
        // Signing
        bool sellerSign;    // state to preparing
        bool riderSign; // state to delivering
        bool buyerSign; // state to completed
        bool buyerCancelSign;
        bool sellerCancelSign;
        OrderState state;
        uint start;
        uint end;
    }
    struct Review {
        string content;
        uint stars;
    }
    mapping(address => User) users;
    mapping(uint => Shop) public shops;
    mapping(uint => Rider) public riders;
    mapping(uint => Order) orders;
    mapping(address => Review[]) public reviews;
    address public admin;
    uint nextOrderId = 1;
    uint nextUserId = 1;
    uint nextShopId = 1;
    uint nextRiderId = 1;
    uint constant MAXREP = 100000;
    bytes32 constant KNG = bytes32("KNG");
    
    event OrderStatusChanged(
        uint _orderId, 
        OrderState _state,
        uint _time);
    event RiderCancelled(
        uint _orderId,
        address indexed _rider,
        uint time);
    
    constructor(address _walletAddress)
    public {
        admin = msg.sender;
        wallet = Wallet(_walletAddress);
    }

    function userRegister(
        uint _phoneNumber,
        string calldata _homeAddress,
        string calldata _userPublicKey)
    external {
        require(users[msg.sender].userId == 0,
            "User already exists");
            
        uint[] memory shopIdInit = new uint[](0);
        users[msg.sender] = User(
            msg.sender,
            nextUserId,
            _phoneNumber,
            50000,
            _homeAddress,
            _userPublicKey,
            shopIdInit,
            0);
        nextUserId.add(1);
    }
    
    function changeDeliveryStatus()
    external 
    userExists(msg.sender) {
        User storage userCopy = users[msg.sender];
        
        // Registering Rider
        if(userCopy.riderId == 0) {
            riders[nextRiderId] = Rider(
                nextRiderId,
                userCopy.userAddress,
                userCopy.homeAddress,
                0,
                0,
                false);
            userCopy.riderId = nextRiderId;
            nextRiderId = nextRiderId.add(1);
        }
        
        // Activate or Deactivate
        riders[userCopy.riderId].activate = !riders[userCopy.riderId].activate;
    }
    
    function shopRegister(string calldata _area)
    external 
    userExists(msg.sender) {
        shops[nextShopId] = Shop(nextShopId, msg.sender, _area);
        users[msg.sender].shopId.push(nextShopId);
        nextShopId = nextShopId.add(1);
    }
    
    function placeOrder(
        uint _shopId, 
        uint _goodsId, 
        uint _amount,
        uint _totalPrice)
    external 
    userExists(msg.sender) {
        wallet.transfer(msg.sender, shops[_shopId].owner, _totalPrice, KNG);
        orders[nextOrderId] = Order(
            nextOrderId,
            msg.sender,
            0,
            _goodsId,
            _amount,
            _totalPrice,
            0,
            false,
            false,
            false,
            false,
            false,
            OrderState.ORDERED,
            now,
            now + 3600);
        emit OrderStatusChanged(
            nextOrderId,
            orders[nextOrderId].state,
            now);
        nextOrderId = nextOrderId.add(1);
    }
    
    function cancelOrder(uint _orderId)
    external
    orderExists(_orderId) {
        Order storage orderCopy = orders[_orderId];
        require(orderCopy.state == OrderState.ORDERED || orderCopy.state == OrderState.PREPARING,
            "order cannot be cancelled in current state");
        require(orderCopy.buyer == msg.sender || shops[orderCopy.shopId].owner == msg.sender,
            "only buyer and seller can cancel the order");
        
        msg.sender == orderCopy.buyer ? orderCopy.buyerCancelSign = true : orderCopy.sellerCancelSign = true;
        
        // if seller does not accept order yet, it can be directly cancelled
        // or both should sign on it.
        if((orderCopy.state == OrderState.ORDERED && msg.sender == orderCopy.buyer)
            || (orderCopy.buyerCancelSign && orderCopy.sellerCancelSign)) {
            wallet.transfer(shops[orderCopy.shopId].owner, orderCopy.buyer, orderCopy.totalPrice, KNG);
            orderCopy.state = OrderState.CANCELLED;
            // Cancel Assigned Rider
            if(orderCopy.riderId != 0) {
                Rider storage riderCopy = riders[orderCopy.riderId];
                if(riderCopy.currentOrderId == _orderId) {
                    riderCopy.currentOrderId = riderCopy.nextOrderId;
                    riderCopy.nextOrderId = 0;
                }
                if(riderCopy.nextOrderId == _orderId) {
                    riderCopy.nextOrderId = 0;
                }
            }
            emit OrderStatusChanged (
                _orderId,
                orderCopy.state,
                now);
        }
    }
    
    function assignRider(uint _riderId, uint _orderId)
    external 
    orderExists(_orderId) riderExists(_riderId) {
        Rider storage riderCopy = riders[_riderId];
        Order storage orderCopy = orders[_orderId];
        require(orderCopy.state == OrderState.ORDERED || orderCopy.state == OrderState.PREPARING,
            "order state does not allow assigning rider");
        require(orderCopy.riderId == 0,
            "this order has rider already");
        require(riderCopy.currentOrderId == 0 || riderCopy.nextOrderId == 0,
            "rider has been assigned already");
        
        orderCopy.riderId = _riderId;
        if(riderCopy.currentOrderId == 0) {
            riderCopy.currentOrderId = _orderId;
        } else {
            riderCopy.nextOrderId = _orderId;
        }
    }
    
    function cancelRider(uint _orderId)
    external
    orderExists(_orderId) riderExists(users[msg.sender].riderId) {
        Rider storage riderCopy = riders[users[msg.sender].riderId];
        Order storage orderCopy = orders[_orderId];
        require(orderCopy.state == OrderState.ORDERED || orderCopy.state == OrderState.PREPARING,
            "order state does not allow assigning rider");
        require(orderCopy.riderId != 0,
            "This order is not assigned rider yet");
        require(riderCopy.currentOrderId == _orderId || riderCopy.nextOrderId == _orderId,
            "this rider is not assigned current order");
        
        riderCopy.currentOrderId == _orderId ? riderCopy.currentOrderId = 0 : riderCopy.nextOrderId = 0;
        orderCopy.riderId = 0;
        /*
        
            decreasing rider's reputation
        
        */
        
        emit RiderCancelled(
            _orderId,
            riderCopy.userAddress,
            now);
    }
    
    function signingOrder(uint _orderId) 
    external
    orderExists(_orderId) {
        Order storage orderCopy = orders[_orderId];
        Rider storage riderCopy = riders[orderCopy.riderId];
        if(msg.sender == shops[orderCopy.shopId].owner) {
            require(orderCopy.state == OrderState.ORDERED, 
                "seller should sign only on ORDERED state");
            require(!orderCopy.sellerSign,
                "seller already signed this order");
            
            orderCopy.sellerSign = true;
            orderCopy.state = OrderState.PREPARING;
            emit OrderStatusChanged(
                _orderId,
                OrderState.PREPARING,
                now);
        } else if(msg.sender == riderCopy.userAddress) {
            require(orderCopy.state == OrderState.PREPARING,
                "rider should sign only on PREPARING state");
            require(riderCopy.currentOrderId == _orderId,
                "rider not currently assigned this order");
            require(!orderCopy.riderSign,
                "rider already signed this order");
            
            orderCopy.riderSign = true;
            orderCopy.state = OrderState.DELIVERING;
            emit OrderStatusChanged(
                _orderId,
                OrderState.DELIVERING,
                now);
        } else if(msg.sender == orderCopy.buyer) {
            require(orderCopy.state == OrderState.DELIVERING,
                "buyer should sign only on DELIVERING state");
            require(!orderCopy.buyerSign,
                "buyer already signed this order");
            
            orderCopy.buyerSign = true;
            orderCopy.state = OrderState.COMPLETED;
            emit OrderStatusChanged(
                _orderId,
                OrderState.PREPARING,
                now);
            
            riderCopy.currentOrderId = riderCopy.nextOrderId;
            riderCopy.nextOrderId = 0;
            
            _scoringReputation(shops[orderCopy.shopId].owner, 1000, true);
            _scoringReputation(riderCopy.userAddress, 1000, true);
            _scoringReputation(orderCopy.buyer, 1000, true);
        } else {
            revert("not allow to sign this order");
        }
    }
    
    function uploadReview(
        uint _orderId, 
        address _from, 
        address _to)
    external {
        
    }
    
    function _scoringReputation(address _user, uint _amount, bool _pos)
    internal 
    userExists(_user) {
        require(_amount <= 2000,
            "more than 2000 score is not allowed");
            
        User storage userCopy = users[_user];
        if(_pos) {
            uint normalizedPoint = 2 * (MAXREP - userCopy.reputation) * _amount / MAXREP;
            userCopy.reputation += normalizedPoint;
        }
        if(!_pos) {
            userCopy.reputation = userCopy.reputation.sub(_amount);
        }
    }
    
    modifier userExists(address _user) {
        require(users[_user].phoneNumber != 0,
            "user does not exists");
        _;
    }
    
    modifier orderExists(uint _orderId) {
        require(orders[_orderId].id != 0,
            "order does not exists");
        _;
    }
    
    modifier riderExists(uint _riderId) {
        Rider storage riderCopy = riders[_riderId];
        require(riderCopy.userAddress != address(0),
            "rider not registered");
        require(riderCopy.activate == true,
            "rider not activated");
        _;
    }
}