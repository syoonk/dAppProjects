// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.8;
pragma experimental "ABIEncoderV2";

import "../node_modules/@openzeppelin/contracts/math/SafeMath.sol";
import "./DeliveryDataInterface.sol";

/*
 *  This contract stores app datas permanently. Save and load data from here.
 */
contract DeliveryDataHolder is DeliveryDataInterface {
  using SafeMath for uint;

  // config address authorization
  mapping(address => bool) approvedWallets;
  mapping(address => bool) approvedApps;
  mapping(address => bool) admins;
  // for getting info
  mapping(address => UserInfo) usersByAddress;
  mapping(uint => address) userAddressById;
  mapping(uint => Rider) ridersById;
  mapping(uint => Shop) shopsById;
  mapping(uint => Order) ordersById;
  mapping(uint => Reputation) reputationsById;
  mapping(uint => Review) reviewsById;
  mapping(uint => mapping(address => mapping(address => bool))) reviewed;
  // rank user by reputation, rank starts from 0. index is matched with user rank.
  address[] usersByRank;
  // ID of each data structures
  uint nextUserId = 1;
  uint nextRiderId = 1;
  uint nextShopId = 1;
  uint nextOrderId = 1;
  uint nextReputationId = 1;
  uint nextReviewId = 1;

  constructor() public {
    admins[msg.sender] = true;
  }

  // approvement of external wallet to use datas
  // If it was approved already then changes to disapproved
  function approveWallet(address _walletAddress)
  external
  onlyAdmin() {
    approvedWallets[_walletAddress] = !approvedWallets[_walletAddress];
  }

  // approvement of external app to use datas
  // If it was approved already then changes to disapproved
  function approveApp(address _appAddress)
  external
  onlyAdmin() {
    approvedApps[_appAddress] = !approvedApps[_appAddress];
  }

  function addUser(
    address _userAddress,
    uint _phoneNumber,
    string memory _homeAddress,
    string memory _userPublicKey)
  public
  onlyApprovedApp() {
    require(usersByAddress[msg.sender].userAddress == address(0),
      "user already exists");

    usersByAddress[msg.sender] = UserInfo({
      userAddress: _userAddress,
      userId: nextUserId,
      phoneNumber: _phoneNumber,
      reputation: MAXREP / 2,
      repRank: nextUserId - 1,  // userId starts from 1 while rank starts from 0
      homeAddress: _homeAddress,
      userPublicKey: _userPublicKey,
      riderId: 0,
      shopIds: new uint[](0) });
    userAddressById[nextUserId] = _userAddress;
    usersByRank.push(msg.sender);

    _rankReputation(msg.sender);

    nextUserId = nextUserId.add(1);
  }

  // Registering Rider and manage its Activation
  function riderStatusChanger(
    address _userAddress,
    string memory _rideArea,
    uint _riderPhoneNumber)
  public
  onlyApprovedApp() userExists(_userAddress) {
    UserInfo storage userCopy = usersByAddress[_userAddress];

    // Register rider if user doesn't have rider Id
    if(userCopy.riderId == 0) {
      ridersById[nextRiderId] = Rider({
        userAddress: _userAddress,
        area: _rideArea,
        riderId: nextRiderId,
        riderPhoneNumber: _riderPhoneNumber,
        currentOrderId: 0,
        nextOrderId: 0,
        activate: false });
      userCopy.riderId = nextRiderId;

      nextRiderId = nextRiderId.add(1);
    }

    // Change rider activation.
    ridersById[userCopy.riderId].activate = !ridersById[userCopy.riderId].activate;
  }

  function shopRegister(
    address _shopOwner,
    string memory _shopArea,
    uint _shopPhoneNumber)
  public
  onlyApprovedApp() userExists(_shopOwner) {
    shopsById[nextShopId] = Shop({
      owner: _shopOwner,
      shopId: nextShopId,
      shopPhoneNumber: _shopPhoneNumber,
      area: _shopArea,
      activate: true });
    usersByAddress[_shopOwner].shopIds.push(nextShopId);

    nextShopId = nextShopId.add(1);
  }

  function shopStatusChanger(uint _shopId)
  public
  onlyApprovedApp() {
    shopsById[_shopId].activate = !shopsById[_shopId].activate;
  }

  function placeOrder(
    address _buyerAddress,
    uint _shopId,
    uint[] memory _goodsIds,
    uint[] memory _buyAmounts,
    uint _totalPrice)
  public
  onlyApprovedApp() shopExists(_shopId) {
    uint[] memory time = new uint[](6);
    time[0] = now;
    ordersById[nextOrderId] = Order({
      buyer: _buyerAddress,
      orderId: nextOrderId,
      shopId: _shopId,
      goodsIds: _goodsIds,
      buyAmounts: _buyAmounts,
      totalPrice: _totalPrice,
      riderId: 0,
      stateTime: time,
      state: OrderState.ORDERED});
    _orderStateChanger(nextOrderId, OrderState.ORDERED);

    nextOrderId = nextOrderId.add(1);
  }

  // the cancel condition will be mananged by approved external source
  function cancelOrder(uint _orderId)
  public
  onlyApprovedApp() orderExists(_orderId) {
    Order storage orderCopy = ordersById[_orderId];
    orderCopy.state = OrderState.CANCELLED;
  }

  // only user relevant to order can sign (buyer, seller, rider)
  function signOrder(uint _orderId, address _signer)
  public
  onlyApprovedApp() orderExists(_orderId)
  returns(OrderState) { // if true returned, raise reputation
    Order storage orderCopy = ordersById[_orderId];
    require(orderCopy.state == OrderState.CANCELLED || orderCopy.state == OrderState.COMPLETED,
      "not allow to sign in current state");
    require(
      _signer == orderCopy.buyer ||
      _signer == shopsById[orderCopy.shopId].owner ||
      _signer == ridersById[orderCopy.riderId].userAddress,
      "user not allowed to sign this order");

    if(orderCopy.state == OrderState.ORDERED) {
      require(_signer == shopsById[orderCopy.shopId].owner,
        "only seller allowed to sign in current state");

      _orderStateChanger(_orderId, OrderState.PREPARING);

      return OrderState.PREPARING;
    }
    if(orderCopy.state == OrderState.PREPARING) {
      require(_signer == ridersById[orderCopy.riderId].userAddress,
        "only rider allowed to sign in current state");

      _orderStateChanger(_orderId, OrderState.DELIVERING);

      return OrderState.DELIVERING;
    }
    if(orderCopy.state == OrderState.DELIVERING) {
      Rider storage riderCopy = ridersById[orderCopy.riderId];
      require(_signer == riderCopy.userAddress,
        "only rider allowed to sign in current state");

      _orderStateChanger(_orderId, OrderState.DELIVERED);
      // Rider get to next order
      riderCopy.currentOrderId = riderCopy.nextOrderId;
      riderCopy.nextOrderId = 0;

      return OrderState.DELIVERED;
    }
    if(orderCopy.state == OrderState.DELIVERED) {
      require(_signer == orderCopy.buyer,
        "only buyer allowed to sign in current state");

      _orderStateChanger(_orderId, OrderState.COMPLETED);

      return OrderState.COMPLETED;
    }
  }

  function assignRider(uint _orderId, uint _riderId)
  public
  onlyApprovedApp() orderExists(_orderId) riderExists(_riderId) orderStateLimitChangingRider(_orderId){
    Rider storage riderCopy = ridersById[_riderId];
    Order storage orderCopy = ordersById[_orderId];
    require(riderCopy.currentOrderId == 0 || riderCopy.nextOrderId == 0,
      "rider cannot hold more than 2 order");
    require(orderCopy.state == OrderState.ORDERED || orderCopy.state == OrderState.PREPARING,
      "rider cannot be assigned current state");
    require(orderCopy.riderId == 0,
      "order is assigned by other rider already");

    riderCopy.currentOrderId == 0 ? riderCopy.currentOrderId = _orderId : riderCopy.nextOrderId = _orderId;
    orderCopy.riderId = _riderId;
  }

  function cancelRider(uint _orderId, uint _riderId)
  public
  onlyApprovedApp() orderExists(_orderId) riderExists(_riderId) orderStateLimitChangingRider(_orderId)
  returns(bool) {
    Rider storage riderCopy = ridersById[_riderId];
    Order storage orderCopy = ordersById[_orderId];
    require(orderCopy.riderId == _riderId,
      "rider is not assigned with this order");

    orderCopy.riderId = 0;
    if(riderCopy.currentOrderId == _orderId) {
      riderCopy.currentOrderId = riderCopy.nextOrderId;
      riderCopy.nextOrderId = 0;
      return true;
    }
    if(riderCopy.nextOrderId == _orderId) {
      riderCopy.nextOrderId = 0;
      return true;
    }
  }

  function uploadReview(
    uint _orderId,
    uint _stars,
    uint[] memory _reputationIds,
    address _from,
    address _to)
  public
  onlyApprovedApp() orderExists(_orderId) userExists(_from) userExists(_to) {
    Order storage orderCopy = ordersById[_orderId];
    address buyer = orderCopy.buyer;
    address seller = shopsById[orderCopy.shopId].owner;
    address rider = ridersById[orderCopy.riderId].userAddress;
    require(_from == buyer || _from == seller || _from == rider,
      "not allow to review with this order");
    require(!reviewed[_orderId][_from][_to],
      "already reviewed");

    ReviewTarget target;
    if(_to == buyer) {
      target = ReviewTarget.BUYER;
    }
    if(_to == seller) {
      target = ReviewTarget.SHOP;
    }
    if(_to == rider) {
      target = ReviewTarget.RIDER;
    }

    reviewsById[nextReviewId] = Review(
      nextReviewId,
      _orderId,
      _stars,
      _reputationIds,
      _from,
      _to,
      false,
      target);
    nextReviewId = nextReviewId.add(1);
  }

  function cancelReview(uint _reviewId)
  public
  onlyApprovedApp() reviewExists(_reviewId) {
    Review storage reviewCopy = reviewsById[_reviewId];
    require(!reviewCopy.cancelled,
      "review already cancelled");

    reviewCopy.cancelled = true;

    // Revert Reputation
    uint[] memory reputationIds = reviewCopy.reputationIds;
    Reputation memory reputation0 = reputationsById[reputationIds[0]];
    Reputation memory reputation1 = reputationsById[reputationIds[1]];
    reputationChanger(
      reputation0.user,
      reputation0.score,
      !reputation0.sign);
    reputationChanger(
      reputation1.user,
      reputation1.score,
      !reputation1.sign);
  }

  function reputationChanger(
    address _user,
    uint _score,
    bool _sign)
  public
  onlyApprovedApp() userExists(_user)
  returns(uint _reputationId) {
    UserInfo storage userCopy = usersByAddress[_user];
    if(_sign) {
      userCopy.reputation = userCopy.reputation.add(_score);
      if(userCopy.reputation > MAXREP) {
        userCopy.reputation = MAXREP;
      }
    } else if(!_sign) {
      userCopy.reputation = userCopy.reputation.sub(_score);
    } else {
      // If it comes here, something is going wrong
      revert("!!Error while scoring reputation!!");
    }
    reputationsById[nextReputationId] = Reputation(
      nextReputationId,
      _score,
      _user,
      _sign);
    emit ReputationChanged(_user, nextReputationId, _score, _sign);

    _reputationId = nextReputationId;
    nextReputationId = nextReputationId.add(1);
  }

  // It is used periodically by admin to normalize the user reputations
  // Ranks are not changed. Reputation gaps of each user will be reduced
  function normalizeReputation()
  external
  onlyAdmin() {
    for(uint i = 1; i < nextUserId; i++) {
      UserInfo storage userCopy = usersByAddress[userAddressById[i]];
      uint userRep = userCopy.reputation;
      uint HALFREP = MAXREP / 2;
      if(userRep > HALFREP) {
        uint scaleAmount = (userRep - HALFREP) * 3 / 10;
        userRep = userRep.sub(scaleAmount);
      }
      if(userRep < HALFREP) {
        uint scaleAmount = (HALFREP - userRep) * 3 / 10;
        userRep = userRep.add(scaleAmount);
      }

      userCopy.reputation = userRep;
    }
  }

  function getUserInfoByAddress(address _user)
  public view
  onlyApproved() userExists(_user)
  returns(UserInfo memory) {  return usersByAddress[_user]; }

  function getUserInfoById(uint _userId)
  public view
  onlyApproved() userExists(userAddressById[_userId])
  returns(UserInfo memory) { return usersByAddress[userAddressById[_userId]]; }

  function getShopInfo(uint _shopId)
  public view
  onlyApproved() shopExists(_shopId)
  returns(Shop memory) { return shopsById[_shopId]; }

  function getRiderInfo(uint _riderId)
  public view
  onlyApproved() riderExists(_riderId)
  returns(Rider memory) { return ridersById[_riderId]; }

  function getOrderInfo(uint _orderId)
  public view
  onlyApproved() orderExists(_orderId)
  returns(Order memory) { return ordersById[_orderId]; }

  function getReviewInfo(uint _reviewId)
  public view
  onlyApproved() reviewExists(_reviewId)
  returns(Review memory) { return reviewsById[_reviewId]; }

  function getReputationInfo(uint _reputationId)
  public view
  onlyApproved()
  returns(Reputation memory) {
    require(reputationsById[_reputationId].user != address(0),
      "this reputation info does not exists");

    return reputationsById[_reputationId];
  }

  function getUsersByRank()
  public view
  onlyApproved()
  returns(address[] memory) { return usersByRank; }

  function _orderStateChanger(uint _orderId, OrderState _state)
  internal {
    Order storage orderCopy = ordersById[_orderId];
    orderCopy.state = _state;
    orderCopy.stateTime[uint(_state)] = now;
    emit OrderStatusChanged(_orderId, now, _state);
  }

  // _pos true means rank needs to go up since user gained scores.
  // _pos false means rank needs to go down since user lost scores.
  function _rankReputation(address _user)
  internal {
    bool _pos = false;
    uint userRank = usersByAddress[_user].repRank;
    // if the user is ranked the bottom or upper ranker has lower reputation _pos is true(rank should go up)
    if(userRank == usersByRank.length - 1 ||
    usersByAddress[_user].reputation > usersByAddress[usersByRank[userRank - 1]].reputation) {
      _pos = true;
    }
    if(_pos) {
      while(userRank - 1 >= 0 &&  // next rank should be available
      usersByAddress[_user].reputation > usersByAddress[usersByRank[userRank - 1]].reputation) {
        address temp = usersByRank[userRank - 1];
        usersByAddress[temp].repRank = userRank;
        usersByAddress[_user].repRank = userRank - 1;
        usersByRank[userRank - 1] = _user;
        usersByRank[userRank] = temp;
      }
    }
    if(!_pos) {
      while(userRank + 1 >= usersByRank.length - 1 && // next rank should be available
      usersByAddress[_user].reputation < usersByAddress[usersByRank[userRank + 1]].reputation) {
        address temp = usersByRank[userRank + 1];
        usersByAddress[temp].repRank = userRank;
        usersByAddress[_user].repRank = userRank + 1;
        usersByRank[userRank + 1] = _user;
        usersByRank[userRank] = temp;
      }
    }
  }

  modifier onlyAdmin() {
    require(admins[msg.sender],
      "only Admin");
    _;
  }

  modifier onlyApprovedApp() {
    require(approvedApps[msg.sender] || msg.sender == address(this),
      "only approved contract");
    _;
  }

  modifier onlyApproved() {
    require(approvedApps[msg.sender] || admins[msg.sender],
      "only approved");
    _;
  }

  modifier userExists(address _user) {
    require(usersByAddress[_user].userAddress != address(0),
      "user does not exist");
    _;
  }

  modifier shopExists(uint _shopId) {
    Shop storage shopCopy = shopsById[_shopId];
    require(shopCopy.owner != address(0),
      "shop does not exist");
    require(shopCopy.activate,
      "shop is not available now");
    _;
  }

  modifier orderExists(uint _orderId) {
    require(ordersById[_orderId].orderId != 0,
      "order does not exist");
    _;
  }

  modifier riderExists(uint _riderId) {
    Rider storage riderCopy = ridersById[_riderId];
    require(riderCopy.userAddress != address(0),
      "rider does not registered");
    require(ridersById[_riderId].activate,
      "rider currently not availabe now");
    _;
  }

  modifier reviewExists(uint _reviewId) {
    Review storage reviewCopy = reviewsById[_reviewId];
    require(reviewCopy.reviewId != 0,
      "review does not exists");
    _;
  }

  modifier orderStateLimitChangingRider(uint _orderId) {
    Order storage orderCopy = ordersById[_orderId];
    require(orderCopy.state == OrderState.ORDERED || orderCopy.state == OrderState.PREPARING,
      "rider cannot be changed in current order state");
    _;
  }
}