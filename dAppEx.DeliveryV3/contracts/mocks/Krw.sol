// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.6;

// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/ERC20.sol";
import "../../node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Krw is ERC20 {
    mapping(address => bool) admins;
    mapping(address => bool) minters;

    constructor() ERC20('KRW', 'Coin equal to Korea Currency')
    public {
        admins[msg.sender] = true;
    }

    function addAdmin(address _address)
    external
    onlyAdmin(msg.sender) {
        admins[_address] = true;
    }

    function addMinters(address _address)
    external
    onlyAdmin(msg.sender) {
        minters[_address] = true;
    }

    function faucet(address _to, uint _amount)
    external
    onlyMinter(msg.sender) {
        _mint(_to, _amount);
    }

    modifier onlyAdmin(address _address) {
        require(admins[_address],
            "only admin");
        _;
    }

    modifier onlyMinter(address _address) {
        require(minters[_address],
            "only minter");
        _;
    }
}