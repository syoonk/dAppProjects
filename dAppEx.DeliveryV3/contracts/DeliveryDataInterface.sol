// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.8;

/*
 *  It is used for basic data structure
 */
contract DeliveryDataInterface{

  enum OrderState {
    ORDERED,
    PREPARING,
    DELIVERING,
    DELIVERED,
    COMPLETED,
    CANCELLED
  }

  // It tells target of the review
  enum ReviewTarget {
    BUYER,
    RIDER,
    SHOP
  }

  // User Infos
  struct UserInfo {
    address userAddress;
    uint userId;
    uint phoneNumber;
    uint reputation;
    uint repRank;
    string homeAddress;
    string userPublicKey;
    uint riderId;
    uint[] shopIds;
  }

  struct Rider {
    address userAddress;
    string area;
    uint riderId;
    uint riderPhoneNumber;
    uint currentOrderId;
    uint nextOrderId;
    bool activate;
  }

  struct Shop {
    address owner;
    uint shopId;
    uint shopPhoneNumber;
    string area;
    bool activate;
  }

  // basic ledger of order.
  struct Order {
    address buyer;
    uint orderId;
    uint shopId;
    uint[] goodsIds;
    uint[] buyAmounts;
    uint totalPrice;
    uint riderId;
    uint[] stateTime;
    OrderState state;
  }

  struct Reputation {
    uint reputaionId;
    uint score;
    address user;
    bool sign;
  }

  struct Review {
    uint reviewId;
    uint orderId;
    uint stars;
    uint[] reputationIds; // It stores changed reputation IDs
    address from;
    address to;
    bool cancelled;
    ReviewTarget reviewTarget;
  }

  bytes32 constant KNG = bytes32("KNG");
  bytes32 constant KRW = bytes32("KRW");
  uint constant MAXREP = 100000;

  event OrderStatusChanged(
    uint _orderId,
    uint _time,
    OrderState _state);

  event ReputationChanged(
    address indexed _user,
    uint _reputationId,
    uint _score,   // This value is actual value
    bool _sign);
}
