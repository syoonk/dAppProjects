// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "../node_modules/@openzeppelin/contracts/math/SafeMath.sol";
import "./DeliveryDataHolder.sol";
import "./DeliveryDataInterface.sol";
import "./Wallet.sol";

/*
 *  This Contract contains all of application functions
 */
contract DeliveryApp is DeliveryDataInterface {
  using SafeMath for uint;
  DeliveryDataHolder dataHolder;
  Wallet wallet;

  enum VoteType {
    CANCELREVIEW,
    PENALTY
  }

  enum VoteState {
    VOTING,
    AGREED,
    REJECTED,
    CANCELLED
  }

  struct Vote {
    VoteType voteType;
    address creator;
    address targetUser; // if type is CANCELREVIEW it is 0
    uint reviewId;  // if type is not CANCELREVIEW it is 0
    uint voteId;
    uint up;
    uint down;
    uint start;
    uint end;
    VoteState state;
  }

  mapping(address => bool) admins;
  mapping(uint => mapping(address => bool)) cancelSign;
  mapping(address => bool) scoringEntrancy;
  // Vote Part
  mapping(uint => Vote) votesById;
  mapping(uint => mapping(address => bool)) voted;
  mapping(uint => mapping(address => bool)) agreements;
  uint nextVoteId = 1;
  uint RIDERFEE = 3000;
  uint MINTKNG = 1e12;
  uint FLOATDIV = 1000;

  constructor(address _dataHolderAddress, address _walletAddress)
  public {
    admins[msg.sender] = true;
    dataHolder = DeliveryDataHolder(_dataHolderAddress);
    wallet = Wallet(_walletAddress);
  }

  function addAdmin(address _user)
  external
  onlyAdmin() {
    admins[_user] = true;
  }

  // admin set Rider Fee
  function setRiderFee(uint _riderfee)
  external
  onlyAdmin() {
    require(_riderfee % 2 == 0,
      "rider fee should be even number");

    RIDERFEE = _riderfee;
  }

  function changeDataHolder(address _dataHolderAddress)
  external
  onlyAdmin() {
    dataHolder = DeliveryDataHolder(_dataHolderAddress);
  }

  function changeWallet(address _walletAddress)
  external
  onlyAdmin() {
    wallet = Wallet(_walletAddress);
  }

  function userRegister(
    uint _phoneNumber,
    string calldata _homeAddress,
    string calldata _userPublicKey)
  external {
    dataHolder.addUser(
      msg.sender,
      _phoneNumber,
      _homeAddress,
      _userPublicKey);
  }

  function riderActivation(string calldata _rideArea, uint _riderPhoneNumber)
  external {
    dataHolder.riderStatusChanger(msg.sender, _rideArea, _riderPhoneNumber);
  }

  function shopRegister(string calldata _shopArea, uint _shopPhoneNumber)
  external {
    dataHolder.shopRegister(msg.sender, _shopArea, _shopPhoneNumber);
  }

  function shopActivation(uint _shopId)
  external {
    Shop memory shopTemp = dataHolder.getShopInfo(_shopId);
    require(msg.sender == shopTemp.owner,
      "not allow to change shop activation");

    dataHolder.shopStatusChanger(_shopId);
  }

  function placeOrder(
    uint _shopId,
    uint[] calldata _goodsIds,
    uint[] calldata _buyAmounts,
    uint _totalPrice)
  external {
    // Rider fee is 3000
    require(_totalPrice >= RIDERFEE,
      "totalPrice must include rider fee");

    dataHolder.placeOrder(
      msg.sender,
      _shopId,
      _goodsIds,
      _buyAmounts,
      _totalPrice);

    // it holds until order completed
    wallet.holdKrw(msg.sender, _totalPrice);
  }

  // Order can be cancelled only in ORDERED, PREPARING state
  // In ORDERED state, buyer can cancel order immediately
  // In PREPARING state, both buyer and seller must sign it to cancel
  function cancelOrder(uint _orderId)
  external {
    Order memory orderTemp = dataHolder.getOrderInfo(_orderId);
    address buyer = orderTemp.buyer;
    address seller = dataHolder.getShopInfo(orderTemp.shopId).owner;
    require(orderTemp.state == OrderState.ORDERED || orderTemp.state == OrderState.PREPARING,
      "not allow to cancel in this state");
    require(msg.sender == buyer || msg.sender == seller,
      "not allow to sign cancelling order");

    bool cancelling = false;
    cancelSign[_orderId][msg.sender] = true;
    if(orderTemp.state == OrderState.ORDERED) {
      if(msg.sender == buyer) {
        cancelling = true;
      }
    }
    // both are signed
    if(cancelSign[_orderId][buyer] && cancelSign[_orderId][seller]) {
        cancelling = true;
    }

    // if cancelling is true, conditions are matched.
    if(cancelling) {
      dataHolder.cancelOrder(_orderId);

      // refund KRW to buyer
      wallet.releaseKrw(buyer, buyer, orderTemp.totalPrice);
    }
  }

  function assignRider(uint _orderId, uint _riderId)
  external
  isRiderSender(_riderId) {
    dataHolder.assignRider(_orderId, _riderId);
  }

  function cancelRider(uint _orderId, uint _riderId)
  external
  isRiderSender(_riderId) {
    dataHolder.cancelRider(_orderId, _riderId);

    // reduce rider reputation
    address rider = dataHolder.getRiderInfo(_riderId).userAddress;
    _scoringReputation(rider, 500, false, true);
  }

  function signOrder(uint _orderId)
  external {
    OrderState state = dataHolder.signOrder(_orderId, msg.sender);

    Order memory orderTemp = dataHolder.getOrderInfo(_orderId);
    if(state == OrderState.DELIVERING) {
      Shop memory shopTemp = dataHolder.getShopInfo(orderTemp.shopId);
      Rider memory riderTemp = dataHolder.getRiderInfo(orderTemp.riderId);
      // seller get the amount of sold
      wallet.releaseKrw(orderTemp.buyer, shopTemp.owner, orderTemp.totalPrice - RIDERFEE);
      // rider get the half of fee
      wallet.releaseKrw(orderTemp.buyer, riderTemp.userAddress, RIDERFEE / 2);

      // Mint KNG Coin
      uint mintAmount = MINTKNG * _kngCommission(shopTemp.owner) / FLOATDIV;
      wallet.mintKng(shopTemp.owner, mintAmount);
    }
    if(state == OrderState.DELIVERED) {
      Rider memory riderTemp = dataHolder.getRiderInfo(orderTemp.riderId);
      // rider get the rest of the fee
      wallet.releaseKrw(orderTemp.buyer, riderTemp.userAddress, RIDERFEE / 2);

      // Mint KNG Coin
      uint mintAmount = MINTKNG * _kngCommission(riderTemp.userAddress) / FLOATDIV;
      wallet.mintKng(riderTemp.userAddress, mintAmount);
    }
    if(state == OrderState.COMPLETED) {
      // all user reputations raised
      Shop memory shopTemp = dataHolder.getShopInfo(orderTemp.shopId);
      Rider memory riderTemp = dataHolder.getRiderInfo(orderTemp.riderId);

      _scoringReputation(orderTemp.buyer, 500, true, true);
      _scoringReputation(shopTemp.owner, 500, true, true);
      _scoringReputation(riderTemp.userAddress, 500, true, true);

      // Mint KNG Coin
      uint mintAmount = MINTKNG * _kngCommission(orderTemp.buyer) / FLOATDIV;
      wallet.mintKng(orderTemp.buyer, mintAmount);
    }
  }

  function uploadReview(
    uint _orderId,
    uint _stars,
    address _to)
  external {
    require(_stars > 0 && _stars <= 5,
      "star must be 1 to 5");

    uint score; bool sign;
    if(_stars <= 2) {
      score = 3000 - 1000 * _stars;
      sign = false;
    }
    if(_stars >= 4) {
      score = _stars * 1000 - 3000;
      sign = true;
    }

    uint[] memory reputationIds = new uint[](2);
    // score uploader for upload review
    reputationIds[0] = _scoringReputation(
      msg.sender,
      500,
      true,
      true);
    // score target
    reputationIds[1] = _scoringReputation(
      _to,
      score,
      sign,
      true);

    dataHolder.uploadReview(
      _orderId,
      _stars,
      reputationIds,
      msg.sender,
      _to);
  }

  function cancelReviewByUploader(uint _reviewId)
  external {
    Review memory reviewTemp = dataHolder.getReviewInfo(_reviewId);
    require(msg.sender == reviewTemp.from,
      "only uploader can cancel review");

    dataHolder.cancelReview(_reviewId);
  }

  function createVote(
    VoteType _voteType,
    address _targetUser,
    uint _reviewId)
  external {
    dataHolder.getUserInfoByAddress(msg.sender);  // To check user does exist
    address targetUser;
    uint reviewId;
    if(_voteType == VoteType.CANCELREVIEW) {
      require(_reviewId != 0,
        "review Id should not be 0 this type");
      dataHolder.getReviewInfo(_reviewId);  // to check review does exist

      targetUser = address(0);
      reviewId = _reviewId;
    } else if(_voteType == VoteType.PENALTY) {
      require(_targetUser != address(0),
        "input address cannot be 0 this vote type");
      dataHolder.getUserInfoByAddress(_targetUser);  // to targetuser does exist

      targetUser = _targetUser;
      reviewId = 0;
    } else {
      revert("vote type is not available");
    }

    votesById[nextVoteId] = Vote(
      _voteType,
      msg.sender,
      targetUser,
      reviewId,
      nextVoteId,
      0,
      0,
      now,
      now + 259200, // for 3 days
      VoteState.VOTING);

    nextVoteId = nextVoteId.add(1);
  }

  function voting(uint _voteId, bool _vote)
  external
  voteExists(_voteId) {
    dataHolder.getUserInfoByAddress(msg.sender);
    Vote storage voteCopy = votesById[_voteId];
    require(now < voteCopy.end,
      "vote is ended");
    require(voteCopy.state != VoteState.CANCELLED,
      "vote is cancelled");

    // if msg.sender already voted it before
    if(voted[_voteId][msg.sender]) {
      if(agreements[_voteId][msg.sender]) {
        voteCopy.up = voteCopy.up.sub(1);
      } else {
        voteCopy.down = voteCopy.down.sub(1);
      }
    }

    voted[_voteId][msg.sender] = true;
    if(_vote) {
      voteCopy.up = voteCopy.up.add(1);
    } else {
      voteCopy.down = voteCopy.down.add(1);
    }
    agreements[_voteId][msg.sender] = _vote;

    _scoringReputation(msg.sender, 500, true, true);
  }

  // After vote finished, confirm and invoke its result
  function confirmingVote(uint _voteId)
  external
  voteExists(_voteId) {
    Vote storage voteCopy = votesById[_voteId];
    require(now >= voteCopy.end,
      "vote not yet ended");
    require(voteCopy.state == VoteState.VOTING,
      "vote should be in VOTING state");
    require(admins[msg.sender] || msg.sender == voteCopy.creator,
      "not allow to confirm vote");

    if(voteCopy.up > voteCopy.down) {
      if(voteCopy.voteType == VoteType.CANCELREVIEW) {
        // review will be cancelled
        dataHolder.cancelReview(voteCopy.reviewId);
      }
      if(voteCopy.voteType == VoteType.PENALTY) {
        // target user get penalties with its reputation
        _scoringReputation(voteCopy.targetUser, 1000, false, true);
      }

      voteCopy.state = VoteState.AGREED;
    } else {
      voteCopy.state = VoteState.REJECTED;
    }
  }

  // Scoring should be internal visiblity for preventing re-entracy.
  function _scoringReputation(
    address _user,
    uint _score,
    bool _sign,
    bool _rel)
  internal
  returns(uint _reputationId) {
    require(_score <= 2000,
      "score cannot over 2000 at once");
    require(!scoringEntrancy[_user],
      "previous scoring is not finished yet");

    scoringEntrancy[_user] = true;

    UserInfo memory userTemp = dataHolder.getUserInfoByAddress(_user);
    // raising score is reduced relatively. Higher reputation get lower score
    uint score = _rel ? 2 * (MAXREP - userTemp.reputation) * _score / MAXREP : _score;
    _reputationId = dataHolder.reputationChanger(_user, score, _sign);

    scoringEntrancy[_user] = false;
  }

  // commission has 3 float point. It must be divided by 1000
  function _kngCommission(address _user)
  internal view
  returns(uint) {
    UserInfo memory userTemp = dataHolder.getUserInfoByAddress(_user);
    address[] memory ranksTemp = dataHolder.getUsersByRank();
    uint maximumRank = ranksTemp.length;
    uint userRank = userTemp.repRank;
    uint commission = 1000 * ((3 * maximumRank / 2) - userRank) / maximumRank;

    return commission;
  }

  modifier onlyAdmin() {
    require(admins[msg.sender],
      "only admin");
    _;
  }

  modifier isRiderSender(uint _riderId) {
    Rider memory riderTemp = dataHolder.getRiderInfo(_riderId);
    require(msg.sender == riderTemp.userAddress,
      "sender does not match with rider");
    _;
  }

  modifier voteExists(uint _voteId) {
    require(votesById[_voteId].creator != address(0),
      "vote does not exists");
    _;
  }
}