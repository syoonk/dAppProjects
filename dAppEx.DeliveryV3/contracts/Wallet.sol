// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.7;

// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";
// import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";
import "../node_modules/@openzeppelin/contracts/math/SafeMath.sol";
import "../node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./DeliveryDataInterface.sol";
import "./DeliveryDataHolder.sol";
import "./mocks/Kng.sol";

/*
 * It manages finances of users and communicates with Coin contracts
 * This wallet only can be used for delivery app
 * It only controls KRW and KNG
 */
contract Wallet is DeliveryDataInterface {
    using SafeMath for uint;
    DeliveryDataHolder dataHolder;

    struct Token {
        bytes32 ticker;
        address tokenAddress;
    }
    mapping(address => bool) admins;
    mapping(address => bool) approvedApps;
    mapping(bytes32 => Token) tokens;
    mapping(address => mapping(bytes32 => uint)) traderBalances;
    // this for the case of refunding.
    // After trade completed or cancelled it will be transferred
    mapping(address => uint) pendingKRW;
    bytes32[] public tokenList;
    uint BASECOMMISSION = 5000;    // percentage by 3 float point
    address owner;  // This is owner of the app

    constructor() public {
        admins[msg.sender] = true;
        owner = msg.sender;
    }

    function addAdmin(address _user)
    external
    onlyAdmin(msg.sender) {
        admins[_user] = true;
    }

    function setDataHolder(uint _dataHolderAddress)
    external
    onlyAdmin(msg.sender) {
        dataHolder = DeliveryDataHolder(_dataHolderAddress);
    }

    // Only Approved Contract can access this functions
    function approveApp(address _contractAddress)
    external
    onlyAdmin(msg.sender) {
        approvedApps[_contractAddress] = true;
    }

    function addToken(bytes32 _ticker, address _address)
    external
    onlyAdmin(msg.sender) {
        require(tokens[_ticker].tokenAddress == address(0),
            "this token is already added");

        tokens[_ticker] = Token(_ticker, _address);
        tokenList.push(_ticker);
    }

    function setBaseCommission(uint _comm)
    external
    onlyAdmin(msg.sender) {
        require(_comm >= 0 && _comm < 100000,
            "commission should be 0 to 100000");

        BASECOMMISSION = _comm;
    }

    function deposit(uint _amount, bytes32 _ticker)
    external
    tokenExist(_ticker) {
        // Wallet should be pre-approved(ERC20 Token) by user from for transferring
        // It will be done by outside source.
        IERC20(tokens[_ticker].tokenAddress).transferFrom(msg.sender, address(this), _amount);

        uint depositAmount = _amount;
        // If token is KRW, commission is going to be applied
        if(_ticker == KRW) {
            UserInfo memory userCopy = dataHolder.getUserInfoByAddress(msg.sender);
            address[] memory usersByRanks = dataHolder.getUsersByRank();
            uint numOfUsers = usersByRanks.length;
            uint userRank = userCopy.repRank;
            uint commission = BASECOMMISSION * (numOfUsers + 2 * userRank) / (2 * numOfUsers);
            uint commissionedKrw = _amount * commission / 100000;
            depositAmount = _amount - commissionedKrw;
            // Send commissioned money to the wallet contract
            traderBalances[address(this)][KRW] = traderBalances[msg.sender][KRW].add(_amount);
        }
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(depositAmount);
    }

    function withdraw(uint _amount, bytes32 _ticker)
    external
    tokenExist(_ticker) enoughBalance(msg.sender, _amount, _ticker) {
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
    }

    function getBalance(bytes32 _ticker)
    external view
    returns(uint) {
        return traderBalances[msg.sender][_ticker];
    }

    // called by user.
    function transfer(
        address _receiver,
        uint _amount,
        bytes32 _ticker)
    external
    enoughBalance(msg.sender, _amount, _ticker) {
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        traderBalances[_receiver][_ticker] = traderBalances[_receiver][_ticker].add(_amount);
    }

    // called by contract. Contract should be pre-approved
    function transferByApp(
        address _sender,
        address _receiver,
        uint _amount,
        bytes32 _ticker)
    public
    onlyApprovedApp(msg.sender) enoughBalance(_sender, _amount, _ticker) {
        traderBalances[_sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        traderBalances[_receiver][_ticker] = traderBalances[_receiver][_ticker].add(_amount);
    }

    // it holds the money before transfer
    // preventing not refunded by not enough balance
    function holdKrw(address _sender, uint _amount)
    public
    onlyApprovedApp(msg.sender) enoughBalance(_sender, _amount, KRW) {
        traderBalances[_sender][KRW] = traderBalances[_sender][KRW].sub(_amount);
        pendingKRW[_sender] = pendingKRW[_sender].add(_amount);
    }

    // it releases the money
    function releaseKrw(
        address _sender,
        address _receiver,
        uint _amount)
    public
    onlyApprovedApp(msg.sender) {
        require(pendingKRW[_sender] >= _amount,
            "not enough pending money");

        pendingKRW[_sender] = pendingKRW[_sender].sub(_amount);
        traderBalances[_receiver][KRW] = traderBalances[_receiver][KRW].add(_amount);
    }

    // Mint KNG Token. It is only called by external approved contract
    function mintKng(address _user, uint _amount)
    public
    onlyApprovedApp(msg.sender) {
        Kng(tokens[KNG].tokenAddress).faucet(_user, _amount);
    }

    function withdrawContractBalance(bytes32 _ticker, uint _amount)
    external
    onlyOnwer() tokenExist(_ticker) {
        require(traderBalances[address(this)][_ticker] >= _amount,
            "contract doesn't have enough amount to withdraw");

        // Send the tokens in wallet contract to token contract
        traderBalances[address(this)][_ticker] = traderBalances[address(this)][_ticker].sub(_amount);
        IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
    }

    modifier onlyOnwer() {
        require(msg.sender == owner,
            "only owner");
        _;
    }

    modifier onlyAdmin(address _user) {
        require(admins[msg.sender],
            "only admins");
        _;
    }

    modifier tokenExist(bytes32 _ticker) {
        require(tokens[_ticker].tokenAddress != address(0),
            "token does not exists");
        _;
    }

    modifier onlyApprovedApp(address _appAddress) {
        require(approvedApps[_appAddress],
            "only approved contract");
        _;
    }

    modifier enoughBalance(
        address _user,
        uint _amount,
        bytes32 _ticker) {
        require(traderBalances[_user][_ticker] >= _amount,
            "not enough balance");
        _;
    }
}