pragma solidity >= 0.5.0 < 0.7.0;
pragma experimental ABIEncoderV2;

contract Ebay {
    struct Auction {
        uint id;
        address payable seller;
        string name;
        string description;
        uint min;
        uint end;
        uint bestOfferId;
        uint[] offerIds;
    }
    struct Offer {
        uint id;
        uint auctionId;
        address payable buyer;
        uint price;
    }
    mapping(uint => Auction) private auctions;
    mapping(uint => Offer) private offers;
    mapping(address => uint[]) userAuctions;
    mapping(address => uint[]) userOffers;
    uint private nextAuctionId = 1;
    uint private nextOfferId = 1;
    
    function deposit()
    external payable {}

    function createAuction(
        string calldata _name,
        string calldata _description,
        uint _min,
        uint _duration) 
    external {
        require(_min > 0, 
            "_min must be > 0");
        require(_duration > 86400 && _duration < 864000, 
            "_duration must be comprised between > 1 day, < 10 days");
        
        uint[] memory offerIds = new uint[](0);
        auctions[nextAuctionId] = Auction(
            nextAuctionId,
            msg.sender,
            _name,
            _description,
            _min,
            now + _duration,
            0,
            offerIds);
        userAuctions[msg.sender].push(nextAuctionId);
        nextAuctionId++;
    }
    
    function createOffer(uint _auctionId)
    external payable 
    auctionExist(_auctionId) {
        Auction storage auction = auctions[_auctionId];
        Offer storage bestOffer = offers[auction.bestOfferId];
        require(now < auction.end, 
            "auction has expired");
        require(msg.value >= auction.min && msg.value > bestOffer.price,
            "msg.value must be > min & > bestOffer");
        
        auction.bestOfferId = nextOfferId;
        offers[nextOfferId] = Offer(
            nextOfferId,
            _auctionId,
            msg.sender,
            msg.value);
        auction.offerIds.push(nextOfferId);
        userOffers[msg.sender].push(nextOfferId);
        nextOfferId++;
    }
    
    function trade(uint _auctionId)
    external 
    auctionExist(_auctionId) {
        Auction storage auction = auctions[_auctionId];
        Offer storage bestOffer = offers[auction.bestOfferId];
        require(now > auction.end,
            "auction is still active");
        
        for(uint i = 0; i < auction.offerIds.length; i++) {
            uint offerId = auction.offerIds[i];
            if(offerId != auction.bestOfferId) {
                Offer storage offer = offers[offerId];
                offer.buyer.transfer(offer.price);
            }
        }
        auction.seller.transfer(bestOffer.price);
    }
    
    function getAuctions()
    view external
    returns(Auction[] memory) {
        Auction[] memory auctionsCopy = new Auction[](nextAuctionId - 1);
        for(uint i = 1; i < nextAuctionId; i++) {
            auctionsCopy[i - 1] = auctions[i];
        }
        return auctionsCopy;
    }
    
    function getUserAuctions(address _user) 
    external view
    returns(Auction[] memory) {
        uint[] storage userAuctionIds = userAuctions[_user];
        Auction[] memory auctionsCopy = new Auction[](userAuctionIds.length);
        for(uint i = 0; i < userAuctionIds.length; i++) {
            uint auctionId = userAuctionIds[i];
            auctionsCopy[i] = auctions[auctionId];
        }
        
        return auctionsCopy;
    }
    
    function getUserOffers(address _user)
    external view
    returns(Offer[] memory) {
        uint[] storage userOfferIds = userOffers[_user];
        Offer[] memory offersCopy = new Offer[](userOfferIds.length);
        for(uint i = 0; i < userOfferIds.length; i++) {
            uint offerId = userOfferIds[i];
            offersCopy[i] = offers[offerId];
        }
        
        return offersCopy;
    }
    
    modifier auctionExist(uint _auctionId) {
        require(_auctionId > 0 && _auctionId < nextAuctionId, 
            "auction does not exist");
        _;
    }
}