const { expectRevert, time } = require('@openzeppelin/test-helpers');
const Ebay = artifacts.require('Ebay');

contract('Ebay', (accounts) => {
  let ebay;
  const auctionConfigs = [
  {
    name: 'Auction0',
    description: 'Selling Item0',
    min: 10,
    duration: 86400 + 1
  },
  {
    name: 'Auction1',
    description: 'Selling Item1',
    min: 10,
    duration: 864000 - 1
  }];
  const [seller1, seller2, buyer1, buyer2] = 
  [accounts[0], accounts[1], accounts[2], accounts[3]];
  beforeEach(async () => {
    ebay = await Ebay.new();
    await ebay.deposit({from: accounts[9], value: web3.utils.toBN(web3.utils.toWei("1", "ether"))});
  });

  it('should NOT create a new auction if duration is not between 1-10 days', async () => {
    await expectRevert(
      ebay.createAuction(
        auctionConfigs[0].name, 
        auctionConfigs[0].description, 
        auctionConfigs[0].min, 
        86399,
        {from: seller1}),
      "_duration must be comprised between > 1 day, < 10 days");
      await expectRevert(
        ebay.createAuction(
          auctionConfigs[0].name, 
          auctionConfigs[0].description, 
          auctionConfigs[0].min, 
          864000 + 1,
          {from: seller1}),
        "_duration must be comprised between > 1 day, < 10 days");
    });

  it('should create an auction', async() => {
    let now = new Array(2);
    for(let i = 0; i < 2; i++) {
      now[i] = parseInt((new Date()).getTime() / 1000) + 2000;
      time.increaseTo(now[i]);
      await ebay.createAuction(
        auctionConfigs[i].name,
        auctionConfigs[i].description,
        auctionConfigs[i].min,
        auctionConfigs[i].duration,
        {from: accounts[i]});
    }
    const auctions = await ebay.getAuctions();
    const userAuction = await ebay.getUserAuctions(seller2);

    assert.equal(auctions.length, 2);
    for(let i = 0; i < 2; i++) {
      assert.equal(auctions[i].id, i + 1);
      assert.equal(auctions[i].seller, accounts[i]);
      assert.equal(auctions[i].name, auctionConfigs[i].name);
      assert.equal(auctions[i].description, auctionConfigs[i].description);
      assert.equal(Number(auctions[i].min), auctionConfigs[i].min);
      assert(Number(auctions[i].end) > now[i] + auctionConfigs[i].duration - 10
      && Number(auctions[i].end) < now[i] + auctionConfigs[i].duration + 10);
    }
    assert.equal(userAuction[0].id, 2);
    assert.equal(userAuction[0].seller, accounts[1]);
    assert.equal(userAuction[0].name, auctionConfigs[1].name);
    assert.equal(userAuction[0].description, auctionConfigs[1].description);
    assert.equal(Number(userAuction[0].min), auctionConfigs[1].min);
    assert(Number(userAuction[0].end) > now[1] + auctionConfigs[1].duration - 10
    && Number(userAuction[0].end) < now[1] + auctionConfigs[1].duration + 10);
  });

  it('should NOT create offer if auction does not exist', async () => {
    for(let i = 0; i < 2; i++) {
      await ebay.createAuction(
        auctionConfigs[i].name,
        auctionConfigs[i].description,
        auctionConfigs[i].min,
        auctionConfigs[i].duration,
        {from: accounts[i]});
    }

    await expectRevert(
      ebay.createOffer(0, {from: buyer1, value: web3.utils.toBN("15")}),
      "auction does not exist");
    await expectRevert(
      ebay.createOffer(3, {from: buyer1, value: web3.utils.toBN("15")}),
      "auction does not exist");
  });

  it('should NOT create offer if auction has expired', async () => {
    let now = new Array(2);
    for(let i = 0; i < 2; i++) {
      now[i] = parseInt((new Date()).getTime() / 1000) + 2000;
      time.increaseTo(now[i]);
      await ebay.createAuction(
        auctionConfigs[i].name,
        auctionConfigs[i].description,
        auctionConfigs[i].min,
        auctionConfigs[i].duration,
        {from: accounts[i]});
    }

    time.increaseTo(now[0] + 86400 + 1); // auction1 has expired    
    await expectRevert(
      ebay.createOffer(1, {from: buyer1, value: web3.utils.toBN("15")}),
      "auction has expired");
    time.increaseTo(now[1] + 86400 * 10 + 1); // auction2 has expired
    await expectRevert(
      ebay.createOffer(2, {from: buyer1, value: web3.utils.toBN("15")}),
      "auction has expired");
  });

  it('should NOT create offer if price too low', async () => {
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    
    await expectRevert(
      ebay.createOffer(1, {from: accounts[0], value: web3.utils.toBN("9")}),
      "msg.value must be > min & > bestOffer");
    await ebay.createOffer(1, {from: accounts[0], value: web3.utils.toBN("12")}),
    await expectRevert(
      ebay.createOffer(1, {from: accounts[0], value: web3.utils.toBN("12")}),
      "msg.value must be > min & > bestOffer");
  });

  it('should create offer', async () => {
    // buyer1 offers auction1 and auction2
    const bid = web3.utils.toBN("12");
    for(let i = 0; i < 2; i++) {
      await ebay.createAuction(
        auctionConfigs[i].name,
        auctionConfigs[i].description,
        auctionConfigs[i].min,
        auctionConfigs[i].duration,
        {from: accounts[i]});
    }
    await ebay.createOffer(1, {from: buyer1, value: bid});
    await ebay.createOffer(2, {from: buyer1, value: bid});
    const userOffers = await ebay.getUserOffers(buyer1);
    const auctions = await ebay.getAuctions();
    
    // offer1 offering auction1
    assert.equal(userOffers.length, 2);
    assert.equal(userOffers[0].id, 1);
    assert.equal(userOffers[0].auctionId, 1);
    assert.equal(userOffers[0].buyer, buyer1);
    assert.equal(Number(userOffers[0].price).bid);
    // offer2 offering auction2
    assert.equal(userOffers[1].id, 2);
    assert.equal(userOffers[1].auctionId, 2);
    assert.equal(userOffers[1].buyer, buyer1);
    assert.equal(Number(userOffers[1].price), bid);
    // auction bestOfferId
    assert.equal(auctions[0].bestOfferId, 1);
    assert.equal(auctions[1].bestOfferId, 2);
  });

  it('should NOT trade if auction does not exist', async () => {
    await expectRevert(
      ebay.trade(2),
      "auction does not exist");
  });

  it('should NOT trade if auction not expired', async () => {
    const bid = web3.utils.toBN("12");
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    await ebay.createOffer(1, {from: buyer1, value: bid});    

    await expectRevert(
      ebay.trade(1),
      "auction is still active");
  });

  it.only('should trade', async () => {
    const bid = web3.utils.toBN("12");
    const bid_min = web3.utils.toBN("10");
    const now = parseInt((new Date()).getTime() / 1000) + 2000;
    time.increaseTo(now);
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    await ebay.createOffer(1, {from: buyer1, value: bid_min});
    // buyer2 will won the auction
    await ebay.createOffer(1, {from: buyer2, value: bid});
    const beforeBalanceBuyer1 = web3.utils.toBN(await web3.eth.getBalance(buyer1));
    const beforeBalanceSeller1 = web3.utils.toBN(await web3.eth.getBalance(seller1));
    time.increaseTo(now + 86400 + 1 + 1);
    await ebay.trade(1, {from: seller2});
    const afterBalanceBuyer1 = web3.utils.toBN(await web3.eth.getBalance(buyer1));
    const afterBalanceSeller1 = web3.utils.toBN(await web3.eth.getBalance(seller1));

    assert(beforeBalanceBuyer1.add(bid_min).eq(afterBalanceBuyer1));
    assert(afterBalanceSeller1.sub(beforeBalanceSeller1).eq(bid));
  });
});
