pragma solidity >= 0.5.0 < 0.7.0;
pragma experimental ABIEncoderV2;

contract Tinder {
    enum Gender {
        MALE,
        FEMALE
    }
    enum SwipeStatus {
        UNKNOWN,
        LIKE,
        DISLIKE
    }
    struct User {
        uint userId;
        address userAddress;
        string userPublicKey;
        string name;
        string city;
        Gender gender;
        uint age;
        string picUrl;
    }
    struct SwipeSession {
        uint start;
        uint count;
    }
    mapping(address => User) private users;
    mapping(bytes32 => mapping(uint => address[])) private userIdsByCity;
    mapping(address => mapping(address => SwipeStatus)) private swipes;
    mapping(address => SwipeSession) private swipeSessions;
    uint public nextUserId;
    
    event NewMatch(
        address indexed from,
        address indexed to, 
        uint date);
    event NewMessage(
        address indexed from,
        address indexed to,
        string content,
        uint date,
        uint conversationId);

    function register(
        string calldata _name,
        string calldata _city,
        Gender _gender,
        uint _age,
        string calldata _picUrl,
        string calldata _publicKey)
    external {
        require(users[msg.sender].age == 0,
            "User already registered");
        require(!_isEmptyString(_name),
            "_name cannot be empty");
        require(!_isEmptyString(_city),
            "_city cannot be empty");
        require(_age > 17, 
            "_age must be 18 or above");
        require(!_isEmptyString(_picUrl),
            "_picUrl cannot be empty");
            
        users[msg.sender] = User(
            nextUserId,
            msg.sender,
            _publicKey,
            _name,
            _city,
            _gender,
            _age,
            _picUrl);
        userIdsByCity[keccak256(abi.encodePacked(_city))][uint(_gender)].push(msg.sender);
        nextUserId++;
    }

    function getUserInfo()
    external view
    userExists(msg.sender)
    returns(User memory) {
        return users[msg.sender];
    }

    function getSwipeSession()
    external view
    returns(SwipeSession memory) {
        return swipeSessions[msg.sender];
    }

    function getSwipes(address _from, address _to)
    external view
    returns(uint) {
        return uint(swipes[_from][_to]);
    }

    function getUsersByCity(string calldata _city, Gender _gender)
    external view
    returns(address[] memory) {
        return userIdsByCity[keccak256(abi.encodePacked(_city))][uint(_gender)];
    }

    function changeCity(string calldata _city)
    external 
    userExists(msg.sender) {
        User storage sender = users[msg.sender];
        string memory cityCopy = sender.city;
        require(keccak256(abi.encodePacked(cityCopy)) != keccak256(abi.encodePacked(_city)),
            "cannot change to current city");

        address[] storage byCityArray = userIdsByCity[keccak256(abi.encodePacked(cityCopy))][uint(sender.gender)];
        for(uint i = 0; i < byCityArray.length; i++) {
            if(byCityArray[i] == msg.sender) {
                for(uint j = i + 1; j < byCityArray.length; j++) {
                    byCityArray[j - 1] = byCityArray[j];
                }
                byCityArray.pop();
                break;
            }
        }
        sender.city = _city;
        userIdsByCity[keccak256(abi.encodePacked(_city))][uint(sender.gender)].push(msg.sender);
    }

    function changePicture(string calldata _picUrl) 
    external
    userExists(msg.sender) {
        users[msg.sender].picUrl = _picUrl;
    }
 
    function getMatchableUsers()
    external view
    userExists(msg.sender)
    returns(User[] memory) {
        User storage user = users[msg.sender];
        uint oppositeGender = user.gender == Gender.MALE ? 1 : 0;
        address[] storage userAdses = userIdsByCity[keccak256(abi.encodePacked(user.city))][oppositeGender];
        uint matchableUserCount;
        for(uint i = 0; i < userAdses.length; i++) {
            address userAds = userAdses[i];
            if(swipes[msg.sender][userAds] != SwipeStatus.DISLIKE) {
                matchableUserCount++;
            }
        }
        User[] memory cityUsers = new User[](matchableUserCount);
        for(uint i = 0; i < matchableUserCount; i++) {
            address userAds = userAdses[i];
            if(swipes[userAds][msg.sender] != SwipeStatus.DISLIKE) {
                cityUsers[i] = users[userAds];
            }
        }
        return cityUsers;
    }
    
    function swipe(SwipeStatus _swipeStatus, address _user)
    external 
    userExists(msg.sender) userExists(_user) isUserSender(_user) {
        require(swipes[msg.sender][_user] == SwipeStatus.UNKNOWN,
            "Cannot swipe same person twice");
            
        SwipeSession storage swipeSession = swipeSessions[msg.sender];
        if(swipeSession.start + 86400 <= now) {
            swipeSession.start = now;
            swipeSession.count = 0;
        }
        require(swipeSession.count <= 2,
            "You have already used up all your swipes for today");
        swipeSession.count++;
        
        if(_swipeStatus == SwipeStatus.DISLIKE) {
            swipes[msg.sender][_user] = _swipeStatus;
            return ;
        }
        swipes[msg.sender][_user] = SwipeStatus.LIKE;
        if(swipes[_user][msg.sender] == SwipeStatus.LIKE) {
            emit NewMatch(msg.sender, _user, now);
        }
    }
    
    function sendMessage(
        address _to, 
        string calldata _content,
        uint _conversationId)
    external 
    userExists(msg.sender) userExists(_to) isUserSender(_to) {
        require(swipes[msg.sender][_to] == SwipeStatus.LIKE 
                && swipes[_to][msg.sender] == SwipeStatus.LIKE, 
            "both users need to have liked each other to send message");
        emit NewMessage(msg.sender, _to, _content, now, _conversationId);
    }
    
    function _isEmptyString(string memory _str)
    internal pure
    returns(bool) {
        bytes memory bytesStr = bytes(_str);
        return bytesStr.length == 0;
    }
    
    modifier userExists(address _user) {
        require(users[_user].age > 0, 
            "User is not registered");
        _;
    }

    modifier isUserSender(address _user) {
        require(msg.sender != _user,
            "cannot do this itself");
        _;
    }
}