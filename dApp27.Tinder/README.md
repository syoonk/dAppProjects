# About Tinder Application
It is dating app motivated by Tinder. User create their info then can get the list of other users in same area. If they like each other, they will be matched and able to send message.

## Used Tools
Solidity, Truffle, NodeJS, React, Metamask

----
## How To Run
It uses metamask.

| EndPoints | |
---|---|
Truffle | http://localhost:9545  
Ganache | http://localhost:8545

### Deploy Contract  
`./npm install`  
`./truffle develop`  
`truffle> migrate --reset`  

### Run Frontend
`./app/npm install web3`
`./app/npm install`  
`./app/npm start`

----
## Features
### 1. Encrypted Messages
Matched users can exchange text messages. Sending message is managed by event and encrypted using by library [crpyto](https://nodejs.org/api/crypto.html). Key Pair method encrypts messages then only matched user can read texts. Every user has to put their private key used for identity and encrypytion. (Private Key must not be used in this way on real application)

----
## Pages
### Input private key to indentify user  
<img src="./images/privateKey.png" width="50%" height="50%"></img>

### Registering
If user is not registered, they will see this page.  
<img src="./images/register.png" width="50%" height="50%"></img>

### Main Page
User will see the user list. If user is male and living in area1, only female living in area1 users will on the list. They choose like or dislike. If both like each other, they will be matched and be able to send text message.  
<img src="./images/matching.png" width="50%" height="50%"></img>

### Send Message
Matched couple can send message. Texts are encrypted by key pair method.  
<img src="./images/matched.png" width="50%" height="50%"></img>