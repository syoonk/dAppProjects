import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import LoadingContainer from "./LoadingContainer.js";

ReactDOM.render(
  <LoadingContainer></LoadingContainer>,
  document.getElementById('root')
);