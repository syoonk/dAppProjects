import React, { useEffect, useState } from "react";

import "./css/Swipe.css";
import Message from "./Message";
import ModalPortal from "./ModalPortal.js";

function Swipe({contract, accounts}) {
  const SwipeStatus = {
    UNKNOWN: 0,
    LIKE: 1,
    DISLIKE: 2
  }

  const [currentUserInfo, setCurrentUserInfo] = useState(null);
  const [matchableUsers, setMatchableUsers] = useState([]);
  const [currentMatchableUsersIndex, setCurrentMatchableUsersIndex] = useState(0);
  const [currentMatchableUserSwipeStatus, setCurrentMatchableUsersSwipeStatus] = useState(SwipeStatus.UNKNOWN);
  const [currentMatchableUserSwipeStatusToUser, setCurrentMatchableUsersSwipeStatusToUser] = useState(SwipeStatus.UNKNOWN);
  const [openMessage, setOpenMessage] = useState(false);
  
  useEffect(() => {
    setCurrentUserInfo(getUserInfo(accounts[0]));
    setCurrentMatchableUsersIndex(0);
    getMatchableUsers();
  }, [accounts]);

  useEffect(() => {
    getSwipeStatus();
  }, [currentMatchableUsersIndex, matchableUsers])
  
  const getUserInfo = async (address) => {
    const userInfo = await contract.methods.getUserInfo().call({from: address}); 
    return userInfo;
  }

  const getMatchableUsers = async () => {
    const usersCopy = await contract.methods.getMatchableUsers().call({from: accounts[0]});
    if(usersCopy.length === 0) {
      setMatchableUsers([]);
      return;
    }

    setMatchableUsers(usersCopy);
  }

  const getSwipeStatus = async () => {
    if(!(matchableUsers.length > 0)) return;
    const matchableUserAddress = matchableUsers[currentMatchableUsersIndex].userAddress;
    const mySwipeStatus = await contract.methods.getSwipes(accounts[0], matchableUserAddress).call();
    const theirSwipeStatus = await contract.methods.getSwipes(matchableUserAddress, accounts[0]).call();

    setCurrentMatchableUsersSwipeStatus(Number(mySwipeStatus));
    setCurrentMatchableUsersSwipeStatusToUser(Number(theirSwipeStatus));
  }

  const clickLeftHandle = (e) => {
    e.preventDefault();
    const currentMatchableUsersIndexCopy = currentMatchableUsersIndex - 1;
    setCurrentMatchableUsersIndex(currentMatchableUsersIndexCopy);
  }

  const clickRightHandle = (e) => {
    e.preventDefault();
    const currentMatchableUsersIndexCopy = currentMatchableUsersIndex + 1;
    setCurrentMatchableUsersIndex(currentMatchableUsersIndexCopy);
  }

  const submitChoiceHandle = async (e) => {
    e.preventDefault();
    const choice = e.target.elements[0].value;
    let confirmed = window.confirm("Your decision is irreversible. Will you go anyway?");
    if(confirmed) {
      const sessionCount = await contract.methods.getSwipeSession().call({from: accounts[0]});
      if(Number(sessionCount.count) === 3) {
        alert(`You spent all sessions today. Count will be refilled at ${(new Date((parseInt(sessionCount.start) + 86400) * 1000).toLocaleString())}`);
        return ;
      }      
        await contract.methods.swipe(choice, matchableUsers[currentMatchableUsersIndex].userAddress).send({from: accounts[0]});
    } else {
      return ;
    }

    getSwipeStatus();
    window.location.reload();
  }

  const clickOpenMessageHandle = (e) => {
    e.preventDefault();
    setOpenMessage(true);
  }

  const clickCloseMessageHandle = (e) => {
    e.preventDefault();
    setOpenMessage(false);
  }

  return (
    <div className="swipe">
      <h2>Here users are waiting for you</h2>
      <div className="swipeBox">
        <button 
          className="swipeButton left" 
          onClick={e => clickLeftHandle(e)}
          disabled={!matchableUsers[currentMatchableUsersIndex - 1] ? "disabled" : ""}
        >◀</button>
        {
          matchableUsers.length > 0 ? (
            <div className="userInfo">
              <img 
                src={matchableUsers[currentMatchableUsersIndex].picUrl}
                style={{
                  width: "300px", 
                  height: "300px",
                  border: "3px solid gray",
                  borderRadius: "50px"}}>
              </img>
              <p>{matchableUsers[currentMatchableUsersIndex].name}</p>
              <p>Age: {matchableUsers[currentMatchableUsersIndex].age}</p>
              {
              (currentMatchableUserSwipeStatus === SwipeStatus.LIKE
              && currentMatchableUserSwipeStatusToUser === SwipeStatus.LIKE) ? (
                <div style={{fontWeight: "bold"}}>
                  <p>Congratulations! You are matched!!!</p>
                  <p style={{cursor: "pointer"}} onClick={e => clickOpenMessageHandle(e)}><u>Send message NOW</u></p>
                  {
                  openMessage && (
                    <ModalPortal>
                      <Message 
                        contract={contract}
                        accounts={accounts}
                        matchedUser={matchableUsers[currentMatchableUsersIndex]}
                        onClose={e => clickCloseMessageHandle(e)}>
                      </Message>
                    </ModalPortal>
                  )
                  }
                </div>) : (
                currentMatchableUserSwipeStatus === SwipeStatus.UNKNOWN ? (
                  <form onSubmit={e => submitChoiceHandle(e)}>
                    <select style={{borderRadius: "10px", padding: "2px", marginBottom: "10px"}}>
                      <option value="" style={{fontWeight: "bold"}}>Choose</option>
                      <option value={SwipeStatus.LIKE}>Like</option>
                      <option value={SwipeStatus.DISLIKE}>Dislike</option>
                    </select>
                    <button className="choiceButton">Submit</button>
                  </form>
                ) : (<p>You already LIKE {Number(currentUserInfo.gender) ? "her" : "him"}</p>)
              )
              }
            </div>
          ) : ( 
            <p style={{marginTop: "225px", display: "inline-block"}}>Sorry.. There is no matchable user right now</p>
          )
        }
        <button
          className="swipeButton right"
          onClick={e => clickRightHandle(e)}
          disabled={!matchableUsers[currentMatchableUsersIndex + 1] ? "disabled" : ""}
        >▶</button>
      </div>
    </div>
  );
}

export default Swipe;