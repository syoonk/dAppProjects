import React, { useEffect, useState, useRef } from "react";

import "./css/Message.css";
import { computeSecret, encrypt, decrypt } from "./utils/cryptoUtils";

const useForceUpdate = () => {
  const [, setState] = useState();
  return () => setState({});
}

function Message({contract, accounts, matchedUser, onClose}) {
  const [messages, setMessages] = useState([]);
  const [secret, setSecret] = useState("");
  const [conversationId, setConversationId] = useState(0);
  const messagesRef = useRef(messages);

  useEffect(() => {
    const conversationIdCopy = parseInt(accounts[0].substring(0, 10)) + parseInt(matchedUser.userAddress.substring(0, 10));
    setSecret(computeSecret(localStorage.getItem('privateKey'), matchedUser.userPublicKey));
    setConversationId(conversationIdCopy);
    console.log("ID:: ", parseInt(accounts[0].substring(0, 10)) + parseInt(matchedUser.userAddress.substring(0, 10)));
    eventListener(conversationIdCopy);
  }, [accounts, matchedUser]);

  const eventListener = (IdFilter) => {
    contract.events.NewMessage({filter: {conversationId: conversationId}, fromBlock: 0}).on('data', events => {
      if(String(IdFilter) === events.returnValues.conversationId) {
        refUpdate(events);
      }
    });
  }

  const forceUpdate = useForceUpdate();

  const refUpdate = (events) => {
    let refCopy = messagesRef.current;
    refCopy.push(events.returnValues);
    messagesRef.current = refCopy;
    setMessages(refCopy);
    console.log("Event is being updated");
    console.log("Event::: ", events.returnValues);
    forceUpdate(); 
  }

  const submitSendHandle = async (e) => {
    e.preventDefault();
    const message = e.target.elements[0].value;
    const encryptedMessage = encrypt(message, secret);
    await contract.methods.sendMessage(matchedUser.userAddress, encryptedMessage, conversationId).send({from: accounts[0]});
    document.getElementById("textBox").value="";
  }

  const decryptingMessage = (_content) => {
    try {
      return decrypt(_content, secret);
    } catch(error) {
      return _content;
    }
  }

  return (
    <div className='messenger'>
          <button className="closeButton" onClick={onClose}>x</button>
      <div className="messengerBox">
        <p style={{
          fontWeight: 'bold', 
          display: "flex"}}>
        <i><u>Talking To {matchedUser.name}... </u></i></p>
        {
        messages.length > 0 && (
          messages.map((message, idx) => (
            message.from.toLowerCase() === accounts[0].toLowerCase() ? (
              <p key={accounts[0] + idx} style={{textAlign: "right", padding: "10px"}}><b>Me</b>: {decryptingMessage(message.content)}<br/><small>({new Date(parseInt(message.date) * 1000).toLocaleString()})</small></p>
            ) : (
              <p key={matchedUser.userAddress + idx} style={{textAlign: "left", padding: "10px"}}><b>{matchedUser.name}</b>: {decryptingMessage(message.content)}<br/><small>({new Date(parseInt(message.date) * 1000).toLocaleString()})</small></p> 
            )
          ))
        )
        }
      </div>
      <form onSubmit={e => submitSendHandle(e)}>
          <textarea id="textBox" className="textBox"></textarea>
          <button className="sendButton">Send</button>
      </form>
    </div>
  );
}

export default Message;