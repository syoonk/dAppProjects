const crypto = require('crypto');

const algorithm = 'aes192';

const privateToPublic = (_privateKey) => {
  if(_privateKey.startsWith('0x')) {
    _privateKey = _privateKey.substr(2);
  }
  let publicKey = crypto.createECDH('secp256k1');
  publicKey.setPrivateKey(_privateKey, 'hex');
  return publicKey.getPublicKey('hex');
}

const computeSecret = (_privateKeyA, _publicKeyB) => {
  if(_privateKeyA.startsWith('0x')) {
    _privateKeyA = _privateKeyA.substr(2);
  }
  let G = crypto.createECDH('secp256k1');
  G.setPrivateKey(_privateKeyA, 'hex');
  return G.computeSecret(_publicKeyB, 'hex', 'hex').slice(1);
}

const encrypt = (_message, _secret) => {
  let cipher = crypto.createCipher(algorithm, _secret);
  let crypted = cipher.update(_message, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

const decrypt = (_encryptedMessage, _secret) => {
  let decipher = crypto.createDecipher(algorithm, _secret);
  let dec = decipher.update(_encryptedMessage, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}

module.exports = {privateToPublic, computeSecret, encrypt, decrypt};