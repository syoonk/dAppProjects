import React, { useEffect, useState } from 'react';

import './css/App.css';
import Swipe from "./Swipe.js";
import User from "./User.js";
import EthCrypto from "eth-crypto";
import { privateToPublic } from "./utils/cryptoUtils";

const Gender = {
  "male": 0,
  "female": 1
}

function App({web3, accounts, contract}) {
  const [userInfo, setUserInfo] = useState(null);
  const [userPublicKey, setUserPublicKey] = useState("");

  useEffect(() => {
    checkingPrivateKeyValidation();
    getUserInfo();
  }, [accounts, userInfo]);
  
  const getUserInfo = async () => {
    try {
      const userInfoCopy = await contract.methods.getUserInfo().call({from: accounts[0]});
      setUserInfo(userInfoCopy);
    } catch(e) {
      setUserInfo(null);
    }
  }

  // Since alert message does not need in this part.
  const checkingPrivateKeyValidation = () => {
    const privateKeyCopy = localStorage.getItem("privateKey");
    try {
      const publicKeyCopy = privateToPublic(privateKeyCopy);      
      if(EthCrypto.publicKey.toAddress(publicKeyCopy).toLowerCase() !== accounts[0].toLowerCase()) {
        localStorage.removeItem("privateKey");
        return ;
      }
      setUserPublicKey(publicKeyCopy);
    } catch(error) {
      localStorage.removeItem("privateKey");
      return ;
    }
  }

  /*
    In real world, wallet private key must not be used either exposed.
  */
  const submitRegisterPrivateKeyHandle = (e) => {
    e.preventDefault();
    const privateKeyCopy = e.target.elements[0].value.trim();
    try {
      const publicKeyCopy = privateToPublic(privateKeyCopy);
      if(EthCrypto.publicKey.toAddress(publicKeyCopy).toLowerCase() !== accounts[0].toLowerCase()) {
        alert("Private Key is not matched with your address");
        return ;
      }
      localStorage.setItem('privateKey', privateKeyCopy);
      setUserPublicKey(publicKeyCopy);
    } catch(error) {
      alert("Private Key invalid");
    }
  }

  const submitRegisterHandle = async (e) => {
    e.preventDefault();
    // Check validation
    for(let i = 0; i < 5; i++) {
      const element = e.target.elements[i];
      console.log(i, element.value);
      if(!element.value) {
        alert(`Fill all elements`);
        return ;
      }
      if(i === 3 && element.value < 18) {
        alert(`Age should be superior to 17`);
        return ;
      }
    }

    const [name, city, gender, age, picUrl, publicKey] = [
      e.target.elements[0].value,
      e.target.elements[1].value,
      Gender[e.target.elements[2].value],
      e.target.elements[3].value,
      e.target.elements[4].value,
      userPublicKey
    ]
    console.log("name: ", name, ", city: ", city, ", gender: ", gender, ", age: ", age, ", picUrl: ", picUrl);
    await contract.methods.register(
      name,
      city,
      gender,
      age, 
      picUrl,
      publicKey).send({from: accounts[0]});

    window.location.reload();
  }

  return (
    !localStorage.getItem(`privateKey`) ? (<div style={{textAlign: "center"}}>
      <h2>Login with your private key</h2>
      <form onSubmit={e => submitRegisterPrivateKeyHandle(e)}>
        <input style={{width: "300px"}} placeholder="Your Private Key here"></input>
        <button style={{marginLeft: "15px"}}>Login</button>
      </form>
    </div>) :
    ( 
    <div className="App">
      { 
      userInfo ? (
        <div>
          <Swipe 
            web3={web3}
            accounts={accounts}
            contract={contract}>
          </Swipe>
          <details style={{textAlign: "left"}}>
            <summary style={{outline: "none", cursor: "Pointer"}}><b>About You</b></summary>
            <User
              accounts={accounts}
              contract={contract}
              user={userInfo}>
            </User>
          </details>
        </div>
      ) : (
        <div className="registerBox" style={{align: "center"}}>
          <h2>Register NOW!</h2>
          <form onSubmit={e => {submitRegisterHandle(e)}}>
            <input placeholder="Your Name"></input><br/>
            <select name="city">
              <option style={{fontWeight: "bold"}} value="">Select City</option>
              <option value="Area1">Area1</option>
              <option value="Area2">Area2</option>
            </select><br/>
            <select name="gender">
              <option style={{fontWeight: "bold"}} value="">Select Gender</option>
              <option>male</option>
              <option>female</option>
            </select><br/>
            <input type="number" placeholder="Age"></input><br/>
            <input placeholder="Your Picture URL"></input><br/>
            <button className="registerButton">Register</button>
          </form>
        </div>
      )
      }
    </div> 
    )
  );
}

export default App;
