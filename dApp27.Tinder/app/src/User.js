import React, { useState, useEffect } from "react";

import "./css/User.css";

function User({contract, accounts, user}) {
  const [expandCity, setExpandCity] = useState(false);
  const [expandPic, setExpandPic] = useState(false);
  const [swipeSession, setSwipeSession] = useState("Loading");

  useEffect(() => {
    getSwipeSession();
  }, []);

  const getSwipeSession = async () => {
    const sessionCountCopy = await contract.methods.getSwipeSession().call({from: accounts[0]});
    setSwipeSession(sessionCountCopy);
  }

  const clickCityHandle = (e) => {
    e.preventDefault(e);
    const expandCityCopy = !expandCity;
    setExpandCity(expandCityCopy);
  }

  const clickPicHandle = (e) => {
    e.preventDefault(e);
    const expandPicCopy = !expandPic;
    setExpandPic(expandPicCopy);
  }

  const submitChangeCityHandle = async (e) => {
    e.preventDefault();
    const newCity = e.target.elements[0].value;
    await contract.methods.changeCity(newCity).send({from: accounts[0]});

    window.location.reload();
  }

  const submitChangePicHandle = async (e) => {
    e.preventDefault();
    const newPicUrl = e.target.elements[0].value;
    await contract.methods.changePicture(newPicUrl).send({from: accounts[0]});

    window.location.reload();
  }

  return (
    <div className="User" style={{marginTop: "15px"}}>
      <img src={user.picUrl} className="userImage"></img>
      <ul style={{lineHeight: "2em"}}>
        <li><b>Name</b>: {user.name}</li>
        <li><b>Gender</b>: {Number(user.gender) ? "female" : "male"}</li>
        <li><b>Age</b>: {user.age}</li>
        <li 
          onClick={e => clickCityHandle(e)} 
          style={{cursor: "pointer"}}
          title={expandCity ? "close" : "click to change city"}>
        <b>City</b>: {user.city}</li>
        {
        expandCity && (<form onSubmit={e => submitChangeCityHandle(e)}>
          <input placeholder="new city"></input>
          <button className="submitButton">submit</button>
        </form>)
        }
        <li 
          onClick={e => clickPicHandle(e)}
          style={{cursor: "pointer"}} 
          title={expandPic ? "close" : "click to change picture"}>
        <b>Picture</b>: <i>{user.picUrl}</i></li>
        {
        expandPic && (<form onSubmit={e => submitChangePicHandle(e)}>
          <input placeholder="new pic url"></input>
          <button className="submitButton">submit</button>
        </form>)
        }
        <li><b>Choice Remains Today</b>: {3 - Number(swipeSession.count)}</li>
      </ul>
    </div>
  )
}

export default User;