const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");
const Tinder = artifacts.require("Tinder");

contract("Tinder", accounts => {
  let tinder = null;
  const users = [accounts[0], accounts[1], accounts[2], accounts[3]];
  const names = ["user0", "user1", "user2", "user3"];
  const areas = ["area1", "area2", "area1", "area2"];
  const Gender = { MALE: 0, FEMALE: 1 };
  const genders = [Gender.MALE, Gender.FEMALE, Gender.MALE, Gender.FEMALE];
  const ages = [18, 20, 24, 26];
  const urls = ["pic1", "pic2", "pic3", "pic4"];
  const SwipeStatus = { UNKNOWN: 0, LIKE: 1, DISLIKE: 2 };

  beforeEach(async () => {
    tinder = await Tinder.new();
    for(let i = 0; i < 4; i++) {
      await tinder.register(
        names[i], 
        areas[i],
        genders[i],
        ages[i],
        urls[i],
        {from: accounts[i]});
    }
  });
  
  it("should NOT register if not fit requiremnet", async () => {
    // already registered
    await expectRevert(
      tinder.register(
        names[0],
        areas[0],
        genders[0],
        ages[0],
        urls[0],
        {from: users[0]}),
      "User already registered");
    // empty name
    await expectRevert(
      tinder.register(
        "",
        areas[0],
        genders[0],
        ages[0],
        urls[0],
        {from: accounts[5]}),
      "_name cannot be empty");
    // empty city
    await expectRevert(
      tinder.register(
        names[0],
        "",
        genders[0],
        ages[0],
        urls[0],
        {from: accounts[5]}),
      "_city cannot be empty");
    // younger age
    await expectRevert(
      tinder.register(
        names[0],
        areas[0],
        genders[0],
        17,
        urls[0],
        {from: accounts[5]}),
      "_age must be 18 or above");
    // empty url
    await expectRevert(
      tinder.register(
        names[0],
        areas[0],
        genders[0],
        ages[0],
        "",
        {from: accounts[5]}),
      "_picUrl cannot be empty");
  });

  it("should register", async () => {
    let userInfos = [];
    for(let i = 0; i < 4; i++) {
      userInfos[i] = await tinder.getUserInfo({from: accounts[i]});
    }
    const userAddresses = await tinder.getUsers();
    const area1Male = await tinder.getUsersByCity("area1", Gender.MALE);
    const area1Female = await tinder.getUsersByCity("area1", Gender.FEMALE);
    const area2Male = await tinder.getUsersByCity("area2", Gender.MALE);
    const area2Female = await tinder.getUsersByCity("area2", Gender.FEMALE);
    
    assert.equal(area1Male.length, 2);
    assert.equal(area1Male[0], users[0]);
    assert.equal(area1Male[1], users[2]);
    assert.equal(area1Female.length, 0);
    assert.equal(area2Male.length, 0);
    assert.equal(area2Female.length, 2);
    assert.equal(area2Female[0], users[1]);
    assert.equal(area2Female[1], users[3]);
    assert.equal(userInfos.length, 4);
    assert.equal(userAddresses.length, 4);
    for(let i = 0; i < 4; i++) {
      assert.equal(userAddresses[i], users[i]);
      assert.equal(userInfos[i].name, names[i]);
      assert.equal(userInfos[i].city, areas[i]);
      assert.equal(userInfos[i].gender, genders[i]);
      assert.equal(userInfos[i].age, ages[i]);
      assert.equal(userInfos[i].picUrl, urls[i]);
    }
  });

  it("should NOT change city", async () => {
    await expectRevert(
      tinder.changeCity("area1", {from: users[0]}),
      "cannot change to current city");
    await expectRevert(
      tinder.changeCity("area2", {from: accounts[9]}),
      "not registered");
  });

  it("should change city", async () => {
    await tinder.changeCity("area2", {from: users[0]});
    const user0Info = await tinder.getUserInfo({from: users[0]});
    const area2MaleUsers = await tinder.getUsersByCity("area2", Gender.MALE);
    const area1MaleUsers = await tinder.getUsersByCity("area1", Gender.MALE);

    assert.equal(area1MaleUsers.length, 1);
    assert.equal(area2MaleUsers.length, 1);
    assert.equal(area2MaleUsers[0], users[0]);
    assert.equal(user0Info.city, "area2");
  });

  it("should get same area matchable users", async () => {
    const add_names = [0, 0, 0, 0, "user4", "user5", "user6", "user7"];
    for(let i = 4; i < 8; i++) {
      await tinder.register(
        add_names[i], 
        areas[i % 2],
        genders[(i + 1) % 2],
        ages[i % 2],
        urls[i % 2],
        {from: accounts[i]});
    }
    // area1 : MALE(0, 2) FEMALE(4, 6)
    // area2 : MALE(5, 7) FEMALE(1, 3)
    const user0MatchableUsers = await tinder.getMatchableUsers({from: users[0]});
    const user1MatchableUsers = await tinder.getMatchableUsers({from: users[1]});

    assert.equal(user0MatchableUsers.length, 2);
    // User0 should get female users 4, 6
    assert.equal(user0MatchableUsers[0].name, "user4");
    assert.equal(user0MatchableUsers[0].city, "area1");
    assert.equal(user0MatchableUsers[0].gender, Gender.FEMALE);
    assert.equal(user0MatchableUsers[0].age, ages[4 % 2]);
    assert.equal(user0MatchableUsers[0].picUrl, urls[4 % 2]);
    assert.equal(user0MatchableUsers[1].name, "user6");
    assert.equal(user0MatchableUsers[1].city, "area1");
    assert.equal(user0MatchableUsers[1].gender, Gender.FEMALE);
    assert.equal(user0MatchableUsers[1].age, ages[6 % 2]);
    assert.equal(user0MatchableUsers[1].picUrl, urls[6 % 2]);
    // User1 should get male users 5, 7
    assert.equal(user1MatchableUsers[0].name, "user5");
    assert.equal(user1MatchableUsers[0].city, "area2");
    assert.equal(user1MatchableUsers[0].gender, Gender.MALE);
    assert.equal(user1MatchableUsers[0].age, ages[5 % 2]);
    assert.equal(user1MatchableUsers[0].picUrl, urls[5 % 2]);
    assert.equal(user1MatchableUsers[1].name, "user7");
    assert.equal(user1MatchableUsers[1].city, "area2");
    assert.equal(user1MatchableUsers[1].gender, Gender.MALE);
    assert.equal(user1MatchableUsers[1].age, ages[7 % 2]);
    assert.equal(user1MatchableUsers[1].picUrl, urls[7 % 2]);
  });

  it("should NOT swipe if user does not exist", async() => {
    await expectRevert(
      tinder.swipe(SwipeStatus.LIKE, users[0], {from: accounts[9]}),
      "User is not registered");
    await expectRevert(
      tinder.swipe(SwipeStatus.LIKE, accounts[9], {from: users[0]}),
      "User is not registered");
  });

  it("should swipe", async () => {
    // users0 matching with users1, users0 dislike users3
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.changeCity("area1", {from: users[3]});
    await tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]});
    const fisrtSwipeMatchableUsers_user0 = await tinder.getMatchableUsers({from: users[0]});

    assert.equal(fisrtSwipeMatchableUsers_user0.length, 1);

    await tinder.swipe(SwipeStatus.DISLIKE, users[3], {from: users[0]});
    const secondSwipeMatchableUsers_user0 = await tinder.getMatchableUsers({from: users[0]});

    assert.equal(secondSwipeMatchableUsers_user0.length, 0);

    const likeRecipe = await tinder.swipe(SwipeStatus.LIKE, users[0], {from: users[1]});

    expectEvent(likeRecipe, "NewMatch", {
      from: users[1],
      to: users[0] });
  });

  it("should NOT swipe same user twice", async() => {
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]});

    await expectRevert(
      tinder.swipe(SwipeStatus.DISLIKE, users[1], {from: users[0]}),
      "Cannot swipe same person twice");
  });

  it("should NOT swipe more than swipe session" , async() => {
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.changeCity("area1", {from: users[3]});
    await tinder.register(
      "user4",
      "area1",
      Gender.FEMALE,
      20,
      "picUrl4",
      {from: accounts[4]} );
    await tinder.register(
      "user5",
      "area1",
      Gender.FEMALE,
      24,
      "picUrl5",
      {from: accounts[5]} );
    for(let i = 3; i < 6; i++) {
      await tinder.swipe(SwipeStatus.LIKE, accounts[i], {from: users[0]});
    }

    await expectRevert(
      tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]}),
      "You have already used up all your swipes for today");
  });

  it("should reset swipe session after a day passed", async () => {
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.changeCity("area1", {from: users[3]});
    await tinder.register(
      "user4",
      "area1",
      Gender.FEMALE,
      20,
      "picUrl4",
      {from: accounts[4]} );
    await tinder.register(
      "user5",
      "area1",
      Gender.FEMALE,
      24,
      "picUrl5",
      {from: accounts[5]} );
    for(let i = 3; i < 6; i++) {
      await tinder.swipe(SwipeStatus.LIKE, accounts[i], {from: users[0]});
    }
    time.increase(86401);
    await tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]});
  });

  it("should NOT send message if not like each other", async () => {
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]});
    
    await expectRevert(
      tinder.sendMessage(users[1], "What the heck?"),
      "both users need to have liked each other to send message");
  });

  it("should send message", async () => {
    await tinder.changeCity("area1", {from: users[1]});
    await tinder.swipe(SwipeStatus.LIKE, users[1], {from: users[0]});
    const matched = await tinder.swipe(SwipeStatus.LIKE, users[0], {from: users[1]});

    expectEvent(matched, "NewMatch", {
      from: users[1],
      to: users[0] });

    const user0SendMessage = await tinder.sendMessage(users[1], "who are you" , {from: users[0]});
    const user1SendMessage = await tinder.sendMessage(users[0], "get away from me", {from: users[1]});

    expectEvent(user0SendMessage, "NewMessage", {
      from: users[0],
      to: users[1],
      content: "who are you" });
    expectEvent(user1SendMessage, "NewMessage", {
      from: users[1],
      to: users[0],
      content: "get away from me" });
  });
});