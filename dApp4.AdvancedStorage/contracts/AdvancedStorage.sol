pragma solidity ^0.5.0;

contract AdvancedStorage {
    uint[] public ids;
    function add(uint _id) public {
        ids.push(_id);
    }

    function get(uint _position) public view returns(uint) {
        return ids[_position];
    }

    function getAll() public view returns(uint[] memory) {
        return ids;
    }

    function length() public view returns(uint) {
        return ids.length;
    }
}