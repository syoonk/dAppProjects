import Web3 from 'web3';
import AdvancedStorage from '../build/contracts/AdvancedStorage.json';

let web3;
let advancedStorage;

const initWeb3 = () => {
    return new Promise((resolve, reject) => {
        //How we creating Web3 instance
        //Case 1: New Metamask present
        if (typeof window.ethereum !== 'undefined') {
            //New Metamask's privacy inhanced, it asked users whether they will connect to it.
            window.ethereum.enable()
                .then(() => {
                    resolve(new Web3(window.ethereum));
                })
                .catch(e => {
                    reject(e)
                });

            return;
        }    
        //Case 2: Old Metamask present
        if(typeof window.web3 !== 'undefined') {
            //injecting provider
            return resolve(new Web3(window.web3.currentProvider));
        }
        //Case 3: There is no metamask, just connect to ganache
        resolve(new Web3('http://localhost:9545'));
    });
};

const initContract = () => {
    const deploymentKey = Object.keys(AdvancedStorage.networks)[0]; // it takes 5777, which is first index value of networks.
    return new web3.eth.Contract(
        AdvancedStorage.abi,
        AdvancedStorage.networks[deploymentKey].address
    );
};

const initApp = () => {
    const $addData = document.getElementById('addData');
    const $data = document.getElementById('data');
    let accounts = [];

    web3.eth.getAccounts()
    .then(_accounts => {
        accounts = _accounts;
        return advancedStorage.methods.getAll().call();
    })
    .then(result => {
        $data.innerHTML = result.join(', ');
    });

    $addData.addEventListener('submit', e => {
        e.preventDefault();
        const data = e.target.elements[0].value;
        advancedStorage.methods.add(data).send({from:accounts[0]})
        .then(result => {
            return advancedStorage.methods.getAll().call();
        })
        .then(result => {
            $data.innerHTML = result.join(', ');            
        });
    });
};

document.addEventListener('DOMContentLoaded', () => {
    initWeb3()
        .then(_web3 => {
            web3 = _web3
            advancedStorage = initContract();
            initApp();
        })
        .catch(e => console.log(e.message));
});