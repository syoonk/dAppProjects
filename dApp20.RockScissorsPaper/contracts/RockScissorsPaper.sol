pragma solidity >= 0.5.0 < 0.7.0;

contract RockScissorsPaper {
    enum State {
        IDLE,
        CREATED,
        JOINED,
        COMMITED,
        REVEALED
    }
    struct Game {
        uint id;
        uint bet;
        address payable[2] players;
        State state;
    }
    struct Move {
        bytes32 hash;
        uint value;
    }
    mapping(uint => Game) public games;
    mapping(uint => mapping(address => Move)) public moves;
    mapping(uint => uint) public winningMoves;
    uint public gameId;
    
    constructor() public {
        winningMoves[1] = 2;
        winningMoves[2] = 3;
        winningMoves[3] = 1;
    }
    
    function getGame(uint _gameId)
    external view
    returns(uint, uint, address[] memory, State) {
        Game storage game = games[_gameId];
        address[] memory _players = new address[](2);
        _players[0] = game.players[0];
        _players[1] = game.players[1];
        return (
            game.id,
            game.bet,
            _players,
            game.state
        );
    }

    function createGame(address payable _participant)
    external payable {
        require(msg.sender != _participant, 
            "participant should not same with creator");
        require(msg.value > 0, 
            'need to send some ether');
        
        address payable[2] memory players;
        players[0] = msg.sender;
        players[1] = _participant;
        games[gameId] = Game(
            gameId,
            msg.value,
            players,
            State.CREATED
        );
        gameId++;
    }
    
    function joinGame(uint _gameId)
    external payable {
        Game storage game = games[_gameId];
        require(game.state == State.CREATED, 
            'must be in CREATED state');
        require(game.players[1] == msg.sender, 
            "sender must be second player");
        require(msg.value == game.bet, 
            "should bet same amount");
        
        game.state = State.JOINED;
    }
    
    function commitMove(uint _gameId, uint _moveId, uint _salt)
    external {
        Game storage game = games[_gameId];
        require(game.state == State.JOINED, 
            "game must be in JOINED state");
        require(game.players[0] == msg.sender || game.players[1] == msg.sender, 
            "can only be called by one of the player");
        require(moves[_gameId][msg.sender].hash == 0, 
            "move already made");
        require(_moveId == 1 || _moveId == 2 || _moveId == 3, 
            "move must be either 1, 2, or 3");
        
        moves[_gameId][msg.sender] = Move(keccak256(abi.encodePacked(_moveId, _salt)), 0);
        if(moves[_gameId][game.players[0]].hash != 0 
        && moves[_gameId][game.players[1]].hash != 0) {
            game.state = State.COMMITED;
        }
    }
    
    function revealMove(uint _gameId, uint _moveId, uint _salt)
    external {
        Game storage game = games[_gameId];
        Move storage move1 = moves[_gameId][game.players[0]];
        Move storage move2 = moves[_gameId][game.players[1]];
        Move storage moveSender = moves[_gameId][msg.sender];
        require(game.state == State.COMMITED,
            "game must be in COMMITED state");
        require(game.players[0] == msg.sender || game.players[1] == msg.sender, 
            "can only be called by one of the player");
        require(moveSender.hash == keccak256(abi.encodePacked(_moveId, _salt)),
            "moveId does not match commitment");
        
        moveSender.value = _moveId;
        if(move1.value != 0 && move2.value != 0) {
            if(move1.value == move2.value) {
                game.players[0].transfer(game.bet);
                game.players[1].transfer(game.bet);
                game.state = State.REVEALED;
                return;
            }
            address payable winner;
            winner = winningMoves[move1.value] == move2.value ? game.players[0] : game.players[1];
            winner.transfer(2 * game.bet);
            game.state = State.REVEALED;
        }
    }
}