const { expectRevert, time } = require("@openzeppelin/test-helpers");
const RockScissorsPaper = artifacts.require("RockScissorsPaper");

contract("RockScissorsPaper", accounts => {
  let contract;
  const [player1, player2] = [accounts[1], accounts[2]];
  const [salt1, salt2] = [10, 20];
  const [rock, scissors, paper] = [1, 2, 3];
  beforeEach(async () => {
    contract = await RockScissorsPaper.new();
  });

  it("Should NOT create game if not ether sent", async () => {
    await expectRevert(
      contract.createGame(player2, { from: player1 }),
      "need to send some ether"
    );
  });

  it("should NOT create if creator is participant", async () => {
    await expectRevert(
      contract.createGame(player1, { from: player1, value: 100 }),
      "participant should not same with creator"
    );
  });

  it("Should create game", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
  });

  it("Should NOT join game if not second player", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    
    await expectRevert(
      contract.joinGame(0, { from: accounts[3], value: 100 }),
      "sender must be second player"
    );
  });

  it("Should NOT join game if not enough ether sent", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    
    await expectRevert(
      contract.joinGame(0, { from: player2, value: 50 }),
      "should bet same amount"
    );
  });

  it("Should NOT join game if not in CREATED state", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });

    await expectRevert(
      contract.joinGame(0, { from: player2, value: 100 }),
      "must be in CREATED state"
    );
  });

  it("Should NOT commit move if game not in JOINED state", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });

    await expectRevert(
      contract.commitMove(0, rock, salt1, { from: player1 }),
      "game must be in JOINED state"
    );
  });

  it("Should NOT commit move if not called by player", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });

    await expectRevert(
      contract.commitMove(0, rock, salt1, { from: accounts[3] }),
      "can only be called by one of the player"
    );
  });

  it("Should NOT commit move if move already made", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });
    await contract.commitMove(0, rock, salt1, { from: player1 });

    await expectRevert(
      contract.commitMove(0, scissors, salt1, { from: player1 }),
      "move already made"
    );
  });

  it("Should NOT commit move if non-existing move", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });

    await expectRevert(
      contract.commitMove(0, 4, salt1, { from: player1 }),
      "move must be either 1, 2, or 3"
    );
  });

  it("Should NOT reveal move if not in state COMMITED", async () => {
    // state CREATED
    await contract.createGame(player2, { from: player1, value: 100 });

    await expectRevert(
      contract.revealMove(0, rock, salt1, { from: player1 }),
      "must be in COMMITED state"
    );
  });

  it("Should NOT reveal move if not in state COMMITED", async () => {
    // state JOINED
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });
    await contract.commitMove(0, rock, salt1, { from: player1 });

    await expectRevert(
      contract.revealMove(0, rock, salt1, { from: player1 }),
      "game must be in COMMITED state"
    );
  });

  it("Should NOT reveal move if not in state COMMITED", async () => {
    // REVEALED
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });
    await contract.commitMove(0, rock, salt1, { from: player1 });
    await contract.commitMove(0, paper, salt2, { from: player2 });
    await contract.revealMove(0, rock, salt1, { from: player1 });
    await contract.revealMove(0, paper, salt2, { from: player2 });

    await expectRevert(
      contract.revealMove(0, rock, salt1, { from: player1 }),
      "game must be in COMMITED state"
    );
  });

  it("Should NOT reveal move if moveId does not match commitment", async () => {
    await contract.createGame(player2, { from: player1, value: 100 });
    await contract.joinGame(0, { from: player2, value: 100 });
    await contract.commitMove(0, rock, salt1, { from: player1 });
    await contract.commitMove(0, paper, salt2, { from: player2 });

    await expectRevert(
      contract.revealMove(0, scissors, salt1, { from: player1 }),
      "moveId does not match commitment"
    );
  });

  it("Full winning game", async () => {
    const bet = web3.utils.toBN(web3.utils.toWei(`1`, `ether`));
    const beforeBalanceP1 = web3.utils.toBN(await web3.eth.getBalance(player1));
    const beforeBalanceP2 = web3.utils.toBN(await web3.eth.getBalance(player2));
    const txCreate = await contract.createGame(player2, { from: player1, value: bet, gasPrice: 1 });
    const txJoin = await contract.joinGame(0, { from: player2, value: bet, gasPrice: 1 });
    const txCommitP1 = await contract.commitMove(0, rock, salt1, { from: player1, gasPrice: 1 });
    const txCommitP2 = await contract.commitMove(0, paper, salt2, { from: player2, gasPrice: 1 });
    const txRevealP1 = await contract.revealMove(0, rock, salt1, { from: player1, gasPrice: 1 });
    const txRevealP2 = await contract.revealMove(0, paper, salt2, { from: player2, gasPrice: 1 });
    // Player2 is winner
    const afterBalanceP1 = web3.utils.toBN(await web3.eth.getBalance(player1));
    const afterBalanceP2 = web3.utils.toBN(await web3.eth.getBalance(player2));

    assert(afterBalanceP1.eq(
        beforeBalanceP1
        .sub(bet)
        .sub(web3.utils.toBN(txCreate.receipt.gasUsed))
        .sub(web3.utils.toBN(txCommitP1.receipt.gasUsed))
        .sub(web3.utils.toBN(txRevealP1.receipt.gasUsed))
      )
    );

    assert(afterBalanceP2.eq(
        beforeBalanceP2
        .add(bet)
        .sub(web3.utils.toBN(txJoin.receipt.gasUsed))
        .sub(web3.utils.toBN(txCommitP2.receipt.gasUsed))
        .sub(web3.utils.toBN(txRevealP2.receipt.gasUsed))
      )
    );
  });

  it("Full draw game", async () => {
    const bet = web3.utils.toBN(web3.utils.toWei(`1`, `ether`));
    const beforeBalanceP1 = web3.utils.toBN(await web3.eth.getBalance(player1));
    const beforeBalanceP2 = web3.utils.toBN(await web3.eth.getBalance(player2));
    const txCreate = await contract.createGame(player2, {
      from: player1,
      value: bet,
      gasPrice: 1
    });
    const txJoin = await contract.joinGame(0, {
      from: player2,
      value: bet,
      gasPrice: 1
    });
    const txCommitP1 = await contract.commitMove(0, rock, salt1, {
      from: player1,
      gasPrice: 1
    });
    const txCommitP2 = await contract.commitMove(0, rock, salt2, {
      from: player2,
      gasPrice: 1
    });
    const txRevealP1 = await contract.revealMove(0, rock, salt1, {
      from: player1,
      gasPrice: 1
    });
    const txRevealP2 = await contract.revealMove(0, rock, salt2, {
      from: player2,
      gasPrice: 1
    });
    const afterBalanceP1 = web3.utils.toBN(await web3.eth.getBalance(player1));
    const afterBalanceP2 = web3.utils.toBN(await web3.eth.getBalance(player2));
    
    assert(afterBalanceP1.eq(
        beforeBalanceP1
        .sub(web3.utils.toBN(txCreate.receipt.gasUsed))
        .sub(web3.utils.toBN(txCommitP1.receipt.gasUsed))
        .sub(web3.utils.toBN(txRevealP1.receipt.gasUsed))
      )
    );
    assert(afterBalanceP2.eq(
        beforeBalanceP2
        .sub(web3.utils.toBN(txJoin.receipt.gasUsed))
        .sub(web3.utils.toBN(txCommitP2.receipt.gasUsed))
        .sub(web3.utils.toBN(txRevealP2.receipt.gasUsed))
      )
    );
  });
});
