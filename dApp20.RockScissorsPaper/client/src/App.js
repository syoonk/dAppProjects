import React, { useEffect, useState } from "react";
import RockScissorsPaper from "./contracts/RockScissorsPaper.json";
import Web3 from "web3";

const states = ["IDLE", "CREATED", "JOINED", "COMMITED", "REVEALED"];
const initPlayer = {
  address: undefined,
  moveId: 0,
  salt: undefined,
  commited: false,
  revealed: false
};
const initGame = {
  id: undefined,
  bet: undefined,
  players: [],
  state: '0'
};
const winningMoves = [0, 2, 3, 1];

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState([]);
  const [contract, setContract] = useState(undefined);
  const [game, setGame] = useState(initGame);
  const [player1, setPlayer1] = useState(initPlayer);
  const [player2, setPlayer2] = useState(initPlayer);

  useEffect(() => {
    const init = async () => {
      let web3;
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = RockScissorsPaper.networks[networkId];
      const contract = new web3.eth.Contract(
        RockScissorsPaper.abi,
        deployedNetwork && deployedNetwork.address
      );
      setWeb3(web3);
      setContract(contract);
    };
    init();
    window.ethereum.on("accountsChanged", accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined"
    );
  };

  const isPlayer1 = () => {
    return (accounts[0].toLowerCase() === player1.address.toLowerCase());
  };

  const cleanUp = () => {
    setGame(initGame);
    setPlayer1(initPlayer);
    setPlayer2(initPlayer);
  };

  useEffect(() => {
    if (isReady()) {
      updateGame();
    }
  }, [accounts, contract, web3]);

  async function updateGame() {
    const gameId = await contract.methods.gameId().call();
    console.log(gameId)
    if(!gameId) {
      return ;
    }
    const _game = await contract.methods.getGame(gameId - 1).call();

    setGame({
      id: _game[0], 
      bet: _game[1], 
      players: _game[2], 
      state: _game[3]
    });

    console.log("dfawfdwadf", game.id, game.bet, game.players, game.state);
  }

  async function submitCreateGameHandle(e) {
    e.preventDefault();
    const _participant = e.target.elements[0].value;
    const _bet = e.target.elements[1].value;
    console.log(_participant, _bet);
    await contract.methods.createGame(_participant)
    .send({
      from: accounts[0],
      value: parseInt(_bet)
    });

    setPlayer1({address: accounts[0]});
    updateGame();
  }

  async function clickJoinGameHandle() {
    await contract.methods.joinGame(game.id)
    .send({
      from: accounts[0],
      value: web3.utils.toBN(game.bet).toString()
    });

    setPlayer2({address: accounts[0]});
    updateGame();
  }

  async function submitCommitMoveHandle(e) {
    e.preventDefault();
    // const _moveId = e.select.options[select.selectedIndex].value;
    const _moveId = e.target.value;
    const _salt = Math.floor(Math.random() * 1000);
    await contract.methods.commitMove(
      game.id,
      _moveId,
      _salt
    ).send({from: accounts[0]});

    isPlayer1() ? (
      setPlayer1({moveId: _moveId, salt: _salt, commited: true})
    ) : (
      setPlayer2({moveId: _moveId, salt: _salt, commited: true})
    );
    updateGame();
  }

  async function clickRevealMoveHandle(e) {
    e.preventDefault();
    const [_moveId, _salt] = isPlayer1() ? (
      [player1.moveId, player1.salt]
    ) : (
      [player2.moveId, player2.salt]
    );

    await contract.methods.revealMove(game.id, _moveId, _salt)
    .send({from: accounts[0]})
    
    isPlayer1() ? setPlayer1({revealed: true}) : setPlayer2({revealed: true});
    updateGame();

    if(game.state === '4') {
      if(player1.moveId === player2.moveId) {
        alert("Draw");
        cleanUp();
      } else {
        winningMoves[player1.moveId] === player2.moveId ? (
          alert(`${player1.address} win!`)
        ) : (
          alert(`${player2.address} win!`)
        );
        cleanUp();
      }
    }             
  }

  if (typeof game.state === "undefined") {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">Rock Scissors Paper</h1>

      <p>Current User: {accounts[0]}</p>
      <p>State: {states[game.state]}</p>
      <p>Bet: {game.bet}</p>
      <div>
        <h2>Players</h2>
        <ul>
          {game.players.map(player => (
            <li key={player}>{player}</li>
          ))}
        </ul>
      </div>

      { game.state === '0' ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Create Game</h2>
          <form onSubmit={e => submitCreateGameHandle(e)}>
            <div className="form-group">
              <label htmlFor="participant">Participant</label>
              <input type="text" className="form-control" id="participant" />
            </div>
            <div className="form-group">
              <label htmlFor="bet">Bet</label>
              <input type="number" className="form-control" id="bet" />
            </div>
            <button type="submit" className="btn btn-primary">
              Create Game
            </button>
          </form>
        </div>
      </div> ) : (null)
      }
      { game.state === '1' 
      && accounts[0].toLowerCase() === game.player[1].toLowerCase() ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Bet</h2>
          <button
            onClick={e => clickJoinGameHandle()}
            type="submit"
            className="btn btn-primary"
          >
            Bet
          </button>
        </div>
      </div> ) : ( null )
      }
      { game.state === '2' ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Commit move</h2>
          <form onSubmit={e => submitCommitMoveHandle(e)}>
            <div className="form-group">
              <label htmlFor="move">Move</label>
              <select className="form-control" id="move">
                <option value="0">Rock</option>
                <option value="1">Paper</option>
                <option value="2">Scissors</option>
              </select>
            </div>
            { (isPlayer1() ? player1.commited : player2.commited) ? (
            <button type="submit" className="btn btn-primary">
            Commit
            </button> ) : (<p>Already Commited..</p>)
            }
          </form>
        </div>
      </div> ) : ( null )
      }
      { game.state === '3' ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Reveal move</h2>
          { (isPlayer1() ? player1.commited : player2.commited) ? (
          <button
            onClick={e => clickRevealMoveHandle()}
            type="submit"
            className="btn btn-primary"
          >
            Reveal
          </button> ) : (<p>Already revealed</p>)
          }
        </div>
      </div> ) : ( null )
      }
    </div>
  );
}

export default App;
