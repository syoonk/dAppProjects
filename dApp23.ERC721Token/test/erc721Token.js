const { expectRevert, expectEvent } = require('@openzeppelin/test-helpers');
const ERC721Token = artifacts.require("ERC721Token");
const BadRecipient = artifacts.require("BadRecipient");

contract("ERC721Token", accounts => {
  let tokenContract = null;
  const [admin, trader1, trader2] =
    [accounts[0], accounts[1], accounts[2]];

  beforeEach(async () => {
    tokenContract = await ERC721Token.new();
    for(let i = 0; i < 3; i++) {
      await tokenContract.mint({from: admin});
    }
  });

  it("should NOT mint if not admin", async () => {
    await expectRevert(
      tokenContract.mint({from: trader1}),
      "only admin");
  });

  it("should mint if admin", async () => {
    const beforeTokenBalance = await tokenContract.balanceOf(admin);
    // mint token id 3
    const receipt = await tokenContract.mint({from: admin});
    const afterTokenBalance = await tokenContract.balanceOf(admin);
    const tokenOwner = await tokenContract.ownerOf(3);
    
    assert.equal(tokenOwner, admin);
    assert((afterTokenBalance.sub(beforeTokenBalance)).eq(web3.utils.toBN('1')));
    expectEvent(receipt, 'Transfer', {
      _from: admin,
      _to: admin,
      _tokenId: web3.utils.toBN('3')});
  });

  it("should NOT transfer if not owner", async () => {
    await expectRevert(
      tokenContract.transferFrom(
        admin, 
        trader1, 
        0,
        {from: trader1}),
        "transfer not authorized");
  });

  // Our require will not be invoked since onERC721Received call the revert itself.
  it.skip('safeTransferFrom() should NOT transfer if recipient contract does not implement erc721recipient interface', async () => {
    const badRecipient = await BadRecipient.new();

    await expectRevert(
      tokenContract.safeTransferFrom(
        admin, 
        badRecipient.address, 
        0,
        {from: admin}
      ),
      'recipient smart contract cannot handle ERC721 tokens'
    );
  });

  it("transferFrom() should transfer", async () => {
    const receipt = await tokenContract.transferFrom(
      admin,
      trader1,
      0,
      {from: admin});
    const trader1Balance = await tokenContract.balanceOf(trader1);
    const adminBalance = await tokenContract.balanceOf(admin);
    const afterOwner = await tokenContract.ownerOf(0);

    assert(trader1Balance.eq(web3.utils.toBN("1")));
    assert(adminBalance.eq(web3.utils.toBN("2")));
    assert.equal(afterOwner, trader1);
    expectEvent(
      receipt, 
      'Transfer',
      {
        _from: admin,
        _to: trader1,
        _tokenId: web3.utils.toBN('0')
      });
  });

  it("safeTransferFrom() should transfer", async () => {
    const receipt = await tokenContract.transferFrom(
      admin,
      tokenContract.address,
      0,
      {from: admin});
    const contractBalance = await tokenContract.balanceOf(tokenContract.address);
    const adminBalance = await tokenContract.balanceOf(admin);
    const afterOwner = await tokenContract.ownerOf(0);

    assert(contractBalance.eq(web3.utils.toBN("1")));
    assert(adminBalance.eq(web3.utils.toBN("2")));
    assert.equal(afterOwner, tokenContract.address);
    expectEvent( receipt, 'Transfer',
      { _from: admin,
        _to: tokenContract.address,
        _tokenId: web3.utils.toBN('0') });
  });

  it("should transfer token when approved", async () => {
    const receiptApprove = await tokenContract.approve(trader1, 0, {from: admin});
    const approved = await tokenContract.getApproved(0);

    expectEvent(receiptApprove, 'Approval', 
      { _owner: admin, 
        _approved: trader1, 
        _tokenId: web3.utils.toBN('0') });
    assert.equal(approved, trader1);

    const receiptTransfer = await tokenContract.transferFrom(
      admin,
      trader1,
      0,
      {from: trader1});
    const traderBalance = await tokenContract.balanceOf(trader1);
    const adminBalance = await tokenContract.balanceOf(admin);
    const tokenOwner = await tokenContract.ownerOf(0);

    expectEvent(receiptTransfer, 'Transfer',
      { _from: admin, 
        _to: trader1, 
        _tokenId: web3.utils.toBN('0') });
    assert.equal(traderBalance.toNumber(), 1);
    assert.equal(adminBalance.toNumber(), 2);
    assert.equal(tokenOwner, trader1);
  });
});