pragma solidity ^0.5.8;

import "./ERC721.sol";
import "./AddressUtils.sol";

contract ERC721Token is ERC721 {
    using AddressUtils for address;
    
    mapping(address => uint) private ownerToTokenCount;
    mapping(uint => address) private idToOwner;
    mapping(uint => address) private idToApproved;
    mapping(address => mapping(address => bool)) private ownerToOperators;
    /**
   * @dev Magic value of a smart contract that can recieve NFT.
   * Equal to: bytes4(keccak256("onERC721Received(address,address,uint256,bytes)")).
   * From: https://github.com/0xcert/ethereum-erc721/blob/master/src/contracts/tokens/nf-token.sol
   */
    bytes4 internal constant MAGIC_ON_ERC721_RECEIVED = 0x150b7a02;
    address public admin;
    uint nextTokenId;
    
    constructor() 
    public {
        admin = msg.sender;
    }
    
    function mint() 
    external {
      require(msg.sender == admin, 
        'only admin');
        
      ownerToTokenCount[msg.sender]++;
      idToOwner[nextTokenId] = msg.sender;
      emit Transfer(msg.sender, msg.sender, nextTokenId);
      nextTokenId++;
    }
    
    function balanceOf(address _owner)
    external view 
    returns(uint) {
        return ownerToTokenCount[_owner];
    }
    
    function ownerOf(uint256 _tokenId) 
    external view 
    returns (address) {
        return idToOwner[_tokenId];
    }
    
    function approve(address _approved, uint _tokenId) 
    external payable {
        address owner = idToOwner[_tokenId];
        require(msg.sender == owner,
            "not authorized");
            
        idToApproved[_tokenId] = _approved;
        emit Approval(msg.sender, _approved, _tokenId);
    }
    
    function setApprovalForAll(address _operator, bool _approved) 
    external {
        ownerToOperators[msg.sender][_operator] = _approved;
        
        emit ApprovalForAll(msg.sender, _operator, _approved);
    }
    
    function getApproved(uint _tokenId) 
    external view 
    returns (address) {
        return idToApproved[_tokenId];
    }
    
    function isApprovedForAll(address _owner, address _operator) 
    external view 
    returns (bool) {
        return ownerToOperators[_owner][_operator];
    }
    
    function transferFrom(
        address _from, 
        address _to, 
        uint256 _tokenId) 
    external payable {
        _transfer(_from, _to, _tokenId);
    }
    
    function safeTransferFrom(
        address _from, 
        address _to, 
        uint _tokenId, 
        bytes calldata _data) 
    external payable {
        _safeTransferFrom(_from, _to, _tokenId, _data);
    }

    function safeTransferFrom(
        address _from, 
        address _to, 
        uint _tokenId) 
    external payable {
        _safeTransferFrom(_from, _to, _tokenId, "");
    }
    
    function _safeTransferFrom(
        address _from, 
        address _to, 
        uint _tokenId, 
        bytes memory _data)
    internal {
        if(_to.isContract()) {
            bytes4 retval = ERC721TokenReceiver(_to).onERC721Received(msg.sender, _from, _tokenId, _data);
            require(retval == MAGIC_ON_ERC721_RECEIVED,
                "recipient smart contract cannot handle ERC721 tokens");
        }

        _transfer(_from, _to, _tokenId);
    }
    
    function _transfer(
        address _from, 
        address _to,
        uint _tokenId)
    internal 
    canTransfer(_tokenId) {
        ownerToTokenCount[_from]--;
        ownerToTokenCount[_to]++;
        idToOwner[_tokenId] = _to;
        
        emit Transfer(_from, _to, _tokenId);
    }
    
    modifier canTransfer(uint _tokenId) {
        address owner = idToOwner[_tokenId];
        require(msg.sender == owner 
            || idToApproved[_tokenId] == msg.sender
            || ownerToOperators[owner][msg.sender] == true,
            "transfer not authorized");
        _;
    }
}