/*
//  Call the tokens metadata
//  Token : owner, approved status
//  It doesn't change any state of contract  
*/
import React, { useState } from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractData } = newContextComponents;

function TokenMetadata() {
  const { drizzle } = useDrizzle();
  const drizzleState = useDrizzleState(state => state);
  const [ownerOfTokenId, setOwnerOfTokenId] = useState(null);
  const [getApprovedTokenId, setGetApprovedTokenId] = useState(null);
  const [isApprovedForAllArgs, setIsApprovedForAllArgs] = useState(null);
  
  const submitOwnerOfHandle = (e) => {
    e.preventDefault();
    setOwnerOfTokenId(String(e.target.elements[0].value));
  }

  const submitGetApprovedHandle = (e) => {
    e.preventDefault();
    setGetApprovedTokenId(String(e.target.elements[0].value));
  }

  const submitIsApprovedForAllHandle = (e) => {
    e.preventDefault();
    setIsApprovedForAllArgs([e.target.elements[0].value, e.target.elements[1].value]);
  }

  return (
    <div className="App">
      <div>
        <h2>Admin</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={drizzleState}
            contract="ERC721Token"
            method="admin"
          ></ContractData>
      </div>
      <div>
        <h2>Token Owner</h2>
        <form onSubmit={e => submitOwnerOfHandle(e)}>
          <input type="number" placeholder="TokenId here"></input>
          <button>Get Owner</button>
        </form>
        {
          ownerOfTokenId && (
            <ContractData
              drizzle={drizzle}
              drizzleState={drizzleState}
              contract="ERC721Token"
              method="ownerOf"
              methodArgs={[ownerOfTokenId]}
            ></ContractData> )
        }
      </div>
      <div>
        <h2>Approved Address for Token</h2>
        <form onSubmit={e => submitGetApprovedHandle(e)}>
          <input type="number" placeholder="TokenId here"></input>
          <button>Get Address</button>
        </form>
        {
          getApprovedTokenId && (
            <ContractData
              drizzle={drizzle}
              drizzleState={drizzleState}
              contract="ERC721Token"
              method="getApproved"
              methodArgs={[getApprovedTokenId]}
            ></ContractData> )
        }
      </div>
      <div>
        <h2>Is approved for operator?</h2>
        <form onSubmit={e => submitIsApprovedForAllHandle(e)}>
          <label htmlFor="ownerAddress">Owner address</label>
          <input id="ownerAddress" type="owner" placeholder="Owner address here"></input>
          <label htmlFor="operatorAddress">Operator address</label>
          <input id="operatorAddress" placeholder="Operator address here"></input>
          <button>Get Approval Status</button>
        </form>
        {
          isApprovedForAllArgs && (
            <ContractData
              drizzle={drizzle}
              drizzleState={drizzleState}
              contract="ERC721Token"
              method="isApprovedForAll"
              methodArgs={[isApprovedForAllArgs]}
            ></ContractData> )
        }
      </div>
    </div>
  );
}

export default TokenMetadata;