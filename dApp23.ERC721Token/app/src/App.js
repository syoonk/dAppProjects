import React from "react";
import { Drizzle } from '@drizzle/store';
import { drizzleReactHooks } from "@drizzle/react-plugin";

import options from "./drizzleOptions";
import LoadingContainer from './LoadingContainer.js';
import Admin from "./Admin.js";
import Wallet from "./Wallet.js";
import TokenMetadata from "./TokenMetadata";

const drizzleOptions = new Drizzle(options);
const { DrizzleProvider } = drizzleReactHooks;

function App() {
  return (
    <div className="container">
      <h1>ERC721 Token</h1>
      <DrizzleProvider drizzle={drizzleOptions}>
        <LoadingContainer>
          <Wallet></Wallet>
          <TokenMetadata></TokenMetadata>
          <Admin></Admin>
        </LoadingContainer>
      </DrizzleProvider>
    </div>
  );
}

export default App;
