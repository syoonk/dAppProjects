import React from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractData, ContractForm } = newContextComponents;

function Wallet() {
  const { drizzle } = useDrizzle();
  const drizzleState = useDrizzleState(state => state);
  
  return (
    <div className="App">
      <h2>Your Address: {drizzleState.accounts[0]} </h2>
      <div>
        <h2>Balance</h2>
        <ContractData
          drizzle={drizzle}
          drizzleState={drizzleState}
          contract="ERC721Token"
          method="balanceOf"
          methodArgs={[drizzleState.accounts[0]]}
        ></ContractData>
      </div>
      <div>
        <h2>Transfer</h2>
        <ContractForm
          drizzle={drizzle}
          contract="ERC721Token"
          method="transferFrom"
        ></ContractForm>
      </div>
      <div>
        <h2>Safe Transfer</h2>
        <ContractForm
          drizzle={drizzle}
          contract="ERC721Token"
          method="safeTransferFrom"
        ></ContractForm>
      </div>
      <div>
        <h2>Approve</h2>
        <ContractForm
          drizzle={drizzle}
          contract="ERC721Token"
          method="approve"
        ></ContractForm>
      </div>
    </div>
  );
}

export default Wallet;