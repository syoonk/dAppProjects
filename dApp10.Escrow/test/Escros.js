const Escrow = artifacts.require("Escrow");

const assertError = async(promise, error) => {
    try{
        await promise;
    } catch(e) {
        assert(e.message.includes(error));
        return;
    }

    assert(false);
}

contract(`Escrow`, (accounts) => {
    let escrow = null;
    const [lawyer, payer, payee] = accounts;
    before(async() => {
        escrow = await Escrow.deployed();
    });

    it("should deposit", async() => {
        await escrow.deposit({ from: payer, value: 900 });
        const escrowBalance = parseInt(await web3.eth.getBalance(escrow.address));
        assert(escrowBalance == 900);
    });

    it("should NOT deposit if sender is NOT payer", async() => {
        assertError(
            escrow.deposit({ from: accounts[4], value: 100 }),
            `sender must be the payer`
        );
    });

    it(`should NOT deposit if transfer exceed amount`, async() => {
        assertError(
            escrow.deposit({ from: accounts[1], value: 2000 }),
            `contract balance will be over the amount`
        );
    });

    it(`should NOT release fund if full amount has not been received`, async() => {
        assertError(
            escrow.release({ from: lawyer }),  // Contract has 900 yet
            `Cannot release fund. Contract balance is not enough yet`
        );
    });
    
    it(`should Not realese funds if the sender is not lawyer`, async() => {
        await escrow.deposit({ from: payer, value: 100 });
        
        assertError(
            escrow.release({ from: accounts[4] }),
            `Lawyer only can release the money`
        );
    });

    it(`should release funds`, async() => {
        const balancePayeeBefore = web3.utils.toBN(await web3.eth.getBalance(payee));
        await escrow.release({ from: lawyer });
        const balancePayeeAfter = web3.utils.toBN(await web3.eth.getBalance(payee));
        
        assert.equal(balancePayeeAfter.sub(balancePayeeBefore).toNumber(), 1000);
    });
});