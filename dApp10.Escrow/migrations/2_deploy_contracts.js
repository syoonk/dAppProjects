const Escrow = artifacts.require("Escrow");

module.exports = function(deployer, _network, _accounts) {
    deployer.deploy(Escrow, _accounts[1], _accounts[2], 1000, {from: _accounts[0]});
}