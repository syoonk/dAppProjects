pragma solidity ^0.5.2;

contract Escrow {
    address public payer;
    address payable public payee;
    address public lawyer;
    uint public amount;

    constructor(address _payer, address payable _payee, uint _amount) public {
        lawyer = msg.sender;
        payer = _payer;
        payee = _payee;
        amount = _amount;
    }

    function deposit() public payable {
        require(msg.sender == payer, "sender must be the payer");
        require(address(this).balance <= amount, "contract balance will be over the amount");
    }

    function release() public {
        require(address(this).balance == amount, "Cannot release fund. Contract balance is not enough yet");
        require(msg.sender == lawyer, "Lawyer only can release the money");
        payee.transfer(amount);
    }

    function balanceOf() public view returns (uint) {
        return address(this).balance;
    }
}