pragma solidity ^0.6.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";
import "./Wallet.sol";
import "./User.sol";

contract Trader {
  using SafeMath for uint;
  Wallet wallet;
  User user;

  enum OrderState {
    ORDERED,
    PREPARING,
    DELIVERING,
    COMPLETED,
    CANCELLED
  }
  struct Order {
    address buyer;
    uint orderId;
    uint shopId;
    uint goodsId;
    uint amount;
    uint totalPrice;
    uint riderId;
    uint start;
    uint end;
    bool buyerCancelSign;
    bool sellerCancelSign;
    OrderState state;
  }
  struct Review {
    uint reviewId;
    uint orderId;
    uint stars;
    uint[] reputationIds;
    address from;
    address to;
  }
  mapping(uint => Order) orders;
  mapping(address => Review[]) public reviews;
  address public admin;
  uint nextOrderId = 1;
  uint nextReviewId = 1;
  bytes32 constant KNG = bytes32("KNG");

  event OrderStatusChanged(
    uint _orderId,
    OrderState _state,
    uint _time);

  constructor(address _walletContractAddress, address _userContractAddress)
  public {
    admin = msg.sender;
    wallet = Wallet(_walletContractAddress);
    user = User(_userContractAddress);
  }

  function placeOrder(
    uint _shopId,
    uint _goodsId,
    uint _amount,
    uint _totalPrice)
    external {
  }
}