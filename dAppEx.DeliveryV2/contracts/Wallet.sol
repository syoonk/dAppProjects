pragma solidity ^0.6.7;
    
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";

contract Wallet {
    using SafeMath for uint;
    
    struct Token {
        bytes32 ticker;
        address tokenAddress;
    }
    mapping(bytes32 => Token) public tokens;
    mapping(address => mapping(bytes32 => uint)) public traderBalances;
    bytes32[] public tokenList;
    address public admin;
    
    constructor() public {
        admin = msg.sender;
    }
    
    function addToken(bytes32 _ticker, address _address)
    external {
        require(msg.sender == admin, 
            "only admin");
        
        tokens[_ticker] = Token(_ticker, _address);
        tokenList.push(_ticker);
    }
    
    function deposit(uint _amount, bytes32 _ticker)
    external
    tokenExist(_ticker) {
        IERC20(tokens[_ticker].tokenAddress).transferFrom(msg.sender, address(this), _amount);
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(_amount);
    }
    
    function withdraw(uint _amount, bytes32 _ticker)
    external
    tokenExist(_ticker) {
        require(traderBalances[msg.sender][_ticker] >= _amount,
            "not enough balance");
        
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
    }
    
    function getUserBalance(address _user, bytes32 _ticker)
    external view
    returns(uint) {
        return traderBalances[_user][_ticker];
    }
    
    function transfer(
        address _sender, 
        address _receiver,
        uint _amount,
        bytes32 _ticker)
    external {
        require(traderBalances[_sender][_ticker] >= _amount,
            "not enough balance to transfer");
        
        traderBalances[_sender][_ticker] = traderBalances[_sender][_ticker].sub(_amount);
        traderBalances[_receiver][_ticker] = traderBalances[_receiver][_ticker].add(_amount);
    }

    
    modifier tokenExist(bytes32 _ticker) {
        require(tokens[_ticker].tokenAddress != address(0),
            "token does not exists");
        _;
    }
}