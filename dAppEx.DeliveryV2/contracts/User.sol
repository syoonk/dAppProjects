pragma solidity ^0.6.7;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";

contract User {
  using SafeMath for uint;

  struct UserInfo {
    address userAddress;
    uint userId;
    uint phoneNumber;
    uint reputation;
    string area;
    string userPublicKey;
    uint riderId;
    uint[] shopId;
  }
  struct Rider {
    uint riderId;
    address userAddress;
    string area;
    uint riderPhoneNumber;
    uint currentOrderId;
    uint nextOrderId;
    bool activate;
  }
  struct Shop {
    uint shopId;
    address owner;
    uint shopPhoneNumber;
    string area;
    bool activate;
  }
  struct Reputation {
    uint score;
    address user;
    bool sign;
  }
  struct Review {
    uint reviewId;
    uint stars;
  }
  mapping(address => UserInfo) users;
  mapping(uint => address) userAddressById;
  mapping(uint => Shop) public shops;
  mapping(uint => Rider) public riders;
  mapping(uint => Reputation) public scoringLists;
  mapping(address => Review[]) public reviews;
  mapping(address => mapping(address => bool)) infoAllowances;
  uint nextUserId = 1;
  uint nextRiderId = 1;
  uint nextShopId = 1;
  uint nextReputationId = 1;
  uint constant MAXREP = 100000;
  address public admin;
  address approvedWallet;
  address approvedTrader;

  event ReputationChanged(
    address indexed _user,
    uint _reputationId,
    uint _score,
    bool _sign);

  constructor()
  public {
    admin = msg.sender;
  }

  function approveWalletContract(address _walletAddress)
  external
  onlyAdmin() {
    approvedWallet = _walletAddress;
  }

  function approvedTraderContract(address _traderAddress)
  external
  onlyAdmin() {
    approvedTrader = _traderAddress;
  }

  function userRegister(
    uint _phoneNumber,
    string calldata _homeAddress,
    string calldata _userPublicKey)
  external {
    require(users[msg.sender].userId == 0,
      "user already registered");

    uint[] memory shopIdInit = new uint[](0);
    users[msg.sender] = UserInfo(
      msg.sender,
      nextUserId,
      _phoneNumber,
      MAXREP / 2,
      _homeAddress,
      _userPublicKey,
      0,
      shopIdInit);
    userAddressById[nextUserId] = msg.sender;

    allowInfo(msg.sender, approvedTrader);
    allowInfo(msg.sender, approvedWallet);

    nextUserId = nextUserId.add(1);
  }

  function changeRiderStatus(string calldata _area, uint _phoneNumber)
  external
  userExist(msg.sender) {
    UserInfo storage userCopy = users[msg.sender];

    // Registering Rider
    if(userCopy.riderId == 0) {
      riders[nextRiderId] = Rider(
        nextRiderId,
        userCopy.userAddress,
        _area,
        _phoneNumber,
        0,
        0,
        false);
      userCopy.riderId = nextRiderId;

      nextRiderId = nextRiderId.add(1);
    }

    riders[userCopy.riderId].activate = !riders[userCopy.riderId].activate;
  }

  function shopRegister(string calldata _area, uint _phoneNumber)
  external
  userExist(msg.sender) {
    shops[nextShopId] = Shop(
      nextShopId,
      msg.sender,
      _phoneNumber,
      _area,
      true);
    users[msg.sender].shopId.push(nextShopId);

    nextShopId = nextShopId.add(1);
  }

  function shopActivate(uint _shopId)
  external
  userExist(msg.sender) {
    require(shops[_shopId].shopId != 0,
      "shop does not exist");
    require(msg.sender == shops[_shopId].owner,
      "user does not own the shop");

    // Activate or Deactivate
    shops[_shopId].activate = !shops[_shopId].activate;
  }

  function relativeScoring(
    address _user,
    uint _score,
    bool _pos)
  external
  onlyApprovedContract() userExist(_user)
  returns(uint) {
    require(_score <= 2000,
      "score cannot over 2000 at once");

    UserInfo storage userCopy = users[_user];
    uint normalizedScore;
    if(_pos) {
      normalizedScore = 2 * (MAXREP - userCopy.reputation) * _score / MAXREP;
      userCopy.reputation = userCopy.reputation.add(normalizedScore);
    }
    if(!_pos) {
      normalizedScore = _score;
      userCopy.reputation = userCopy.reputation.sub(normalizedScore);
    }
    scoringLists[nextReputationId] = Reputation(
      normalizedScore,
      _user,
      _pos);

    emit ReputationChanged(
      _user,
      nextReputationId,
      normalizedScore,
      _pos);

    nextReputationId = nextReputationId.add(1);

    return nextReputationId - 1;
  }

  function directScoring(
    address _user,
    uint _score,
    bool _pos)
  external
  onlyApprovedContract() userExist(_user)
  returns(uint) {
    UserInfo storage userCopy = users[_user];
    userCopy.reputation = _pos ? userCopy.reputation.add(_score) : userCopy.reputation.sub(_score);
    scoringLists[nextReputationId] = Reputation(
      _score,
      _user,
      _pos);

    emit ReputationChanged(
      _user,
      nextReputationId,
      _score,
      _pos);

    nextReputationId = nextReputationId.add(1);

    return nextReputationId - 1;
  }

  function addReview(
    address _user,
    uint _reviewId,
    uint _stars)
  external
  onlyApprovedContract() userExist(_user) {
    reviews[_user].push(Review(_reviewId, _stars));
  }

  function deleteReview(address _user, uint _reviewId)
  external
  onlyApprovedContract() userExist(_user) {
    Review[] storage reviewsCopy = reviews[_user];
    for(uint i = 0; i < reviewsCopy.length; i++) {
      if(reviewsCopy[i].reviewId == _reviewId) {
        for(uint j = i + 1; j < reviewsCopy.length; j++) {
          reviewsCopy[j - 1] = reviewsCopy[j];
        }
        reviewsCopy.pop();
        return ;
      }
    }
  }

  function getUserInfo(address _user)
  public view
  userExist(_user)
  returns(
    address _userAddress,
    uint _userId,
    uint _phoneNumber,
    uint _reputation,
    string memory _area,
    string memory _publicKey,
    uint _riderId,
    uint[] memory _shopId) {
    require(infoAllowances[_user][msg.sender] == true || msg.sender == _user,
      "user not allows to access info");

    UserInfo memory userTemp = users[_user];
    _userAddress = _user;
    _userId = userTemp.userId;
    _phoneNumber = userTemp.phoneNumber;
    _reputation = userTemp.reputation;
    _area = userTemp.area;
    _publicKey = userTemp.userPublicKey;
    _riderId = userTemp.riderId;
    _shopId = userTemp.shopId;
  }

  function getRiderInfo(uint _riderId)
  public view
  returns(
    address _userAddress,
    uint _riderPhoneNumber,
    uint _currentOrderId,
    uint _nextOrderId,
    string memory _area,
    bool _activate) {
    Rider memory riderTemp = riders[_riderId];
    require(riderTemp.riderId != 0,
      "rider does not exist");

    _userAddress = riderTemp.userAddress;
    _riderPhoneNumber = riderTemp.riderPhoneNumber;
    _currentOrderId = riderTemp.currentOrderId;
    _nextOrderId = riderTemp.nextOrderId;
    _area = riderTemp.area;
    _activate = riderTemp.activate;
  }

  function getShopInfo(uint _shopId)
  public view
  returns(
    address _owner,
    uint _shopPhoneNumber,
    string memory _area,
    bool _activate) {
    Shop memory shopTemp = shops[_shopId];
    require(shopTemp.owner != address(0),
      "shop does not exist");

    _owner = shops[_shopId].owner;
    _shopPhoneNumber = shops[_shopId].shopPhoneNumber;
    _area = shops[_shopId].area;
    _activate = shops[_shopId].activate;
  }

  function allowInfo(address _user, address _toAllow)
  public
  userExist(_user) userExist(_toAllow) {
    require(msg.sender == _user || msg.sender == approvedTrader,
      "only contract and user allow");

    infoAllowances[msg.sender][_toAllow] = !infoAllowances[msg.sender][_toAllow];
  }

  modifier onlyAdmin() {
    require(msg.sender == admin,
      "only admin");
    _;
  }

  modifier userExist(address _user) {
    require(users[_user].userId != 0,
      "user does not exist");
    _;
  }

  modifier onlyApprovedContract() {
    require(msg.sender == approvedTrader || msg.sender == approvedWallet,
      "this contract is not approved");
    _;
  }
}