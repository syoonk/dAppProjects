pragma solidity ^0.5.2;
pragma experimental ABIEncoderV2;

contract Voting {
    struct Choice {
        uint id;
        string name;
        uint votes;
    }
    
    struct Ballot {
        uint id;
        string name;
        Choice[] choices;
        uint end;
    }
    
    mapping(uint => Ballot) public ballots;
    mapping(address => bool) public voters;
    // If this address has voted for this uint ballot
    mapping(address => mapping(uint => bool)) public votes;
    
    uint public nextBallotId;
    address public admin;
    
    constructor() public {
        admin = msg.sender;
    }
    
    modifier onlyAdmin() {
        require(msg.sender == admin, "only admin can execute");
        _;
    }
    
    function getBallot(uint _id) external view returns(Ballot memory) {
      return ballots[_id];
    }

    function addVoters(address[] calldata  _voters) external onlyAdmin() {
        for(uint i = 0; i < _voters.length; i++) {
            voters[_voters[i]] = true;
        }
    }
    
    function createBallot(string memory _name, string[] memory _choices, uint _offset) public onlyAdmin() {
        ballots[nextBallotId].id = nextBallotId;
        ballots[nextBallotId].name = _name;
        ballots[nextBallotId].end = now + _offset;
        for(uint i = 0; i < _choices.length; i++) {
            ballots[nextBallotId].choices.push(Choice(i, _choices[i], 0));
        }

        nextBallotId++;
    }
    
    function vote(uint _ballotId, uint _choiceId) external {
        require(voters[msg.sender] == true, "only approved voters can vote");
        require(votes[msg.sender][_ballotId] == false, "voters can vote only once each ballot");
        require(now < ballots[_ballotId].end, "ballot is already ended");
        
        votes[msg.sender][_ballotId] = true;
        ballots[_ballotId].choices[_choiceId].votes++;
    }
    
    function results(uint _ballotId) view external returns(Choice[] memory) {
        require(now >= ballots[_ballotId].end, "ballot is not ended");
        return ballots[_ballotId].choices;
    }
}