const Voting = artifacts.require("Voting");
const { expectRevert, time } = require("@openzeppelin/test-helpers");

contract("Voting", accounts => {
  let voting = null;
  before(async () => {
    voting = await Voting.deployed();
  });

  const admin = accounts[0];
  const voter1 = accounts[1];
  const voter2 = accounts[2];
  const voter3 = accounts[3];
  const nonVoter = accounts[4];

  it("should add voters", async () => {
    await voting.addVoters([voter1, voter2, voter3]);
    const results = await Promise.all(
      [voter1, voter2, voter3].map(voter => voting.voters(voter))
    );

    results.forEach(el => assert.equal(el, true));
  });

  it("should create a new ballot", async () => {
    await voting.createBallot("ballot 1", [`c1`, `c2`, `c3`], 5, {
      from: admin
    }); // id 0
    const result = await voting.ballots(0);

    assert.equal(result.name, "ballot 1");
    // assert.equal(result.choices, [`c1`, `c2`, `c3`]);
  });

  it("should NOT create a new ballot if not admin", async () => {
    await expectRevert(
      voting.createBallot(`ballot 2`, [`c1`, `c2`, `c3`], 5, { from: voter1 }),
      "only admin can execute"
    );
  });

  it("should NOT vote if not voter", async () => {
    await voting.createBallot(`ballot 3`, [`c1`, `c2`, `c3`], 5, {
      from: admin
    }); // id 1

    await expectRevert(
      voting.vote(1, 0, { from: nonVoter }),
      "only approved voters can vote"
    );
  });

  it("should NOT vote after vote ended", async () => {
    await voting.createBallot(`ballot 4`, [`c1`, `c2`, `c3`], 5, {
      from: admin
    }); // id 2
    await time.increase(5001);

    await expectRevert (
      voting.vote(2, 0, {from:voter1}),
      "ballot is already ended"
    );
  });

  it("should vote properly", async () => {
    await voting.createBallot(`ballot 5`, [`c1`, `c2`, `c3`], 5, {
      from: admin
    }); // id 3
    await voting.vote(3, 0, {from: voter1});
    await voting.vote(3, 0, { from: voter2 });
    await voting.vote(3, 1, { from: voter3 });
    await time.increase(5001);
    const result = await voting.results(3);

    assert.equal(result[0].votes, 2);
    assert.equal(result[1].votes, 1);
    assert.equal(result[2].votes, 0);
  });
});
