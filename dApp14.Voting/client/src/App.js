import React, { useEffect, useState } from "react";
import Voting from "./contracts/Voting.json";
import getWeb3 from "./utils.js";
import Web3 from 'web3';

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [admin, setAdmin] = useState(undefined);
  const [ballots, setBallots] = useState([]);
  const [contract, setContract] = useState(undefined);
  const [voters, setVoters] = useState([]);

  useEffect(() => {
    const init = async () => {
      if(window.ethereum) {
        const web3 = new Web3(window.ethereum);
        const accounts = await window.ethereum.enable();
        const networkId = await web3.eth.net.getId();
        const deployedNetwork = Voting.networks[networkId];
        const contract = new web3.eth.Contract(
          Voting.abi,
          deployedNetwork && deployedNetwork.address
        );
        const admin = await contract.methods.admin().call();

        setWeb3(web3);
        setAccounts(accounts);
        setContract(contract);
        setAdmin(admin);
      } else if(window.web3) {
        // Use Mist/MetaMask's provider.
        const web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        const networkId = await web3.eth.net.getId();
        const deployedNetwork = Voting.networks[networkId];
        const contract = new web3.eth.Contract(
          Voting.abi,
          deployedNetwork && deployedNetwork.address
        );
        const admin = await contract.methods.admin().call();

        setWeb3(web3); 
        setWeb3(web3);
        setAccounts(accounts);
        setContract(contract);
        setAdmin(admin);
      } else {
        const provider = new Web3.providers.HttpProvider(
          "http://127.0.0.1:9545"
        );
        const web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
      }
    };

    init();

    window.ethereum.on("accountsChanged", accounts => {
      setAccounts(accounts);
    });
  }, []);

  useEffect(() => {
    if(isReady()) {
      updateBallots();
    }

    const accounts = window.ethereum.on("accountsChanged", accounts => {
      setAccounts(accounts);
    });
  }, [accounts, contract, web3, admin]);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined" &&
      typeof admin !== "undefined"
    );
  };

  async function updateBallots() {
    const nextBallotId = parseInt(await contract.methods.nextBallotId().call());
    const ballots = [];

    for(let i = 0; i < nextBallotId; i++) {
      const [ballot, hasVoted] = await Promise.all(
        [contract.methods.getBallot(i).call(),
        contract.methods.votes(accounts[0], i).call()]
      );
      ballots.push({...ballot, hasVoted});
    }

    setBallots(ballots);
  }

  async function createBallotSubmitHandle(e) {
    e.preventDefault();
    const name = e.target.elements[0].value;
    const choices = e.target.elements[1].value.split(",").map(el => el.trim());
    const duration = e.target.elements[2].value;

    await contract.methods
      .createBallot(name, choices, parseInt(duration))
      .send({ from: accounts[0] });
    await updateBallots(); 
  }

  async function addVoterSubmitHandle(e) {
    e.preventDefault();
    const votersCopy = e.target.elements[0].value.split(",").map(el => el.trim());
    setVoters(votersCopy);
    
    await contract.methods.addVoters(votersCopy).send({ from: accounts[0] });
  }

  async function voteSubmitHandle(e, ballotId) {
    e.preventDefault();
    const select = e.target.elements[0];
    const choiceId = select.options[select.selectedIndex].value;

    await contract.methods.vote(ballotId, choiceId).send({ from: accounts[0] });
    await updateBallots();
  }

  function isFinished(ballot) {
    const now = (new Date()).getTime();
    const ballotEnd = (new Date(parseInt(ballot.end) * 1000)).getTime();
    return ((ballotEnd - now) > 0 ? false : true); 
  }

  if (!isReady()) {
    return <div>Loading...
      <h2>web3 : {typeof web3}</h2>
      <h2>accounts : {accounts}</h2>
      <h2>contract : {typeof contract}</h2>
      <h2>admin : {admin}</h2>
    </div>;
  }

  return (
    <div className="container">
      <div>
        <ul>
          <li>admin: {admin}</li>
          <li>Current Account: {accounts[0]}</li>
          {voters.map(el => (
            <li key={el}> 
            {el} </li>
          ))}
        </ul>
      </div>

      <h1 className="text-center">Voting</h1>

      {accounts[0].toLowerCase() === admin.toLowerCase() ? ( 
        <>
      <div className="row">
        <div className="col-sm-12">
          <h2>Create ballot</h2>
          <form onSubmit={e => createBallotSubmitHandle(e)}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" />
            </div>
            <div className="form-group">
              <label htmlFor="choices">Choices</label>
              <input type="text" className="form-control" id="choices" />
            </div>
            <div className="form-group">
              <label htmlFor="duration">Duration (s)</label>
              <input type="text" className="form-control" id="duration" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Add voters</h2>
          <form onSubmit={e => addVoterSubmitHandle(e)}>
            <div className="form-group">
              <label htmlFor="voters">Voters</label>
              <input type="text" className="form-control" id="voters" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />
      </>
      ) : null }

      <div className="row">
        <div className="col-sm-12">
          <h2>Votes</h2>
          <table className="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Votes</th>
                <th>Vote</th>
                <th>Ends on</th>
              </tr>
            </thead>
      <tbody>{ballots.map(ballot => (
        <tr key={ballot.id}> 
          <td>{ballot.id}</td>
          <td>{ballot.name}</td>
      <td>{ballot.choices.map(choice => (
        <li key={choice.id}>
          id: {choice.id}, 
        name: {choice.name}, 
        votes: {choice.votes}</li>
      ))}</td>
      <td>
        {isFinished(ballot) ? "Vote Finished" : (
          ballot.hasVoted ? "You already voted" : (
        <form onSubmit={e => voteSubmitHandle(e, ballot.id)}>
          <div className="form-group">
            <label htmlFor="choice">Choice</label>
            <select id="choice" className="form-control">
              {ballot.choices.map(choice => (
                <option key={choice.id} value={choice.id}>{choice.name}</option>
              ))}
            </select>
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        ))}
        </td>
      <td>{(new Date(parseInt(ballot.end) * 1000)).toLocaleString()}</td>
        </tr>
))}</tbody>
          </table>
        </div>
      </div> 
    </div>
  );
}

export default App;
