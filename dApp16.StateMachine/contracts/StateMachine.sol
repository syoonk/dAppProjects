pragma solidity >=0.5.0 <0.7.0;

contract StateMachine {
    enum State {PENDING, ACTIVE, CLOSED}

    State public state = State.PENDING;
    uint256 public amount;
    uint256 public interest;
    uint256 public end;
    uint256 public duration;
    address payable public borrower;
    address payable public lender;

    constructor(
        uint256 _amount,
        uint256 _interest,
        uint256 _duration,
        address payable _borrower,
        address payable _lender
    ) public {
        amount = _amount;
        interest = _interest;
        duration = _duration;
        end = now + _duration;
        borrower = _borrower;
        lender = _lender;
    }

    function fund() external payable {
        require(msg.sender == lender, "only lender can lend");
        require(
            address(this).balance == amount,
            "can only lend the exact amount"
        );
        _transitionTo(State.ACTIVE);
        borrower.transfer(amount);
    }

    function reimburse() external payable {
        require(msg.sender == borrower, "only borrower can reimburse");
        require(
            msg.value == amount + interest,
            "borrower need to reimburse exact amount and interest"
        );
        _transitionTo(State.CLOSED);
        lender.transfer(amount + interest);
    }

    function _transitionTo(State _to) internal {
        require(_to != State.PENDING, "cannot go back to PENDING state");
        require(_to != state, "cannot trasition to current state");
        if (_to == State.ACTIVE) {
            require(
                state == State.PENDING,
                "can only transition to ACTIVE from PENDING"
            );
            state = State.ACTIVE;
        } else if (_to == State.CLOSED) {
            require(
                state == State.ACTIVE,
                "can only transition to CLOSED from ACTIVE"
            );
            require(now >= end, "loan hasn't matured yet");
            state = State.CLOSED;
        }
    }
}
