const StateMachine = artifacts.require("StateMachine");

module.exports = (deployer, _network, _accounts) => {
  deployer.deploy(StateMachine, 2000, 4000, 60, _accounts[1], _accounts[2]);
};