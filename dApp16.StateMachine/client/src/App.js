import React, { useEffect, useState } from "react";
import StateMachine from "./contracts/StateMachine.json";
import Web3 from "web3";

const states = ["PENDING", "ACTIVE", "CLOSED"];

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [contract, setContract] = useState(undefined);
  const [loan, setLoan] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = StateMachine.networks[networkId];
      const contract = new web3.eth.Contract(
        StateMachine.abi,
        deployedNetwork && deployedNetwork.address
      );

      setWeb3(web3);
      setContract(contract);

      const loan = await Promise.all([
        contract.methods.amount().call(),
        contract.methods.interest().call(),
        contract.methods.end().call(),
        contract.methods.duration().call(),
        contract.methods.borrower().call(),
        contract.methods.lender().call(),
        contract.methods.state().call()
      ]);

      setLoan({
        amount: loan[0],
        interest: loan[1],
        end: loan[2],
        duration: loan[3],
        borrower: loan[4],
        lender: loan[5],
        state: parseInt(loan[6])
      });
    };
    init();
    window.ethereum.on("accountsChanged", accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined" &&
      typeof loan !== "undefined"
    );
  };

  useEffect(() => {
    if (isReady()) {
      updateState();
    }
  }, [accounts, web3, contract, loan]);

  async function updateState() {
    const numOfState = parseInt(await contract.methods.state().call());

    setLoan({ ...loan, numOfState });
  }

  async function clickFundHandle() {
    await contract.methods
      .fund()
      .send({ from: accounts[0], value: loan.amount });

    updateState();

    window.location.reload();
  }

  async function clickReimburseHandle() {
    await contract.methods
      .reimburse()
      .send({
        from: accounts[0],
        value: Number(loan.amount) + Number(loan.interest)
      });

    updateState();

    window.location.reload();
  }

  function isFinished() {
    const now = new Date().getTime();
    const loanEnd = new Date(parseInt(loan.end) * 1000).getTime();
    return loanEnd > now ? false : true;
  }

  if (!isReady()) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">Loan State Machine</h1>
      <p>Current User: {accounts[0]}</p>
      <p>Borrower: {loan.borrower}</p>
      <p>Lender: {loan.lender}</p>
      <p>Amount: {loan.amount} Wei</p>
      <p>Interest: {loan.interest} Wei</p>
      <p>Duration: {loan.duration} </p>
      <p>End: {new Date(parseInt(loan.end) * 1000).toLocaleString()}</p>
      <p>State: {states[loan.state]}</p>
      {accounts[0].toLowerCase() === loan.lender.toLowerCase() ? (
        loan.state === 0 ? (
          <div className="row">
            <div className="col-sm-12">
              <h2>Fund {loan.amount} Wei</h2>
              <button
                onClick={e => clickFundHandle()}
                type="submit"
                className="btn btn-primary"
              >
                Fund
              </button>
            </div>
          </div>
        ) : (
          <p>Loan has been funded</p>
        )
      ) : null}

      {accounts[0].toLowerCase() === loan.borrower.toLowerCase() ? (
        <div className="row">
          <div className="col-sm-12">
            <h2>
              Reimburse {loan.amount} Wei (principal) + {loan.interest} Wei
              (interest) = {Number(loan.amount) + Number(loan.interest)} Wei
              (Total)
            </h2>
            { Number(loan.state) === 2 ? ( <p>loan has been reimbursed</p>) : (
            <button
              onClick={e => clickReimburseHandle()}
              type="submit"
              className="btn btn-primary"
              disabled={loan.state === 1 && isFinished() ? false : true}
            >
              Reimburse
            </button> )}
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default App;
