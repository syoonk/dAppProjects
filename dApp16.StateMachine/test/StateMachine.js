const { expectRevert, time } = require("@openzeppelin/test-helpers");
const StateMachine = artifacts.require("StateMachine.sol");

contract("StateMachine", accounts => {
  let stateMachine;
  const amount = 2000;
  const interest = 4000;
  const duration = 60;

  const [borrower, lender] = [accounts[1], accounts[2]];
  before(async () => {
    stateMachine = await StateMachine.deployed(
      amount,
      interest,
      duration,
      borrower,
      lender
    );
  });

  it("Should NOT accept fund if not lender", async () => {
    await expectRevert(
      stateMachine.fund({ from: accounts[0], value: amount }),
      "only lender can lend"
    );
  });

  it("Should NOT accept fund if not exact amount", async () => {
    await expectRevert(
      stateMachine.fund({ from: lender, value: 2100 }),
      "can only lend the exact amount"
    );
  });

  it("Should accept fund", async () => {
    await stateMachine.fund({ from: lender, value: amount });
  });

  it("Should NOT reimburse if not borrower", async () => {
    await expectRevert(
      stateMachine.reimburse({ from: accounts[0], value: amount + interest }),
      "only borrower can reimburse"
    );
  });

  it("Should NOT reimburse if not exact amount", async () => {
    await expectRevert(
      stateMachine.reimburse({ from: borrower, value: amount }),
      "borrower need to reimburse exact amount and interest"
    );
  });

  it("Should NOT reimburse if loan hasnt matured yet", async () => {
    await expectRevert(
      stateMachine.reimburse({ from: borrower, value: amount + interest }),
      "loan hasn't matured yet"
    );
  });

  it("Should reimburse", async () => {
    await time.increase(60000);

    await stateMachine.reimburse({ from: borrower, value: amount + interest });
  });
});
