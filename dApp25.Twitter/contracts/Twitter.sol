pragma solidity >= 0.5.0 < 0.7.0;
pragma experimental ABIEncoderV2;

contract Twitter {
    struct Tweet {
        uint id;
        address author;
        string content;
        uint createdAt;
    }
    struct Message {
        uint id;
        string content;
        address from;
        address to;
        uint createdAt;
    }
    mapping(uint => Tweet) private tweets;
    mapping(address => uint[]) private tweetsOf;
    mapping(uint => Message[]) private conversations;
    mapping(address => address[]) private following;
    mapping(address => address[]) private followers;
    mapping(address => mapping(address => bool)) private operators;
    uint private nextTweetId;
    uint private nextMessageId;
    event TweetSent(
        uint id, 
        address indexed author, 
        string content,
        uint createdAt);
    event MessageSent(
        uint id,
        string content,
        address indexed from,
        address indexed to,
        uint createdAt);
    
    function tweet(string calldata _content) 
    external {
        _tweet(msg.sender, _content);
    }
    
    function tweetFrom(address _from, string calldata _content)
    external 
    canOperate(_from) {
        _tweet(_from, _content);
    }
    
    function sendMessage(string calldata _content, address _to)
    external {
        _sendMessage(_content, msg.sender, _to);
    }
    
    function sendMessageFrom(
        string calldata _content, 
        address _from,
        address _to)
    external 
    canOperate(_from) {
        _sendMessage(_content, _from, _to);
    }
    
    function follow(address _followed)
    external {
        require(_followed != msg.sender,
            "cannot follow itself");

        following[msg.sender].push(_followed);
        followers[_followed].push(msg.sender);
    }

    function unFollow(address _unFollow)
    external {
        uint followingLength = following[msg.sender].length;
        require(followingLength > 0,
            "no following to unfollow");

        for(uint i = 0; i < followingLength; i++) {
            if(following[msg.sender][i] == _unFollow) {
                for(uint j = i; j < followingLength - 1; j++) {
                    following[msg.sender][j] = following[msg.sender][j + 1];
                }
                following[msg.sender].pop();
                break;
            }
        }
        uint followersLength = followers[_unFollow].length;
        for(uint i = 0; i < followersLength; i++) {
            if(followers[_unFollow][i] == msg.sender) {
                for(uint j = i; j < followersLength - 1; j++) {
                    followers[_unFollow][j] = followers[_unFollow][j + 1];
                }
                followers[_unFollow].pop();
                break;
            }
        }
    }

    function allow(address _operator)
    external {
        operators[msg.sender][_operator] = true;
    }

    function disallow(address _operator)
    external {
        operators[msg.sender][_operator] = false;
    }

    function getFollowing(address _user)
    external view
    returns(address[] memory) {
        require(following[_user].length > 0,
            "no followings to return");

        return following[_user];
    }

    function getFollowers(address _user)
    external view
    returns(address[] memory) {
        require(followers[_user].length > 0,
            "no followers to return");
        
        return followers[_user];
    }

    function getLatestMessagesWith(address _to, uint _count)
    external view 
    returns(Message[] memory) {
        uint conversationId = uint(msg.sender) + uint(_to);
        uint numOfMessages = conversations[conversationId].length;
        require(numOfMessages > 0,
            "there is no message to get");
        // require(_count >= 0 && _count <= numOfMessages,
        //     "too few or too many messages to return");

        if(_count <= 0 || _count >= numOfMessages) _count = numOfMessages;
        Message[] memory messagesCopy = new Message[](_count);
        uint j = 0;
        for(uint i = numOfMessages - _count; i < numOfMessages; i++) {
            messagesCopy[j] = conversations[conversationId][i];
            j++;
        }
        return messagesCopy;
    }
    
    function getLatestTweets(uint _count)
    view external
    returns(Tweet[] memory) {
        // require(_count >= 0 && _count <= nextTweetId,
        //     "Too few or too many tweets to return");
        
        if(_count <= 0 || _count >= nextTweetId) _count = nextTweetId;
        Tweet[] memory tweetsCopy = new Tweet[](_count);
        uint j = 0;
        for(uint i = nextTweetId - _count; i < nextTweetId; i++) {
            tweetsCopy[j] = tweets[i];
            j++;
        }
        return tweetsCopy;
    }
    
    function getTweetsOf(address _user, uint _count)
    view external 
    returns(Tweet[] memory) {
        uint[] storage tweetIdsPointer = tweetsOf[_user];
        // require(_count >= 0 && _count <= tweetIdsPointer.length,
        //     "Too few or too many tweets to return");
        
        if(_count <= 0 || _count >= tweetIdsPointer.length) _count = tweetIdsPointer.length;
        Tweet[] memory tweetsCopy = new Tweet[](_count);
        uint j = 0;
        for(uint i = tweetIdsPointer.length - _count; i < tweetIdsPointer.length; i++) {
            tweetsCopy[j] = tweets[tweetIdsPointer[i]];
            j++;
        }
        return tweetsCopy;
    }
    
    function _tweet(address _from, string memory _content) 
    internal {
        tweets[nextTweetId] = Tweet(
            nextTweetId, 
            _from, 
            _content, 
            now);
        tweetsOf[_from].push(nextTweetId);
        emit TweetSent(
            nextTweetId, 
            _from, 
            _content, 
            now);
        nextTweetId++;   
    }
    
    function _sendMessage(
        string memory _content, 
        address _from,
        address _to)
    internal {
        uint conversationId = uint(_from) + uint(_to);
        conversations[conversationId].push(Message(
            nextMessageId, 
            _content, 
            _from, 
            _to, 
            now));
        emit MessageSent(
            nextMessageId,
            _content,
            _from,
            _to,
            now);
        nextMessageId++;
    }
    
    modifier canOperate(address _from) {
        require(operators[_from][msg.sender] == true,
            "Operator not authorized");
        _;
    }
}