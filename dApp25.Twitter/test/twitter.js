const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const Twitter = artifacts.require("Twitter");

contract("Twitter", accounts => {
  let twitter = null;
  const [user1, user2] = [accounts[0], accounts[1]];
  const operator = accounts[5];
  const firstUserTweets = [
    "I'm first user - First Message",
    "I'm fisrt user - Second Message",
    "I'm first user - Third Message" ];
  const secondUserTweets = [
    "I'm second user - First Message",
    "I'm second user - Second Message",
    "I'm second user - Third Message" ];
  
  beforeEach(async () => {
    twitter = await Twitter.new();
    await Promise.all(
      firstUserTweets.map(tweet => {
        twitter.tweet(tweet, {from: user1})
      }));
    await Promise.all(
      secondUserTweets.map(tweet => {
        twitter.tweet(tweet, {from: user2})
      }));
  });

  it("should return all tweets of user", async () => {
    const user1Tweets = await twitter.getTweetsOf(user1, 0, {from: user1});

    assert.equal(user1Tweets.length, 3);
    for(let i = 0; i < 3; i++) {
      assert.equal(user1Tweets[i].author, user1);
      assert.equal(user1Tweets[i].content, firstUserTweets[i]);
    }
  });

  it("should return latest tweet of user", async () => {
    const user1Tweets = await twitter.getTweetsOf(user1, 1, {from: user1});

    assert.equal(user1Tweets.length, 1);
    assert.equal(user1Tweets[0].author, user1);
    assert.equal(user1Tweets[0].content, firstUserTweets[2]);
  });

  it("should NOT return tweets if too many tweets asked", async () => {
    await expectRevert(
      twitter.getTweetsOf(user1, 4, {from: user1}),
      "Too few or too many tweets to return");

    await expectRevert(
      twitter.getLatestTweets(7, {from: user1}),
      "Too few or too many tweets to return");
  });

  it("should return latest tweets", async () => {
    const tweets = await twitter.getLatestTweets(0);

    assert.equal(tweets.length, 6);
    for(let i = 0; i < 3; i++) {
      assert.equal(tweets[i].author, user1);
      assert.equal(tweets[i].content, firstUserTweets[i]);
    }
    for(let i = 3; i < 6; i++) {
      assert.equal(tweets[i].author, user2);
      assert.equal(tweets[i].content, secondUserTweets[i - 3]);
    }
  });

  it("should NOT return latest tweets if too many asked", async () => {
    await expectRevert(
      twitter.getLatestTweets(7),
      "Too few or too many tweets to return");
  });

  it("should send tweet", async () => {
    const message = "This message is to be 7th";
    const receipt = await twitter.tweet(message, {from: user1});
    
    expectEvent(receipt, "TweetSent", {
      id: web3.utils.toBN("6"),
      author: user1,
      content: message });
  });

  it("should send message", async () => {
    // % 2, 0 for user1 send, 1 for user2 send
    const users = [user1, user2];
    const messages = [
      "hi", 
      "hello", 
      "I'm user1",
      "I'm user2",
      "Bye",
      "Good Bye"];
    let receipts = new Array(messages.length);
    for(let i = 0; i < messages.length; i++) {
      const order = i % 2;
      receipts[i] = !order
        ? await twitter.sendMessage(messages[i], users[1], {from: users[0]})
        : await twitter.sendMessage(messages[i], users[0], {from: users[1]});
    }
    const conversation = await twitter.getLatestMessagesWith(user2, 0, {from: user1});
    
    assert.equal(conversation.length, 6);
    for(let i = 0; i < messages.length; i++) {
      const sender = i % 2;
      const receiver = i % 2 ? 0 : 1;
      assert.equal(conversation[i].from, users[sender])
      assert.equal(conversation[i].content, messages[i]);
      expectEvent(receipts[i], "MessageSent", {
        content: messages[i],
        from: users[sender],
        to: users[receiver] });
    }
  });

  it("should follow", async () => {
    // user1 user2 accounts[2] follow accounts[3]
    for(let i = 0; i < 3; i++) {
      await twitter.follow(accounts[3], {from: accounts[i]});
    }
    let followings = new Array(3);
    for(let i = 0; i < 3; i++) {
      followings[i] = await twitter.getFollowing(accounts[i]);
    }
    const followers = await twitter.getFollowers(accounts[3]); 

    assert.equal(followers.length, 3);
    for(let i = 0; i < 3; i++) {
      assert.equal(followings[i].length, 1);
      assert.equal(followings[i][0], accounts[3]);
      assert.equal(followers[i], accounts[i]);
    }
  });

  it("should unFollow", async () => {
    // user1 user2 accounts[2] follow accounts[3] and users2 will unfollow
    for(let i = 0; i < 3; i++) {
      await twitter.follow(accounts[3], {from: accounts[i]});
    }
    await twitter.unFollow(accounts[3], {from: user2});
    const followers = await twitter.getFollowers(accounts[3]);
    
    await expectRevert(
      twitter.getFollowing(user2, { from: user2 }),
      "no followings to return");
    assert.equal(followers.length, 2);
    assert.equal(followers[0], user1);
    assert.equal(followers[1], accounts[2]);
  });

  it("should NOT follow itself", async () => {
    await expectRevert(
      twitter.follow(user1, {from: user1}),
      "cannot follow itself");
  });

  it("should NOT send a tweet from if not operator", async () => {
    await expectRevert(
      twitter.tweetFrom(user1, "It would be reverted", {from: operator}),
      "Operator not authorized");
  });

  it("should send a tweet from approved operator", async () => {
    await twitter.allow(operator, {from: user1});
    const receipt = await twitter.tweet("It should be sent");
    const tweets = await twitter.getLatestTweets(0);

    assert.equal(tweets.length, 7);
    assert.equal(tweets[6].author, user1);
    assert.equal(tweets[6].content, "It should be sent");
    expectEvent(receipt, "TweetSent", {
      author: user1,
      content: "It should be sent"});
  });

  it("should NOT send a message from if not operator", async () => {
    await expectRevert(
      twitter.sendMessageFrom(
        "It should not be sent", 
        user1, 
        user2, 
        {from: operator}),
      "Operator not authorized");
  });

  it("should send a message from approved operator", async () => {
    const messageUser1 = "Hi";
    const messageUser2 = "Hello";
    await twitter.allow(operator, {from: user1});
    const receiptUser1 = await twitter.sendMessageFrom(
      messageUser1,
      user1,
      user2,
      {from: operator});
    const receiptUser2= await twitter.sendMessage(messageUser2, user1, {from: user2});
    const conversation = await twitter.getLatestMessagesWith(user2, 0, {from: user1});

    assert.equal(conversation.length, 2);
    assert.equal(conversation[0].content, messageUser1);
    assert.equal(conversation[0].from, user1);
    assert.equal(conversation[0].to, user2);
    assert.equal(conversation[1].content, messageUser2);
    assert.equal(conversation[1].from, user2);
    assert.equal(conversation[1].to, user1);
    expectEvent(receiptUser1, "MessageSent", {
      content: messageUser1,
      from: user1,
      to: user2 });
    expectEvent(receiptUser2, "MessageSent", {
      content: messageUser2,
      from: user2,
      to: user1 });
  });
});
