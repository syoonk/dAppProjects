const Twitter = artifacts.require("Twitter");

module.exports = async (deployer, _networks, _accounts) => {
  await deployer.deploy(Twitter);
  const twitter = await Twitter.deployed();
  await Promise.all([
    twitter.tweet("First Tweet"),
    twitter.tweet("Second Tweet"),
    twitter.tweet("Third Tweet"),
    twitter.tweet("Fourth Tweet"),
    twitter.tweet("Fifth Tweet"),
  ]);
}