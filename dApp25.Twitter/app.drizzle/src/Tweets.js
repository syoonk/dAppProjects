import React, { useState } from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractData, ContractForm } = newContextComponents;

export default () => {
  const { drizzle } = useDrizzle();
  const drizzleState = useDrizzleState(state => state);
  const [numTweetsShow, setNumTweetsShow] = useState(0);

  return (
    <div className="App">
    <div>
      <h2>Tweets</h2>
      <ContractData
        drizzle={drizzle}
        drizzleState={drizzleState}
        contract="Twitter"
        method="getLatestMessages"
        methodArgs={[numTweetsShow]}
        // render={
        //   (tweets) => {
        //     return (
        //       tweets.map(tweet => (
        //         <h5>{tweet}</h5>
        //       ))
        //     )
        //   }
        // }
      ></ContractData>
    </div>
    </div>
  );
}