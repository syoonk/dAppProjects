import React, { Fragment } from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";

const { useDrizzleState } = drizzleReactHooks;

function LoadingContainer({children}) {
  const drizzleState = useDrizzleState(state => state);
  if(drizzleState.drizzleStatus.initialied === false) {
    return "Loading ...";
  }
  return (
    <Fragment>
      {children}
      <h2>Your Address: {drizzleState.accounts[0]} </h2>
    </Fragment>
  )
}

export default LoadingContainer;