import React from 'react';
import { Drizzle } from '@drizzle/store';
import { drizzleReactHooks } from "@drizzle/react-plugin";

import options from "./drizzleOptions.js";
import LoadingContainer from "./LoadingContainer.js";
import Tweets from "./Tweets.js";

const drizzle = new Drizzle(options);
const { DrizzleProvider } = drizzleReactHooks;

function App() {
  return (
    <div className="App">
      <h1>Twitter</h1>
      <DrizzleProvider drizzle={drizzle}>
        <LoadingContainer>
          <Tweets></Tweets>
        </LoadingContainer>
      </DrizzleProvider>
    </div>
  );
}

export default App;
