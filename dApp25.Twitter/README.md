# About Twitter Application
This application is motivated by Twitter. User uploads short post and follows other users. And they also can send the message.

## Used Tools
Solidity, Truffle, NodeJS, React, Metamask

----
## How To Run
It uses metamask.

| EndPoints ||
---|---|
Truffle | http://localhost:9545  
Ganache | http://localhost:8545

### Deploy Contract  
`./npm install`  
`./truffle develop`  
`truffle> migrate --reset`  

### Run Frontend
`./client/npm install`  
`./client/npm start`

----
## Pages
<img src="./images/main.png" width="60%" height="60%"></img>