import React, { useEffect, useState } from "react";

import "./UserInfo.css";

function UserInfo({contract, accounts}) {
  const [followings, setFollowings] = useState([]);
  const [followers, setFollowers] = useState([]);

  useEffect(() => {
    getFollowings();
    getFollowers();
  }, [accounts]);

  const getFollowings = async () => {
    let followingsCopy = [];
    try {
      followingsCopy = await contract.methods.getFollowing(accounts[0]).call();
    } catch {
      followingsCopy = [];
    }

    setFollowings(followingsCopy);
  }

  const getFollowers = async () => {
    let followersCopy = [];
    try {
      followersCopy = await contract.methods.getFollowers(accounts[0]).call();
    } catch {
      followersCopy = [];
    }

    setFollowers(followersCopy);
  }

  const submitFollowHandle = async (e) => {
    e.preventDefault();
    const follow = e.target.elements[0].value.trim();
    await contract.methods.follow(follow).send({from: accounts[0]});

    getFollowings();
  }

  const submitUnfollowHandle = async (e) => {
    e.preventDefault();
    const unfollow = e.target.elements[0].value.trim();
    await contract.methods.unFollow(unfollow).send({from: accounts[0]});

    getFollowings();
  }

  return (
    <div>
      <h2>User Infos</h2>
      { /* Follow, UnFollow */ }
      <div className="follow">
        <form className="addFollow" onSubmit={e => submitFollowHandle(e)}>
          <input className="addressBox" placeholder="address here"></input>
          <button>Follow</button>
        </form> 
        <form className="unfollow" onSubmit={e => submitUnfollowHandle(e)}>
          <input className="addressBox" placeholder="address here"></input>
          <button>unfollow</button>
        </form>
      </div>
      { /* Follow Lists */ }
      <div className="follow">
        <form className="followingList">
          <h3>My Followings</h3>
          <div id="followingDiv" className="followListBox">
            {
              followings.map(following => <p key={following + accounts[0]}>{following}</p>)
            }
          </div>
        </form>
        <form className="followerList">
          <h3>My Followers</h3>
          <div className="followListBox">
            {
              followers.map(follower => <p key={follower + accounts[0]}>{follower}</p>)
            }
          </div>
        </form>
      </div>
    </div>
  );
}

export default UserInfo;