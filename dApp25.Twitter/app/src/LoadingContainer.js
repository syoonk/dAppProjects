import React, { useEffect, useState } from "react";
import App from "./App.js";
import Twitter from "./contracts/Twitter.json";
import Web3 from "web3";

function LoadingContainer() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState([]);
  const [contract, setContract] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      // Searching for modern dapp browser
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } 
      // Legacy dapp browser
      else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } 
      // Develop console host
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Twitter.networks[networkId];
      const twitter = new web3.eth.Contract(
        Twitter.abi,
        deployedNetwork && deployedNetwork.address
      );

      setWeb3(web3);
      setContract(twitter);
    };

    init();
    window.ethereum.on('accountsChanged', accounts => {
      setAccounts(accounts);
    });
  
  }, []);

  const isReady = () => {
    return (
      typeof web3 !== 'undefined' 
      && typeof contract !== 'undefined' 
      && accounts.length > 0
    );
  };
  
  if(!isReady()) {
    return (
    <div>
      Loading...
      web3: {typeof web3}
      accounts: {typeof accounts}
      contract: {typeof contract}
    </div>
    );
  }
  
  return (
    <App
      web3={web3}
      accounts={accounts}
      contract={contract}
    ></App>
  );
};

export default LoadingContainer;