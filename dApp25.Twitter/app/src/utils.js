import Web3 from "web3";
import Twitter from "./contracts/Twitter.json";

const getWeb3 = () => {
  return new Promise((resolve, reject) => {
    window.addEventListener("load", async () => {
      // modern browser
      if(window.ethereum) {
        const web3 = new Web3(window.ethereum);
        console.log("Enabaling Modern Browser");
        try {
          await window.ethereum.enable();
          resolve(web3);
        } catch(error) {
          reject(error);
        }
      } 
      // legacy browser
      else if(window.web3) {
        const web3 = window.web3;
        console.log("Injected web3 detected.");
        resolve(web3);
      } 
      // localhost, truffle develop console
      else {  
        const provider = new Web3.providers.HttpProvider("http://localhost:8545");
        const web3 = new Web3(provider);
        console.log("No web3 instance injected, using local web3.");
        resolve(web3);
      }
    });
  });
};

const getContracts = async (web3) => {
  const networkId = await web3.eth.net.getId();
  const deployedNetwork = Twitter.networks[networkId];
  const twitter = new web3.eth.Contract(Twitter.abi, deployedNetwork && deployedNetwork.address);

  return twitter;
};

export { getWeb3, getContracts };