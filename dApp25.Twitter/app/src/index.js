import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import LoadingContainer from "./LoadingContainer";

ReactDOM.render(
  <LoadingContainer></LoadingContainer>,
  document.getElementById('root')
);