import React, { useState, useEffect } from 'react';

import Tweets from "./Tweets.js";
import UserInfo from "./UserInfo.js";
import Messages from "./Messages.js";

import "./App.css";

function App({web3, accounts, contract}) {
  return (
    <div className="App">
      <>Current User: {accounts[0]}</>
      <Tweets contract={contract} accounts={accounts}></Tweets>
      <hr className="divLine" align="left"></hr>
      <UserInfo contract={contract} accounts={accounts}></UserInfo>
      <hr className="divLine" align="left"></hr>
      <Messages contract={contract} accounts={accounts}></Messages>
    </div>
  );
}

export default App;