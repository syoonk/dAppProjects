import React, { useState, useEffect } from "react";

import "./Tweets.css";

function Tweets({contract, accounts}) {
  const [counts, setCounts] = useState(5);
  const [tweets, setTweets] = useState([]);
  const [filtered, setFiltered] = useState(false);

  useEffect(() => {
    getTweets();
  }, [counts]);

  const getTweets = async () => {
    const tweetsCopy = await contract.methods.getLatestTweets(counts).call();
    setTweets(tweetsCopy);
  };

  const changeCountsHandle = async (e) => {
    e.preventDefault();
    const countsCopy = e.target.value;

    setCounts(countsCopy);
  }

  const submitFilterHandle = (e) => {
    e.preventDefault();
    let tweetsCopy = [];

    if(!filtered) {
      const filterUser = (e.target.elements[1].value).toLowerCase();
      tweetsCopy = tweets.filter(tweet => tweet.author.toLowerCase() === filterUser);

      setTweets(tweetsCopy);
      setFiltered(true);
    } else {
      getTweets();
      setFiltered(false);
    }
  }

  const submitUploadTweetHandle = async (e) => {
    e.preventDefault();

    await contract.methods.tweet(e.target.elements[0].value).send({from: accounts[0]});
    getTweets();
  };
  
  return (
    <div className="tweet">
      <h2>Tweets</h2>
      { /* Get Latest Tweets */ }
      <div>
        <form onSubmit={e => submitFilterHandle(e)}>
          Counts:&nbsp;
          <input
            className="countBox" 
            type="number"
            value={counts}
            min="0"
            onChange={e => changeCountsHandle(e)}
          ></input>
          &nbsp;&nbsp;&nbsp;
          UserFilter: &nbsp;
          <input 
            type="text" 
            placeholder="address here"
            style={{width: "280px"}}
            disabled={filtered ? "disabled" : ""}
          ></input>
          <button>{filtered ? "Unfilter" : "Filter"}</button>
          <div className="greenbox">
          {
            tweets.map((tweet) => (
            <p key={tweet.id} align="left">[{tweet.author}]:<br/> <b>{tweet.content}</b>, "{(new Date(parseInt(Number(tweet.createdAt) * 1000))).toLocaleString()}"</p>
            ))
          }
          </div>
        </form>
      </div>
      { /* Upload Tweet */ }
      <div>
        <form onSubmit={e => submitUploadTweetHandle(e)}>
          <textarea
            className="uploadBox"
            cols="50"
            rows="5"
          ></textarea>
          <button className="submitButton">Upload</button>
        </form>
      </div>
    </div>
  );
}

export default Tweets;