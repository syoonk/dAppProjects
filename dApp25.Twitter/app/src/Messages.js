import React, { useEffect, useState } from "react"

import "./Messages.css";

function Messages({contract, accounts}) {
  const [counts, setCounts] = useState(5);
  const [messages, setMessages] = useState([]);
  const [talkingTo, setTalkingTo] = useState(undefined);

  useEffect(() => {
    setMessages([]);
    setTalkingTo("");
    document.getElementById("talkTo").value="";
  }, [accounts]);

  useEffect(() => {
    getMessages();
  }, [talkingTo]);

  const getMessages = async () => {
    let messagesCopy = [];

    try {
      messagesCopy = await contract.methods.getLatestMessagesWith(talkingTo, counts).call({from: accounts[0]});
    } catch {
      messagesCopy = [];
    }

    setMessages(messagesCopy);
  };

  function submitLoadMessageHandle(e) {
    e.preventDefault();
    
    const talkingToCopy = (e.target.elements[1].value).trim();
        
    setTalkingTo(talkingToCopy);

    // getMessages();
  }

  const changeCountsHandle = (e) => {
    e.preventDefault();

    const countsCopy = e.target.value;
    setCounts(countsCopy);
  };

  const submitUploadMessageHandle = async (e) => {
    e.preventDefault();

    const message = e.target.elements[0].value;
    await contract.methods.sendMessage(message, talkingTo).send({from: accounts[0]});

    getMessages();
  }
  
  return (
    <div>
      <h2>Private Message</h2>
      { /* Get Message */ }
      <div>  
        <form onSubmit={e => submitLoadMessageHandle(e)}>
          Counts:&nbsp;
          <input
            className="countBox" 
            type="number"
            value={counts}
            min="0"
            onChange={e => changeCountsHandle(e)}
          ></input>
          &nbsp;&nbsp;&nbsp;Open: &nbsp;
          <input 
            id="talkTo"
            type="text"
            placeholder="address here"
            style={{width: "320px"}}
          ></input>
          <button>Submit</button>
          <div className="messageBox">
            <p><b>Talking To: {talkingTo}</b></p>
          {
            messages.map((message) => (
              <p key={message.id} align={ message.from.toLowerCase() === accounts[0].toLowerCase()
                ? "right" : "left" }>{message.content}</p>
            ))
          }
          </div>
        </form>
      </div>
      { /* Upload Message */ }
      <div>
        <form onSubmit={e => submitUploadMessageHandle(e)}>
          <textarea
            className="uploadBox"
            cols="50"
            rows="5"
          ></textarea>
          <button className="submitButton">Send</button>
        </form>
      </div>
    </div>
  );
}

export default Messages;