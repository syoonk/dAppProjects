const Fibonacci = artifacts.require("Fibonacci");

contract("Fibonacci", (accounts) => {
  let fibonacci = null;
  before(async () => {
    fibonacci = await Fibonacci.deployed();
  });

  it('should calculate fibonacci correctly', async () => {
    const result = await fibonacci.fib(10);
    assert.equal(result.toNumber(), 55);
  });
});