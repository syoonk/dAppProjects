pragma solidity ^0.5.2;

contract Fibonacci {
    function fib(uint _n) pure external returns (uint) {
        if(_n == 0) return 0;
        
        uint fi_1 = 1;
        uint fi_2 = 1;
        
        for(uint i = 2; i < _n; i++) {
            uint fi = fi_1 + fi_2;
            fi_2= fi_1;
            fi_1 = fi;
        }
        
        return fi_1;
    }
}