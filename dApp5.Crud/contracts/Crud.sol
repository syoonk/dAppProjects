pragma solidity ^0.5.0;

contract Crud {
    struct User {
        uint id;
        string name;
    }

    User[] public users;
    uint public nextId = 1;

    function create(string memory _name) public {
        users.push(User(nextId, _name));
        nextId++;
    }

    function read(uint _id) public view returns(uint, string memory) {
        uint i = _findId(_id);
        return(users[i].id, users[i].name);
    }

    function update(uint _id, string memory _name) public {
        uint i = _findId(_id);
        users[i].name = _name;
    }

    function destroy(uint _id) public {
        uint i = _findId(_id);
        delete users[i];
    }

    function _findId(uint _id) internal view returns(uint) {
        require(_id != 0, 'id = 0 is not available');
        for(uint i = 0; i < users.length; i++) {
            if(users[i].id == _id) {
                return i;
            }
        }

        revert('User does not exist!');
    }
}