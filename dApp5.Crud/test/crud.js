const Crud = artifacts.require("Crud");

contract('Crud', () => {
    let crud = null;
    before(async() => {
        crud = await Crud.deployed();
    });

    it('should create new user', async() => {
        await crud.create('Kong');
        const user = await crud.read(1);    // test read also 
        assert(user[0].toNumber);
        assert(user[1] === 'Kong');
    });

    it('should update user', async() => {
        await crud.update(1, 'Kkong');
        const user = await crud.read(1);
        assert(user[0].toNumber);
        assert(user[1] === 'Kkong');
    });

    it('should NOT update non-existing user', async() => {
        try {
            await crud.update(2, 'Konggg');
        } catch(e) {
            assert(e.message.includes('User does not exist!'));
            // exit and execute next test...
            return;
        }
        // We know it would make the error on contract but it will pass the test without any issue.
        // that's not I want, so add this if it does not catch the error.
        assert(false);
    });

    it('should destroy user', async() => {
        await crud.destroy(1);
        
        try {
            const user = (await crud.read(1)).toNumber();
        } catch (e) {
            assert(e.message/*.includes('User does not exist!')*/);
            console.log(e.message);
            return;
        }

        assert(false);
    });

    it('should not destory non-existing user', async() => {
        try {
            await crud.destroy(10);
        } catch (e) {
            assert(e.message.includes('User does not exist!'));

            return;
        }

        assert(false);
    });

    it('id 0 should not be called', async() => {
        try {
            await crud.destory(0);
        } catch (e) {
            assert(e.message.includes('id = 0 is not available'));
          //  console.log(e.message);
            return;
        }   

        assert(false);
    });
});