import React from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

import KittyList from"./KittyList";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractForm, ContractData } = newContextComponents;

export default () => {
  const { drizzle } = useDrizzle();
  const drizzleState = useDrizzleState(state => state);

  return (
    <div>
      <div>
        <h2>Breed</h2>
        <ContractForm
          drizzle={drizzle}
          contract="CryptoKitty"
          method="breed"
        ></ContractForm>
      </div>
      <div>
        <h2>Player Kitties</h2>
        <ContractData
          drizzle={drizzle}
          drizzleState={drizzleState}
          contract="CryptoKitty"
          method="tokenURIBase"
          render={
            (uriBase) => {
              return (
                <ContractData
                  drizzle={drizzle}
                  drizzleState={drizzleState}
                  contract="CryptoKitty"
                  method="getAllKittiesOf"
                  methodArgs={[drizzleState.accounts[0]]}
                  render={
                    (kitties) => (
                      <KittyList
                        kitties={kitties}
                        uriBase={uriBase}
                      ></KittyList>
                    )
                  }
                ></ContractData>
              )
            }
          }
        ></ContractData>
      </div>
    </div>
  );
};
