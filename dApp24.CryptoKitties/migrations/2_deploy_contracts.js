const CryptoKitty = artifacts.require("CryptoKitty");

module.exports = async (deployer, _networks, _acocunts) => {
  await deployer.deploy(CryptoKitty, "Kong", "Kongtty", "http://robohash.org");
  const cryptoKitty = await CryptoKitty.deployed();
  await Promise.all([
    cryptoKitty.mint(),
    cryptoKitty.mint(),
    cryptoKitty.mint(),
    cryptoKitty.mint()
  ]);
}