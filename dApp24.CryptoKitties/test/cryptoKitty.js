const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const CryptoKitty = artifacts.require("CryptoKitty");

contract("CryptoKitty", (accounts) => {
  let cryptoKitty = null;
  const [admin, player] = [accounts[0], accounts[1]];

  beforeEach(async () => {
    cryptoKitty = await CryptoKitty.new("Kong", "Kongtty", "Kong_Server");
  });

  it("should NOT mint if not admin", async () => {
    await expectRevert(
      cryptoKitty.mint({ from: player }),
      "only admin");
  });

  it("should mint", async () => {
    await cryptoKitty.mint({ from: admin });
    await cryptoKitty.mint({ from: admin });
    const firstKitty = await cryptoKitty.kitties(0);
    const secondKitty = await cryptoKitty.kitties.call(1);
    const firstOwner = await cryptoKitty.ownerOf(0);
    const secondOwner = await cryptoKitty.ownerOf.call(1);
    const nextId = await cryptoKitty.nextId();
  
    assert.equal(firstOwner, admin);  
    assert.equal(secondOwner, admin);  
    assert.equal((firstKitty.id).toNumber(), 0);
    assert.equal((secondKitty.id).toNumber(), 1);
    assert.equal((firstKitty.generation).toNumber(), 1);
    assert.equal((secondKitty.generation).toNumber(), 1);
    assert((firstKitty.geneA).toNumber() < 10 && (firstKitty.geneB).toNumber() < 10);
    assert((secondKitty.geneA).toNumber() < 10 && (secondKitty.geneB).toNumber() < 10);
    assert.equal(nextId.toNumber(), 2);
  });

  it("should NOT breed if token not exist", async () => {
    await cryptoKitty.mint({ from: admin });

    await expectRevert(
      cryptoKitty.breed(0, 1, { from: admin }),
      "both kitties must exist");
  });

  it("should NOT breed if sender not own first kitty", async () => {
    await cryptoKitty.mint({ from: admin });
    await cryptoKitty.mint({ from: admin });

    await expectRevert(
      cryptoKitty.breed(0, 1, { from: player }),
      "msg.sender must own first kitty");
  });

  it("should breed", async () => {
    await cryptoKitty.mint({ from: admin });
    await cryptoKitty.mint({ from: admin });
    // Kitty #0 is transferring to player
    await cryptoKitty.transferFrom(
      admin,
      player,
      0,
      { from: admin });
    // breed kitty #2
    await cryptoKitty.breed(0, 1, { from: player });
    const kitty = await cryptoKitty.kitties(2);
    const owner = await cryptoKitty.ownerOf(2);
    const nextId = await cryptoKitty.nextId();

    assert.equal(nextId.toNumber(), 3);
    assert.equal(owner, player);
    assert.equal((kitty.id).toNumber(), 2);
    assert.equal((kitty.generation).toNumber(), 2);
    assert((kitty.geneA).toNumber() < 10 && (kitty.geneB).toNumber() < 10);
  });
});