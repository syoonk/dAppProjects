pragma solidity ^0.5.8;
pragma experimental ABIEncoderV2;

import "./ERC721Token.sol";

contract CryptoKitty is ERC721Token {
    struct Kitty {
        uint id;
        uint generation;
        uint geneA;
        uint geneB;
    }
    mapping(uint => Kitty) public kitties;
    uint public nextId;
    address public admin;
    
    constructor(
        string memory _name,
        string memory _symbol,
        string memory _tokenURIBase)
    ERC721Token(_name, _symbol, _tokenURIBase) 
    public {
        admin = msg.sender;
    }
    
    function getAllKitties()
    external view
    returns(Kitty[] memory) {
        Kitty[] memory kittiesCopy = new Kitty[](nextId);
        for(uint i = 0; i < nextId; i++) {
            kittiesCopy[i] = kitties[i];
        }
        return kittiesCopy;
    }

    function getAllKittiesOf(address _owner)
    external view
    returns(Kitty[] memory) {
        uint length = balanceOf(_owner);
        Kitty[] memory kittiesCopy = new Kitty[](length);
        uint j;
        for(uint i = 0; i < nextId; i++) {
            if(ownerOf(i) == _owner) {
                kittiesCopy[j] = kitties[i];
                j++;
            }
        }
        return kittiesCopy;
    }

    function mint()
    external {
        require(admin == msg.sender, 
            "only admin");
        
        kitties[nextId] = Kitty(nextId, 1, _random(10), _random(10));
        _mint(nextId, msg.sender);
        nextId++;
    }
    
    function breed(uint _kitty1Id, uint _kitty2Id)
    external {
        require(_kitty1Id < nextId && _kitty2Id < nextId,
            "both kitties must exist");
        Kitty storage kitty1 = kitties[_kitty1Id];
        Kitty storage kitty2 = kitties[_kitty2Id];
        require(ownerOf(_kitty1Id) == msg.sender,
            "msg.sender must own first kitty");
            
        uint nextGen = kitty1.generation > kitty2.generation ? kitty1.generation + 1 : kitty2.generation + 1;
        uint geneA = _random(4) > 1 ? kitty1.geneA : kitty2.geneA;
        uint geneB = _random(4) > 1 ? kitty1.geneB : kitty2.geneB;
        kitties[nextId] = Kitty(nextId, nextGen, geneA, geneB);
        _mint(nextId, msg.sender);
        nextId++;
    }
    
    function _random(uint _max)
    internal view 
    returns(uint) {
        return uint(keccak256(abi.encodePacked(block.timestamp, block.difficulty))) % _max;
    }
}