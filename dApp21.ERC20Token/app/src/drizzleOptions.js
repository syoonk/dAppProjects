import ERC20Token from "./contracts/ERC20Token";

const options = {
  contracts: [ERC20Token]
};

export default options;