import React from 'react';
import { Drizzle } from "@drizzle/store";
import { drizzleReactHooks } from "@drizzle/react-plugin";

import options from "./drizzleOptions.js";
import LoadingContainer from "./LoadingContainer.js";
import TokenMetadata from "./TokenMetadata.js";
import TokenWallet from "./TokenWallet.js";

const drizzleOptions = new Drizzle(options);
const { DrizzleProvider } = drizzleReactHooks;

function App() {
  return (
    <div className="container">
      <h1>ERC20 Token</h1>
        <DrizzleProvider drizzle={drizzleOptions}>
          <LoadingContainer>
            <TokenMetadata></TokenMetadata>
            <TokenWallet></TokenWallet>
          </LoadingContainer>
        </DrizzleProvider>
    </div>
  );
}

export default App;
