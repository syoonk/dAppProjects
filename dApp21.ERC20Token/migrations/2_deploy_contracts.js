const ERC20Token = artifacts.require("ERC20Token");

module.exports = (deployer, _network, _accounts) => {
  deployer.deploy(ERC20Token, 'kong', 'KNG', 18, web3.utils.toWei(`1`), web3.utils.toWei(`1`, 'wei'));
};