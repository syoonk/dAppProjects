const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const ERC20Token = artifacts.require("ERC20Token");

const tokenPrice = web3.utils.toBN(web3.utils.toWei('2'));
const tokenSupply = web3.utils.toBN(100);

contract("ERC20Token", (accounts) => {
  let erc20Token = null;
  beforeEach(async () => {
    erc20Token = await ERC20Token.new('kongToken', 'kng', 18, tokenSupply, tokenPrice);
  });

/*
	1. should return total Supply
	2. should return correct balance
	3. should transfer tokens
	4. should NOT transfer token if amount not enough
	5. should transfer token when approved
should NOT transfer token if not approved
*/

  it("should return total supply", async () => {
    const totalSupply = web3.utils.toBN(await erc20Token.totalSupply());

    assert(totalSupply.eq(tokenSupply));
  });

  it("should return correct balance", async () => {
    const balance = web3.utils.toBN(await erc20Token.balanceOf(accounts[0]));

    assert(balance.eq(tokenSupply));
  });

  it("should transfer tokens", async () => {
    const amount = web3.utils.toBN(10);
    const beforeBalance = web3.utils.toBN(
      await erc20Token.balanceOf(accounts[0]));
    const receiptTransfer = await erc20Token.transfer(accounts[1], amount, {from: accounts[0]});
    const afterBalance = web3.utils.toBN(
      await erc20Token.balanceOf(accounts[0]));

    assert(afterBalance.eq(beforeBalance.sub(amount)));
    expectEvent(receiptTransfer, 'Transfer', {
      from: accounts[0],
      to: accounts[1],
      tokens: amount
    });
  });

  it("should NOT transfer token if amount not enough", async () => {
    const amount = web3.utils.toBN(110);
    
    await expectRevert(
      erc20Token.transfer(accounts[1], amount, {from: accounts[0]}),
      "balance is not enough"
    );
  });

  it("should transfer token when approved", async () => {
    const amount = web3.utils.toBN(50);
    const receiptApprove = await erc20Token.approve(accounts[1], amount);
    
    await erc20Token.transferFrom(accounts[0], accounts[1], amount, {from: accounts[1]});
    expectEvent(receiptApprove, 'Approval', {
      tokenOwner: accounts[0],
      spender: accounts[1],
      tokens: amount
    });
  });

  it("should NOT transfer token if not approved", async () => {
    const amount = web3.utils.toBN(50);
    
    await expectRevert(
      erc20Token.transferFrom(accounts[0], accounts[1], amount, {from: accounts[1]}),
      "allowed balance is not enough"
    );
  });
});