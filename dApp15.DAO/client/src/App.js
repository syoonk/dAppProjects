import React, { useEffect, useState } from "react";
import DAO from "./contracts/DAO.json";
import Web3 from "web3";
// import { getWeb3 } from './utils.js';

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [contract, setContract] = useState(undefined);
  const [admin, setAdmin] = useState(undefined);
  const [totalShares, setTotalShares] = useState(undefined);
  const [proposals, setProposals] = useState([]);
  const [availableFunds, setAvailableFunds] = useState(0);
  const [userShare, setUserShare] = useState(0);

  useEffect(() => {
    const init = async () => {
      // Modern dapp browsers...
      let web3;

      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        // setWeb3(web3);
        try {
          // Request account access if needed
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
          // Acccounts now exposed
        } catch (error) {
          throw error;
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
        // Use Mist/MetaMask's provider.
        web3 = window.web3;
        // setWeb3(web3);
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }
      // Fallback to localhost; use dev console port by default...
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        // setWeb3(web3);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = DAO.networks[networkId];
      const contract = new web3.eth.Contract(
        DAO.abi,
        deployedNetwork && deployedNetwork.address
      );

      const admin = await contract.methods.admin().call();

      setWeb3(web3);
      setContract(contract);
      setAdmin(admin);
    };
    init();
    window.ethereum.on("accountsChanged", accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined" &&
      typeof admin !== "undefined"
    );
  };

  useEffect(() => {
    if (isReady()) {
      updateTotalShares();
      updateUserShare();
      updateFunds();
      updateProposals();
    }
  }, [accounts, contract, web3, admin]);

  async function updateUserShare() {
    const share = await contract.methods.shares(accounts[0]).call();

    setUserShare(share);
  }

  async function updateFunds() {
    const funds = await contract.methods.availableFunds().call();

    setAvailableFunds(funds);
  }

  async function updateTotalShares() {
    const totalShares = await contract.methods.totalShares().call();

    setTotalShares(totalShares);
  }

  async function updateProposals() {
    const nextProposalId = parseInt(
      await contract.methods.nextProposalId().call()
    );
    const proposals = [];

    for (let i = 0; i < nextProposalId; i++) {
      const [proposal, hasVoted] = [
        await contract.methods.proposals(i).call(),
        await contract.methods.votes(accounts[0], i).call()
      ];
      proposals.push({ ...proposal, hasVoted });
    }
    setProposals(proposals);

    updateFunds();
  }

  async function clickExecuteProposalHandle(proposalId) {
    await contract.methods
      .executeProposal(proposalId)
      .send({ from: accounts[0] });
    await updateProposals();
    await updateFunds();
  }

  async function submitWithdrawEtherHandle(e) {
    e.preventDefault();
    const amount = e.target.elements[0].value;
    const to = e.target.elements[1].value;

    await contract.methods
      .withdrawEther(amount, to)
      .send({ from: accounts[0] });
    await updateFunds();
  }

  async function submitContributeHandle(e) {
    e.preventDefault();
    const contributeAmount = e.target.elements[0].value;
    await contract.methods
      .contribute()
      .send({ from: accounts[0], value: contributeAmount });

    await updateTotalShares();
    await updateFunds();
    await updateUserShare();
  }

  async function submitRedeemSharesHandle(e) {
    e.preventDefault();
    const redeemAmount = e.target.elements[0].value;
    await contract.methods
      .redeemShare(redeemAmount)
      .send({ from: accounts[0] });

    await updateTotalShares();
    await updateFunds();
    await updateUserShare();
  }

  async function submitTransferSharesHandle(e) {
    e.preventDefault();
    const transferAmount = e.target.elements[0].value;
    const receiver = e.target.elements[1].value;
    await contract.methods
      .transferShare(transferAmount, receiver)
      .send({ from: accounts[0] });

    await updateUserShare();
  }

  async function clickVoteHandle(ballotId) {
    await contract.methods.vote(ballotId).send({ from: accounts[0] });

    updateProposals();
    updateFunds();
  }

  async function submitCreateProposalHandle(e) {
    e.preventDefault();
    const name = e.target.elements[0].value;
    const amount = parseInt(e.target.elements[1].value);
    const recipient = e.target.elements[2].value;
    await contract.methods
      .createProposal(name, amount, recipient)
      .send({ from: accounts[0] });

    updateProposals();
    updateFunds();
  }

  function isFinished(proposal) {
    const now = new Date().getTime();
    const proposalEnd = new Date(parseInt(proposal.end) * 1000);
    return proposalEnd > now > 0 ? false : true;
  }

  if (!isReady()) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">DAO</h1>

      <p>Total Shares: {totalShares}</p>
      <p>Available Funds: {availableFunds}</p>
      <p>Contributed : {userShare}</p>

      {accounts[0].toLowerCase() === admin.toLowerCase() ? (
        <>
          <div className="row">
            <div className="col-sm-12">
              <h2>Withdraw ether</h2>
              <form onSubmit={e => submitWithdrawEtherHandle(e)}>
                <div className="form-group">
                  <label htmlFor="amount">Amount</label>
                  <input type="text" className="form-control" id="amount" />
                </div>
                <div className="form-group">
                  <label htmlFor="to">To</label>
                  <input type="text" className="form-control" id="to" />
                </div>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
          </div>
          <hr />
        </>
      ) : null}

      <div className="row">
        <div className="col-sm-12">
          <h2>Contribute</h2>
          <form onSubmit={e => submitContributeHandle(e)}>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="text" className="form-control" id="amount" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Redeem shares</h2>
          <form onSubmit={e => submitRedeemSharesHandle(e)}>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="text" className="form-control" id="amount" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Transfer shares</h2>
          <form onSubmit={e => submitTransferSharesHandle(e)}>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="text" className="form-control" id="amount" />
            </div>
            <div className="form-group">
              <label htmlFor="amount">Receiver</label>
              <input type="text" className="form-control" id="receiver" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Create proposal</h2>
          <form onSubmit={e => submitCreateProposalHandle(e)}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" />
            </div>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="text" className="form-control" id="amount" />
            </div>
            <div className="form-group">
              <label htmlFor="recipient">Recipient</label>
              <input type="text" className="form-control" id="recipient" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Proposals</h2>
          <table className="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Amount</th>
                <th>Recipient</th>
                <th>Votes</th>
                <th>Vote</th>
                <th>Ends on</th>
                <th>Executed</th>
              </tr>
            </thead>
            <tbody>
              {proposals.map(proposal => (
                <tr key={proposal.id}>
                  <td>{proposal.id}</td>
                  <td>{proposal.name}</td>
                  <td>{proposal.amount}</td>
                  <td>{proposal.recipient}</td>
                  <td>{proposal.votes}</td>
                  <td>
                    {isFinished(proposal) ? (
                      "Vote finished"
                    ) : proposal.hasVoted ? (
                      "You already voted"
                    ) : (
                      <button
                        onClick={e => clickVoteHandle(proposal.id)}
                        type="submit"
                        className="btn btn-primary"
                      >
                        Vote
                      </button>
                    )}
                  </td>
                  <td>
                    {new Date(parseInt(proposal.end) * 1000).toLocaleString()}
                  </td>
                  <td>
                    {proposal.executed ? (
                      "Yes"
                    ) : admin.toLowerCase() === accounts[0].toLowerCase() ? (
                      <button
                        onClick={e => clickExecuteProposalHandle(proposal.id)}
                        type="submit"
                        className="btn btn-primary"
                      >
                        Execute
                      </button>
                    ) : (
                      "No"
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
