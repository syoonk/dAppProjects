pragma solidity >=0.5.0 <0.7.0;

contract DAO {
    struct Proposal {
        uint256 id;
        string name;
        uint256 amount;
        address payable recipient;
        uint256 votes;
        uint256 end;
        bool executed;
        bool transferred;
    }

    mapping(address => bool) public investors;
    mapping(address => uint256) public shares; // address of investor to specific shares
    mapping(uint256 => Proposal) public proposals;
    mapping(address => mapping(uint256 => bool)) public votes; // did investor vote for this proposal(uint id)?
    uint256 public totalShares;
    uint256 public availableFunds;
    uint256 public contributionEnd;
    uint256 public nextProposalId;
    uint256 public voteTime;
    uint256 public quorum;
    address public admin;

    constructor(uint256 _contributionTime, uint256 _voteTime, uint256 _quorum)
        public
    {
        require(_quorum > 0 && _quorum <= 100, "quorum must be 0 < x <= 100");
        contributionEnd = now + _contributionTime;
        voteTime = _voteTime;
        quorum = _quorum;
        admin = msg.sender;
    }

    function contribute() external payable {
        require(now < contributionEnd, "Contribution time ended");
        investors[msg.sender] = true;
        shares[msg.sender] += msg.value;
        totalShares += msg.value;
        availableFunds += msg.value;
    }

    function redeemShare(uint256 _amount) external {
        require(shares[msg.sender] >= _amount, "not enough shares");
        require(availableFunds >= _amount, "not enough availableFunds");
        shares[msg.sender] -= _amount;
        totalShares -= _amount;
        availableFunds -= _amount;
        msg.sender.transfer(_amount);
    }

    function transferShare(uint256 _amount, address _to) external {
        require(shares[msg.sender] >= _amount, "not enough shares");
        shares[msg.sender] -= _amount;
        shares[_to] += _amount;
        investors[_to] = true;
    }

    function createProposal(
        string calldata _name,
        uint256 _amount,
        address payable _recipient
    ) external onlyInvestor() {
        require(availableFunds >= _amount, "amount too big");
        proposals[nextProposalId] = Proposal(
            nextProposalId,
            _name,
            _amount,
            _recipient,
            0,
            now + voteTime,
            false,
            false
        );
        availableFunds -= _amount;
        nextProposalId++;
    }

    function vote(uint256 _proposalId) external onlyInvestor() {
        Proposal storage proposal = proposals[_proposalId];
        require(now < proposal.end, "Voting time of this proposal ended");
        require(
            votes[msg.sender][_proposalId] == false,
            "Investor already voted this proposal"
        );
        votes[msg.sender][_proposalId] = true;
        proposal.votes += shares[msg.sender];
    }

    function executeProposal(uint256 _proposalId) external onlyAdmin() {
        Proposal storage proposal = proposals[_proposalId];
        require(now >= proposal.end, "proposal time is not ended");
        require(proposal.executed == false, "proposal already executed");
        require(
            proposal.transferred == false,
            "proposal fund already transferred"
        );

        if (((proposal.votes * 100) / totalShares) >= quorum) {
            proposal.transferred = true;
            _transferEther(proposal.amount, proposal.recipient);
        } else {
            availableFunds += proposal.amount; // cancel holding fund
        }

        proposal.executed = true;
    }

    function withdrawEther(uint256 _amount, address payable _to)
        external
        onlyAdmin()
    {
        availableFunds -= _amount;
        _transferEther(_amount, _to);
    }

    function _transferEther(uint256 _amount, address payable _to) internal {
        require(
            address(this).balance >= _amount,
            "There is not enough funds remained in contracts"
        );
        // require(_amount <= availableFunds, "not enough availableFunds");
        // availableFunds -= _amount;
        _to.transfer(_amount);
    }

    modifier onlyInvestor() {
        require(investors[msg.sender] == true, "only investors");
        _;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin, "only Admin");
        _;
    }
}
