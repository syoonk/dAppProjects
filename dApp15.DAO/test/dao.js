const DAO = artifacts.require("DAO");
const { expectRevert, time } = require("@openzeppelin/test-helpers");

contract("DAO", (accounts) => {
  let dao = null;
  before(async () => {
    dao = await DAO.deployed(5, 5, 60);
  });

  admin = accounts[0];
  investor1 = accounts[1];
  investor2 = accounts[2];
  investor3 = accounts[3];
  recipient = accounts[4];

  it('Should accept contribution', async () => {
    await dao.contribute({from: investor1, value: 200 });
    await dao.contribute({from: investor2, value: 300 });
    await dao.contribute({from: investor3, value: 400 });
    const total = await dao.totalShares();
    const avail = await dao.availableFunds();
    const share1= await dao.shares(investor1);
    const share2 = await dao.shares(investor2);
    const share3 = await dao.shares(investor3);

    assert.equal(total, 900);
    assert.equal(avail, 900);
    assert.equal(share1, 200);
    assert.equal(share2, 300);
    assert.equal(share3, 400);
  });

  
  it('Should NOT accept contribution after contributionTime', async () => {
    await time.increase(5001);
    await expectRevert(
      dao.contribute({from: investor1, value: 500}),
      "Contribution time ended"
    );
  });

  it('Should create proposal', async () => {
    // proposal id 0
    await dao.createProposal(`proposal0`, 100, recipient, {from: investor1});
    const proposal0 = await dao.proposals(0);
    const availFunds = await dao.availableFunds();

    assert.equal(proposal0.name, `proposal0`);
    assert.equal(proposal0.amount, 100);
    assert.equal(proposal0.recipient, recipient);
    assert.equal(availFunds, 800);
  });

  it('Should NOT create proposal if not from investor', async () => {
    await expectRevert(
      dao.createProposal(`proposal1`, 100, recipient, {from: admin}),
      "only investors"
    );
  });

  it('Should NOT create proposal if amount too big', async () => {
    await expectRevert (
      dao.createProposal(`proposal1`, 900, recipient, {from: investor1 }),
      "amount too big"
    );
  });

  it('Should vote', async () => {
    // proposal id 1
    await dao.createProposal(`proposal1`, 100, recipient, {from: investor1 });
    await dao.vote(1, {from: investor1});
    await dao.vote(1, { from: investor3 });
    const proposal1 = await dao.proposals(1);

    assert.equal(proposal1.votes, 600);
  });

  it('Should NOT vote if not investor', async () => {
    // proposal id 2
    await dao.createProposal(`proposal2`, 100, recipient, {from: investor1}); 
    await expectRevert(
      dao.vote(2, {from: admin}),
      "only investors"
    );
  });

  it('Should NOT vote if already voted', async () => {
    // proposal id 3
    await dao.createProposal(`proposal3`, 100, recipient, {from: investor1});
    await dao.vote(3, {from: investor2});
    await expectRevert(
      dao.vote(3, {from: investor2}),
      "Investor already voted this proposal"
    );
  });

  it('Should NOT vote if after proposal end date', async () => {
    await time.increase(5001);
    await expectRevert(
      dao.vote(3, {from: investor1}),
      "Voting time of this proposal ended"
    );
  });

  it('Should execute proposal', async () => {
    await time.increase(5001);
    const beforeBalance = web3.utils.toBN(await web3.eth.getBalance(recipient));
    // proposal 1 is executed, it should transfer ether
    await dao.executeProposal(1, {from: admin});
    const afterBalance = web3.utils.toBN(await web3.eth.getBalance(recipient));

    assert.equal(afterBalance.sub(beforeBalance).toNumber(), 100);

    const beforeBalance3 = web3.utils.toBN(await web3.eth.getBalance(recipient));
    const beforeAvailFund = web3.utils.toBN(await dao.availableFunds());
    // proposal 3 is executed, it should not transfer ether
    await dao.executeProposal(3, { from: admin });
    const afterBalance3 = web3.utils.toBN(await web3.eth.getBalance(recipient));
    const afterAvailFund = web3.utils.toBN(await dao.availableFunds());

    assert.equal(afterBalance3.sub(beforeBalance3), 0);
    // vote is not enough so funds should be withdrawn
    assert.equal(afterAvailFund.sub(beforeAvailFund).toNumber(), 100);
  });

  it('Should NOT execute proposal twice', async () => {
    await expectRevert(
      dao.executeProposal(3, { from: admin }),
      "proposal already executed"
    );
  });

  it('Should NOT execute proposal before end date', async () => {
    // proposal id 4
    await dao.createProposal(`proposal4`, 100, recipient, {from: investor1 });
    await expectRevert(
      dao.executeProposal(4, {from: admin}),
      "proposal time is not ended"
    );
  });

  it('Should withdraw ether', async () => {
    const beforeBalance = web3.utils.toBN(await web3.eth.getBalance(recipient));
    await dao.withdrawEther(100, recipient, {from: admin});
    const afterBalance = web3.utils.toBN(await web3.eth.getBalance(recipient));

    assert.equal(afterBalance.sub(beforeBalance).toNumber(), 100);
  });

  it('Should NOT withdraw ether if not admin', async () => {
    await expectRevert(
      dao.withdrawEther(100, recipient, {from: investor1}),
      "only Admin"
    );
  });

  it('Should NOT withdraw ether if trying to withdraw too much', async () => {
    await expectRevert(
      dao.withdrawEther(900, recipient, {from:admin}),
      "There is not enough funds remained in contracts"
    );
  });
});