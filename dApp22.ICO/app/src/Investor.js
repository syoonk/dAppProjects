import React, { useState, useEffect } from "react";
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractData, ContractForm } = newContextComponents;

export default () => {
  const [isInvestor, setIsInvestor] = useState(false);
  const { drizzle } = useDrizzle();
  const state = useDrizzleState(state => state);

  useEffect(() => {
    const init = async() => {
      const blackOrWhite = await drizzle.contracts.ICO.methods.isInvestor(state.accounts[0])
      .call();
      setIsInvestor(blackOrWhite);
    };
    init();
  }, []);
  
  if(!isInvestor) {
    return <p>You are not investor</p>;
  }

  return (
    <div>
      <div>
        <h2>Investment (token)</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="getSale"
            methodArgs={[state.accounts[0]]}
          ></ContractData>
      </div>
      <div>
        <h2>Buy</h2>
          <ContractForm
            drizzle={drizzle}
            contract="ICO"
            method="buy"
          ></ContractForm>
      </div>
    </div>
  );
}
