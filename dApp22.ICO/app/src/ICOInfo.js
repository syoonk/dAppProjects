import React from 'react';
import { drizzleReactHooks } from "@drizzle/react-plugin";
import { newContextComponents } from "@drizzle/react-components";

const { useDrizzle, useDrizzleState } = drizzleReactHooks;
const { ContractData } = newContextComponents;

export default () => {
  const { drizzle } = useDrizzle();
  const state = useDrizzleState(state => state);
  return (
    <div>
      <div>
        <h2>Token</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="token"
          ></ContractData>
      </div>
      <div>
        <h2>End</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="end"
            render={end => {
              if(!end) return 0;
              return (new Date(parseInt(end) * 1000)).toLocaleString();
            }}
          ></ContractData>
      </div>
      <div>
        <h2>Token Price (Token/Wei)</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="price"
          ></ContractData>
      </div>
      <div>
        <h2>Minimum Purchase (wei) </h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="minPurchase"
          ></ContractData>
      </div>
      <div>
        <h2>Maximum Purchase (wei)</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="maxPurchase"
          ></ContractData>
      </div>
      <div>
        <h2>Available Tokens</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="availableTokens"
          ></ContractData>
      </div>
      <div>
        <h2>Released</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="released"
          ></ContractData>
      </div>
      <div>
        <h2>Admin</h2>
          <ContractData
            drizzle={drizzle}
            drizzleState={state}
            contract="ICO"
            method="admin"
          ></ContractData>
      </div>
    </div>
  );
}