import React from "react";
import { Drizzle } from '@drizzle/store';
import { drizzleReactHooks } from "@drizzle/react-plugin";

import options from "./drizzleOptions";
import LoadingContainer from './LoadingContainer.js';
import ICOInfo from "./ICOInfo.js";
import Investor from "./Investor.js";
import Admin from "./Admin.js";

const drizzleOptions = new Drizzle(options);
const { DrizzleProvider } = drizzleReactHooks;

function App() {
  return (
    <div className="container">
      <h1>ICO</h1>
      <DrizzleProvider drizzle={drizzleOptions}>
        <LoadingContainer>
          <ICOInfo></ICOInfo>
          <Investor></Investor>
          <Admin></Admin>
        </LoadingContainer>
      </DrizzleProvider>
    </div>
  );
}

export default App;
