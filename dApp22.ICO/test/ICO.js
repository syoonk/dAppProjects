const { expectRevert, time } = require("@openzeppelin/test-helpers");
const ICO = artifacts.require("ICO");
const Token = artifacts.require("ERC20Token");

contract("ICO", accounts => {
  let ico;
  let token;
  const admin = accounts[0];
  const name = "Kong";
  const symbol = "KNG";
  const decimals = 18;
  const initialBalance = web3.utils.toBN(web3.utils.toWei("1000"));

  beforeEach(async () => {
    ico = await ICO.new(name, symbol, decimals, initialBalance);
    const tokenAddress = await ico.token();
    token = await Token.at(tokenAddress);
  });

  it("should create an erc20 token", async () => {
    const totalSupply = web3.utils.toBN(await token.totalSupply());
    const _name = await token.name();
    const _symbol = await token.symbol();
    const _decimals = await token.decimals();

    assert.equal(_name, name);
    assert.equal(_symbol, symbol);
    assert.equal(_decimals, decimals);
    assert(initialBalance.eq(totalSupply));
  });

  it("should start the ICO", async () => {
    const end = 120;
    const price = 2;
    const availableTokens = web3.utils.toBN(web3.utils.toWei(`100`));
    const minPurchase = web3.utils.toBN(web3.utils.toWei("1"));
    const maxPurchase = web3.utils.toBN(web3.utils.toWei("50"));
    // const startTime = parseInt(new Date().getTime() / 1000);
    // time.increaseTo(startTime);
    await ico.start(end, price, availableTokens, minPurchase, maxPurchase, {
      from: admin
    });
    // const expectedEndTime = web3.utils.toBN(startTime + end);
    // const _end = web3.utils.toBN(await ico.end());
    const _price = await ico.price();
    const _availableTokens = web3.utils.toBN(await ico.availableTokens());
    const _minPurchase = web3.utils.toBN(await ico.minPurchase());
    const _maxPurchase = web3.utils.toBN(await ico.maxPurchase());

    // assert.equal(expectedEndTime.idiv(1000), _end.idiv(1000));
    assert.equal(price, _price);
    assert(availableTokens.eq(_availableTokens));
    assert(minPurchase.eq(_minPurchase));
    assert(maxPurchase.eq(_maxPurchase));
  });

  it("should NOT start the ICO", async () => {
    const end = 120;
    const price = 2;
    const availableTokens = web3.utils.toBN(web3.utils.toWei(`100`));
    const minPurchase = web3.utils.toBN(web3.utils.toWei("1"));
    const maxPurchase = web3.utils.toBN(web3.utils.toWei("50"));
    // endTime
    await expectRevert(
      ico.start(0, price, availableTokens, minPurchase, maxPurchase),
      "end should be > 0"
    );
    // availableTokens > total
    await expectRevert(
      ico.start(
        end,
        price,
        web3.utils.toBN(web3.utils.toWei(`10000`)),
        minPurchase,
        maxPurchase
      ),
      "available tokens should be > 0 && < totalSupply"
    );
    // availableTokens = 0
    await expectRevert(
      ico.start(end, price, 0, minPurchase, maxPurchase),
      "available tokens should be > 0 && < totalSupply"
    );
    // minPurchase < price
    await expectRevert(
      ico.start(end, price, availableTokens, 1, maxPurchase),
      "minimum purchase should be >= price && <= maximumPurchase"
    );
    // minPurchase > maxPurchase
    await expectRevert(
      ico.start(end, price, availableTokens, 1000, 100),
      "minimum purchase should be >= price && <= maximumPurchase"
    );
    // maxPurchase < minPurchase ==> Same requiement with above
    // maxPurchase > availTokenValue;
    await expectRevert(
      ico.start(
        end,
        price,
        availableTokens,
        minPurchase,
        web3.utils.toBN(web3.utils.toWei("60"))
      ),
      "maximum purchase should be > minimum && <= availTokenValue"
    );
    // admin
    await expectRevert(
      ico.start(end, price, availableTokens, minPurchase, maxPurchase, 
        { from: accounts[1] }),
      "only admin"
    );
    // already activated
    await ico.start(end, price, availableTokens, minPurchase, maxPurchase);

    await expectRevert(
      ico.start(end, price, availableTokens, minPurchase, maxPurchase),
      "ICO is already activated"
    );
  });

  context("Sale started", () => {
    // let start;
    const end = 100;
    const price = 2;
    const availableTokens = web3.utils.toBN(web3.utils.toWei("30"));
    const minPurchase = web3.utils.toBN(web3.utils.toWei("1"));
    const maxPurchase = web3.utils.toBN(web3.utils.toWei("10"));
    beforeEach(async () => {
      // start = parseInt((new Date()).getTime() / 1000);
      // time.increaseTo(start);
      ico.start(end, price, availableTokens, minPurchase, maxPurchase);
      for (let i = 1; i <= 3; i++) {
        await ico.whitelist(accounts[i]);
      }
    });

    it("should NOT let non-investors buy", async () => {
      await expectRevert(
        ico.buy(
          { from: accounts[4], value: minPurchase }),
        "only investors"
      );
    });

    it("should NOT buy non-multiple of price", async () => {
      const buyAmount = minPurchase.add(web3.utils.toBN(1));

      await expectRevert(
        ico.buy(
          { from: accounts[1], value: buyAmount }),
        "have to send a multiple of price"
      );
    });

    it("should NOT buy if not between min and max purchase", async () => {
      const buyAmount = web3.utils.toBN(web3.utils.toBN("12"));

      await expectRevert(
        ico.buy(
          { from: accounts[1], value: buyAmount }),
        "value must follow >= minPurchase, <= maxPurchase"
      );
    });

    it("should NOT buy if not enough tokens left", async () => {
      for(let i = 1; i <= 3; i++) {
        await ico.buy(
          {from: accounts[i], value: web3.utils.toBN(web3.utils.toWei(`5`))});
      }
      await ico.whitelist(accounts[5]);

      await expectRevert(
        ico.buy(
          { from: accounts[5], value: minPurchase }),
        "Not enough token left for sale"
      );
    });

    it.only('full ico process: investors buy, admin release and withdraw', async () => {
      const priceBN = web3.utils.toBN(price);
      const purchaseAmountEther = web3.utils.toBN(web3.utils.toWei(`5`))
      await ico.buy(
        {from: accounts[1], value: purchaseAmountEther});

      // check not release ICO not END
      await expectRevert(
        ico.release(
          {from: admin}),
        "ICO must have ended"
      );

      await ico.buy({from: accounts[2], value: purchaseAmountEther});
      await ico.buy({from: accounts[3], value: purchaseAmountEther});

      let beforeBalance = [];
      for(let i = 1; i <= 3; i++) {
        beforeBalance[i] = web3.utils.toBN(await ico.getBalance(
          {from: accounts[i]}));
      }
      time.increase(end * 1000);
      await ico.release({from: admin});
      let afterBalance = [];
      for(let i = 1; i <= 3; i++) {
        afterBalance[i] = web3.utils.toBN(await ico.getBalance(
          {from: accounts[i]}));
      }
      const purchaseAmountToken = purchaseAmountEther.mul(priceBN);

      // Check release
      for(let i = 1; i <= 3; i++) {
        assert(afterBalance[i].eq(beforeBalance[i].add(purchaseAmountToken)));
      }

      // check not release twice
      await expectRevert(
        ico.release({from: admin}),
        "tokens must not have been released"
      );

      const withDrawBalance = web3.utils.toBN(web3.utils.toWei('15'));
      const beforeBalanceA = web3.utils.toBN(await web3.eth.getBalance(admin));
      const withdrawReceipt = await ico.withdraw(admin, withDrawBalance, 
        {from: admin, gasPrice: 1});
      const afterBalanceA = web3.utils.toBN(await web3.eth.getBalance(admin));
      const usedGas = web3.utils.toBN(withdrawReceipt.receipt.gasUsed);
      
      // Check withdraw
      assert(afterBalanceA.eq(beforeBalanceA.add(withDrawBalance).sub(usedGas)));
    });
  });
});
