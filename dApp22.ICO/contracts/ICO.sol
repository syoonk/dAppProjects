pragma solidity ^0.5.7;

import "./ERC20Token.sol";
/*
    Restraints: Created token price must be cheaper than 1 wei
*/
contract ICO {
    struct Sale {
        address investor;
        uint quantity;
        bool released;
    }
    Sale[] public sales;
    mapping(address => bool) public investors;
    address public token;
    address public admin;
    uint public end;
    uint public price;  // tokens per wei (token must be cheaper than 1 wei)
    uint public availableTokens;
    uint public minPurchase;    // amount of ether(not token)
    uint public maxPurchase;    // amount of ether(not token)
    bool public released;
    
    constructor(
        string memory _name,
        string memory _symbol,
        uint8 _decimals,
        uint _initialSupply )
    public {
        token = address(new ERC20Token(
                                _name,
                                _symbol,
                                _decimals,
                                _initialSupply ));
        admin = msg.sender;
    }
    
    function getSale(address _investor)
    external view 
    returns(uint) {
        for(uint i = 0; i < sales.length; i++) {
            if(sales[i].investor == _investor) {
                return sales[i].quantity;
            }
        }
        return 0;
    }

    function isInvestor(address _investor)
    external view
    returns(bool) {
        return investors[_investor];
    }

    function start(
        uint _end,
        uint _price,
        uint _availableTokens,
        uint _minPurchase,
        uint _maxPurchase )
    external
    onlyAdmin() icoNotActive() {
        require(_end > 0, 
            "end should be > 0");
        uint totalSupply = ERC20Token(token).totalSupply();
        uint availCoinToEther = _availableTokens / _price;
        require(_availableTokens > 0 && _availableTokens <= totalSupply,
            "available tokens should be > 0 && < totalSupply");
        require(_minPurchase >= 0 && _minPurchase <= _maxPurchase, 
            'minimum purchase should be >= 0 && <= maximumPurchase');
        require(_maxPurchase > _minPurchase && _maxPurchase <= availCoinToEther,
            "maximum purchase should be > minimum && <= availTokenValue");
        
        end = _end + now;
        price = _price;
        availableTokens = _availableTokens;
        minPurchase = _minPurchase;
        maxPurchase = _maxPurchase;
    }
    
    function whitelist(address _investor)
    external
    onlyAdmin() {
        investors[_investor] = true;
    }
    
    function buy()
    external payable
    onlyInvestors() icoActive() {
        require(msg.value % price == 0,
            "have to send a multiple of price");
        require(msg.value >= minPurchase && msg.value <= maxPurchase,
            "value must follow >= minPurchase, <= maxPurchase");
        uint quantity = price * msg.value;
        require(quantity <= availableTokens,
            "Not enough token left for sale");
        availableTokens -= quantity;
        sales.push(Sale(msg.sender, quantity, false));
    }
    
    function release()
    external
    onlyAdmin() icoEnded() tokensNotReleased() {
        ERC20Token tokenInstance = ERC20Token(token);
        for(uint i = 0; i < sales.length; i++) {
            Sale storage sale = sales[i];
            require(sale.released == false, 
                "This coins are already released");
            sale.released = true;
            tokenInstance.transfer(sale.investor, sale.quantity);
        }
        
        released = true;
    }
    
    function withdraw(address payable _to, uint _amount)
    external
    onlyAdmin() icoEnded() tokensReleased() {
        _to.transfer(_amount);
    }
    
    function getInvestedBalance()
    external view
    onlyAdmin()
    returns(uint) {
        return(address(this).balance);
    }
    
    function getBalance()
    external view 
    returns(uint) {
        return ERC20Token(token).balanceOf(msg.sender);
    }
    
    modifier tokensNotReleased() {
        require(released == false,
            "tokens must not have been released");
        _;
    }
    
    modifier tokensReleased() {
        require(released == true,
            "tokens are not released yet");
        _;
    }
    
    modifier onlyInvestors() {
        require(investors[msg.sender] == true,
            "only investors");
        _;
    }
    
    modifier icoEnded() {
        require(end > 0 && (now >= end || availableTokens == 0),
            "ICO must have ended");
        _;
    }
    
    modifier icoActive() {
        require(end > 0 && now < end,
            "ICO must be active");
        _;
    }
    
    modifier icoNotActive() {
        require(end == 0, 
            "ICO is already activated");
        _;
    }
    
    modifier onlyAdmin() {
        require(msg.sender == admin,
            "only admin");
        _;
    }
}