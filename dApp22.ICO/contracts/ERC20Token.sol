pragma solidity ^0.5.7;

contract ERC20Interface {
    function transfer(address to, uint tokens) public returns (bool success);
    function transferFrom(address from, address to, uint tokens) public returns (bool success);
    function balanceOf(address tokenOwner) public view returns (uint balance);
    function approve(address spender, uint tokens) public returns (bool success);
    function allowance(address tokenOwner, address spender) public view returns (uint remaining);
    function totalSupply() public view returns (uint);

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);
}

contract ERC20Token is ERC20Interface {
    string public name;
    string public symbol;
    uint8 public decimals;
    uint _totalSupply;
    // balances(uint) of TokenHolders(address)
    mapping(address => uint) public balances;
    // From => to
    mapping(address => mapping(address => uint)) public allowed;
    
    constructor(
        string memory _name, 
        string memory _symbol, 
        uint8 _decimals, 
        uint _initialSupply )
    public {
        name = _name;
        symbol = _symbol;
        decimals = _decimals;
        _totalSupply = _initialSupply;
        balances[msg.sender] = _totalSupply;
    }
    
    function transfer(address _to, uint _amount)
    public
    returns(bool) {
        require(balances[msg.sender] >= _amount,
            "balance is not enough");
        
        balances[msg.sender] -= _amount;
        balances[_to] += _amount;
        emit Transfer(msg.sender, _to, _amount);
        return true;
    }
    
    function transferFrom(address _from, address _to, uint _amount)
    public
    returns(bool) {
        require(balances[_from] >= _amount,
            "balance is not enough");
        require(allowed[_from][msg.sender] >= _amount,
            "allowed balance is not enough");
        
        allowed[_from][msg.sender] -= _amount;
        balances[_from] -= _amount;
        balances[msg.sender] += _amount;
        emit Transfer(_from, msg.sender, _amount);
        return true;
    }
    
    function approve(address _spender, uint _amount)
    public
    returns(bool) {
        require(_spender != msg.sender,
            "sender should not be spender");
        
        allowed[msg.sender][_spender] = _amount;
        emit Approval(msg.sender, _spender, _amount);
        return true;
    }
    
    function allowance(address _owner, address _spender)
    public view
    returns(uint) {
        return allowed[_owner][_spender];
    }
    
    function balanceOf(address _owner)
    public view 
    returns(uint) {
        return balances[_owner];
    }
    
    function totalSupply()
    public view
    returns(uint) {
        return _totalSupply;
    }
}