const ERC20Token = artifacts.require("ERC20Token");
const ICO = artifacts.require("ICO");

const initialSupply = web3.utils.toBN(web3.utils.toWei('200'));

module.exports = (deployer, _networks, _accounts) => {
  deployer.deploy(ICO, "kong", "KNG", 18, initialSupply);
}