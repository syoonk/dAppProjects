const MultiSig = artifacts.require("MultiSig");

module.exports = (deployer, networks, _accounts) => {
  deployer.deploy(
    MultiSig,
    [_accounts[0], _accounts[1], _accounts[2], _accounts[3]],
    2,
    { value: 1000 }
  );
};
