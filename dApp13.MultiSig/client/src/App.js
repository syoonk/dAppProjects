import React, { useEffect, useState } from "react";
import MultiSig from "./contracts/MultiSig.json";
import { getWeb3 } from "./utils.js";

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [contract, setContract] = useState(undefined);
  const [balance, setBalance] = useState(null);
  const [currentTransfer, setCurrentTransfer] = useState(undefined);
  const [quorum, setQuorum] = useState(0);

  useEffect(() => {
    const init = async () => {
      const web3 = await getWeb3();
      const accounts = await web3.eth.getAccounts();
      const networkId = await web3.eth.net.getId();
      const deployedNetwork = MultiSig.networks[networkId];
      const contract = new web3.eth.Contract(
        MultiSig.abi,
        deployedNetwork && deployedNetwork.address
      );

      const quorum = await contract.methods.quorum().call();

      setWeb3(web3);
      setAccounts(accounts);
      setContract(contract);
      setQuorum(quorum);
    };
    init();

    window.ethereum.on("accountsChanged", _accounts => {
      setAccounts(_accounts);
    });
  }, []);
  
  useEffect(() => {
    if (typeof contract !== "undefined" && typeof web3 !== undefined) {
      updateBalance();
      updateCurrentTransfer();
    }
  }, [accounts, contract, web3]);

  if (!web3) {
    return <div>Loading...</div>;
  }

  const updateBalance = async () => {
    const balance = await web3.eth.getBalance(contract.options.address);
    setBalance(balance);
  };

  const updateCurrentTransfer = async () => {
    let currentTransferId = (await contract.methods.nextId().call() - 1);

    if (currentTransferId >= 0) {
      let currentTransfer = await contract.methods
        .transfers(currentTransferId)
        .call();
      let alreadyApproved = await contract.methods
        .approvals(accounts[0], currentTransferId)
        .call();

      setCurrentTransfer({ ...currentTransfer, alreadyApproved });
    }
  };

  const createSubmitHandle = async e => {
    e.preventDefault();
    const amount = e.target.elements[0].value;
    const to = e.target.elements[1].value;

    await contract.methods
      .createTransfer(amount, to)
      .send({ from: accounts[0] });
  };

  const sendClickHandle = async e => {
    await contract.methods
      .sendTransfer(currentTransfer.id)
      .send({ from: accounts[0] });

    updateBalance();
    updateCurrentTransfer();
  };

  return (
    <div className="container">
      <h1 className="text-center">Multisig</h1>

      <div className="row">
        <div className="col-sm-12">
          <p>
            Balance: <b>{balance}</b> wei{" "}
          </p>
        </div>
      </div>

      <div className="row">
        <div className="col-sm-12">
          <label>Set Transfer Id</label>
          <input type="number" className="form-control" id="Transfer Id"></input>
          <button></button>
        </div>
      </div>

      {!currentTransfer || currentTransfer.approvals > quorum || currentTransfer.sent ? (
        <div className="row">
          <div className="col-sm-12">
            <h4> current Account : {accounts} </h4>
            <h2>Create transfer</h2>
            <form onSubmit={e => createSubmitHandle(e)}>
              <div className="form-group">
                <label htmlFor="amount">Amount</label>
                <input type="number" className="form-control" id="amount" />
              </div>
              <div className="form-group">
                <label htmlFor="to">To</label>
                <input type="text" className="form-control" id="to" />
              </div>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </form>
          </div>
        </div>
      ) : (
        <div className="row">
          <div className="col-sm-12">
            <h2>Approve transfer</h2>
            <ul>
              <li>Transfer Id: {currentTransfer.id}</li>
              <li>Amount: {currentTransfer.amount}</li>
              <li>Approvals: {currentTransfer.approvals}</li>
            </ul>
            {currentTransfer.alreadyApproved ? (
              "Already Approved"
            ) : (
              <button
                onClick={e => sendClickHandle(e)}
                type="submit"
                className="btn btn-primary"
              >
                Submit
              </button>
            )}
          </div>
        </div>
      )}
    </div>
  );
}

export default App;
