const { expectRevert } = require("@openzeppelin/test-helpers");
const MultiSig = artifacts.require("MultiSig");

/* contract has 1000 Wei balance given from deployment */

contract("MultiSig", (accounts) => {
  let multiSig = null;
  before(async () => {
    multiSig = await MultiSig.deployed();
  });

  it("should create transfer", async () => {
    await multiSig.createTransfer(100, accounts[5], { from: accounts[0] });
    const transfer = await multiSig.transfers(0);
    assert.equal(transfer.id, 0);
    assert.equal(transfer.amount.toNumber(), 100);
  });

  it("should NOT create transfer", async () => {
    await expectRevert(
      multiSig.createTransfer(
        100,
        accounts[5],
        { from: accounts[6] },
      ), "only approver allowed"
    );
  });

  it("should NOT send transfer if the quorum not reached", async () => {
    const balanceBefore = web3.utils.toBN(await web3.eth.getBalance(accounts[7]));
    await multiSig.createTransfer(100, accounts[7], { from: accounts[0] });
    await multiSig.sendTransfer(1, { from: accounts[1] });
    const balanceAfter = web3.utils.toBN(await web3.eth.getBalance(accounts[7]));

    assert(balanceAfter.sub(balanceAfter).isZero());
  });

  it("should send transfer if the quorum reached", async () => {
    const balanceBefore = web3.utils.toBN(await web3.eth.getBalance(accounts[7]));
    await multiSig.createTransfer(100, accounts[7], { from: accounts[0] });
    await multiSig.sendTransfer(2, { from: accounts[1] });
    await multiSig.sendTransfer(2, { from: accounts[0] });
    await multiSig.sendTransfer(2, { from: accounts[2] });
    const balanceAfter = web3.utils.toBN(await web3.eth.getBalance(accounts[7]));

    assert.equal(balanceAfter.sub(balanceBefore).toNumber(), 100);
  });
});
