pragma solidity ^0.5.2;

contract MultiSig {
    address[] public approvers;
    uint public quorum;
    struct Transfer {
        uint id;
        uint amount;
        address payable to;
        uint approvals;
        bool sent;
    }
    
    mapping(uint => Transfer) public transfers;
    mapping(address => mapping(uint => bool)) public approvals;
    uint public nextId;
    
    constructor(address[] memory _approvers, uint _quorum) public payable {
        approvers = _approvers;
        quorum = _quorum;
    }
    
    function createTransfer(uint _amount, address payable _to) external onlyApprover() {
        transfers[nextId] = Transfer(nextId, _amount, _to, 0, false);
        
        nextId++;
    }
    
    function sendTransfer(uint _id) external onlyApprover() {
        require(transfers[_id].sent == false, "transfer is already sent");
        require(approvals[msg.sender][_id] == false, "This approver has already approve this transfer");

        // if(approvals[msg.sender][_id] == false) {
        //     approvals[msg.sender][_id] = true;
        //     transfers[_id].approvals++;
        // }
        
        approvals[msg.sender][_id] = true;
        transfers[_id].approvals++;
        
        if(transfers[_id].approvals > quorum) {
            address payable to = transfers[_id].to;
            uint amount = transfers[_id].amount;
            transfers[_id].sent = true;
            to.transfer(amount);
            
            return ;
        }
    }
    
    modifier onlyApprover() {
        bool allowed = false;
        for(uint i = 0; i < approvers.length; i++) {
            if(approvers[i] == msg.sender) {
                allowed = true;
            }
        }
        require(allowed == true, "only approver allowed");
        _;
    }
}