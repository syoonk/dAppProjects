import React, {useEffect, useState} from "react";

import "./css/User.css";

function Wallet({web3, contract, accounts}) {
  const [balance, setBalance] = useState("Loading");
  const [isAdmin, setIsAdmin] = useState(false);
  const [contractBalance, setContractBalance] = useState("Loading");
  const [walletCollapse, setWalletCollapse] = useState(false);
  const [auctionCollapse, setAuctionCollapse] = useState(false);
  const [commission, setCommission] = useState("Loading");

  useEffect(() => {
    getAdmin();
    getCommission();
    if(isAdmin) {
      getContractBalance();
    }
    getUserBalance();
  }, [accounts, contractBalance, balance]);

  const getAdmin = async () => {
    const admin = await contract.methods.admin().call();
    setIsAdmin(admin.toLowerCase() === accounts[0].toLowerCase());
  }

  const getCommission = async () => {
    const commissionCopy = await contract.methods.commission().call();
    setCommission(commissionCopy);
  }

  const getUserBalance = async () => {
    const balanceCopy = await contract.methods.getBalance().call({from: accounts[0]});
    setBalance(balanceCopy);
  }

  const getContractBalance = async () => {
    const balanceCopy = await contract.methods.getContractBalance().call();
    setContractBalance(balanceCopy);
  }

  const submitAdminWithdrawHandle = async (e) => {
    e.preventDefault();
    const amount = web3.utils.toBN(e.target.elements[0].value);
    try {
      checkEnoughBalance(contractBalance, amount);

      getContractBalance();
    } catch {
      return;
    }
  }

  const submitDepositHandle = async (e) => {
    e.preventDefault();
    const amount = web3.utils.toBN(e.target.elements[0].value);
    await contract.methods.deposit().send({from: accounts[0], value: amount});

    getUserBalance();
  }

  const submitWithdrawHandle = async (e) => {
    e.preventDefault();
    const amount = web3.utils.toBN(e.target.elements[0].value);
    try {
      checkEnoughBalance(balance, amount);
      await contract.methods.withdraw(amount).send({from: accounts[0]});

      getUserBalance();
    } catch {
      return ;
    }    
  }

  const submitBidHandle = async (e) => {
    e.preventDefault();
    const auctionId = e.target.elements[0].value;
    const bidAmount = web3.utils.toBN(e.target.elements[1].value);
    await contract.methods.createOffer(auctionId, bidAmount).send({from: accounts[0]});

    getUserBalance();

    window.location.reload();
  }

  const submitCancelBidHandle = async (e) => {
    e.preventDefault();
    const auctionId = e.target.elements[0].value;
    await contract.methods.cancelOffers(auctionId).send({from: accounts[0]});

    getUserBalance();

    window.location.reload();
  }

  const submitCreateAuctionHandle = async (e) => {
    e.preventDefault();
    const name = e.target.elements[0].value;
    const description = e.target.elements[1].value;
    const startPrice = web3.utils.toBN(e.target.elements[2].value);
    const duration = e.target.elements[3].value;
    await contract.methods.createAuction(
      name,
      description,
      startPrice,
      duration).send({from: accounts[0]});

    window.location.reload();
  }

  const submitCommissionHandle = async (e) => {
    e.preventDefault();
    const amount = Number(e.target.elements[0].value);
    if(amount < 0 || amount > 100) {
      alert("amount should be 0 ~ 100 ");
      return ;
    }
    await contract.methods.changeCommission(Number(e.target.elements[0].value));

    getCommission();
  }

  const checkEnoughBalance = (_balance, _amount) => {
    if(web3.utils.toBN(_balance).sub(_amount).isNeg()) {
      alert("Not enough balance to withdraw");
      throw Error();
    }
  }

  const clickWalletHandle = () => { const copy = walletCollapse; setWalletCollapse(!copy) }
  const clickAuctionHandle = () => { const copy = auctionCollapse; setAuctionCollapse(!copy) }

  return(
    <div className="wallet">
      <h2>Welcome {accounts[0].substring(0, 10)}...</h2>

      <hr style={{marginTop:"40px"}}></hr>
      <h3 style={{cursor: "pointer"}} onClick={() => clickWalletHandle()}>{walletCollapse ? "▶" : "▼"} Wallet</h3>
      { walletCollapse ? null : (
      <div>
        <h4>Your Balance<br/> 
        {balance} wei</h4>  
        <h4>Deposit (Commission: {commission}%)</h4>
        <form className="walletForm" onSubmit={e => submitDepositHandle(e)}>
          <input type="number" placeholder="wei"></input>
          <button>Deposit</button>
        </form>
        <h4>Withdraw</h4>
        <form className="walletForm" onSubmit={e => submitWithdrawHandle(e)}>
          <input type="number" placeholder="wei"></input>
          <button>Withdraw</button>
        </form>
      </div> )
      }

      <hr style={{marginTop:"40px"}}></hr>
      <h3 style={{cursor: "pointer"}} onClick={() => clickAuctionHandle()}>{auctionCollapse ? "▶" : "▼"} Auction</h3>
      { auctionCollapse ? null : (
        <div>
          <h4><u>Bid</u></h4>
          <form className="auctionForm" onSubmit={e => submitBidHandle(e)}>
            <label className="label">● Auction Id:</label> <input className="argsBoxes" type="number"></input><br/>
            <label className="label">● Bid(wei):</label> <input className="argsBoxes" type="number"></input><br/>
            <button style={{float: "right"}}>Bid</button>
          </form>
          <h4><u>Cancel Bid</u></h4>
          <form className="auctionForm" onSubmit={e => submitCancelBidHandle(e)}>
            <label className="label">● Auction Id:</label> <input className="argsBoxes" type="number"></input><br/>
            <button style={{float: "right"}}>Cancel</button>
          </form>
          <h4><u>Create Auction</u></h4>
          <form className="auctionForm" onSubmit={e => submitCreateAuctionHandle(e)}>
            <label className="label">● Name:</label> <input className="argsBoxes" type="text"></input><br/>
            <label className="label">● Description:</label> <input className="argsBoxes" type="text"></input><br/>
            <label className="label">● Start Price(wei):</label> <input className="argsBoxes" type="number"></input><br/>
            <label className="label">● Duration(second):</label> <input className="argsBoxes" type="number" placeholder="86400 <, < 864000"></input><br/>
            <button style={{float: "right"}}>Upload</button>
          </form>
        </div> )
      }

      { isAdmin && (
      <div>
        <hr style={{marginTop:"40px"}}></hr>
        <h2>Admin Menu</h2>
        <h4>Contract Balance <br/>
        {contractBalance}</h4>
        <h4>Withdraw</h4>
        <form className="walletForm" onSubmit={e => submitAdminWithdrawHandle(e)}>
          <input type="number" placeholder="wei"></input>
          <button>Withdraw</button>
        </form>
        <h4>Change Commission</h4>
        <form onSubmit={e => submitCommissionHandle(e)}>
          <input 
            type="number" 
            placeholder="0~100"
            style={{width: "30px"}}>
          </input>
          <button>Change</button>
        </form>
      </div> )
      } 
    </div>
  );
}

export default Wallet;