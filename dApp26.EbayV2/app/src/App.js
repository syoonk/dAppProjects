import React from 'react';

import './css/App.css';
import User from "./User.js";
import ShopList from "./ShopList.js";

function App({web3, contract, accounts}) {
  return (
    <div className="App">
      <User web3={web3} contract={contract} accounts={accounts}></User>
      <ShopList contract={contract} accounts={accounts}></ShopList>
    </div>
  );
}

export default App;
