import React, {useEffect, useState} from "react";

import "./css/ShopList.css";

function ShopList({web3, contract, accounts}) {
  const [auctions, setAuctions] = useState([]);
  const [offers, setOffers] = useState([]);  
  const [userOfferAuctionsBid, setUserOfferAuctionsBid] = useState({});

  useEffect(() => {
    getAuctions();
    getOffers();
    getUserOffers();
  }, [accounts]);

  const getAuctions = async () => {
    const auctionsCopy = await contract.methods.getAuctions().call();
    if(!auctionsCopy)  setAuctions([]);
    setAuctions(auctionsCopy);
  }

  const getOffers = async () => {
    const offersCopy = await contract.methods.getOffers().call();
    if(!offersCopy) setOffers([]);
    setOffers(offersCopy);
  }

  const getUserOffers = async () => {
    const offersCopy = await contract.methods.getUserOffers(accounts[0]).call();
    if(!offersCopy) {
      setUserOfferAuctionsBid({});
      return;
    }
    let userOffersAuctionsBidCopy = {};
    for(let i = 0; i < offersCopy.length; i++) {
      userOffersAuctionsBidCopy[offersCopy[i].auctionId] = 
      offersCopy[i].bidTransferred ? "Canceled" : offersCopy[i].price + " wei";
    }
    setUserOfferAuctionsBid(userOffersAuctionsBidCopy);
  }

  return (
    <div className="auction">
      <h2>Auction List</h2>
      { 
        // id, seller(cut), name, Description ,currentBestPrice, your OfferPrice, endTime
      }
      <table className="auctionList">
        <tbody>
          <tr id="title">
            <th style={{width: "50px"}}>ID</th>
            <th style={{width: "100px"}}>Name</th>
            <th style={{width: "130px"}}>Seller</th>
            <th style={{width: "200px"}}>Highest</th>
            <th style={{width: "200px"}}>My Bid</th>
            <th style={{width: "150px"}}>End</th>
            <th style={{width: "200px"}}>Description</th>
          </tr>
          { auctions.length > 0 ? (
          auctions.map(auction => (
            <tr key={"auction"+auction.id}>
              <td>{auction.id}</td>
              <td>{auction.name}</td>
              <td>{auction.seller.substring(0, 10)}...</td>
              <td>{auction.bestPrice} wei</td>
              <td>{userOfferAuctionsBid[auction.id] || "N/A"}</td>
              <td>{(new Date(parseInt(auction.end) * 1000)).toLocaleString()}</td>
              <td>
                <details>
                  <summary style={{cursor: "pointer"}}>Description</summary>
                  {auction.description}
                </details>
              </td>
            </tr>
          ))
          ) : (<tr><td>No auctions</td></tr>)}
        </tbody>
      </table>
      
    </div>
  );
}

export default ShopList;