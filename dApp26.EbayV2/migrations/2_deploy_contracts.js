const Ebay = artifacts.require("Ebay");

module.exports = async (deployer, _network, _accounts) => {
  await deployer.deploy(Ebay);
  const ebay = await Ebay.deployed();

  const amount = web3.utils.toBN("1000000");
  for(let i = 0; i < 5; i++) {
    await ebay.deposit({from: _accounts[i], value: amount});
  }

  await ebay.createAuction(
    "Deploy Auction1",
    "Deployment1",
    10,
    86401,
    {from: _accounts[1]});
  await ebay.createAuction(
    "Deploy Auction2",
    "Deployment2",
    100,
    100000,
    {from: _accounts[2]});
  await ebay.createOffer(1, web3.utils.toBN("50"), {from: _accounts[3]});
  await ebay.createOffer(2, web3.utils.toBN("500"), {from: _accounts[3]});
  await ebay.createOffer(1, web3.utils.toBN("70"), {from: _accounts[4]});
  await ebay.createOffer(2, web3.utils.toBN("700"), {from: _accounts[4]});
}