pragma solidity >= 0.5.0 < 0.7.0;
pragma experimental ABIEncoderV2;

contract Ebay {
    struct Auction {
        uint id;
        address payable seller;
        string name;
        string description;
        uint min;
        uint bestPrice;
        uint end;
        uint bestOfferId;
        uint[] offerIds;
        bool traded;
    }
    struct Offer {
        uint id;
        uint auctionId;
        address payable buyer;
        uint price;
        bool bidTransferred;
    }
    mapping(uint => Auction) private auctions;
    mapping(uint => Offer) private offers;
    mapping(address => uint[]) private userAuctions;
    mapping(address => uint[]) private userOffers;
    mapping(address => uint) private balance;
    uint private nextAuctionId = 1;
    uint private nextOfferId = 1;
    uint public commission = 1;
    address payable public admin;
    
    constructor()
    public {
        admin = msg.sender;
    }
    
    function changeCommission(uint _commission)
    external 
    onlyAdmin() {
        commission = _commission;
    }

    function getContractBalance()
    external view
    onlyAdmin()
    returns(uint) {
        return address(this).balance;
    }

    function withdrawContractBalance(uint _amount)
    external
    onlyAdmin() lessThanBalance(_amount) {
        admin.transfer(_amount);
    }
    
    function deposit()
    external payable {
        require(msg.value >= 1000 wei, 
            "msg.value must be superior to 1000 wei");
        
        uint amount = msg.value * 99 / 100;
        balance[msg.sender] += amount;
    }
    
    function withdraw(uint _amount)
    external payable 
    lessThanBalance(_amount) {
        require(_amount <= balance[msg.sender], 
            "_amount is too much");
            
        balance[msg.sender] -= _amount;
        msg.sender.transfer(_amount);
    }
    
    function getBalance()
    external view
    returns(uint) {
        return balance[msg.sender];
    }

    function createAuction(
        string calldata _name,
        string calldata _description,
        uint _min,
        uint _duration) 
    external {
        require(_min > 0, 
            "_min must be > 0");
        require(_duration > 86400 && _duration < 864000, 
            "_duration must be comprised between > 1 day, < 10 days");
        
        uint[] memory offerIds = new uint[](0);
        auctions[nextAuctionId] = Auction(
            nextAuctionId,
            msg.sender,
            _name,
            _description,
            _min,
            _min,
            now + _duration,
            0,
            offerIds,
            false);
        userAuctions[msg.sender].push(nextAuctionId);
        nextAuctionId++;
    }
    
    function createOffer(uint _auctionId, uint _bid)
    external payable 
    auctionExist(_auctionId) {
        Auction storage auction = auctions[_auctionId];
        Offer storage bestOffer = offers[auction.bestOfferId];
        require(auction.seller != msg.sender,
            "cannot offer own auction");
        require(now < auction.end, 
            "auction has expired");
        require(_bid <= balance[msg.sender],
            "not enough balance to bid");
        require(_bid >= auction.min && _bid > bestOffer.price,
            "msg.value must be > min & > bestOffer");
        
        cancelOffers(_auctionId);
        auction.bestOfferId = nextOfferId;
        auction.bestPrice = _bid;
        balance[msg.sender] -= _bid;
        offers[nextOfferId] = Offer(
            nextOfferId,
            _auctionId,
            msg.sender,
            _bid,
            false);
        auction.offerIds.push(nextOfferId);
        userOffers[msg.sender].push(nextOfferId);
        nextOfferId++;
    }
    
    function cancelOffers(uint _auctionId)
    public 
    auctionExist(_auctionId) {
        uint[] memory senderOffers = userOffers[msg.sender];
        for(uint i = 0; i < senderOffers.length; i++) {
            Offer storage offer = offers[senderOffers[i]];
            if(offer.auctionId == _auctionId) {
                offer.bidTransferred = true;
                balance[msg.sender] += offer.price;
            }
        }
    }
    
    function trade(uint _auctionId)
    external 
    auctionExist(_auctionId) {
        Auction storage auction = auctions[_auctionId];
        Offer storage bestOffer = offers[auction.bestOfferId];
        require(now > auction.end,
            "auction is still active");
        require(bestOffer.bidTransferred == false, 
            "this offer already transferred");
        
        for(uint i = 0; i < auction.offerIds.length; i++) {
            uint offerId = auction.offerIds[i];
            if(offerId != auction.bestOfferId) {
                Offer storage offer = offers[offerId];
                offer.bidTransferred = true;
                balance[offer.buyer] += offer.price;
            }
        }
        bestOffer.bidTransferred = true;
        auction.traded = true;
        balance[auction.seller] += bestOffer.price;
    }
    
    function getAuctions()
    view external
    returns(Auction[] memory) {
        Auction[] memory auctionsCopy = new Auction[](nextAuctionId - 1);
        for(uint i = 1; i < nextAuctionId; i++) {
            auctionsCopy[i - 1] = auctions[i];
        }
        return auctionsCopy;
    }
    
    function getUserAuctions(address _user) 
    external view
    returns(Auction[] memory) {
        uint[] storage userAuctionIds = userAuctions[_user];
        Auction[] memory auctionsCopy = new Auction[](userAuctionIds.length);
        for(uint i = 0; i < userAuctionIds.length; i++) {
            uint auctionId = userAuctionIds[i];
            auctionsCopy[i] = auctions[auctionId];
        }
        
        return auctionsCopy;
    }
    
    function getOffers()
    external view
    returns(Offer[] memory) {
        Offer[] memory offersCopy = new Offer[](nextOfferId - 1);
        for(uint i = 1; i < nextOfferId; i++) {
            offersCopy[i - 1] = offers[i];
        }
        return offersCopy;
    }

    function getUserOffers(address _user)
    external view
    returns(Offer[] memory) {
        uint[] storage userOfferIds = userOffers[_user];
        Offer[] memory offersCopy = new Offer[](userOfferIds.length);
        for(uint i = 0; i < userOfferIds.length; i++) {
            uint offerId = userOfferIds[i];
            offersCopy[i] = offers[offerId];
        }
        
        return offersCopy;
    }
    
    modifier auctionExist(uint _auctionId) {
        require(_auctionId > 0 && _auctionId < nextAuctionId, 
            "auction does not exist");
        _;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin,
            "only admin");
        _;
    }

    modifier lessThanBalance(uint _amount) {
        require(_amount <= address(this).balance,
            "_amount is too much");
        
        _;
    }
}