const { expectRevert, time } = require("@openzeppelin/test-helpers");
const Ebay = artifacts.require("Ebay");

contract("Ebay", (accounts) => {
  let ebay;
  const auctionConfigs = [
  {
    name: 'Auction0',
    description: 'Selling Item0',
    min: 10,
    duration: 86400 + 1
  },
  {
    name: 'Auction1',
    description: 'Selling Item1',
    min: 10,
    duration: 864000 - 1
  }];
  const admin = accounts[0]; 
  const [seller1, seller2, buyer1, buyer2] =
    [accounts[1], accounts[2], accounts[3], accounts[4]];
  beforeEach(async () => {
    ebay = await Ebay.new();
    for(let i = 0; i < 5; i++) {
      await ebay.deposit({from: accounts[i], value: 10000});
    }
  });

  it("should deposit correctly", async () => {
    let balances = new Array(5);
    for(let i = 0; i < 5; i++) {
      balances[i] = await ebay.getBalance({from: accounts[i]});
    }
    
    balances.map(balance => {
      assert(balance.eq(web3.utils.toBN(9900)));
    });
  });

  it("should NOT deposit if value is too low", async () => {
    await expectRevert(
      ebay.deposit({from: seller1, value: web3.utils.toBN("999")}),
      "msg.value must be superior to 1000 wei");
  });

  it("should NOT withdraw in 2 cases", async () => {
    await expectRevert(
      ebay.withdraw(web3.utils.toBN("500001"), {from: seller1}),
      "_amount is too much");
    await expectRevert(
      ebay.withdraw(web3.utils.toBN("9901"), {from: seller1}),
      "_amount is too much");
  });

  it("should withdraw", async () => {
    const totalValue = web3.utils.toBN("9900");
    const beforeBalance_seller1 = web3.utils.toBN(await web3.eth.getBalance(seller1));
    const beforeWallet_seller1 = web3.utils.toBN(await ebay.getBalance({from: seller1}));
    const withdrawReceipt = await ebay.withdraw(totalValue, {from: seller1, gasPrice: 1});
    const afterBalance_seller1 = web3.utils.toBN(await web3.eth.getBalance(seller1));
    const afterWallet_seller1 = web3.utils.toBN(await ebay.getBalance({from: seller1}));
    const gasUsed = web3.utils.toBN(withdrawReceipt.receipt.gasUsed);

    assert(afterWallet_seller1.add(totalValue).eq(beforeWallet_seller1));
    assert((afterBalance_seller1.sub(beforeBalance_seller1).add(gasUsed))
      .eq(totalValue));
  });

  it("should NOT create offer if balance is not enough", async () => {
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    
    await expectRevert(
      ebay.createOffer(1, web3.utils.toBN("9901"), {from: buyer1}),
      "not enough balance to bid");
  });

  it("should create offer", async () => {
    const bid = web3.utils.toBN("9900");
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    const beforeBalance_buyer1 = web3.utils.toBN(await ebay.getBalance({from: buyer1}));
    await ebay.createOffer(1, bid, {from: buyer1});
    const afterBalance_buyer1 = web3.utils.toBN(await ebay.getBalance({from: buyer1}));
    const auction = await ebay.getAuctions();
    const offer = await ebay.getUserOffers(buyer1);

    assert.equal(auction[0].bestOfferId, 1);
    assert.equal(offer[0].id, 1);
    assert.equal(offer[0].auctionId, 1);
    assert.equal(offer[0].buyer, buyer1);
    assert.equal(offer[0].price, bid.toNumber());
    assert(beforeBalance_buyer1.sub(bid).eq(afterBalance_buyer1));
  });

  it("should trade", async () => {
    const bid = web3.utils.toBN("12");
    const bid_min = web3.utils.toBN("10");
    await ebay.createAuction(
      auctionConfigs[0].name,
      auctionConfigs[0].description,
      auctionConfigs[0].min,
      auctionConfigs[0].duration,
      {from: seller1});
    await ebay.createOffer(1, bid_min, {from: buyer1});
    // buyer2 will won the auction
    await ebay.createOffer(1, bid, {from: buyer2});
    const beforeBalanceBuyer1 = web3.utils.toBN(await ebay.getBalance({from: buyer1}));
    const beforeBalanceSeller1 = web3.utils.toBN(await ebay.getBalance({from: seller1}));
    time.increase(86400 + 1 + 1);
    await ebay.trade(1, {from: seller2});
    const afterBalanceBuyer1 = web3.utils.toBN(await ebay.getBalance({from: buyer1}));
    const afterBalanceSeller1 = web3.utils.toBN(await ebay.getBalance({from: seller1}));

    assert(beforeBalanceBuyer1.add(bid_min).eq(afterBalanceBuyer1));
    assert(afterBalanceSeller1.sub(beforeBalanceSeller1).eq(bid));

  });
});