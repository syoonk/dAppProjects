# About Ebay Applicatoin
Sample Auction application motivated by Ebay. Users can upload their product and initiate auction.

## Used Tools
Solidity, Truffle, NodeJs, React, Metamask

----
## How To Run
It uses metamask.

| EndPoints ||
---|---|
Truffle | http://localhost:9545  
Ganache | http://localhost:8545

### Deploy Contract  
`./npm install`  
`./truffle develop`  
`truffle> migrate --reset`  

### Run Frontend
`./app/npm install`  
`./app/npm start`

----
## Pages
<img src="./images/main.png" width="60%" height="60%"></img>