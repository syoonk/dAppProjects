pragma solidity ^0.6.3;
pragma experimental ABIEncoderV2;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/token/ERC20/IERC20.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";

contract Dex {
    using SafeMath for uint;
    
    struct Token {
        bytes32 ticker;
        address tokenAddress;
    }
    enum Side {
        BUY,
        SELL
    }
    struct Order {
        uint id;
        address trader;
        Side side;
        bytes32 ticker;
        uint amount;
        uint filled;
        uint price;
        uint date;
    }
    mapping(bytes32 => Token) public tokens;
    bytes32[] public tokenList;
    mapping(address => mapping(bytes32 => uint)) public traderBalances;
    mapping(bytes32 => mapping(uint => Order[])) public orderBook;
    address public admin;
    uint public nextOrderId;
    uint public nextTradeId;
    bytes32 constant DAI = bytes32('DAI');
    event NewTrade(
        uint tradeId,
        uint orderId,
        bytes32 indexed ticker,
        address indexed trader1,
        address indexed trader2,
        uint amount,
        uint price,
        uint data);
    
    constructor()
    public {
        admin = msg.sender;
    }
    
    function addToken(bytes32 _ticker, address _tokenAddress)
    external 
    onlyAdmin() {
        tokens[_ticker] = Token(_ticker, _tokenAddress);
        tokenList.push(_ticker);
    }
    
    function deposit(uint _amount, bytes32 _ticker)
    external 
    tokenExist(_ticker) {
        IERC20(tokens[_ticker].tokenAddress).transferFrom(
            msg.sender,
            address(this),
            _amount);
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(_amount);
    }
    
    function withdraw(uint _amount, bytes32 _ticker)
    external 
    tokenExist(_ticker) {
        require(traderBalances[msg.sender][_ticker] >= _amount,
            "balance too low");
            
        traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(_amount);
        IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
    }
    
    function createLimitOrder(
        bytes32 _ticker,
        uint _amount,
        uint _price,
        Side _side)
    external 
    tokenExist(_ticker) tokenIsNotDai(_ticker) {
        if(_side == Side.SELL) {
            require(traderBalances[msg.sender][_ticker] >= _amount,
                "token balance is too low");
        } else {
            require(traderBalances[msg.sender][DAI] >= _amount.mul(_price),
                "dai balance is too low");
        }
        
        Order[] storage orders = orderBook[_ticker][uint(_side)];
        orders.push(Order(
            nextOrderId,
            msg.sender,
            _side,
            _ticker,
            _amount,
            0,
            _price,
            now));
            
        // Sorting new order
        uint l = orders.length > 0 ? orders.length - 1 : 0;
        while(l > 0) {
            if(_side == Side.BUY && orders[l - 1].price > orders[l].price)   break;
            if(_side == Side.SELL && orders[l - 1].price < orders[l].price)   break;
            Order memory order = orders[l - 1];
            orders[l - 1] = orders[l];
            orders[l] = order;
            l = l.sub(1);
        }
        
        nextOrderId.add(1);
    }
    
    function createMarketOrder(
        bytes32 _ticker,
        uint _amount,
        Side _side)
    external
    tokenExist(_ticker) tokenIsNotDai(_ticker) {
        if(_side == Side.SELL) {
            require(traderBalances[msg.sender][_ticker] >= _amount,
                "token balance is too low");
        }
        
        Order[] storage orders = orderBook[_ticker][uint(_side == Side.BUY ? Side.SELL : Side.BUY)];
        uint i;
        uint remaining = _amount;
        while(i < orders.length && remaining > 0) {
            uint available = orders[i].amount.sub(orders[i].filled);
            uint matched = (remaining > available) ? available : remaining;
            remaining = remaining.sub(matched);
            orders[i].filled = orders[i].filled.add(matched);
            emit NewTrade(
                nextTradeId,
                orders[i].id,
                _ticker,
                orders[i].trader,
                msg.sender,
                matched,
                orders[i].price,
                now);
            if(_side == Side.SELL) {
                traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].sub(matched);
                traderBalances[msg.sender][DAI] = traderBalances[msg.sender][DAI].add(matched.mul(orders[i].price));
                traderBalances[orders[i].trader][_ticker] = traderBalances[orders[i].trader][_ticker].add(matched);
                traderBalances[orders[i].trader][DAI] = traderBalances[orders[i].trader][DAI].sub(matched.mul(orders[i].price));
            }
            if(_side == Side.BUY) {
                require(traderBalances[msg.sender][DAI] >= matched.mul(orders[i].price), 
                    "dai balance is too low");
                    
                traderBalances[msg.sender][_ticker] = traderBalances[msg.sender][_ticker].add(matched);
                traderBalances[msg.sender][DAI] = traderBalances[msg.sender][DAI].sub(matched.mul(orders[i].price));
                traderBalances[orders[i].trader][_ticker] = traderBalances[orders[i].trader][_ticker].sub(matched);
                traderBalances[orders[i].trader][DAI] = traderBalances[orders[i].trader][DAI].add(matched.mul(orders[i].price));
            }
            nextTradeId.add(1);
            i = i.add(1);
        }
        
        // Remove order that totally filled
        i = 0;
        while(i < orders.length && orders[i].filled == orders[i].amount) {
            for(uint j = i; j < orders.length - 1; j++) {
                orders[j] = orders[j + 1];
            }
            orders.pop();
            i = i.add(1);
        }
    }
    
    function getOrders(bytes32 _ticker, Side _side)
    external view
    returns(Order[] memory) {
        return orderBook[_ticker][uint(_side)];
    }
    
    function getTokens()
    external view
    returns(Token[] memory) {
        Token[] memory tokensCopy = new Token[](tokenList.length);
        for(uint i = 0; i < tokenList.length; i++) {
            tokensCopy[i] = Token(tokens[tokenList[i]].ticker, tokens[tokenList[i]].tokenAddress);
        }
        return tokensCopy;
    }
    
    modifier tokenIsNotDai(bytes32 ticker) {
        require(ticker != DAI,
            "cannot trade DAI");
        _;
    }
    
    modifier tokenExist(bytes32 ticker) {
        require(tokens[ticker].tokenAddress != address(0),
            "this token does not exist");
        _;
    }
    
    modifier onlyAdmin() {
        require(msg.sender == admin,
            "only admin");
        _;
    }
}