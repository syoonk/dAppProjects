const { expectRevert } = require("@openzeppelin/test-helpers");
const Dex = artifacts.require("Dex.sol");
const Dai = artifacts.require("./mocks/Dai.sol");
const Bat = artifacts.require("./mocks/Bat.sol");
const Rep = artifacts.require("./mocks/Rep.sol");
const Zrx = artifacts.require("./mocks/Zrx.sol");

const SIDE = {
  BUY: 0,
  SELL: 1
};

contract('Dex', (accounts) => {
  let dex, dai, bat, rep, zrx;
  const admin = accounts[0];
  const [trader1, trader2] = [accounts[1], accounts[2]];
  const [DAI, BAT, REP, ZRX] = 
    ['DAI', 'BAT', 'REP', 'ZRX'].map(ticker => web3.utils.fromAscii(ticker));
  const toWei = (_val) => {
    return web3.utils.toBN(web3.utils.toWei(_val));
  };

  beforeEach(async () => {
    dex = await Dex.new();
    [dai, bat, rep, zrx] = await Promise.all([
      Dai.new(),
      Bat.new(),
      Rep.new(),
      Zrx.new()]);
    await Promise.all([
      dex.addToken(DAI, dai.address),
      dex.addToken(BAT, bat.address),
      dex.addToken(REP, rep.address),
      dex.addToken(ZRX, zrx.address)
    ]);
    const seedAmount = web3.utils.toBN(web3.utils.toWei('1000'));
    const seedTokenBalance = async (_token, _trader) => {
      await _token.faucet(_trader, seedAmount);
      await _token.approve(
        dex.address,
        seedAmount,
        {from: _trader}
      );
    };
    await Promise.all(
      [dai, bat, rep, zrx].map(
        token => seedTokenBalance(token, trader1)
      )
    );
    await Promise.all(
      [dai, bat, rep, zrx].map(
        token => seedTokenBalance(token, trader2)
      )
    );
  });

  it("should create market order & match", async () => {
    const buyPrices = [web3.utils.toBN("2"), web3.utils.toBN("3"), web3.utils.toBN("4")];
    const sellPrices = [web3.utils.toBN("4"), web3.utils.toBN("3"), web3.utils.toBN("2")];
    const amount = toWei("10");
    // Buy Order:: BAT// 10@2, 10@3, 10@4
    // Deposit 90 DAI for Buy
    await Promise.all(
      buyPrices.map(price => dex.deposit(
        amount.mul(price), 
        DAI, 
        {from: trader1}))
    );
    await Promise.all(
      buyPrices.map(price => dex.createLimitOrder(
        BAT, 
        amount,
        price, 
        SIDE.BUY, 
        {from: trader1}))
    );
    // Sell Order:: REP// 10@4, 10@3, 10@2
    // Deposit 30 REP for Sell
    await dex.deposit(amount.mul(sellPrices[1]), REP, {from: trader2});
    await Promise.all(
      sellPrices.map(price => dex.createLimitOrder(
        REP, 
        amount,
        price, 
        SIDE.SELL, 
        {from: trader2}))
    );
    // Order List
    // @BuyOrderBook:: [4, 3, 2]@BAT, @SellOrderBook:: [2, 3, 4]@REP, each 10 amount 
    // Buy REP, trader1 from trader2
    const paymentBuy = (buyPrices[0].mul(toWei("10"))).add(buyPrices[1].mul(toWei("8")));
    await dex.deposit(
      paymentBuy,
      DAI,
      {from: trader1});
    const beforeRepBalanceBuy1 = web3.utils.toBN(await dex.traderBalances(trader1, REP));
    const beforeDaiBalanceBuy1 = web3.utils.toBN(await dex.traderBalances(trader1, DAI));
    const beforeRepBalanceBuy2 = web3.utils.toBN(await dex.traderBalances(trader2, REP));
    const beforeDaiBalanceBuy2 = web3.utils.toBN(await dex.traderBalances(trader2, DAI));
    await dex.createMarketOrder(
      REP,
      toWei("18"),
      SIDE.BUY,
      {from: trader1});
    const afterRepBalanceBuy1 = web3.utils.toBN(await dex.traderBalances(trader1, REP));
    const afterDaiBalanceBuy1 = web3.utils.toBN(await dex.traderBalances(trader1, DAI));
    const afterRepBalanceBuy2 = web3.utils.toBN(await dex.traderBalances(trader2, REP));
    const afterDaiBalanceBuy2 = web3.utils.toBN(await dex.traderBalances(trader2, DAI));
    const sellOrders = await dex.getOrders(REP, SIDE.SELL);
    
    assert((afterRepBalanceBuy1.sub(beforeRepBalanceBuy1)).eq(toWei("18")));
    assert((beforeDaiBalanceBuy1.sub(afterDaiBalanceBuy1)).eq(paymentBuy));
    assert((beforeRepBalanceBuy2.sub(afterRepBalanceBuy2)).eq(toWei("18")));
    assert((afterDaiBalanceBuy2.sub(beforeDaiBalanceBuy2)).eq(paymentBuy));
    assert.equal(sellOrders.length, 2);
    assert(web3.utils.toBN(sellOrders[0].filled).eq(toWei("8")));
    
    // Sell BAT, trader2 to trader1
    await dex.deposit(
      toWei("18"),
      BAT,
      {from: trader2});
    const beforeBatBalanceSell2 = web3.utils.toBN(await dex.traderBalances(trader2, BAT));
    const beforeDaiBalanceSell2 = web3.utils.toBN(await dex.traderBalances(trader2, DAI));
    const beforeBatBalanceSell1 = web3.utils.toBN(await dex.traderBalances(trader1, BAT));
    const beforeDaiBalanceSell1 = web3.utils.toBN(await dex.traderBalances(trader1, DAI));
    await dex.createMarketOrder(
      BAT,
      toWei("18"),
      SIDE.SELL,
      {from: trader2});
    const afterBatBalanceSell2 = web3.utils.toBN(await dex.traderBalances(trader2, BAT));
    const afterDaiBalanceSell2 = web3.utils.toBN(await dex.traderBalances(trader2, DAI));
    const afterBatBalanceSell1 = web3.utils.toBN(await dex.traderBalances(trader1, BAT));
    const afterDaiBalanceSell1 = web3.utils.toBN(await dex.traderBalances(trader1, DAI));
    const paymentSell = (sellPrices[0].mul(toWei("10"))).add(sellPrices[1].mul(toWei("8")));
    const buyOrders = await dex.getOrders(BAT, SIDE.BUY);
    
    assert((beforeBatBalanceSell2.sub(afterBatBalanceSell2)).eq(toWei("18")));
    assert((afterDaiBalanceSell2.sub(beforeDaiBalanceSell2)).eq(paymentSell));
    assert((afterBatBalanceSell1.sub(beforeBatBalanceSell1)).eq(toWei("18")));
    assert((beforeDaiBalanceSell1.sub(afterDaiBalanceSell1)).eq(paymentSell));
    assert.equal(buyOrders.length, 2);
    assert(web3.utils.toBN(buyOrders[0].filled).eq(toWei("8")));
  });

  it("should NOT create market order if balance too low", async () => {
    //SELL
    await dex.deposit(
      toWei("15"),
      BAT,
      {from: trader2});
    
    await expectRevert(
      dex.createMarketOrder(
        BAT,
        toWei("18"),
        SIDE.SELL,
        {from: trader2}),
      "token balance is too low");
    
    //BUY
    await dex.createLimitOrder(
      BAT,
      toWei("15"),
      web3.utils.toBN("2"),
      SIDE.SELL,
      {from: trader2});
    await dex.deposit(
      toWei("10"),
      DAI,
      {from: trader1});
    
    await expectRevert(
      dex.createMarketOrder(
        BAT,
        toWei("10"),
        SIDE.BUY,
        {from: trader1}),
      "dai balance is too low");
  });

  it("should NOT create market order if token is DAI", async () => {
    await dex.deposit(
      toWei("100"),
      DAI,
      {from: trader1});
    
    await expectRevert(
      dex.createMarketOrder(
        DAI,
        toWei("10"),
        SIDE.SELL,
        {from: trader1}),
      "cannot trade DAI");
    await expectRevert(
      dex.createMarketOrder(
        DAI,
        toWei("10"),
        SIDE.BUY,
        {from: trader1}),
      "cannot trade DAI");
    });

    it("should NOT create market order if token does not not exist", async () => {
      await dex.deposit(
        toWei("100"),
        DAI,
        {from: trader1});
      
      await expectRevert(
        dex.createMarketOrder(
          web3.utils.fromAscii("KNG"),
          toWei("10"),
          SIDE.BUY,
          {from: trader1}),
        "this token does not exist");
    });
});