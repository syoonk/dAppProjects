const { expectRevert, expectEvent, time } = require("@openzeppelin/test-helpers");
const ethers = require("ethers");

const Admins = artifacts.require("./Admins.sol");
const AppDatas = artifacts.require("./AppDatas.sol");
const Wallet = artifacts.require("./finance/Wallet.sol");
// Data Contracts
const DeliveryOrder = artifacts.require("./data/DeliveryOrder.sol");
const DeliveryReputation = artifacts.require("./data/DeliveryReputation.sol");
const DeliveryReview = artifacts.require("./data/DeliveryReview.sol");
const DeliveryRider = artifacts.require("./data/DeliveryRider.sol");
const DeliveryShop = artifacts.require("./data/DeliveryShop.sol");
const DeliveryUser = artifacts.require("./data/DeliveryUser.sol");
// App Contracts
const DeliveryAppOrder = artifacts.require("./app/DeliveryAppOrder.sol");
const DeliveryAppReview = artifacts.require("./app/DeliveryAppReview.sol");
const DeliveryAppRider = artifacts.require("./app/DeliveryAppRider.sol");
const DeliveryAppShop = artifacts.require("./app/DeliveryAppShop.sol");
const DeliveryAppUser = artifacts.require("./app/DeliveryAppUser.sol");
const DeliveryAppVoting = artifacts.require("./app/DeliveryAppVoting.sol");
// Utils
const AdjustingScore = artifacts.require("./utils/AdjustingScore.sol");
const KRWComm = artifacts.require("./utils/KRWCommission.sol");
const KNGScoring = artifacts.require("./utils/KNGScoring.sol");
// Coins
const Kng = artifacts.require("./mocks/Kng.sol");
const Krw = artifacts.require("./mocks/Krw.sol");


const DataType = {
  USER: 0,
  SHOP: 1,
  RIDER: 2,
  ORDER: 3,
  REVIEW: 4,
  REPUTATION: 5
};

const AppType = {
  USER:       0,
  RIDER:      1,
  SHOP:       2,
  ORDER:      3,
  REVIEW:     4,
  VOTING:     5
}

contract("App", accounts => {
  const [KNG, KRW] = [ethers.utils.formatBytes32String('KNG'), ethers.utils.formatBytes32String('KRW')];
 
  let adminsInst;
  let appDatasInst;
  let walletInst;
  let dataInsts = new Array(6);
  let appInsts = new Array(6);
  let krwComInst, kngScoringInst, adjRepuInst;
  let kng, krw;

  const [owner, admin, _] = accounts;
  const users = new Array(8);
  for(let i = 0; i < 8; i++) {
    users[i] = accounts[i + 2];
  }
  const userInfo = { phone: 11, home: "area1", pubKey: "abc" };

  beforeEach(async () => {
    adminsInst = await Admins.new();
    appDatasInst = await AppDatas.new();
    walletInst = await Wallet.new();
    // Deploy Datas
    dataInsts = await Promise.all([
      DeliveryUser.new(),
      DeliveryShop.new(),
      DeliveryRider.new(),
      DeliveryOrder.new(),
      DeliveryReview.new(),
      DeliveryReputation.new() ]);    
    // Deploy Apps
    appInsts = await Promise.all([
      DeliveryAppUser.new(),
      DeliveryAppRider.new(),
      DeliveryAppShop.new(),
      DeliveryAppOrder.new(),
      DeliveryAppReview.new(),
      DeliveryAppVoting.new() ]);
    // Deploy Utils
    krwComInst = await KRWComm.new();
    kngScoringInst = await KNGScoring.new();
    adjRepuInst = await AdjustingScore.new();
    // Deploy Coins
    [kng, krw] = await Promise.all([ Kng.new(), Krw.new() ]);

    /**
     * ADMIN INITIALIZATION
     */
    // set admin
    await adminsInst.addAdmin(admin, {from: owner});
    // set data deployed addresses
    await Promise.all(dataInsts.map( (inst, idx) => adminsInst.approveData(inst.address, idx, {from: admin})) );
    // set app deployed addresses
    await adminsInst.approveApp(appDatasInst.address, {from: admin});
    await Promise.all(appInsts.map( inst => adminsInst.approveApp(inst.address, {from: admin}) ));
    // set util deployed addresses
    await adminsInst.approveUtil(krwComInst.address, {from: owner}); 
    await adminsInst.approveUtil(kngScoringInst.address, {from: owner}); 
    await adminsInst.approveUtil(adjRepuInst.address, {from: owner}); 
    // set admin address to each contract
    await walletInst.setAdmin(adminsInst.address, {from: owner});
    await Promise.all(dataInsts.map( (inst) => inst.setAdmin(adminsInst.address, {from: owner})) );
    await krwComInst.setAdmin(adminsInst.address, {from: owner});
    await kngScoringInst.setAdmin(adminsInst.address, {from: owner});
    await adjRepuInst.setAdmin(adminsInst.address, {from: owner});

    /**
     * SET UTILS
     */
    // set util to wallet contract
    await walletInst.setKrwCommissionUtil(krwComInst.address, {from: admin});
    await walletInst.setKngScoringUtil(kngScoringInst.address, {from: admin});
    // set util to reputation contract
    await dataInsts[DataType.REPUTATION].setAdjustScoreUtil(adjRepuInst.address, {from: admin});
    
    /**
     * APP INITIALIZATION
     */
    // set data addresses
    await appDatasInst.setDataAddresses(0, dataInsts[DataType.USER].address, {from: owner});
    await appDatasInst.setDataAddresses(1, dataInsts[DataType.RIDER].address, {from: owner});
    await appDatasInst.setDataAddresses(2, dataInsts[DataType.SHOP].address, {from: owner});
    await appDatasInst.setDataAddresses(3, dataInsts[DataType.ORDER].address, {from: owner});
    await appDatasInst.setDataAddresses(4, dataInsts[DataType.REVIEW].address, {from: owner});
    await appDatasInst.setDataAddresses(5, dataInsts[DataType.REPUTATION].address, {from: owner});
    // app data addresses
    for(let i = 0; i < 6; i++) {
      await appInsts[i].setAppDatas(appDatasInst.address, {from: owner});
      await appInsts[i].setWallet(walletInst.address, {from: owner});
    }

    // registering users, they have same user infos
    for(let user of users) {
      await appInsts[AppType.USER].registerUser(
        userInfo.phone,
        userInfo.home,
        userInfo.pubKey,
        {from: user} );
    }

    // add tokens to wallet contract
    await walletInst.addToken(KNG, kng.address, {from: admin});
    await walletInst.addToken(KRW, krw.address, {from: admin});

    // faucet the KRW coin and deposit to wallet
    const seedAmount = web3.utils.toBN("100000000");
    for(let user of users) {
      await krw.faucet(user, seedAmount);
      await krw.approve(walletInst.address, seedAmount, {from: user});
      await walletInst.deposit(seedAmount, KRW, {from: user});
    }
  });

  /**
   * USER PART
   */
  it("should register user properly", async () => {   
    userAppInst = appInsts[AppType.USER];
    const [
      ids,
      phones,
      riderIds,
      shopIds,
      reviews,
      orders,
      homes,
      pubKeys,
      repus,
      ranks,
      adByRank] = [
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8),
        Array(8) ];

    for(let i = 0; i < users.length; i++) {
      ids[i] = await userAppInst.getUserId({from: users[i]});
      phones[i] = await userAppInst.getUserPhoneNumber({from: users[i]});
      riderIds[i] = await userAppInst.getUserRiderId({from: users[i]});
      shopIds[i] = await userAppInst.getUserShopIds({from: users[i]});
      reviews[i] = await userAppInst.getUserReviews({from: users[i]});
      orders[i] = await userAppInst.getUserOrders({from: users[i]});
      homes[i] = await userAppInst.getUserHomeAddress({from: users[i]});
      pubKeys[i] = await userAppInst.getUserPublicKey({from: users[i]});
      repus[i] = await userAppInst.getUserReputation({from: users[i]});
      ranks[i] = await userAppInst.getUserRank({from: users[i]});
      adByRank[i] = await userAppInst.getUserByRank(i);
    }
    const ranksList = await userAppInst.getRanks();
    const maxRank = await userAppInst.getMaxRank();

    for(let i = 0; i < users.length; i++) {
      assert.equal(ids[i], i + 1);
      assert.equal(phones[i], userInfo.phone);
      assert.equal(riderIds[i], 0);
      assert.equal(shopIds[i].length, 0);
      assert.equal(reviews[i].length, 0);
      assert.equal(orders[i].length, 0);
      assert.equal(homes[i], userInfo.home);
      assert.equal(pubKeys[i], userInfo.pubKey);
      assert.equal(repus[i], 50000);
      assert.equal(ranks[i], i);
      assert.equal(adByRank[i], users[i]);
      assert.equal(ranksList[i], users[i]);
    }
  });

  it("should change user info", async () => {
    userAppInst = appInsts[AppType.USER];
    
    await userAppInst.changePhoneNumber(010101, {from: users[3]});
    await userAppInst.changeHomeAddress("changedHome", {from: users[3]});
    const changedPhone = await userAppInst.getUserPhoneNumber({from: users[3]});
    const changedHome = await userAppInst.getUserHomeAddress({from: users[3]});

    assert.equal(changedPhone, 010101);
    assert.equal(changedHome, "changedHome");
  });

  it("should NOT register", async () => {
    await expectRevert(
      appInsts[AppType.USER].registerUser(
        userInfo.phone,
        userInfo.home,
        userInfo.pubKey,
        {from: users[3]}),
      "user already exists");
  });

  /**
   * RIDER PART
   */
  it("should register rider", async () => {
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[3]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[0]});

    const idFromUserData = await appInsts[AppType.USER].getUserRiderId({from: users[3]});
    const idFromUser0Data = await appInsts[AppType.USER].getUserRiderId({from: users[0]});
    const riderAddress = await riderAppInst.getRiderAddress(1);
    const id = await riderAppInst.getRiderId(users[3]);
    const phone = await riderAppInst.getRiderPhoneNumber(1);
    const curOrderId = await riderAppInst.getRiderCurrentOrderId(1);
    const nextOrderId = await riderAppInst.getRiderNextOrderId(1);
    const completedOrders = await riderAppInst.getRiderCompletedOrders(1);
    const activation = await riderAppInst.getRiderActivation(1);
    const idUser0 = await riderAppInst.getRiderId(users[0]);
    const id2Address = await riderAppInst.getRiderAddress(2);

    assert.equal(idFromUserData, 1);
    assert.equal(idFromUser0Data, 2);
    assert.equal(riderAddress, users[3]);
    assert.equal(id, 1);
    assert.equal(phone, riderInfo.phone);
    assert.equal(curOrderId, 0);
    assert.equal(nextOrderId, 0);
    assert.equal(completedOrders.length, 0);
    assert.equal(activation, true);
    assert.equal(idUser0, 2);
    assert.equal(id2Address, users[0]);
  });

  it("should change rider info", async () => {
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[3]});

    await riderAppInst.changeRiderPhoneNumber(3333, {from: users[3]});
    await riderAppInst.changeRiderDeliveryArea("bbb", {from: users[3]});

    const riderPhone = await riderAppInst.getRiderPhoneNumber(1);
    const riderArea = await riderAppInst.getRiderArea(1);

    assert.equal(riderPhone, 3333);
    assert.equal(riderArea, "bbb");

    await riderAppInst.deactivateRider({from: users[3]});

    const deactivation = await riderAppInst.getRiderActivation(1);
    
    await riderAppInst.activateRider({from: users[3]});

    const activation = await riderAppInst.getRiderActivation(1);

    assert(!deactivation);
    assert(activation);
  });

  it("should NOT register if user has already been assigned rider id", async () => {
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[3]});

    await expectRevert(
      riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[3]}),
      "user is already assigned riderId");  
  });

  it("should NOT excute if rider does not exist", async () => {
    riderAppInst = appInsts[AppType.RIDER];

    await expectRevert(
      riderAppInst.changeRiderPhoneNumber(123, {from: users[3]}),
      "this user is not assigned riderId");
    await expectRevert(
      riderAppInst.changeRiderDeliveryArea("abc", {from: users[3]}),
      "this user is not assigned riderId");
  });

  /**
   * SHOP PART
   */
  it("should register shop", async () => {
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};

    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[3]});
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});

    const user0ShopIds = await appInsts[AppType.USER].getUserShopIds({from: users[0]});
    const shopOwner1 = await shopAppInst.getShopOwner(1);
    const shopOwner2 = await shopAppInst.getShopOwner(2);
    const shopOwner3 = await shopAppInst.getShopOwner(3);
    const shopPhone = await shopAppInst.getShopPhoneNumber(1);
    const shopReviews = await shopAppInst.getShopReviews(1);
    const shopOrders = await shopAppInst.getShopOrders(1);
    const shopAddress = await shopAppInst.getShopAddress(1);
    const shopActivation = await shopAppInst.getShopActivation(1);

    assert.equal(user0ShopIds.length, 2);
    assert.equal(user0ShopIds[0], 1);
    assert.equal(user0ShopIds[1], 3);
    assert.equal(shopOwner1, users[0]);
    assert.equal(shopOwner2, users[3]);
    assert.equal(shopOwner3, users[0]);
    assert.equal(shopPhone, shopInfo.phone);
    assert.equal(shopReviews.length, 0);
    assert.equal(shopOrders.length, 0);
    assert.equal(shopAddress, shopInfo.address);
    assert.equal(shopActivation, true);
  });

  it("should change info", async () => {
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    
    await shopAppInst.changeShopPhoneNumber(1, 111, {from: users[0]});
    await shopAppInst.changeShopAddress(1, "bbb", {from: users[0]});
    await shopAppInst.deactivateShop(1, {from: users[0]});

    const shopPhone = await shopAppInst.getShopPhoneNumber(1);
    const shopAddress = await shopAppInst.getShopAddress(1);
    const shopActivation = await shopAppInst.getShopActivation(1);
    
    assert.equal(shopPhone, 111);
    assert.equal(shopAddress, "bbb");
    assert.equal(shopActivation, false);

    await shopAppInst.activateShop(1, {from: users[0]});
    
    const shopReActivated = await shopAppInst.getShopActivation(1);

    assert.equal(shopReActivated, true);
  });

  it("should NOT excute if now shop owner", async () => {
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    
    await expectRevert(
      shopAppInst.changeShopPhoneNumber(1, 111, {from: users[1]}),
      "user is not owner");
    await expectRevert(
      shopAppInst.changeShopAddress(1, "bbb", {from: users[1]}),
      "user is not owner");
    await expectRevert(
      shopAppInst.deactivateShop(1, {from: users[1]}),
      "user is not owner");
  });

  /**
   * ORDER PART
   */
  it("should place order", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const userAppInst = appInsts[AppType.USER];
    const shopInfo = { phone: 234, address: "aaa"};
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[2]});
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    const orderInfos1 = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const orderInfos2 = { goodsIds: [3, 4], totalPrice: web3.utils.toBN(30000) };
    const orderInfos3 = { goodsIds: [5, 6], totalPrice: web3.utils.toBN(40000) };

    const beforeOrderBuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});
    const beforeOrderBuyerPendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});

    await orderAppInst.placeOrder(
      1,
      orderInfos1.goodsIds,
      orderInfos1.totalPrice,
      {from: users[3]});
    await orderAppInst.placeOrder(
      1,
      orderInfos2.goodsIds,
      orderInfos2.totalPrice,
      {from: users[4]});
    await orderAppInst.placeOrder(
      1,
      orderInfos3.goodsIds,
      orderInfos3.totalPrice,
      {from: users[3]});

    const afterOrderBuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});
    const afterOrderBuyerPendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});

    assert( afterOrderBuyerBalance.eq(beforeOrderBuyerBalance.sub(orderInfos1.totalPrice.add(orderInfos3.totalPrice))) );
    assert( afterOrderBuyerPendingBalance.eq(beforeOrderBuyerPendingBalance.add(orderInfos1.totalPrice.add(orderInfos3.totalPrice))) );

    // order id 1
    const order1Buyer = await orderAppInst.getOrderBuyer(1);
    const order1ShopOwner = await orderAppInst.getOrderShopOwner(1);
    const order1Rider = await orderAppInst.getOrderRiderAddress(1);
    const order1ShopId = await orderAppInst.getOrderShopId(1);
    const order1RiderId = await orderAppInst.getOrderRiderId(1);
    const order1TotalPrice = await orderAppInst.getOrderTotalPrice(1);
    const order1GoodsIds = await orderAppInst.getOrderGoodsIds(1);
    const order1StateTimes = await orderAppInst.getOrderStateTimes(1);
    const order1State = await orderAppInst.getOrderState(1);
    const order1Signs = await orderAppInst.getOrderCancelSign(1);
    // order id 2
    const order2Buyer = await orderAppInst.getOrderBuyer(2);
    const order2ShopOwner = await orderAppInst.getOrderShopOwner(2);
    const order2Rider = await orderAppInst.getOrderRiderAddress(2);
    const order2ShopId = await orderAppInst.getOrderShopId(2);
    const order2RiderId = await orderAppInst.getOrderRiderId(2);
    const order2TotalPrice = await orderAppInst.getOrderTotalPrice(2);
    const order2GoodsIds = await orderAppInst.getOrderGoodsIds(2);
    const order2StateTimes = await orderAppInst.getOrderStateTimes(2);
    const order2State = await orderAppInst.getOrderState(2);
    const order2Signs = await orderAppInst.getOrderCancelSign(2);
    // order id 3
    const order3Buyer = await orderAppInst.getOrderBuyer(3);
    const order3ShopOwner = await orderAppInst.getOrderShopOwner(3);
    const order3Rider = await orderAppInst.getOrderRiderAddress(3);
    const order3ShopId = await orderAppInst.getOrderShopId(3);
    const order3RiderId = await orderAppInst.getOrderRiderId(3);
    const order3TotalPrice = await orderAppInst.getOrderTotalPrice(3);
    const order3GoodsIds = await orderAppInst.getOrderGoodsIds(3);
    const order3StateTimes = await orderAppInst.getOrderStateTimes(3);
    const order3State = await orderAppInst.getOrderState(3);
    const order3Signs = await orderAppInst.getOrderCancelSign(3);

    // order 1 datas
    assert.equal(order1Buyer, users[3]);
    assert.equal(order1ShopOwner, users[0]);
    assert.equal(order1Rider, 0);
    assert.equal(order1ShopId, 1);
    assert.equal(order1RiderId, 0);
    assert(order1TotalPrice.eq(orderInfos1.totalPrice));
    assert.equal(order1GoodsIds[0], orderInfos1.goodsIds[0]);
    assert.equal(order1GoodsIds[1], orderInfos1.goodsIds[1]);
    assert.equal(order1StateTimes.length, 6);
    console.log(new Date(order1StateTimes[0].toNumber() * 1000).toLocaleString());
    for(let i = 1; i < 6; i++) {
      assert.equal(order1StateTimes[i], 0);
    }
    assert.equal(order1State, 0);
    assert.equal(order1Signs.length, 2);
    assert.equal(order1Signs[0], false);
    assert.equal(order1Signs[1], false);
    // order 3 datas
    assert.equal(order3Buyer, users[3]);
    assert.equal(order3ShopOwner, users[0]);
    assert.equal(order3Rider, 0);
    assert.equal(order3ShopId, 1);
    assert.equal(order3RiderId, 0);
    assert(order3TotalPrice.eq(orderInfos3.totalPrice));
    assert.equal(order3GoodsIds[0], orderInfos3.goodsIds[0]);
    assert.equal(order3GoodsIds[1], orderInfos3.goodsIds[1]);
    assert.equal(order3StateTimes.length, 6);
    console.log(new Date(order1StateTimes[0].toNumber() * 1000).toLocaleString());
    for(let i = 1; i < 6; i++) {
      assert.equal(order1StateTimes[i], 0);
    }
    assert.equal(order3State, 0);
    assert.equal(order3Signs.length, 2);
    assert.equal(order3Signs[0], false);
    assert.equal(order3Signs[1], false);

    const shopOrders = await shopAppInst.getShopOrders(1, {from: users[0]});
    const user3Orders = await userAppInst.getUserOrders({from: users[3]});
    const user4Orders = await userAppInst.getUserOrders({from: users[4]});

    assert.equal(shopOrders.length, 3);
    for(let i = 0; i < shopOrders.length; i++) {
      assert.equal(shopOrders[i], i + 1);
    }
    assert.equal(user3Orders.length, 2);
    assert.equal(user3Orders[0], 1);
    assert.equal(user3Orders[1], 3);
    assert.equal(user4Orders.length, 1);
    assert.equal(user4Orders[0], 2);

    // Rider1 :: 2, 3  // Rider2 :: 1
    await riderAppInst.takeOrder(2, {from: users[1]});
    await riderAppInst.takeOrder(3, {from: users[1]});
    await riderAppInst.takeOrder(1, {from: users[2]});
    
    const rider1CurrentOrder = await riderAppInst.getRiderCurrentOrderId(1);
    const rider1NextOrder = await riderAppInst.getRiderNextOrderId(1);
    const order2AssignedRider = await orderAppInst.getOrderRiderId(2);
    const rider2CurrentOrder = await riderAppInst.getRiderCurrentOrderId(2);

    assert.equal(rider1CurrentOrder, 2);
    assert.equal(rider1NextOrder, 3);
    assert.equal(order2AssignedRider, 1);
    assert.equal(rider2CurrentOrder, 1);
    
    // Rider1 :: 3  // rider2 :: 1
    await riderAppInst.cancelAssignedOrder(2, {from: users[1]});

    const order2RiderCancelled = await orderAppInst.getOrderRiderId(2);
    const rider1CurrentOrderCancelled = await riderAppInst.getRiderCurrentOrderId(1);
    const rider1NextOrderCancelled = await riderAppInst.getRiderNextOrderId(1);

    assert.equal(rider1CurrentOrderCancelled, 3);
    assert.equal(rider1NextOrderCancelled, 0);
    assert.equal(order2RiderCancelled, 0);

    // Rider1 :: 3  // Rider2 :: 1, 2
    await riderAppInst.takeOrder(2, {from: users[2]});
    
    const rider2NextOrder = await riderAppInst.getRiderNextOrderId(2);
    const order2ReAssignedRider = await orderAppInst.getOrderRiderId(2);
    const order1AssignedRider = await orderAppInst.getOrderRiderId(1);

    assert.equal(rider2NextOrder, 2);
    assert.equal(order2ReAssignedRider, 2);
    assert.equal(order1AssignedRider, 2);
  });

  it("should sign order and change order state", async () => {
    const userAppInst = appInsts[AppType.USER];
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
  
    const beforeCompleteBuyerPendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});
    const beforeCompleteShopOwnerBalance = await walletInst.getBalance(users[0], KRW, {from: users[3]});
    const beforeCompleteRiderBalance = await walletInst.getBalance(users[1], KRW, {from: users[3]});
    const RIDERFEE = await orderAppInst.getOrderRiderFee();

    assert.equal(RIDERFEE.toNumber(), 3000);

    const PREPARING = 1;
    const DELIVERING = 2;
    const DELIVERED = 3;
    const COMPLETED = 4;

    // ORDERED to PREPARING
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[3]}),
      "only shop owner can sign this state");
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[1]}),
      "only shop owner can sign this state");
    await orderAppInst.signOrder(1, {from: users[0]});
    const orderStateSign1 = await orderAppInst.getOrderState(1);

    assert.equal(orderStateSign1, PREPARING);
    
    // PREPARING to DELIVERING
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[3]}),
      "only rider can sign this state");
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[0]}),
      "only rider can sign this state");
    await orderAppInst.signOrder(1, {from: users[1]});
    const orderStateSign2 = await orderAppInst.getOrderState(1);

    const afterCompleteShopOwnerBalance = await walletInst.getBalance(users[0], KRW, {from: users[3]});
    const halfCompleteRiderBalance = await walletInst.getBalance(users[1], KRW, {from: users[3]});
    const deliveringBuyerPendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});

    assert.equal(orderStateSign2, DELIVERING);
    assert( afterCompleteShopOwnerBalance.eq(beforeCompleteShopOwnerBalance.add(orderInfos.totalPrice.sub(RIDERFEE))) );
    assert( halfCompleteRiderBalance.eq(beforeCompleteRiderBalance.add(RIDERFEE.div(web3.utils.toBN(2)))) );
    assert( deliveringBuyerPendingBalance.eq(RIDERFEE.div(web3.utils.toBN(2))) );
    
    // DELIVERING to DELIVERED
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[3]}),
      "only rider can sign this state");
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[0]}),
      "only rider can sign this state");
    await orderAppInst.signOrder(1, {from: users[1]});
    const orderStateSign3 = await orderAppInst.getOrderState(1);

    const afterCompleteRiderBalance = await walletInst.getBalance(users[1], KRW, {from: users[3]});
    const afterCompleteBuyerPendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});

    assert.equal(orderStateSign3, DELIVERED);
    assert( afterCompleteRiderBalance.eq(beforeCompleteRiderBalance.add(RIDERFEE)) );
    assert( afterCompleteBuyerPendingBalance.eq(web3.utils.toBN(0)) );
    
    // DELIVERED to COMPLETED
    const beforeBuyerRepu = await userAppInst.getUserReputation({from: users[3]});
    const beforeRiderRepu = await userAppInst.getUserReputation({from: users[1]});
    const beforeShopRepu = await userAppInst.getUserReputation({from: users[0]});

    await expectRevert(
      orderAppInst.signOrder(1, {from: users[1]}),
      "only buyer can sign this state");
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[0]}),
      "only buyer can sign this state");
    await orderAppInst.signOrder(1, {from: users[3]});
    const orderStateSign4 = await orderAppInst.getOrderState(1);

    const afterBuyerRepu = await userAppInst.getUserReputation({from: users[3]});
    const afterRiderRepu = await userAppInst.getUserReputation({from: users[1]});
    const afterShopRepu = await userAppInst.getUserReputation({from: users[0]});

    assert.equal(orderStateSign4, COMPLETED);
    const REPUSCORE = web3.utils.toBN(1000);
    assert( afterBuyerRepu.eq(beforeBuyerRepu.add(REPUSCORE)) );
    assert( afterRiderRepu.eq(beforeRiderRepu.add(REPUSCORE)) );
    assert( afterShopRepu.eq(beforeShopRepu.add(REPUSCORE)) );
  });

  it("should cancel order", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});
    
    // buyer cancel order in ORDERED state, it should be cancelled immediately
    const beforeOrder1BuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});    
    // order id 1
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    const afterOrder1PendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});
    // cancelled
    await orderAppInst.cancelOrderSign(1, {from: users[3]});
    const afterCancelOrder1State = await orderAppInst.getOrderState(1);
    const afterCancelOrder1StateTimes = await orderAppInst.getOrderStateTimes(1);
    const afterCancelOrder1BuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});
    const afterCancelOrder1PendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});
    
    assert.equal(afterCancelOrder1State, 5);
    assert( afterOrder1PendingBalance.eq(orderInfos.totalPrice) );
    assert( afterCancelOrder1BuyerBalance.eq(beforeOrder1BuyerBalance) );
    assert.equal( afterCancelOrder1PendingBalance.toNumber(), 0);
    for(let i = 0; i < afterCancelOrder1StateTimes.length; i++) {
      console.log(`State ${i} Time:: `, new Date(afterCancelOrder1StateTimes[i] * 1000).toLocaleString());
    }
    
    // in PREPARED state, it only can be cancelled with both shop and buyer signed.
    // order id 2
    const beforeOrder2BuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});    
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    const afterOrder2PendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});
    await orderAppInst.signOrder(2, {from: users[0]});
    // buyer cancel sign but should not be cancelled
    await orderAppInst.cancelOrderSign(2, {from: users[3]});
    const afterBuyerCancelOrder2State = await orderAppInst.getOrderState(2);
    // shop cancel sign and it should be cancelled
    await orderAppInst.cancelOrderSign(2, {from: users[0]});
    const afterBuyerShopCancelOrder2State = await orderAppInst.getOrderState(2);
    const afterCancelOrder2BuyerBalance = await walletInst.getBalance(users[3], KRW, {from: users[3]});
    const afterCancelOrder2PendingBalance = await walletInst.getPendingKrwBalance(users[3], {from: users[3]});

    assert.equal(afterBuyerCancelOrder2State, 1);
    assert.equal(afterBuyerShopCancelOrder2State, 5);
    assert( afterOrder2PendingBalance.eq(orderInfos.totalPrice) );
    assert( afterCancelOrder2BuyerBalance.eq(beforeOrder2BuyerBalance) );
    assert.equal( afterCancelOrder2PendingBalance.toNumber(), 0);


    // in ORDERED state, it cannot be cancelled by only shop sign

    // after PREPARED state, it cannot be cancelled
  });

  it("should NOT cancel order", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    // in ORDERED state, it cannot be cancelled by only shop sign
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});

    // in DELIVERING state
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    
    await expectRevert(
      orderAppInst.cancelOrderSign(1, {from: users[3]}),
      "only ORDERED or PREPARING state can be cancelled");

    // in DELIVERED state
    await orderAppInst.signOrder(1, {from: users[1]});
    
    await expectRevert(
      orderAppInst.cancelOrderSign(1, {from: users[3]}),
      "only ORDERED or PREPARING state can be cancelled");

    // in COMPLETED state
    await orderAppInst.signOrder(1, {from: users[3]});
    
    await expectRevert(
      orderAppInst.cancelOrderSign(1, {from: users[3]}),
      "only ORDERED or PREPARING state can be cancelled");

    // cannot sign cancel except buyer and shop
    // order Id 2
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    
    await expectRevert(
      orderAppInst.cancelOrderSign(2, {from: users[1]}),
      "only buyer, shop owner can sign to cancel");
  });

  it("should NOT sign order", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});

    // user is not relevant to order
    await expectRevert(
      orderAppInst.signOrder(1, {from: users[2]}),
      "this user is not allowed to sign this order");

    // state is in cancelled
    await orderAppInst.cancelOrderSign(1, {from: users[3]});

    await expectRevert(
      orderAppInst.signOrder(1, {from: users[0]}),
      "order has been cancelled");
  });

  it("should NOT assign rider twice", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderAppInst = appInsts[AppType.RIDER];
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[2]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[2]});

    await expectRevert(
      riderAppInst.takeOrder(1, {from: users[1]}),
      "order has already been assigned");
  });

  /**
   * REVIEW PART
   */
  const [BUYER, RIDER, SHOP] = [0, 1, 2];

  it("should upload review", async () => {
    const repuDataInst = dataInsts[DataType.REPUTATION];
    const userAppInst = appInsts[AppType.USER];
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[3]});
    
    // buyer to shop, reviewId 1, repIds 4, 5
    await reviewAppInst.uploadReview(
      SHOP,
      1,
      10,
      {from: users[3]});
    // buyer to rider, reviewId 2, repIds 6, 7
    await reviewAppInst.uploadReview(
      RIDER,
      1,
      8,
      {from: users[3]});
    // shop to buyer, reviewId 3, repIds 8, 9
    await reviewAppInst.uploadReview(
      BUYER,
      1,
      6,
      {from: users[0]});
    // shop to rider, reviewId 4, repIds 10
    await reviewAppInst.uploadReview(
      RIDER,
      1,
      5,
      {from: users[0]});
    // rider to buyer, reviewId 5, repIds 11, 12
    await reviewAppInst.uploadReview(
      BUYER,
      1,
      3,
      {from: users[1]});
    // rider to shop, reviewId 6, repIds  13, 14
    await reviewAppInst.uploadReview(
      SHOP,
      1,
      1,
      {from: users[1]});

    const [fromUserTypes, toUserTypes, orderIds, stars, cancel] =
      [Array(7), Array(7), Array(7), Array(7), Array(7)];
    for(let i = 1; i < 7; i++) {
      fromUserTypes[i] = await reviewAppInst.getReviewFromUserType(i);
      toUserTypes[i] = await reviewAppInst.getReviewToUserType(i);
      orderIds[i] = await reviewAppInst.getReviewOrderId(i);
      stars[i] = await reviewAppInst.getReviewStars(i);
      cancel[i] = await reviewAppInst.getReviewCancelled(i);
    }

    assert.equal(fromUserTypes[1], 0);
    assert.equal(fromUserTypes[2], 0);
    assert.equal(fromUserTypes[3], 2);
    assert.equal(fromUserTypes[4], 2);
    assert.equal(fromUserTypes[5], 1);
    assert.equal(fromUserTypes[6], 1);
    assert.equal(toUserTypes[1], 2);
    assert.equal(toUserTypes[2], 1);
    assert.equal(toUserTypes[3], 0);
    assert.equal(toUserTypes[4], 1);
    assert.equal(toUserTypes[5], 0);
    assert.equal(toUserTypes[6], 2);
    for(let i = 1; i < 7; i++) assert.equal(orderIds[i], 1);
    assert.equal(stars[1], 10);
    assert.equal(stars[2], 8);
    assert.equal(stars[3], 6);
    assert.equal(stars[4], 5);
    assert.equal(stars[5], 3);
    assert.equal(stars[6], 1);
    for(let i = 1; i < 7; i++) assert.equal(cancel[i], false);

    const [repuScoreRecords, repuTargetRecords, repuPosRecords, repuRecordReverted]
     = [Array(15), Array(15), Array(15), Array(15)];
    for(let i = 4; i < 15; i++) {
      repuScoreRecords[i] = await repuDataInst.getRecordScore(i);
      repuTargetRecords[i] = await repuDataInst.getRecordTarget(i);
      repuPosRecords[i] = await repuDataInst.getRecordPos(i);
      repuRecordReverted[i] = await repuDataInst.getRecordReverted(i);
    }
    const buyerReputation = await userAppInst.getUserReputation(users[3]);
    const shopReputation = await userAppInst.getUserReputation(users[0]);
    const riderReputation = await userAppInst.getUserReputation(users[1]);
    const buyerRank = await userAppInst.getUserRank(users[3]);
    const shopRank = await userAppInst.getUserRank(users[0]);
    const riderRank = await userAppInst.getUserRank(users[1]);

    console.log("BUYER:: ", users[3]);
    console.log("RIDER:: ", users[1]);
    console.log("SHOP:: ", users[0]);
    console.log("BUYER REPU:: ", buyerReputation.toNumber(), " BUYER RANK:: ", buyerRank.toNumber());
    console.log("RIDER REPU:: ", riderReputation.toNumber(), " RIDER RANK:: ", riderRank.toNumber());
    console.log("SHOP REPU:: ", shopReputation.toNumber(), " SHOP RANK:: ", shopRank.toNumber());
    console.log("/** REPUTATION RECORDS **/");
    for(let i = 4; i < 15; i++) {
      console.log("Record ID:: ", i, " Score:: ", repuScoreRecords[i].toNumber(), "pos:: ", repuPosRecords[i], " Target:: ", repuTargetRecords[i]);
      assert.equal(repuRecordReverted[i], false);
    }

    for(let i = 4; i < 11; i++) { assert.equal(repuPosRecords[i], true); }
    for(let i = 11; i < 15; i++) {
      // negative review score --> pos = false
      if(i % 2 == 0) { assert.equal(repuPosRecords[i], false); }
      if(i % 2 == 1) { assert.equal(repuPosRecords[i], true); }
    }

    const user0ReviewIds = await userAppInst.getUserReviews(users[0]);
    const user1ReviewIds = await userAppInst.getUserReviews(users[1]);
    const user3ReviewIds = await userAppInst.getUserReviews(users[3]);
    const riderReviewIds = await riderAppInst.getRiderReviews(1);
    const shopReviewIds = await shopAppInst.getShopReviews(1);
    
    assert.equal(user0ReviewIds.length, 2);
    assert.equal(user0ReviewIds[0], 1);
    assert.equal(user0ReviewIds[1], 6);
    assert.equal(shopReviewIds.length, 2);
    assert.equal(shopReviewIds[0], 1);
    assert.equal(shopReviewIds[1], 6);
    assert.equal(user1ReviewIds.length, 2);
    assert.equal(user1ReviewIds[0], 2);
    assert.equal(user1ReviewIds[1], 4);
    assert.equal(riderReviewIds.length, 2);
    assert.equal(riderReviewIds[0], 2);
    assert.equal(riderReviewIds[1], 4);
    assert.equal(user3ReviewIds.length, 2);
    assert.equal(user3ReviewIds[0], 3);
    assert.equal(user3ReviewIds[1], 5);
  });

  it("should NOT review", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});

    // order does not completed
    await expectRevert(
      reviewAppInst.uploadReview(
        SHOP,
        1,
        10,
        {from: users[3]}),
      "order should be in completed state");

    await orderAppInst.signOrder(1, {from: users[3]});

    // order does not exist
    await expectRevert(
      reviewAppInst.uploadReview(
        SHOP,
        2,
        10,
        {from: users[3]}),
      "order does not exist");

    // user not relevant with order
    await expectRevert(
      reviewAppInst.uploadReview(
        SHOP,
        1,
        10,
        {from: users[2]}),
      "uploader does not relevant to order");
    await expectRevert(
      reviewAppInst.uploadReview(
        3,
        1,
        10,
        {from: users[3]}),
      "target user is not available");

    // self review
    await expectRevert(
      reviewAppInst.uploadReview(
        SHOP,
        1,
        10,
        {from: users[0]}),
      "not allow self review");

    // order is cancelled
    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await orderAppInst.cancelOrderSign(2, {from: users[3]});

    await expectRevert(
      reviewAppInst.uploadReview(
        SHOP,
        2,
        10,
        {from: users[3]}),
      "order should be in completed state");
  });

  it("should cancel review", async () => {
    const userAppInst = appInsts[AppType.USER];
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[3]});

    const user0BeforeUploadReview = await userAppInst.getUserReputation(users[0]);
    const user3BeforeUploadReview = await userAppInst.getUserReputation(users[3]);

    await reviewAppInst.uploadReview(
      SHOP,
      1,
      10,
      {from: users[3]});
  
    const user0AfterUploadReview = await userAppInst.getUserReputation(users[0]);
    const user3AfterUploadReview = await userAppInst.getUserReputation(users[3]);
    
    await reviewAppInst.cancelReview(1, {from: users[3]});

    const reviewCancelled = await reviewAppInst.getReviewCancelled(1);
    const user0AfterCancelReview = await userAppInst.getUserReputation(users[0]);
    const user3AfterCancelReview = await userAppInst.getUserReputation(users[3]);

    console.log("AFTER:: ", user0AfterUploadReview.toNumber());
    console.log("BEFORE:: ", user0BeforeUploadReview.toNumber());

    assert( user0AfterCancelReview.eq(user0BeforeUploadReview) );
    assert( user3AfterCancelReview.eq(user3BeforeUploadReview) );
    assert.equal(reviewCancelled, true);
  });

  it("shoudl NOT cancel review", async () => {
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[3]});

    await reviewAppInst.uploadReview(
      SHOP,
      1,
      10,
      {from: users[3]});
    
    await expectRevert(
      reviewAppInst.cancelReview(1, {from: users[0]}),
      "only creator can cancel the review");
  });

  /**
   * VOTING PART
   */
  const [REVIEW, PENALTY, PRAISE] = [0, 1, 2];
  const [VOTING, CONFIRMED, CANCELLED] = [0, 1, 2];
  
  it("should penalty", async () => {
    const userAppInst = appInsts[AppType.USER];
    const voteAppInst = appInsts[AppType.VOTING];
    
    // user0 create penalty vote to user1
    // user2 nay, user3 nay, user4 yay, user5 yay, user6 yay, user7 yay
    // user1 get penalty
    await voteAppInst.startVote(
      PENALTY,
      users[1],
      0,
      {from: users[0]});
    
    const voteType = await voteAppInst.getVoteType(1);
    const voteCreator = await voteAppInst.getVoteCreator(1);
    const voteTargetUser = await voteAppInst.getVoteTargetUser(1);
    const voteStartTime = await voteAppInst.getVoteStart(1);
    const voteEndTime = await voteAppInst.getVoteEnd(1);
    
    console.log(`Vote Start:: ${new Date(voteStartTime * 1000).toLocaleString()}
      Vote End:: ${new Date(voteEndTime * 1000).toLocaleString()}`);
    assert.equal(voteType, PENALTY);
    assert.equal(voteCreator, users[0]);
    assert.equal(voteTargetUser, users[1]);

    await voteAppInst.vote(1, false, {from: users[2]});
    await voteAppInst.vote(1, false, {from: users[3]});
    await voteAppInst.vote(1, true, {from: users[4]});
    await voteAppInst.vote(1, true, {from: users[5]});
    await voteAppInst.vote(1, true, {from: users[6]});
    await voteAppInst.vote(1, true, {from: users[7]});

    time.increase(86400 * 7 * 1000 + 1);
    
    await voteAppInst.confirmVote(1);
    
    const ups = await voteAppInst.getVoteUps(1);
    const downs = await voteAppInst.getVoteDowns(1);
    const result = await voteAppInst.getVoteResult(1);
    const userRepus = Array(8);
    for(let i = 0; i < 8; i++) {
      userRepus[i] = await userAppInst.getUserReputation(users[i]);
    }

    assert.equal(ups.toNumber(), 4);
    assert.equal(downs.toNumber(), 2);
    assert.equal(result, true);
    assert( userRepus[0].eq(web3.utils.toBN("50000")) );
    assert( userRepus[1].eq(web3.utils.toBN("49000")) );
    assert( userRepus[2].eq(web3.utils.toBN("50100")) );
    assert( userRepus[3].eq(web3.utils.toBN("50100")) );
    for(let i = 4; i < 8; i++) console.log(`user${i} Repu:: ${userRepus[i].toNumber()}`);
  });

  it("should praise", async () => {
    const userAppInst = appInsts[AppType.USER];
    const voteAppInst = appInsts[AppType.VOTING];
    
    // user0 create praise vote to user1
    // user2 nay, user3 nay, user4 yay, user5 yay, user6 yay, user7 yay
    // user1 get praise
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});
    
    const voteType = await voteAppInst.getVoteType(1);
    const voteCreator = await voteAppInst.getVoteCreator(1);
    const voteTargetUser = await voteAppInst.getVoteTargetUser(1);
    const voteStartTime = await voteAppInst.getVoteStart(1);
    const voteEndTime = await voteAppInst.getVoteEnd(1);
    
    console.log(`Vote Start:: ${new Date(voteStartTime * 1000).toLocaleString()}
      Vote End:: ${new Date(voteEndTime * 1000).toLocaleString()}`);
    assert.equal(voteType, PRAISE);
    assert.equal(voteCreator, users[0]);
    assert.equal(voteTargetUser, users[1]);

    await voteAppInst.vote(1, false, {from: users[2]});
    await voteAppInst.vote(1, false, {from: users[3]});
    await voteAppInst.vote(1, true, {from: users[4]});
    await voteAppInst.vote(1, true, {from: users[5]});
    await voteAppInst.vote(1, true, {from: users[6]});
    await voteAppInst.vote(1, true, {from: users[7]});

    time.increase(86400 * 7 * 1000 + 1);
    
    await voteAppInst.confirmVote(1);
    
    const ups = await voteAppInst.getVoteUps(1);
    const downs = await voteAppInst.getVoteDowns(1);
    const result = await voteAppInst.getVoteResult(1);
    const userRepus = Array(8);
    for(let i = 0; i < 8; i++) {
      userRepus[i] = await userAppInst.getUserReputation(users[i]);
    }

    assert.equal(ups.toNumber(), 4);
    assert.equal(downs.toNumber(), 2);
    assert.equal(result, true);
    assert( userRepus[0].eq(web3.utils.toBN("50000")) );
    assert( userRepus[1].eq(web3.utils.toBN("51000")) );
    assert( userRepus[2].eq(web3.utils.toBN("50100")) );
    assert( userRepus[3].eq(web3.utils.toBN("50100")) );
    for(let i = 4; i < 8; i++) console.log(`user${i} Repu:: ${userRepus[i].toNumber()}`);    
  });

  it("should cancel review", async () => {
    // user 0, 1, 2 got 1000 repu for completing order
    // user 0, 2 got repu for review but it would be cancelled
    // so user 0, 1, 2 will have 51000 repu in result
    // 3 ~ 7 user will get repu result of vote

    const userAppInst = appInsts[AppType.USER];
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const voteAppInst = appInsts[AppType.VOTING];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[2]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[2]});

    const user0BeforeUploadReview = await userAppInst.getUserReputation(users[0]);
    const user2BeforeUploadReview = await userAppInst.getUserReputation(users[2]);

    await reviewAppInst.uploadReview(
      SHOP,
      1,
      10,
      {from: users[2]});
  
    const user0AfterUploadReview = await userAppInst.getUserReputation(users[0]);
    const user2AfterUploadReview = await userAppInst.getUserReputation(users[2]);
    
    await voteAppInst.startVote(
      REVIEW,
      users[0],
      1,
      {from: users[0]});
    
    const voteType = await voteAppInst.getVoteType(1);
    const voteCreator = await voteAppInst.getVoteCreator(1);
    const voteReviewId = await voteAppInst.getVoteReviewId(1);
    const voteStartTime = await voteAppInst.getVoteStart(1);
    const voteEndTime = await voteAppInst.getVoteEnd(1);
    
    console.log(`Vote Start:: ${new Date(voteStartTime * 1000).toLocaleString()}
      Vote End:: ${new Date(voteEndTime * 1000).toLocaleString()}`);
    assert.equal(voteType, REVIEW);
    assert.equal(voteCreator, users[0]);
    assert.equal(voteReviewId, 1);

    await voteAppInst.vote(1, false, {from: users[3]});
    await voteAppInst.vote(1, false, {from: users[4]});
    await voteAppInst.vote(1, true, {from: users[5]});
    await voteAppInst.vote(1, true, {from: users[6]});
    await voteAppInst.vote(1, true, {from: users[7]});

    time.increase(86400 * 7 * 1000 + 1);

    await voteAppInst.confirmVote(1);
    
    const ups = await voteAppInst.getVoteUps(1);
    const downs = await voteAppInst.getVoteDowns(1);
    const result = await voteAppInst.getVoteResult(1);
    const userRepus = Array(8);
    for(let i = 0; i < 8; i++) {
      userRepus[i] = await userAppInst.getUserReputation(users[i]);
    }

    assert.equal(ups.toNumber(), 3);
    assert.equal(downs.toNumber(), 2);
    assert.equal(result, true);
    for(let i = 0; i < 8; i++) console.log(`user${i} Repu:: ${userRepus[i].toNumber()}`);    

    const reviewCancelled = await reviewAppInst.getReviewCancelled(1);
    const user0AfterCancelReview = await userAppInst.getUserReputation(users[0]);
    const user2AfterCancelReview = await userAppInst.getUserReputation(users[2]);

    console.log("AFTER:: ", user0AfterUploadReview.toNumber());
    console.log("BEFORE:: ", user0BeforeUploadReview.toNumber());

    assert( user0AfterCancelReview.eq(user0BeforeUploadReview) );
    assert( user2AfterCancelReview.eq(user2BeforeUploadReview) );
    assert.equal(reviewCancelled, true);
  });

  it("should cancel vote", async () => {
    const voteAppInst = appInsts[AppType.VOTING];    
    await voteAppInst.startVote(
      PENALTY,
      users[1],
      0,
      {from: users[0]});
    
    await voteAppInst.cancelVote(1, {from: users[0]});
    
    const voteState = await voteAppInst.getVoteState(1);

    assert.equal(voteState, 2);
  });

  it("should NOT start vote", async () => {
    const voteAppInst = appInsts[AppType.VOTING];
    
    // user does not exist
    await expectRevert(
      voteAppInst.startVote(
        PRAISE,
        accounts[0],
        0,
        {from: users[0]}),
      "user does not exist");
    await expectRevert(
      voteAppInst.startVote(
        PENALTY,
        accounts[0],
        0,
        {from: users[0]}),
      "user does not exist");

    // review does not exist
    await expectRevert(
      voteAppInst.startVote(
        REVIEW,
        users[3],
        1,
        {from: users[0]}),
      "review does not exist");

    // self target
    await expectRevert(
      voteAppInst.startVote(
        PRAISE,
        users[0],
        0,
        {from: users[0]}),
      "creator cannot be target");

    // review already cancelled
    const orderAppInst = appInsts[AppType.ORDER];
    const shopAppInst = appInsts[AppType.SHOP];
    const riderAppInst = appInsts[AppType.RIDER];
    const reviewAppInst = appInsts[AppType.REVIEW];
    const shopInfo = { phone: 234, address: "aaa"};
    const orderInfos = { goodsIds: [2, 3], totalPrice: web3.utils.toBN(20000) };
    const riderInfo = { phone: 222, area: "aaa" };
    await shopAppInst.registerShop(shopInfo.phone, shopInfo.address, {from: users[0]});
    await riderAppInst.registerRider(riderInfo.phone, riderInfo.area, {from: users[1]});

    await orderAppInst.placeOrder(
      1,
      orderInfos.goodsIds,
      orderInfos.totalPrice,
      {from: users[3]});
    await riderAppInst.takeOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[0]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[1]});
    await orderAppInst.signOrder(1, {from: users[3]});

    await reviewAppInst.uploadReview(
      SHOP,
      1,
      10,
      {from: users[3]});
    
    await reviewAppInst.cancelReview(1, {from: users[3]});

    await expectRevert(
      voteAppInst.startVote(
        REVIEW,
        users[3],
        1,
        {from: users[0]}),
      "review has already been cancelled");
  });

  it("should NOT vote", async () => {
    const voteAppInst = appInsts[AppType.VOTING];
    
    // vote does not exist
    await expectRevert(
      voteAppInst.vote(1, true, {from: users[1]}),
      "vote does not exist");

    // vote Id 1
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});

    // creator vote
    await expectRevert(
      voteAppInst.vote(1, true, {from: users[0]}),
      "creator cannot vote");

    // target user vote
    await expectRevert(
      voteAppInst.vote(1, true, {from: users[1]}),
      "target user cannot vote");

    // already voted
    await voteAppInst.vote(1, false, {from: users[2]});

    await expectRevert(
      voteAppInst.vote(1, true, {from: users[2]}),
      "user already voted");

    // not in voting state
    await voteAppInst.cancelVote(1, {from: users[0]});

    await expectRevert(
      voteAppInst.vote(1, true, {from: users[2]}),
      "can vote only in VOTING state");
    
    // vote is expired
    // vote id 2
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});

    time.increase(86400 * 1000 * 7 + 1);

    await expectRevert(
      voteAppInst.vote(2, true, {from: users[2]}),
      "vote time is expired");
  });

  it("should NOT cancel", async () => {
    const voteAppInst = appInsts[AppType.VOTING];

    // vote id 1
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});
    
    // not creator
    await expectRevert(
      voteAppInst.cancelVote(1, {from: users[1]}),
      "only creator can cancel vote");
    
    await voteAppInst.cancelVote(1, {from: users[0]});

    // not in VOTING state
    // cancelled
    await expectRevert(
      voteAppInst.cancelVote(1, {from: users[0]}),
      "cancel only in VOTING state");

    // vote id 2
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});
    
    time.increase(86400 * 1000 * 7 + 1);
    await voteAppInst.confirmVote(2, {from: users[0]});

    // completed
    await expectRevert(
      voteAppInst.cancelVote(2, {from: users[0]}),
      "cancel only in VOTING state");
  });

  it.only("should NOT confirm", async () => {
    const voteAppInst = appInsts[AppType.VOTING];

    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});

    // not finished
    await expectRevert(
      voteAppInst.confirmVote(1, {from: users[0]}),
      "vote is not finished yet");
    
    // not creator or owner
    await expectRevert(
      voteAppInst.confirmVote(1, {from: users[1]}),
      "not allow to confirm");

    // not in VOTING state
    // cancelled
    await voteAppInst.cancelVote(1, {from: users[0]});
    
    time.increase(86400 * 1000 * 7 + 1);

    await expectRevert(
      voteAppInst.confirmVote(1, {from: users[0]}),
      "only confirm in VOTING state");

    // confirmed
    await voteAppInst.startVote(
      PRAISE,
      users[1],
      0,
      {from: users[0]});

    time.increase(86400 * 1000 * 7 + 1);

    await voteAppInst.confirmVote(2, {from: users[0]});

    await expectRevert(
      voteAppInst.confirmVote(2, {from: users[0]}),
      "only confirm in VOTING state");
  });
});