const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");

const Admins = artifacts.require("./Admins.sol");
const DeliveryOrder = artifacts.require("./data/DeliveryOrder.sol");
const DeliveryReputation = artifacts.require("./data/DeliveryReputation.sol");
const DeliveryReview = artifacts.require("./data/DeliveryReview.sol");
const DeliveryRider = artifacts.require("./data/DeliveryRider.sol");
const DeliveryShop = artifacts.require("./data/DeliveryShop.sol");
const DeliveryUser = artifacts.require("./data/DeliveryUser.sol");
const AdjustingScore = artifacts.require("./utils/AdjustingScore.sol");

const DataType = {
  USER: 0,
  SHOP: 1,
  RIDER: 2,
  ORDER: 3,
  REVIEW: 4,
  REPUTATION: 5
};

contract("Data", accounts => {
  let adminsInst;
  let dataInsts = new Array(6);
  const [owner, admin, app, user1, user2, user3, _] = accounts;
  const user1Info = { phone: 11, home: "area1", pubKey: "abc" };
  const user2Info = { phone: 22, home: "area2", pubKey: "def" };
  const user3Info = { phone: 33, home: "area3", pubKey: "def" };
  
  beforeEach(async () => {
    // Each test has new deployment of contracs
    [adminsInst, ...dataInsts] = await Promise.all([
      Admins.new(),
      DeliveryUser.new(),
      DeliveryShop.new(),
      DeliveryRider.new(),
      DeliveryOrder.new(),
      DeliveryReview.new(),
      DeliveryReputation.new() ]);    
    // set admin
    await adminsInst.addAdmin(admin, {from: owner});
    // set data instance addresses
    await Promise.all(dataInsts.map( (inst, idx) => adminsInst.approveData(inst.address, idx, {from: admin})) );
    // set app instance addresses (address is accounts[2] for test case)
    await adminsInst.approveApp(app, {from: admin});
    // set admin address to each contract
    await Promise.all(dataInsts.map( (inst) => inst.setAdmin(adminsInst.address, {from: owner})) );

    // register user1
    await dataInsts[DataType.USER].registerUser(
      user1,
      user1Info.phone,
      user1Info.home,
      user1Info.pubKey,
      {from: app} );
    // // regeister user2
    await dataInsts[DataType.USER].registerUser(
      user2,
      user2Info.phone,
      user2Info.home,
      user2Info.pubKey,
      {from: app} );
    await dataInsts[DataType.USER].registerUser(
      user3,
      user3Info.phone,
      user3Info.home,
      user3Info.pubKey,
      {from: app} );
  });

  /**
   * ADMIN TEST PART
   */
  it("should NOT approve or disapprove if not admin", async () => {
    await expectRevert(
      adminsInst.addAdmin(accounts[4], {from: accounts[9]}),
      "only admin");
    await expectRevert(
      adminsInst.removeAdmin(admin, {from: accounts[9]}),
      "only admin");
    await expectRevert(
      adminsInst.approveData(accounts[4], DataType.USER, {from: accounts[8]}),
      "only admin");
    await expectRevert(
      adminsInst.disapproveData(accounts[4], DataType.USER, {from: accounts[9]}),
      "only admin");
    await expectRevert(
      adminsInst.approveApp(accounts[4], {from: accounts[9]}),
      "only admin");
    await expectRevert(
      adminsInst.disapproveApp(accounts[4], {from: accounts[9]}),
      "only admin");
  });

  it("should return data address", async () => {
    for(let i = 0; i < 6; i++) {
      // it should be able to return by any accounts
      const address = await adminsInst.getDataAddress(i, {from: accounts[8]});

      assert.equal(address, dataInsts[i].address);
    }
  });

  it("should NOT pass modifier triggers if not approved", async () => {
    await expectRevert(
      adminsInst.onlyAdminTrigger(app),
      "only admin");
    await expectRevert(
      adminsInst.onlyApprovedDataTrigger(app),
      "only approved data");
    await expectRevert(
      adminsInst.onlyApprovedAppTrigger(admin),
      "only approved app");
  });

  it("should pass modifier triggers", async () => {
    let results = new Array(6);
    try{
      reuslts = await Promise.all([adminsInst.onlyAdminTrigger(admin),
      adminsInst.onlyApprovedDataTrigger(data),
      adminsInst.onlyApprovedAppTrigger(app),
      adminsInst.onlyApprovedAppTrigger(data) ]);
    } catch(e) {
      console.log("error while passing triggers");
    }

    results.map(result => assert(result));
  });

  /**
   * USER PART
   */
  it("should NOT set if not owner", async() => {
    await expectRevert(
      dataInsts[DataType.USER].setAdmin(admin, {from: app}),
      "only owner");
  });

  it("should return user infos", async() => {
    const userInst = dataInsts[DataType.USER];
    
    // of user1
    const user1Address = await userInst.getUserAddress(1, {from: app});
    const user1Id = await userInst.getUserId(user1, {from: app});
    const user1PhoneNo = await userInst.getUserPhoneNumber(user1, {from: app});
    const user1RiderId = await userInst.getUserRiderId(user1, {from: app});
    const user1ShopId = await userInst.getUserShopIds(user1, {from: app});
    const user1Reviews = await userInst.getUserReviews(user1, {from: app});
    const user1Home = await userInst.getUserHomeAddress(user1, {from: app});
    const user1PubKey = await userInst.getUserPublicKey(user1, {from: app});
    // of user2
    const user2Address = await userInst.getUserAddress(2, {from: app});
    const user2Id = await userInst.getUserId(user2, {from: app});
    const user2PhoneNo = await userInst.getUserPhoneNumber(user2, {from: app});
    const user2RiderId = await userInst.getUserRiderId(user2, {from: app});
    const user2ShopId = await userInst.getUserShopIds(user2, {from: app});
    const user2Reviews = await userInst.getUserReviews(user2, {from: app});
    const user2Home = await userInst.getUserHomeAddress(user2, {from: app});
    const user2PubKey = await userInst.getUserPublicKey(user2, {from: app});

    // Check user1's
    assert.equal(user1Address, user1);
    assert.equal(user1Id, 1);
    assert.equal(user1PhoneNo, 11);
    assert.equal(user1RiderId, 0);
    assert.equal(user1ShopId.length, 0);
    assert.equal(user1Reviews.length, 0);
    assert.equal(user1Home, user1Info.home);
    assert.equal(user1PubKey, "abc");
    // Check user2's
    assert.equal(user2Address, user2);
    assert.equal(user2Id, 2);
    assert.equal(user2PhoneNo, user2Info.phone);
    assert.equal(user2RiderId, 0);
    assert.equal(user2ShopId.length, 0);
    assert.equal(user2Reviews.length, 0);
    assert.equal(user2Home, user2Info.home);
    assert.equal(user2PubKey, user2Info.pubKey);
  });
  
  it("should change informations", async () => {
    const userInst = dataInsts[DataType.USER];
    const phoneNo = "111", home = "area11";

    await userInst.changePhoneNumber(user1, phoneNo, {from: app});
    await userInst.changeHomeAddress(user1, home, {from: app});
    const user1Phone = await userInst.getUserPhoneNumber(user1, {from: app});
    const user1Home = await userInst.getUserHomeAddress(user1, {from: app});

    assert.equal(user1Phone, phoneNo);
    assert.equal(user1Home, home);
  });

  it("should NOT execute functions if not data", async () => {
    const userInst = dataInsts[DataType.USER];
    
    await expectRevert(
      userInst.setShopId(user1, 1, {from: app}),
      "only approved data");
    await expectRevert(
      userInst.setRiderId(user1, 1, {from: app}),
      "only approved data");
    await expectRevert(
      userInst.addOrder(user1, 1, {from: app}),
      "only approved data");
    await expectRevert(
      userInst.addReview(user1, 1, {from: app}),
      "only approved data");
  });

  /**
   * SHOP PART
   */
  it("should register shop", async () => {
    const userInst = dataInsts[DataType.USER];
    const shopInst = dataInsts[DataType.SHOP];
    const shopPhone = 11, shopAdd = "area1";
    
    await shopInst.registerShop(
      user1,
      shopPhone,
      shopAdd,
      {from: app});

    const shop1Owner = await shopInst.getShopOwner(1);
    const shop1Phone = await shopInst.getShopPhoneNumber(1);
    const shop1Reviews = await shopInst.getShopReviews(1);
    const shop1Address = await shopInst.getShopAddress(1);
    const shop1Activation = await shopInst.getShopActivation(1);
    const user1ShopId = await userInst.getUserShopIds(user1, {from: app});

    assert.equal(shop1Owner, user1);
    assert.equal(shop1Phone, shopPhone);
    assert.equal(shop1Reviews.length, 0);
    assert.equal(shop1Address, shopAdd);
    assert.equal(shop1Activation, true);
    assert.equal(user1ShopId.length, 1);
    assert.equal(user1ShopId[0], 1);

    await shopInst.deactivateShop(1, {from: app});
    const shop1AfterActivation = await shopInst.getShopActivation(1);

    assert.equal(shop1AfterActivation, false);
  });

  it("should NOT execute if shop does not exist", async () => {
    const shopInst = dataInsts[DataType.SHOP];

    await expectRevert(
      shopInst.activateShop(1, {from: app}),
      "shop does not exist");
    await expectRevert(
      shopInst.deactivateShop(1, {from: app}),
      "shop does not exist");
    await expectRevert(
      shopInst.addReview(1, 1, {from: app}),
      "shop does not exist");
    // only one case test for getter functions
    await expectRevert(
      shopInst.getShopOwner(1, {from: app}),
      "shop does not exist");
  });

  it("should NOT excute if shop is not activated", async () => {
    const shopInst = dataInsts[DataType.SHOP];
    const shopPhone = 11, shopAdd = "area1";
    
    await shopInst.registerShop(
      user1,
      shopPhone,
      shopAdd,
      {from: app});
    await shopInst.deactivateShop(1, {from: app});

    await expectRevert(
      shopInst.addOrder(1, 1, {from: app}),
      "shop is not activated");
  });

  /**
   * RIDER PART
   */
  it("should register rider", async () => {
    const userInst = dataInsts[DataType.USER];
    const riderInst = dataInsts[DataType.RIDER];
    const phone = 111, riderAddress = "area1";

    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});
    await riderInst.registerRider(
      user2,
      phone,
      riderAddress,
      {from: app});
    
    const rider1Add = await riderInst.getRiderAddress(1);
    const rider1Id = await riderInst.getRiderId(user1);
    const rider1Phone = await riderInst.getRiderPhoneNumber(1);
    const rider1CurOrder = await riderInst.getRiderCurrentOrderId(1);
    const rider1NextOrder = await riderInst.getRiderNextOrderId(1);
    const rider1CompletedOrders = await riderInst.getRiderCompletedOrders(1);
    const rider1RiderReviews = await riderInst.getRiderReviews(1);
    const rider1Area = await riderInst.getRiderArea(1);
    const rider1Activation = await riderInst.getRiderActivation(1);
    const user1RiderId = await userInst.getUserRiderId(user1, {from: app});
    const rider2Id = await riderInst.getRiderId(user2);
    const user2RiderId = await userInst.getUserRiderId(user2, {from: app});

    assert.equal(rider1Add, user1);
    assert.equal(rider1Id, 1);
    assert.equal(rider1Phone, phone);
    assert.equal(rider1CurOrder, 0);
    assert.equal(rider1NextOrder, 0);
    assert.equal(rider1CompletedOrders.length, 0);
    assert.equal(rider1RiderReviews.length, 0);
    assert.equal(rider1Area, riderAddress);
    assert.equal(rider1Activation, true);
    assert.equal(user1RiderId, 1);
    assert.equal(rider2Id, 2);
    assert.equal(user2RiderId, 2);

    await riderInst.deactivateRider(1, {from: app});
    const rider1AfterActivation = await riderInst.getRiderActivation(1);
    
    assert.equal(rider1AfterActivation, false);
  });

  it("should NOT register if user has already been assigned rider", async () => {
    const riderInst = dataInsts[DataType.RIDER];
    const phone = 111, riderAddress = "area1";

    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});
      
    await expectRevert(
      riderInst.registerRider(
        user1,
        phone,
        riderAddress,
        {from: app}),
      "user is already assigned riderId");
  });

  it("should NOT execute if rider does not exist", async () => {
    const riderInst = dataInsts[DataType.RIDER];
    
    await expectRevert(
      riderInst.activateRider(1, {from: app}),
      "rider does not exist");
    await expectRevert(
      riderInst.deactivateRider(1, {from: app}),
      "rider does not exist");
    await expectRevert(
      riderInst.addReview(1, 1, {from: app}),
      "rider does not exist");
    await expectRevert(
      riderInst.getRiderAddress(1),
      "rider does not exist");
  });

  it("should NOT execute if rider is not activated", async () => {
    const riderInst = dataInsts[DataType.RIDER];
    const userInst = dataInsts[DataType.USER];
    const shopInst = dataInsts[DataType.SHOP];
    const phone = 111, riderAddress = "area1";

    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});

    await riderInst.deactivateRider(1, {from: app});

    await expectRevert(
      riderInst.takeOrder(1, 1, {from: app}),
      "rider is not activated");
    await expectRevert(
      riderInst.cancelAssignedOrder(1, 1, {from: app}),
      "rider is not activated");
  });

  /**
   * ORDER PART
   */
  it("should place order", async () => {
    const userInst = dataInsts[DataType.USER];
    const shopInst = dataInsts[DataType.SHOP];
    const orderInst = dataInsts[DataType.ORDER];
    const shopPhone = 11, shopAdd = "area1";
    const [goodsIds, buyAmounts, totalPrice] = [[2, 3], [2, 1], 30000];
    
    await shopInst.registerShop(
      user1,
      shopPhone,
      shopAdd,
      {from: app});

    const placeOrderReceipt = await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});

    const orderBuyer = await orderInst.getOrderBuyer(1);
    const orderShopId = await orderInst.getOrderShopId(1);
    const orderRiderId = await orderInst.getOrderRiderId(1);
    const orderTotalPrice = await orderInst.getOrderTotalPrice(1);
    const orderGoodsIds = await orderInst.getOrderGoodsIds(1);
    const orderBuyAmounts = await orderInst.getOrderBuyAmounts(1);
    const orderStateTimes = await orderInst.getOrderStateTimes(1);
    const orderState = await orderInst.getOrderState(1);
    const user1Orders = await userInst.getUserOrders(user1, {from: app});
    const shopOrders = await shopInst.getShopOrders(1);
    
    assert.equal(orderBuyer, user1);
    assert.equal(orderShopId, 1);
    assert.equal(orderRiderId, 0);
    assert.equal(orderTotalPrice, totalPrice);
    assert.equal(orderGoodsIds.length, 2);
    assert.equal(orderGoodsIds[0], 2);
    assert.equal(orderGoodsIds[1], 3);
    assert.equal(orderBuyAmounts.length, 2);
    assert.equal(orderBuyAmounts[0], 2);
    assert.equal(orderBuyAmounts[1], 1);
    assert.equal(orderStateTimes.length, 6);
    console.log("State Time:: ", new Date(orderStateTimes[0] * 1000).toLocaleString());
    assert.equal(orderState, 0);
    assert.equal(user1Orders.length, 1);
    assert.equal(user1Orders[0], 1);
    assert.equal(shopOrders.length, 1);
    assert.equal(shopOrders[0], 1);

    expectEvent(placeOrderReceipt, 'OrderStateChanged',
      {_orderId: "1", _time: orderStateTimes[0], _state: "0"});
  });

  it("should NOT execute if order does not exist", async () => {
    const orderInst = dataInsts[DataType.ORDER];
    const riderInst = dataInsts[DataType.RIDER];
    const phone = 111, riderAddress = "area1";

    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});

    await expectRevert(
      riderInst.takeOrder(1, 1, {from: app}),
      "order does not exist");
    await expectRevert(
      orderInst.assignRider(1, 1, user1, {from: app}),
      "order does not exist");
    await expectRevert(
      orderInst.cancelRider(1, {from: app}),
      "order does not exist");
    await expectRevert(
      orderInst.orderStateChanger(1, 1, {from: app}),
      "order does not exist");
    await expectRevert(
      riderInst.takeOrder(1, 1, {from: app}),
      "order does not exist");
    await expectRevert(
      riderInst.cancelAssignedOrder(1, 1, {from: app}),
      "order does not exist");
  });

  it("should assgin rider and change order state", async () => {
    const riderInst = dataInsts[DataType.RIDER];
    const shopInst = dataInsts[DataType.SHOP];
    const orderInst = dataInsts[DataType.ORDER];
    const phone = 111, riderAddress = "area1";
    const shopPhone = 11, shopAdd = "area1";
    const [goodsIds, buyAmounts, totalPrice] = [[2, 3], [2, 1], 30000];
   
    await shopInst.registerShop(
      user1,
      shopPhone,
      shopAdd,
      {from: app});
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    // rider id 1
    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});
    // rider id 2
    await riderInst.registerRider(
      user2,
      phone,
      riderAddress,
      {from: app});
  
    // user2 rider take order and cancel it
    const assignRiderReceipt = await riderInst.takeOrder(2, 1, {from: app});
    const firstAssignedRider = await orderInst.getOrderRiderId(1);
    const firstRiderCurrentOrder = await riderInst.getRiderCurrentOrderId(2);
    const firstRiderNextOrder = await riderInst.getRiderNextOrderId(2);
    const cancelRiderReceipt = await riderInst.cancelAssignedOrder(2, 1, {from: app});
    const firstCancelledRider = await orderInst.getOrderRiderId(1);
    const firstRiderCurrentOrderAfterCancel = await riderInst.getRiderCurrentOrderId(2);
    const firstRidernextOrderAfterCancel = await riderInst.getRiderNextOrderId(2);

    assert.equal(firstAssignedRider, 2);
    assert.equal(firstRiderCurrentOrder, 1);
    assert.equal(firstRiderNextOrder, 0);
    assert.equal(firstCancelledRider, 0);
    assert.equal(firstRiderCurrentOrderAfterCancel, 0);
    assert.equal(firstRidernextOrderAfterCancel, 0);
    // expectEvent(assignRiderReceipt, "RiderAssigned",
    //   {_orderId: 1, _riderId: 2});
    // expectEvent(cancelRiderReceipt, "RiderCancelled",
    //   {_orderId: 1, _riderId: 2});
    /**
     *  These events are emitted properly.
     */
    
    await riderInst.takeOrder(1, 1, {from: app});
    const secondAssignedRider = await orderInst.getOrderRiderId(1);
    const secondRiderCurrentOrder = await riderInst.getRiderCurrentOrderId(1);
    const stateToPreparingReceipt = await orderInst.orderStateChanger(1, 1, {from: app});
    const stateToDeliveringReceipt = await orderInst.orderStateChanger(1, 2, {from: app});
    const stateToDeliveredReceipt = await orderInst.orderStateChanger(1, 3, {from: app});
    const stateToCompletedReceipt = await orderInst.orderStateChanger(1, 4, {from: app});
    const stateToCancelledReceipt = await orderInst.orderStateChanger(1, 5, {from: app});
    
    assert.equal(secondAssignedRider, 1);
    assert.equal(secondRiderCurrentOrder, 1);
    expectEvent(stateToPreparingReceipt, "OrderStateChanged",
      {_orderId: "1", _state: "1"});
    expectEvent(stateToDeliveringReceipt, "OrderStateChanged",
      {_orderId: "1", _state: "2"});
    expectEvent(stateToDeliveredReceipt, "OrderStateChanged",
      {_orderId: "1", _state: "3"});
    expectEvent(stateToCompletedReceipt, "OrderStateChanged",
      {_orderId: "1", _state: "4"});
    expectEvent(stateToCancelledReceipt, "OrderStateChanged",
      {_orderId: "1", _state: "5"});
  });

  it("should NOT execute assign functions", async () => {
    const riderInst = dataInsts[DataType.RIDER];
    const shopInst = dataInsts[DataType.SHOP];
    const orderInst = dataInsts[DataType.ORDER];
    const phone = 111, riderAddress = "area1";
    const shopPhone = 11, shopAdd = "area1";
    const [goodsIds, buyAmounts, totalPrice] = [[2, 3], [2, 1], 30000];
   
    await shopInst.registerShop(
      user1,
      shopPhone,
      shopAdd,
      {from: app});
    // order id 1
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    // order id 2
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    // order id 3
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    // rider id 1
    await riderInst.registerRider(
      user1,
      phone,
      riderAddress,
      {from: app});
    // rider id 2
    await riderInst.registerRider(
      user2,
      phone,
      riderAddress,
      {from: app});

    
    // the case if order has been assigned already
    await riderInst.takeOrder(1, 1, {from: app});
    
    await expectRevert(
      riderInst.takeOrder(2, 1, {from: app}),
      "order has already been assigned");
    await expectRevert(
      riderInst.cancelAssignedOrder(2, 1, {from: app}),
        "rider is not assigned this order");

    // the case if rider already took all 2 orders
    await riderInst.takeOrder(1, 2, {from: app});
    const riderCurOrder = await riderInst.getRiderCurrentOrderId(1);
    const riderNextOrder = await riderInst.getRiderNextOrderId(1);

    assert.equal(riderCurOrder, 1);
    assert.equal(riderNextOrder, 2);
    await expectRevert(
      riderInst.takeOrder(1, 3, {from: app}),
      "rider is already assigned 2 orders");
  });

  /**
   * REVIEW PART
   */
  const UserType = {
    BUYER: 0,
    RIDER: 1,
    SHOP: 2
  };
  
  it("should upload review", async () => {
    const userInst = dataInsts[DataType.USER];
    const shopInst = dataInsts[DataType.SHOP];
    const orderInst = dataInsts[DataType.ORDER];
    const riderInst = dataInsts[DataType.RIDER];
    const reviewInst = dataInsts[DataType.REVIEW];
    const shopPhone = 11, shopAdd = "area1";
    const [goodsIds, buyAmounts, totalPrice] = [[2, 3], [2, 1], 30000];
    const phone = 111, riderAddress = "area1";

    // BUYER: user1, SHOP: user2, RIDER: user3
    await shopInst.registerShop(
      user2,
      shopPhone,
      shopAdd,
      {from: app});
    await riderInst.registerRider(
      user3,
      phone,
      riderAddress,
      {from: app});
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    await riderInst.takeOrder(1, 1, {from: app});
    // order state should be in COMPLETED state
    await orderInst.orderStateChanger(1, 4, {from: app});
    
    // review id 1
    await reviewInst.uploadReview(
      UserType.BUYER,
      UserType.SHOP,
      1,
      5,
      {from: app});

    const fromType = await reviewInst.getReviewFromType(1);
    const toType = await reviewInst.getReviewToType(1);
    const orderId = await reviewInst.getReviewOrderId(1);
    const stars = await reviewInst.getReviewStars(1);
    const cancelled = await reviewInst.getReviewCancelled(1);

    assert.equal(fromType, 0);
    assert.equal(toType, 2);
    assert.equal(orderId, 1);
    assert.equal(stars, 5);
    assert.equal(cancelled, false);

    const user1Reviews = await userInst.getUserReviews(user1, {from: app});
    const user2Reviews = await userInst.getUserReviews(user2, {from: app});
    const user3Reviews = await userInst.getUserReviews(user3, {from: app});
    const riderReviews = await riderInst.getRiderReviews(1);
    const shopReviews = await shopInst.getShopReviews(1);

    assert.equal(user1Reviews.length, 0);
    assert.equal(user2Reviews.length, 1);
    assert.equal(user3Reviews.length, 0);
    assert.equal(riderReviews.length, 0);
    assert.equal(shopReviews.length, 1);
    assert.equal(user2Reviews[0], 1);
    assert.equal(shopReviews[0], 1);

    // review id 2
    await reviewInst.uploadReview(
      UserType.BUYER,
      UserType.RIDER,
      1,
      5,
      {from: app});
    // review id 3
    await reviewInst.uploadReview(
      UserType.RIDER,
      UserType.BUYER,
      1,
      5,
      {from: app});
    // review id 4
    await reviewInst.uploadReview(
      UserType.RIDER,
      UserType.SHOP,
      1,
      5,
      {from: app});
    // review id 5
    await reviewInst.uploadReview(
      UserType.SHOP,
      UserType.BUYER,
      1,
      5,
      {from: app});
    // review id 6
    await reviewInst.uploadReview(
      UserType.SHOP,
      UserType.RIDER,
      1,
      5,
      {from: app});

    const user1ReviewsAll = await userInst.getUserReviews(user1, {from: app});
    const user2ReviewsAll = await userInst.getUserReviews(user2, {from: app});
    const user3ReviewsAll = await userInst.getUserReviews(user3, {from: app});
    const riderReviewsAll = await riderInst.getRiderReviews(1);
    const shopReviewsAll = await shopInst.getShopReviews(1);

    assert.equal(user1ReviewsAll.length, 2);
    assert.equal(user2ReviewsAll.length, 2);
    assert.equal(user3ReviewsAll.length, 2);
    assert.equal(riderReviewsAll.length, 2);
    assert.equal(shopReviewsAll.length, 2);
    assert.equal(user1ReviewsAll[0], 3);
    assert.equal(user1ReviewsAll[1], 5);
    assert.equal(user2ReviewsAll[0], 1);
    assert.equal(user2ReviewsAll[1], 4);
    assert.equal(user3ReviewsAll[0], 2);
    assert.equal(user3ReviewsAll[1], 6);
    assert.equal(shopReviewsAll[0], 1);
    assert.equal(shopReviewsAll[1], 4);
    assert.equal(riderReviewsAll[0], 2);
    assert.equal(riderReviewsAll[1], 6);

    await reviewInst.cancelReview(3, {from: app});
    const cancelledReview = await reviewInst.getReviewCancelled(3);

    assert.equal(cancelledReview, true);
  });

  it("should NOT upload review", async () => {
    const userInst = dataInsts[DataType.USER];
    const shopInst = dataInsts[DataType.SHOP];
    const orderInst = dataInsts[DataType.ORDER];
    const riderInst = dataInsts[DataType.RIDER];
    const reviewInst = dataInsts[DataType.REVIEW];
    const shopPhone = 11, shopAdd = "area1";
    const [goodsIds, buyAmounts, totalPrice] = [[2, 3], [2, 1], 30000];
    const phone = 111, riderAddress = "area1";

    // if order does not exist
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "order does not exist");
    
    await shopInst.registerShop(
      user2,
      shopPhone,
      shopAdd,
      {from: app});
    await riderInst.registerRider(
      user3,
      phone,
      riderAddress,
      {from: app});
    await orderInst.placeOrder(
      user1,
      1,
      goodsIds,
      buyAmounts,
      totalPrice,
      {from: app});
    await riderInst.takeOrder(1, 1, {from: app});
    
    // if state is not in completed state
    // ORDERED
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "order should be in completed state");
    // PREPARING
    await orderInst.orderStateChanger(1, 1, {from: app});
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "order should be in completed state");
    // DELIVERING
    await orderInst.orderStateChanger(1, 2, {from: app});
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "order should be in completed state");
    // DELIVERED
    await orderInst.orderStateChanger(1, 3, {from: app});
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "order should be in completed state");
    
    // if it is self review
    await orderInst.orderStateChanger(1, 4, {from: app});
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.BUYER,
        1,
        5,
        {from: app}),
      "not allow self review");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.SHOP,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "not allow self review");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.RIDER,
        UserType.RIDER,
        1,
        5,
        {from: app}),
      "not allow self review");
    
    // if stars out of boundaries
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.RIDER,
        1,
        11,
        {from: app}),
      "stars should be <= 10, >= 1");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.RIDER,
        1,
        0,
        {from: app}),
      "stars should be <= 10, >= 1");

    // if review twice
    await reviewInst.uploadReview(
      UserType.BUYER,
      UserType.RIDER,
      1,
      5,
      {from: app});
    await reviewInst.uploadReview(
      UserType.BUYER,
      UserType.SHOP,
      1,
      5,
      {from: app});
    await reviewInst.uploadReview(
      UserType.SHOP,
      UserType.BUYER,
      1,
      5,
      {from: app});
    await reviewInst.uploadReview(
      UserType.SHOP,
      UserType.RIDER,
      1,
      5,
      {from: app});
    await reviewInst.uploadReview(
      UserType.RIDER,
      UserType.BUYER,
      1,
      5,
      {from: app});
    await reviewInst.uploadReview(
      UserType.RIDER,
      UserType.SHOP,
      1,
      5,
      {from: app});
    
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.RIDER,
        1,
        5,
        {from: app}),
      "allow review only once");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.BUYER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "allow review only once");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.SHOP,
        UserType.RIDER,
        1,
        5,
        {from: app}),
      "allow review only once");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.SHOP,
        UserType.BUYER,
        1,
        5,
        {from: app}),
      "allow review only once");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.RIDER,
        UserType.BUYER,
        1,
        5,
        {from: app}),
      "allow review only once");
    await expectRevert(
      reviewInst.uploadReview(
        UserType.RIDER,
        UserType.SHOP,
        1,
        5,
        {from: app}),
      "allow review only once");
  });

  /**
   * REPUTATION PART
   */
  it("should create user reputation information", async () => {
    const repuInst = dataInsts[DataType.REPUTATION];
    const user1Repu = await repuInst.getUserReputation(user1);
    const user1Rank = await repuInst.getUserRank(user1);
    const user1Address = await repuInst.getUserByRank(0);
    const user2Repu = await repuInst.getUserReputation(user2);
    const user2Rank = await repuInst.getUserRank(user2);
    const user2Address = await repuInst.getUserByRank(1);
    const user3Repu = await repuInst.getUserReputation(user3);
    const user3Rank = await repuInst.getUserRank(user3);
    const user3Address = await repuInst.getUserByRank(2);
    const ranks = await repuInst.getRanks();
    const maxRank = await repuInst.getMaxRank();

    assert.equal(user1Repu, 50000);
    assert.equal(user1Rank, 0);
    assert.equal(user1Address, user1);
    assert.equal(user2Repu, 50000);
    assert.equal(user2Rank, 1);
    assert.equal(user2Address, user2);
    assert.equal(user3Repu, 50000);
    assert.equal(user3Rank, 2);
    assert.equal(user3Address, user3);
    assert.equal(ranks.length, 3);
    assert.equal(ranks[0], user1);
    assert.equal(ranks[1], user2);
    assert.equal(ranks[2], user3);
    assert.equal(maxRank, 2);
  });

  it("should NOT execute if user does not exist", async () => {
    const repuInst = dataInsts[DataType.REPUTATION];

    await expectRevert(
      repuInst.reputationChanger(accounts[8], 1000, true),
      "user does not exist");
    await expectRevert(
      repuInst.getUserRank(accounts[9]),
      "user does not exist");
    await expectRevert(
      repuInst.getUserByRank(3),
      "this rank is not available");
  })
   
  /**
   * 7 users, user1 ~ 7, accounts[3 ~ 9]
   */
  it("should change reputation and rank", async () => {
    const [user4, user5, user6, user7] = [accounts[6], accounts[7], accounts[8], accounts[9]];
    const userInfo = { phone: 11, home: "area1", pubKey: "abc" };
    const userInst = dataInsts[DataType.USER];
    const repuInst = dataInsts[DataType.REPUTATION];
    for(let i = 6; i < 10; i++) {
      await userInst.registerUser(
        accounts[i],
        userInfo.phone,
        userInfo.home,
        userInfo.pubKey,
        {from: app});
    }
    
    const scores = [-2000, 6000, 0, -4000, 3000, 7000, 9000];
    // rank: 7, 6, 2, 5, 3, 1, 4
    const afterRanks = [5, 2, 4, 6, 3, 1, 0];
    const afterReps = new Array(7).fill(null).map((el, idx) => 50000 + scores[idx]);
    
    let receipts = new Array(10);
    let pos = new Array(7);
    for(let i = 3; i < 10; i++) {
      pos[i - 3] = true;
      if(scores[i - 3] < 0) {
        pos[i - 3] = false;
        scores[i - 3] -= 2 * scores[i - 3];
      }
      receipts[i] = await repuInst.reputationChanger(
        accounts[i], 
        scores[i - 3], 
        pos[i - 3], 
        {from: app});
    }

    for(let i = 3; i < 10; i++) {
      expectEvent(receipts[i], "ReputationChanged", { 
        _user: accounts[i], 
        _repId: String(i - 2), 
        _amount: String(scores[i - 3]), 
        _pos: pos[i - 3] });
      // RankChanged Events is well being emitted
      // expectEvent(receipts[i], "RankChanged", {
      //   _user: accounts[i], _beforeRank: String(i - 3)});
    }

    let userReps = new Array(10);
    let userRanks = new Array(10);
    for(let i = 3; i < 10; i++) {
      userReps[i] = await repuInst.getUserReputation(accounts[i]);
      userRanks[i] = await repuInst.getUserRank(accounts[i]);      
    }
    const ranks = await repuInst.getRanks();

    console.log("UserRanks::: ", userRanks);
    console.log("UserReps::: ", userReps);
    
    for(let i = 3; i < 10; i++) {
      assert.equal(userReps[i], afterReps[i - 3]);
      assert.equal(userRanks[i], afterRanks[i - 3]);
    }
    afterRanks.map((el, idx) => assert.equal(ranks[el], accounts[idx + 3]))
  });

  it("should adjust score", async () => {
    const repuInst = dataInsts[DataType.REPUTATION];
    const utilInst = await AdjustingScore.new();

    await utilInst.setAdmin(adminsInst.address, {from: owner});
    await adminsInst.approveUtil(utilInst.address, {from: admin});
    await repuInst.setAdjustScoreUtil(utilInst.address, {from: admin});
    // it makes user3 reputation 90000
    await repuInst.reputationChanger(user3, 40000, true, {from: app});
    // it makes user1 reputation 10000
    await repuInst.reputationChanger(user1, 40000, false, {from: app});
    
    const [user1BeforeRepu, user2BeforeRepu, user3BeforeRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];
    
    // expect 400
    await repuInst.adjustedReputationChanger(
      user3,
      2000,
      true,
      {from: app});
    // expect 2000
    await repuInst.adjustedReputationChanger(
      user2,
      2000,
      true,
      {from: app});
    // expect 3600
    await repuInst.adjustedReputationChanger(
      user1,
      2000,
      true,
      {from: app});
    const [user1AfterRepu, user2AfterRepu, user3AfterRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];

    assert( user3AfterRepu.eq(user3BeforeRepu.add(web3.utils.toBN("400"))) );
    assert( user2AfterRepu.eq(user2BeforeRepu.add(web3.utils.toBN("2000"))) );
    assert( user1AfterRepu.eq(user1BeforeRepu.add(web3.utils.toBN("3600"))) );

    // should reduce reputation properly, reducing should not be adjusted
    // current score::: user1:13600, user2:52000, user3:90400
    // expect 400
    await repuInst.adjustedReputationChanger(
      user3,
      90000,
      false,
      {from: app});
    // expect 12000 
    await repuInst.adjustedReputationChanger(
      user2,
      40000,
      false,
      {from: app});
    // expect 3600
    await repuInst.adjustedReputationChanger(
      user1,
      10000,
      false,
      {from: app});

    const [user1AfterAfterRepu, user2AfterAfterRepu, user3AfterAfterRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];
    
    assert( user1AfterAfterRepu.eq(web3.utils.toBN("3600")) );
    assert( user2AfterAfterRepu.eq(web3.utils.toBN("12000")) );
    assert( user3AfterAfterRepu.eq(web3.utils.toBN("400")) );

    const ranksAfter = await repuInst.getRanks();
    assert.equal(ranksAfter[0], user2);
    assert.equal(ranksAfter[1], user1);
    assert.equal(ranksAfter[2], user3);
  });

  it("should revert reputation", async () => {
    const repuInst = dataInsts[DataType.REPUTATION];
    const utilInst = await AdjustingScore.new();
    await utilInst.setAdmin(adminsInst.address, {from: owner});
    await adminsInst.approveUtil(utilInst.address, {from: admin});
    await repuInst.setAdjustScoreUtil(utilInst.address, {from: admin});

    const [user1BeforeRepu, user2BeforeRepu, user3BeforeRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];
    
    // expect +2000, repu Id = 1
    await repuInst.adjustedReputationChanger(
      user3,
      2000,
      true,
      {from: app});
    // expect +3000, repu Id = 2
    await repuInst.adjustedReputationChanger(
      user2,
      3000,
      true,
      {from: app});
    // expect +4000, repu Id = 3
    await repuInst.adjustedReputationChanger(
      user1,
      4000,
      true,
      {from: app});

    const [user1AfterRepu, user2AfterRepu, user3AfterRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];

    await repuInst.revertReputation(1, {from: app});
    await repuInst.revertReputation(3, {from: app});

    const [user1RevertedRepu, user2RevertedRepu, user3RevertedRepu] = [
      await repuInst.getUserReputation(user1),
      await repuInst.getUserReputation(user2),
      await repuInst.getUserReputation(user3) ];

    assert( user1BeforeRepu.eq(user1AfterRepu.sub(web3.utils.toBN("4000"))) );
    assert( user2BeforeRepu.eq(user2AfterRepu.sub(web3.utils.toBN("3000"))) );
    assert( user3BeforeRepu.eq(user3AfterRepu.sub(web3.utils.toBN("2000"))) );
    assert( user1RevertedRepu.eq(user1BeforeRepu) );
    assert( user3RevertedRepu.eq(user3BeforeRepu) );
    // user2 repu is not reverted
    assert( user2RevertedRepu.eq(user2BeforeRepu.add(web3.utils.toBN("3000"))) );
  });

  it("should normalize", async () => {
    const [user4, user5, user6, user7] = [accounts[6], accounts[7], accounts[8], accounts[9]];
    const userInfo = { phone: 11, home: "area1", pubKey: "abc" };
    const userInst = dataInsts[DataType.USER];
    const repuInst = dataInsts[DataType.REPUTATION];
    for(let i = 6; i < 10; i++) {
      await userInst.registerUser(
        accounts[i],
        userInfo.phone,
        userInfo.home,
        userInfo.pubKey,
        {from: app});
    }
    
    const scores = [-40000, 60000, 0, -4000, 30000, 7000, 19000];
    // rank: 7, 6, 2, 5, 3, 1, 4
    const afterRanks = [6, 0, 4, 5, 1, 3, 2];
    const afterReps = new Array(7).fill(null).map((el, idx) => 50000 + scores[idx]);

    let receipts = new Array(10);
    let pos = new Array(7);
    for(let i = 3; i < 10; i++) {
      pos[i - 3] = true;
      if(scores[i - 3] < 0) {
        pos[i - 3] = false;
        scores[i - 3] -= 2 * scores[i - 3];
      }
      receipts[i] = await repuInst.reputationChanger(
        accounts[i], 
        scores[i - 3], 
        pos[i - 3], 
        {from: app});
    }

    const ranksBeforeNormalize = await repuInst.getRanks();
    const repusBeforeNormalize = new Array(10);
    for(let i = 3; i < 10; i++) repusBeforeNormalize[i] = await repuInst.getUserReputation(accounts[i]);

    await repuInst.normalizeReputation({from: admin});

    const ranksAfterNormalize = await repuInst.getRanks();
    const repusAfterNormalize = new Array(10);
    for(let i = 3; i < 10; i++) repusAfterNormalize[i] = await repuInst.getUserReputation(accounts[i]);

    for(let i = 0; i < ranksBeforeNormalize.length; i++) assert.equal(ranksBeforeNormalize[i], ranksAfterNormalize[i]);      
    
    ranksBeforeNormalize.map((user, idx) => console.log(`BEFORE RANK ${idx}:: ${user}`));
    ranksAfterNormalize.map((user, idx) => console.log(`AFTER RANK ${idx}:: ${user}`));
    
    for(let i = 3; i < 10; i++) {
      console.log(`BEFORE accounts ${i} :: ${repusBeforeNormalize[i]}`);
      console.log(`AFTER accounts ${i} :: ${repusAfterNormalize[i]}`);
    }
  });
});