const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const ethers = require("ethers");

const Admins = artifacts.require("./Admins.sol");
const DeliveryUser = artifacts.require("./data/DeliveryUser.sol");
const DeliveryReputation = artifacts.require("./data/DeliveryReputation.sol");
const KRWComm = artifacts.require("./utils/KRWCommission.sol");
const KNGScoring = artifacts.require("./utils/KNGScoring.sol");
const Wallet = artifacts.require("./finance/Wallet.sol");
const Kng = artifacts.require("./mocks/Kng.sol");
const Krw = artifacts.require("./mocks/Krw.sol");

contract("Wallet", accounts => {
  const bnWei = (_val) => { return web3.utils.toBN(web3.utils.toWei(_val)); }
  const [KNG, KRW] = [ethers.utils.formatBytes32String('KNG'), ethers.utils.formatBytes32String('KRW')];

  let adminsInst, walletInst, userInst, repuInst, comInst, kngScoringInst;
  let kng, krw;

  const [owner, admin, app, _] = accounts;
  // Set users, users[0~6] = accounts[3~9]
  let users = [];
  for(let i = 3; i < 10; i++) {
    users.push(accounts[i]);
  }

  beforeEach(async () => {
    adminsInst = await Admins.new();
    walletInst = await Wallet.new();
    userInst = await DeliveryUser.new();
    repuInst = await DeliveryReputation.new();
    comInst = await KRWComm.new();
    kngScoringInst = await KNGScoring.new();
    [kng, krw] = await Promise.all([ Kng.new(), Krw.new() ]);

    // set admin
    await adminsInst.addAdmin(admin, {from: owner});
    // set data instance addresses
    await adminsInst.approveData(userInst.address, 0, {from: admin});
    await adminsInst.approveData(repuInst.address, 5, {from: admin});
    // set util instance address
    await adminsInst.approveUtil(comInst.address, {from: admin});
    await adminsInst.approveUtil(kngScoringInst.address, {from: admin});
    // set app instance address (address is accounts[2] for test case)
    await adminsInst.approveApp(app, {from: admin});
    // set admin address to each contract
    await userInst.setAdmin(adminsInst.address, {from: owner});
    await repuInst.setAdmin(adminsInst.address, {from: owner});
    await walletInst.setAdmin(adminsInst.address, {from: owner});
    await comInst.setAdmin(adminsInst.address, {from: owner});
    await kngScoringInst.setAdmin(adminsInst.address, {from: owner});
    // set krw commission util to wallet contract
    await walletInst.setKrwCommissionUtil(comInst.address, {from: admin});
    await walletInst.setKngScoringUtil(kngScoringInst.address, {from: admin});
    
    // register user
    // const userInfo = { phone: 11, home: "area", pubKey: "abc" };
    // for(let i = 0; i < 7; i++) {
    //   await userInst.registerUser(
    //     users[i],
    //     userInfo.phone,
    //     userInfo.home,
    //     userInfo.pubKey,
    //     {from: app});
    // }

    // add token info to wallet contract
    await walletInst.addToken(KNG, kng.address, {from: admin});
    await walletInst.addToken(KRW, krw.address, {from: admin});

    // faucet the coin to each user with 1000 ether amount
    const seedAmount = bnWei("1000");
    const seedTokenBalance = async (_token, _user) => {
      await _token.faucet(_user, seedAmount);
      await _token.approve(walletInst.address, seedAmount, {from: _user});
    };
    for(let i = 0; i < 7; i++) {
      await Promise.all( [kng, krw].map(token => seedTokenBalance(token, users[i])) );
    }
  });

  it("should NOT execute if not admin", async () => {
    await expectRevert(
      walletInst.addToken(ethers.utils.formatBytes32String("HAHA"), kng.address, {from: app}),
      "only admin");
    await expectRevert(
      walletInst.setKrwCommissionUtil(krw.address, {from: app}),
      "only admin");
  });

  it("should NOT set util if util not approved", async () => {
    await expectRevert(
      walletInst.setKrwCommissionUtil(krw.address, {from: admin}),
      "only approved util");
  });

  it("should get token balances", async () => {
    let kngBalances = new Array(7);
    let krwBalances = new Array(7);
    for(let i = 0; i < 7; i++) {
      kngBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
      krwBalances[i] = await krw.balanceOf(users[i], {from: users[i]});
    }

    const seedAmount = bnWei("1000");
    kngBalances.map( balance => assert(balance.eq(seedAmount)) );
    krwBalances.map( balance => assert(balance.eq(seedAmount)) );
  });
  
  it("should deposit properly", async () => {
    const depositAmount = bnWei("100");
    await Promise.all( users.map(user => walletInst.deposit(depositAmount, KNG, {from: user})) );
    await Promise.all( users.map(user => walletInst.deposit(depositAmount, KRW, {from: user})) );
    
    let [kngBalances, krwBalances] = [new Array(7), new Array(7)];
    for(let i = 0; i < 7; i++) {
      kngBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
      krwBalances[i] = await krw.balanceOf(users[i], {from: users[i]});
    }
    let [kngWalletBalances, krwWalletBalances] = [new Array(7), new Array(7)];
    for(let i = 0; i < 7; i++) {
      kngWalletBalances[i] = await walletInst.getBalance(users[i], KNG, {from: users[i]});
      krwWalletBalances[i] = await walletInst.getBalance(users[i], KRW, {from: users[i]});
    }
    

    for(let i = 0; i < 7; i++) {
      let commission = await (comInst.calculateCommission(users[i]));
      // Check via console since it is not divisible
      console.log(`user${i} commission:: `, commission.toNumber());
    }

    // token contract balances
    const remainedAmount = bnWei("900");
    kngBalances.map( balance => assert(balance.eq(remainedAmount)) );
    krwBalances.map( balance => assert(balance.eq(remainedAmount)) );
    // wallet kng balances
    kngWalletBalances.map( balance => assert(balance.eq(depositAmount)) );
    // rank 0 charged fee 2.5, rank 6 charged fee 7.5
    // check only 2 cases since some commissions are not divisible
    assert(krwWalletBalances[0].eq(bnWei("97.5")));
    assert(krwWalletBalances[6].eq(bnWei("92.5")));

    const contractKngBalance = await walletInst.getBalance(walletInst.address, KNG, {from: walletInst.address});
    const contractKrwBalance = await walletInst.getBalance(walletInst.address, KRW, {from: walletInst.address});

    assert.equal(contractKngBalance, 0);
    // Check via console since it is quite complicated to calculate
    console.log(contractKrwBalance.toString());
  });

  it("should NOT deposit", async () => {
    // if token does not exist
    await expectRevert(
      walletInst.deposit(1000, ethers.utils.formatBytes32String("HAHA"), {from: users[0]}),
      "token does not exist");
    // if not enough token balance. Revert messages from ERC20
    await expectRevert(
      walletInst.deposit(bnWei("1001"), KRW),
      "ERC20: transfer amount exceeds balance");
  });

  it("should withdraw", async () => {
    const depositAmount = bnWei("100");
    await Promise.all( users.map(user => walletInst.deposit(depositAmount, KNG, {from: user})) );
    await Promise.all( users.map(user => walletInst.deposit(depositAmount, KRW, {from: user})) );
    
    let [beforeKngTokenBalances, beforeKngWalletBalances, beforeKrwTokenBalances, beforeKrwWalletBalances]
      = [Array(7), Array(7), Array(7), Array(7)];
    for(let i = 0; i < 7; i++) {
      beforeKngTokenBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
      beforeKrwTokenBalances[i] = await krw.balanceOf(users[i], {from: users[i]});
      beforeKngWalletBalances[i] = await walletInst.getBalance(users[i], KNG, {from: users[i]});
      beforeKrwWalletBalances[i] = await walletInst.getBalance(users[i], KRW, {from: users[i]});
    }

    const withdrawAmount = bnWei("50");
    await Promise.all( users.map(user => walletInst.withdraw(withdrawAmount, KNG, {from: user})) );
    await Promise.all( users.map(user => walletInst.withdraw(withdrawAmount, KRW, {from: user})) );

    let [afterKngTokenBalances, afterKngWalletBalances, afterKrwTokenBalances, afterKrwWalletBalances] 
      = [Array(7), Array(7), Array(7), Array(7)];
    for(let i = 0; i < 7; i++) {
      afterKngTokenBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
      afterKrwTokenBalances[i] = await krw.balanceOf(users[i], {from: users[i]});
      afterKngWalletBalances[i] = await walletInst.getBalance(users[i], KNG, {from: users[i]});
      afterKrwWalletBalances[i] = await walletInst.getBalance(users[i], KRW, {from: users[i]});
    }

    for(let i = 0; i < 7; i++) {
      assert( afterKngTokenBalances[i].eq(beforeKngTokenBalances[i].add(withdrawAmount)) );
      assert( afterKrwTokenBalances[i].eq(beforeKrwTokenBalances[i].add(withdrawAmount)) );
      assert( afterKngWalletBalances[i].eq(beforeKngWalletBalances[i].sub(withdrawAmount)) );
      assert( afterKrwWalletBalances[i].eq(beforeKrwWalletBalances[i].sub(withdrawAmount)) );
    }
  });

  it("should NOT withdraw", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});

    // if not enough Balance
    await expectRevert(
      walletInst.withdraw(bnWei("101"), KRW, {from: users[0]}),
      "not enough balance");
    // if token does not exist
    await expectRevert(
      walletInst.withdraw(bnWei("50"), ethers.utils.formatBytes32String("HAHA"), {from: users[0]}),
      "token does not exist");
  });

  it("should transfer", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});
    await walletInst.deposit(bnWei("100"), KNG, {from: users[0]});

    let [beforeKngWalletBalances, beforeKrwWalletBalances] = [Array(2), Array(2)];
    for(let i = 0; i < 2; i++) {
      beforeKngWalletBalances[i] = await walletInst.getBalance(users[i], KNG, {from: users[i]});
      beforeKrwWalletBalances[i] = await walletInst.getBalance(users[i], KRW, {from: users[i]});
    }

    const transferAmount = bnWei("50");
    await walletInst.transfer(
      users[0],
      users[1],
      transferAmount,
      KRW,
      {from: users[0]});
    await walletInst.transfer(
      users[0],
      users[1],
      transferAmount,
      KNG,
      {from: users[0]});

    let [afterKngWalletBalances, afterKrwWalletBalances] = [Array(2), Array(2)];
    for(let i = 0; i < 2; i++) {
      afterKngWalletBalances[i] = await walletInst.getBalance(users[i], KNG, {from: users[i]});
      afterKrwWalletBalances[i] = await walletInst.getBalance(users[i], KRW, {from: users[i]});
    }

    assert(afterKngWalletBalances[0].eq(beforeKngWalletBalances[0].sub(transferAmount)));
    assert(afterKrwWalletBalances[0].eq(beforeKrwWalletBalances[0].sub(transferAmount)));
    assert(afterKngWalletBalances[1].eq(beforeKngWalletBalances[1].add(transferAmount)));
    assert(afterKrwWalletBalances[1].eq(beforeKrwWalletBalances[1].add(transferAmount)));
  });

  it("should NOT transfer", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});
    
    // if not enough balance
    await expectRevert(
      walletInst.transfer(
        users[0],
        users[1],
        bnWei("101"),
        KRW,
        {from: users[0]}),
      "not enough balance");
    // if token does not exist
    await expectRevert(
      walletInst.transfer(
        users[0],
        users[1],
        bnWei("50"),
        ethers.utils.formatBytes32String("HAHA"),
        {from: users[0]}),
      "token does not exist");
  });

  it("should hold and release KRW", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});
    
    let [beforeKrwBalances, afterKrwBalances] = [Array(2), Array(2)]; 
    beforeKrwBalances[0] = await walletInst.getBalance(users[0], KRW, {from: users[0]});
    beforeKrwBalances[1] = await walletInst.getBalance(users[1], KRW, {from: users[1]});

    const holdAmount = bnWei("50");
    await walletInst.holdKrw(users[0], holdAmount, {from: app});
    let beforeHoldingAmount = await walletInst.getPendingKrwBalance(users[0], {from: users[0]});

    afterKrwBalances[0] = await walletInst.getBalance(users[0], KRW, {from: users[0]});
    
    assert(beforeHoldingAmount.eq(holdAmount));
    assert(afterKrwBalances[0].eq(beforeKrwBalances[0].sub(holdAmount)));

    const releaseAmount = bnWei("30");
    await walletInst.releaseKRW(
      users[0],
      users[1],
      releaseAmount,
      {from: app});
    let afterHoldingAmount = await walletInst.getPendingKrwBalance(users[0], {from: users[0]});
    
    afterKrwBalances[1] = await walletInst.getBalance(users[1], KRW, {from: users[1]});
    
    assert(afterHoldingAmount.eq(beforeHoldingAmount.sub(releaseAmount)));
    assert(afterKrwBalances[1].eq(beforeKrwBalances[1].add(releaseAmount)));

    // release to self, remained amount is "20"
    beforeKrwBalances[0] = await walletInst.getBalance(users[0], KRW, {from: users[0]});
    beforeHoldingAmount = await walletInst.getPendingKrwBalance(users[0], {from: users[0]});

    await walletInst.releaseKRW(
      users[0],
      users[0],
      bnWei("20"),
      {from: app});
    afterHoldingAmount = await walletInst.getPendingKrwBalance(users[0], {from: users[0]});

    afterKrwBalances[0] = await walletInst.getBalance(users[0], KRW, {from: users[0]});

    assert(beforeHoldingAmount.eq(bnWei("20")));
    assert(afterHoldingAmount.eq(bnWei("0")));
    assert( afterKrwBalances[0].eq(beforeKrwBalances[0].add(bnWei("20"))) );
  });

  it("should NOT hold", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});

    // if not enough balance
    await expectRevert(
      walletInst.holdKrw( users[0], bnWei("101"), {from: app} ), 
      "not enough balance");    
    // not allow to execute
    await expectRevert(
        walletInst.holdKrw( users[0], bnWei("50"), {from: users[0]} ), 
        "only approved app");    
    });

  it("should NOT release", async () => {
    await walletInst.deposit(bnWei("100"), KRW, {from: users[0]});
    await walletInst.holdKrw(users[0], bnWei("90"), {from: app});

    // if not enough balance
    await expectRevert(
      walletInst.releaseKRW(
        users[0], 
        users[0], 
        bnWei("91"), 
        {from: app}),
        "not enough pending balance");
    // not allow to execute
    await expectRevert(
      walletInst.releaseKRW(
        users[0], 
        users[0], 
        bnWei("80"), 
        {from: users[0]}),
        "only approved app");
  });

  it("should faucet KNG token", async () => {
    const faucetAmount = web3.utils.toBN("1000");
    const beforeKngBalances = new Array(7);
    const afterKngBalances = new Array(7);

    for(let i = 0; i < 7; i++) {
      beforeKngBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
    }

    for(let i = 0; i < 7; i++) {
      await walletInst.faucetKngToken(users[i], faucetAmount, {from: app});
    }

    for(let i = 0; i < 7; i++) {
      afterKngBalances[i] = await kng.balanceOf(users[i], {from: users[i]});
    }

    console.log(afterKngBalances[0].toString());
    console.log(beforeKngBalances[0].toString());

    assert( (afterKngBalances[0].sub(beforeKngBalances[0])).eq(web3.utils.toBN("1500")) );
    assert( (afterKngBalances[3].sub(beforeKngBalances[3])).eq(web3.utils.toBN("1000")) );
    assert( (afterKngBalances[6].sub(beforeKngBalances[6])).eq(web3.utils.toBN("500")) );
  });

  it.only("front deposit test", async () => {
    const userInfo = { phone: 11, home: "area", pubKey: "abc" };
      await userInst.registerUser(
        users[0],
        userInfo.phone,
        userInfo.home,
        userInfo.pubKey,
        {from: app});
        // await userInst.registerUser(
        //   users[1],
        //   userInfo.phone,
        //   userInfo.home,
        //   userInfo.pubKey,
        //   {from: app});
  
    const maxRank = await repuInst.getMaxRank();
    const userRank = await repuInst.getUserRank(users[0]);

    console.log(maxRank.toNumber(), userRank.toNumber());
    
    const amount = 123;
    await walletInst.deposit(amount, KRW, {from: users[0]});
  });
});