const ethers = require("ethers");

// Admins
const Admins = artifacts.require("./Admins.sol");
// Utils
const AdjustingScore = artifacts.require("./utils/AdjustingScore.sol");
const KNGScoring = artifacts.require("./utils/KNGScoring.sol");
const KRWCommission = artifacts.require("./utils/KRWCommission.sol");
// Finance
const Kng = artifacts.require("./finance/mocks/Kng.sol");
const Krw = artifacts.require("./finance/mocks/Krw.sol");
const Wallet = artifacts.require("./finance/Wallet.sol");
// Data
const DeliveryOrder = artifacts.require("./data/DeliveryOrder.sol");
const DeliveryReputation = artifacts.require("./data/DeliveryReputation.sol");
const DeliveryReview = artifacts.require("./data/DeliveryReview.sol");
const DeliveryRider = artifacts.require("./data/DeliveryRider.sol");
const DeliveryShop = artifacts.require("./data/DeliveryShop.sol");
const DeliveryUser = artifacts.require("./data/DeliveryUser.sol");
// App
const AppDatas = artifacts.require("./app/AppDatas.sol");
const DeliveryAppOrder = artifacts.require("./app/DeliveryAppOrder.sol");
const DeliveryAppRider = artifacts.require("./app/DeliveryAppRider.sol");
const DeliveryAppReview = artifacts.require("./app/DeliveryAppReview.sol");
const DeliveryAppShop = artifacts.require("./app/DeliveryAppShop.sol");
const DeliveryAppUser = artifacts.require("./app/DeliveryAppUser.sol");
const DeliveryAppVoting = artifacts.require("./app/DeliveryAppVoting.sol");

module.exports = async (deployer, _network, _accounts) => {
  const [KNG, KRW] = [ethers.utils.formatBytes32String('KNG'), ethers.utils.formatBytes32String('KRW')]
  const [owner, admin, _] = _accounts;

  // Deploy contracts
  await Promise.all( [
      Admins,
      AdjustingScore, KNGScoring, KRWCommission,
      Kng, Krw, Wallet,
      DeliveryOrder, DeliveryReputation, DeliveryReview, DeliveryRider, DeliveryShop, DeliveryUser,
      AppDatas, DeliveryAppOrder, DeliveryAppRider, DeliveryAppReview, DeliveryAppShop, DeliveryAppUser, DeliveryAppVoting
    ].map(_contract => deployer.deploy(_contract)) );
    
  adminsInst = await Admins.deployed();
  repuUtilInst = await AdjustingScore.deployed();
  kngUtilInst = await KNGScoring.deployed();
  krwUtilInst = await KRWCommission.deployed();
  kngInst = await Kng.deployed();
  krwInst = await Krw.deployed();
  walletInst = await Wallet.deployed();
  orderDataInst = await DeliveryOrder.deployed();
  repuDataInst = await DeliveryReputation.deployed();
  reviewDataInst = await DeliveryReview.deployed();
  riderDataInst = await DeliveryRider.deployed();
  shopDataInst = await DeliveryShop.deployed();
  userDataInst = await DeliveryUser.deployed();
  appDatasInst = await AppDatas.deployed();
  orderAppInst = await DeliveryAppOrder.deployed();
  riderAppInst = await DeliveryAppRider.deployed();
  reviewAppInst = await DeliveryAppReview.deployed();
  shopAppInst = await DeliveryAppShop.deployed();
  userAppInst = await DeliveryAppUser.deployed();
  voteAppInst = await DeliveryAppVoting.deployed();

  /**
   * INITIALIZATION
   *
   * ADMIN PART
   */
  // add admin user
  await adminsInst.addAdmin(admin, {from: owner});
  // add datas
  await adminsInst.approveData(orderDataInst.address, 3, {from: admin});
  await adminsInst.approveData(repuDataInst.address, 5, {from: admin});
  await adminsInst.approveData(reviewDataInst.address, 4, {from: admin});
  await adminsInst.approveData(riderDataInst.address, 2, {from: admin});
  await adminsInst.approveData(shopDataInst.address, 1, {from: admin});
  await adminsInst.approveData(userDataInst.address, 0, {from: admin});
  // add apps
  await adminsInst.approveApp(appDatasInst.address, {from: admin});
  await adminsInst.approveApp(orderAppInst.address, {from: admin});
  await adminsInst.approveApp(riderAppInst.address, {from: admin});
  await adminsInst.approveApp(reviewAppInst.address, {from: admin});
  await adminsInst.approveApp(shopAppInst.address, {from: admin});
  await adminsInst.approveApp(userAppInst.address, {from: admin});
  await adminsInst.approveApp(voteAppInst.address, {from: admin});
  // add utils
  await adminsInst.approveUtil(repuUtilInst.address, {from: admin});
  await adminsInst.approveUtil(kngUtilInst.address, {from: admin});
  await adminsInst.approveUtil(krwUtilInst.address, {from: admin});
  
  /**
   * UTIL PART
   */
  // set admin
  await repuUtilInst.setAdmin(adminsInst.address, {from: owner});
  await kngUtilInst.setAdmin(adminsInst.address, {from: owner});
  await krwUtilInst.setAdmin(adminsInst.address, {from: owner});

  /**
   * FINANCE PART
   */
  // set admin
  await walletInst.setAdmin(adminsInst.address, {from: owner});
  // set util
  await walletInst.setKngScoringUtil(kngUtilInst.address, {from: admin});
  await walletInst.setKrwCommissionUtil(krwUtilInst.address, {from: admin});
  // add token
  await walletInst.addToken(KRW, krwInst.address, {from: admin});
  await walletInst.addToken(KNG, kngInst.address, {from: admin});
  // pre faucet krw tokens to each user
  const seedAmount = web3.utils.toBN("10000000000");
  const seedTokenBalance = async (_token, _user) => {
    await _token.faucet(_user, seedAmount);
    await _token.approve(walletInst.address, seedAmount, {from: _user});
  }
  // for(let i = 0; i < 10; i++) {
  //   await Promise.all( [kngInst, krwInst].map(_token => seedTokenBalance(_token, _accounts[i])) );
  // }
  await Promise.all( _accounts.map(_user => seedTokenBalance(krwInst, _user)) );

  /**
   * DATA PART
   */
  // set admin each data
  await orderDataInst.setAdmin(adminsInst.address, {from: owner});
  await repuDataInst.setAdmin(adminsInst.address, {from: owner});
  await reviewDataInst.setAdmin(adminsInst.address, {from: owner});
  await riderDataInst.setAdmin(adminsInst.address, {from: owner});
  await shopDataInst.setAdmin(adminsInst.address, {from: owner});
  await userDataInst.setAdmin(adminsInst.address, {from: owner});
  // set repu util
  await repuDataInst.setAdjustScoreUtil(repuUtilInst.address, {from: admin});

  /**
   * APP PART
   */
  // set data addresses
  await appDatasInst.setDataAddresses(0, userDataInst.address, {from: owner});
  await appDatasInst.setDataAddresses(1, riderDataInst.address, {from: owner});
  await appDatasInst.setDataAddresses(2, shopDataInst.address, {from: owner});
  await appDatasInst.setDataAddresses(3, orderDataInst.address, {from: owner});
  await appDatasInst.setDataAddresses(4, reviewDataInst.address, {from: owner});
  await appDatasInst.setDataAddresses(5, repuDataInst.address, {from: owner});
  // set appdata address to each app
  await userAppInst.setAppDatas(appDatasInst.address, {from: owner});
  await riderAppInst.setAppDatas(appDatasInst.address, {from: owner});
  await shopAppInst.setAppDatas(appDatasInst.address, {from: owner});
  await orderAppInst.setAppDatas(appDatasInst.address, {from: owner});
  await reviewAppInst.setAppDatas(appDatasInst.address, {from: owner});
  await voteAppInst.setAppDatas(appDatasInst.address, {from: owner});
  // set wallet address to each app
  await userAppInst.setWallet(walletInst.address, {from: owner});
  await riderAppInst.setWallet(walletInst.address, {from: owner});
  await shopAppInst.setWallet(walletInst.address, {from: owner});
  await orderAppInst.setWallet(walletInst.address, {from: owner});
  await reviewAppInst.setWallet(walletInst.address, {from: owner});
  await voteAppInst.setWallet(walletInst.address, {from: owner});

  for(let i = 0; i < 5; i++) {
    await userAppInst.registerUser(
      01012341234,
      "area1",
      "0x123",
      {from: _accounts[i]} );
    await walletInst.deposit(web3.utils.toBN("100000"), KRW, {from: _accounts[i]});
  }
  await shopAppInst.registerShop(010000000000, "area1", {from: _accounts[0]});
  await riderAppInst.registerRider(01011111111, "area1", {from: _accounts[1]});
  await orderAppInst.placeOrder(
    1,
    [2, 3, 3, 5],
    25000,
    {from: _accounts[2]} );
  await riderAppInst.takeOrder(1, {from: _accounts[1]});
  await orderAppInst.signOrder(1, {from: _accounts[0]});
  await orderAppInst.signOrder(1, {from: _accounts[1]});
  await orderAppInst.signOrder(1, {from: _accounts[1]});
  await orderAppInst.signOrder(1, {from: _accounts[2]});

  const [BUYER, RIDER, SHOP] = [0, 1, 2];

  await reviewAppInst.uploadReview(
    SHOP,
    1,
    10,
    {from: _accounts[2]});
  await reviewAppInst.uploadReview(
    RIDER,
    1,
    8,
    {from: _accounts[2]});
  await reviewAppInst.uploadReview(
    BUYER,
    1,
    6,
    {from: _accounts[0]} );
  await reviewAppInst.uploadReview(
    RIDER,
    1,
    5,
    {from: _accounts[0]} );
  await reviewAppInst.uploadReview(
    BUYER,
    1,
    3,
    {from: _accounts[1]} );
  await reviewAppInst.uploadReview(
    SHOP,
    1,
    1,
    {from: _accounts[1]} );

  // orderID 2
  await orderAppInst.placeOrder(
    1,
    [2, 3, 3, 5],
    25000,
    {from: _accounts[2]} );
  // orderID 3  
  await orderAppInst.placeOrder(
    1,
    [5, 5, 5, 6],
    40000,
    {from: _accounts[3]} );
  // orderID 4
  await orderAppInst.placeOrder(
    1,
    [7, 8, 9, 1],
    55000,
    {from: _accounts[4]} );
  await riderAppInst.takeOrder(4, {from: _accounts[1]});
  await orderAppInst.signOrder(4, {from: _accounts[0]});

  const [REVIEW, PENALTY, PRAISE] = [0, 1, 2];

  await voteAppInst.startVote(
    PENALTY,
    _accounts[0],
    0,
    {from: _accounts[1]} );
  await voteAppInst.startVote(
    PRAISE,
    _accounts[0],
    0,
    {from: _accounts[1]} );
  await voteAppInst.startVote(
    REVIEW,
    _accounts[0],
    1,
    {from: _accounts[1]} );
  await voteAppInst.startVote(
    PENALTY,
    _accounts[0],
    0,
    {from: _accounts[1]} );
  await voteAppInst.cancelVote(4, {from: _accounts[1]});
}