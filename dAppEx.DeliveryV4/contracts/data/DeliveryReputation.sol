// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryUser.sol";
import "../utils/AdjustingScore.sol";

/**
 * It ranks and scores the user reputations
 */
contract DeliveryReputation is CommonData {
  using SafeMath for uint;

  struct ReputationRecord {
    uint repId;
    uint score;
    address target;
    bool pos;
    bool reverted;
  }

  struct UserRepInfo {
    address userAddress;
    uint reputation;
    uint rank;
  }

  event ReputationChanged(address _user, uint _repId, uint _amount, bool _pos);
  event RankChanged(address _user, uint _beforeRank, uint _afterRank);

  mapping(uint => ReputationRecord) reputationRecords;
  mapping(address => UserRepInfo) userRepInfos;
  address[] users_byRank;
  uint nextRepId = 1;

  uint constant public MAXREP = 100000;

  address adjustScoreUtilAddress;

  constructor() public { owner = msg.sender; }

  modifier userExists(address _userAddress) {
    require(userRepInfos[_userAddress].userAddress != address(0),
      "user does not exist");
    _;
  }

  modifier recordExists(uint _repId) {
    require(reputationRecords[_repId].repId != 0,
      "record does not exist");
    _;
  }

  function setAdjustScoreUtil(address _utilAddress)
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);
    Admins(admin).onlyApprovedUtilTrigger(_utilAddress);

    adjustScoreUtilAddress = _utilAddress;
  }

  /**
   * @notice It triggers by User Contract only once per user when the user newly register
   */
  function initializeUserRep(address _userAddress)
  public {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);
    require(userRepInfos[_userAddress].userAddress == address(0),
      "user already exists");

    userRepInfos[_userAddress] = UserRepInfo(_userAddress, MAXREP / 2, users_byRank.length);
    users_byRank.push(_userAddress);
    _rankByReputation(_userAddress);
  }

  /**
   * @notice It raises or reduces the user reputation
   * @dev commission fee should be pre-applied by external sources
   * @param _pos If it is positive, reputation goes up, otherwise goes down
   */
  function reputationChanger(
    address _userAddress,
    uint _amount,
    bool _pos)
  public
  userExists(_userAddress)
  returns(uint _repId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    UserRepInfo storage userRepInfoCopy = userRepInfos[_userAddress];
    if(_pos) {
      userRepInfoCopy.reputation = (userRepInfoCopy.reputation.add(_amount) <= MAXREP) ?
        userRepInfoCopy.reputation.add(_amount) : MAXREP;
    }
    if(!_pos) {
      userRepInfoCopy.reputation = userRepInfoCopy.reputation.sub(_amount);
    }

    reputationRecords[nextRepId] = ReputationRecord(
      nextRepId,
      _amount,
      _userAddress,
      _pos,
      false);

    emit ReputationChanged(_userAddress, nextRepId, _amount, _pos);

    _repId = nextRepId;
    nextRepId = nextRepId.add(1);

    _rankByReputation(_userAddress);
  }

  function adjustedReputationChanger(
    address _userAddress,
    uint _score,
    bool _pos)
  public
  userExists(_userAddress)
  returns(uint _repId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    UserRepInfo storage userRepInfoCopy = userRepInfos[_userAddress];

    uint score;
    if(_pos) {
      score = AdjustingScore(adjustScoreUtilAddress).adjustScore(_userAddress, _score);
      userRepInfoCopy.reputation = userRepInfoCopy.reputation.add(score);
    }
    if(!_pos) {
      // Reducing reputation doesn't adjust score
      score = _score;
      userRepInfoCopy.reputation = userRepInfoCopy.reputation.sub(score);
    }

    reputationRecords[nextRepId] = ReputationRecord(
      nextRepId,
      score,
      _userAddress,
      _pos,
      false);

    emit ReputationChanged(_userAddress, nextRepId, score, _pos);

    _repId = nextRepId;
    nextRepId = nextRepId.add(1);

    _rankByReputation(_userAddress);
  }

  function revertReputation(uint _repId)
  public
  returns(uint) {
    require(reputationRecords[_repId].repId != 0,
      "record does not exists");
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    ReputationRecord storage recordCopy = reputationRecords[_repId];
    uint id = reputationChanger(recordCopy.target, recordCopy.score, !recordCopy.pos);

    return id;
  }

  function normalizeReputation()
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);

    uint HALFREP = MAXREP / 2;
    for(uint i = 0; i < users_byRank.length; i++) {
      address user = users_byRank[i];
      uint userRep = userRepInfos[user].reputation;

      if(userRep > HALFREP) {
        uint scaleAmount = (userRep - HALFREP) * 3 / 10;
        userRep = userRep.sub(scaleAmount);
      }
      if(userRep < HALFREP) {
        uint scaleAmount = (HALFREP - userRep) * 3 / 10;
        userRep = userRep.add(scaleAmount);
      }

      userRepInfos[user].reputation = userRep;
    }
  }

  function getUserReputation(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint) { return userRepInfos[_userAddress].reputation; }

  function getUserRank(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint) { return userRepInfos[_userAddress].rank; }

  function getUserByRank(uint _rank)
  public view
  returns(address) {
    require(_rank < users_byRank.length,
      "this rank is not available");

    return users_byRank[_rank];
  }

  function getRanks()
  public view
  returns(address[] memory) { return users_byRank; }

  /**
   * @notice It returns number of registered users
   */
  function getMaxRank()
  public view
  returns(uint) { return users_byRank.length; }

  function getRecordScore(uint _repId)
  public view
  recordExists(_repId)
  returns(uint) { return reputationRecords[_repId].score; }

  function getRecordTarget(uint _repId)
  public view
  recordExists(_repId)
  returns(address) { return reputationRecords[_repId].target; }

  function getRecordPos(uint _repId)
  public view
  recordExists(_repId)
  returns(bool) { return reputationRecords[_repId].pos; }

  function getRecordReverted(uint _repId)
  public view
  recordExists(_repId)
  returns(bool) { return reputationRecords[_repId].reverted; }

  function _rankByReputation(address _user)
  internal {
    uint userRank = userRepInfos[_user].rank;
    uint BEFORERANK = userRank;
    // _pos decide adjusting direction
    bool _pos;

    // If user is placed bottom rank, only way is raising rank
    if(userRank == users_byRank.length - 1) _pos = true;
    // This part will compare upper rank, rank should larger than 0 for preventing refer negative index
    if(userRank > 0) {
      // Compare with one step higher rank reputation
      if(userRepInfos[_user].reputation > userRepInfos[users_byRank[userRank - 1]].reputation) {
        _pos = true;
      }
    }

    // raise the rank
    if(_pos) {
      // For preventing refer negative index
      while(userRank > 0) {
        // if one step higher rank has higher reputation, no more change needed
        if(userRepInfos[_user].reputation <= userRepInfos[users_byRank[userRank - 1]].reputation) {
          break;
        }

        address temp = users_byRank[userRank - 1];
        userRepInfos[temp].rank = userRank;
        userRepInfos[_user].rank = userRank - 1;
        users_byRank[userRank - 1] = _user;
        users_byRank[userRank] = temp;

        userRank = userRank.sub(1);
      }
    }
    // lower the rank
    if(!_pos) {
      // for preventing refer over the array length
      while(userRank < users_byRank.length - 1) {
        // if one step lower rank has lower reputation, no more change needed
        if(userRepInfos[_user].reputation >= userRepInfos[users_byRank[userRank + 1]].reputation) {
          break;
        }

        address temp = users_byRank[userRank + 1];
        userRepInfos[temp].rank = userRank;
        userRepInfos[_user].rank = userRank + 1;
        users_byRank[userRank + 1] = _user;
        users_byRank[userRank] = temp;

        userRank = userRank.add(1);
      }
    }

    if(BEFORERANK != userRepInfos[_user].rank) {
      emit RankChanged(_user, BEFORERANK, userRepInfos[_user].rank);
    }
  }
}