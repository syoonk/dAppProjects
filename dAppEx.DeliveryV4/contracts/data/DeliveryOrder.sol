// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryUser.sol";
import "./DeliveryShop.sol";
import "./DeliveryRider.sol";

/**
 *
 */
contract DeliveryOrder is CommonData {
  using SafeMath for uint;

  enum OrderState {
    ORDERED,    // 0,
    PREPARING,  // 1,
    DELIVERING, // 2,
    DELIVERED,  // 3,
    COMPLETED,  // 4,
    CANCELLED } // 5

  struct Order {
    address buyer;
    address shopOwner;
    address riderAddress;
    uint orderId;
    uint shopId;
    uint riderId;
    uint totalPrice;
    uint[] goodsIds;
    uint[] stateTime;
    OrderState state;
  }

  mapping(uint => Order) orders;
  uint nextOrderId = 1;

  event OrderStateChanged(
    uint _orderId,
    uint _time,
    OrderState _state);
  event RiderAssigned(uint _orderId, uint _riderId);
  event RiderCancelled(uint _orderId, uint _riderId);

  constructor() public { owner = msg.sender; }

  modifier orderExists(uint _orderId) {
    require(orders[_orderId].orderId != 0,
      "order does not exist");
    _;
  }

  /**
   * @dev use and shop exist checker is called in addOrder of each contract
   */
  function placeOrder(
    address _buyer,
    uint _shopId,
    uint[] memory _goodsIds,
    uint _totalPrice)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    address USERDATA = Admins(admin).getDataAddress(uint(DataType.USER));
    address SHOPDATA = Admins(admin).getDataAddress(uint(DataType.SHOP));
    DeliveryShop shopInst = DeliveryShop(SHOPDATA);
    address shopOwner = shopInst.getShopOwner(_shopId);

    orders[nextOrderId] = Order(
      _buyer,
      shopOwner,
      address(0),
      nextOrderId,
      _shopId,
      0,
      _totalPrice,
      _goodsIds,
      new uint[](6),
      OrderState.ORDERED);
    orders[nextOrderId].stateTime[uint(OrderState.ORDERED)] = now;

    DeliveryUser(USERDATA).addOrder(_buyer, nextOrderId);
    shopInst.addOrder(_shopId, nextOrderId);

    emit OrderStateChanged(nextOrderId, now, OrderState.ORDERED);

    nextOrderId = nextOrderId.add(1);
  }

  /**
   * @dev It is called from takeOrder from rider data contract
   */
  function assignRider(
    uint _orderId,
    uint _riderId,
    address _riderAddress)
  public
  orderExists(_orderId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);
    Order storage orderCopy = orders[_orderId];
    require(orderCopy.riderId == 0,
      "order has already been assigned");

    orderCopy.riderId = _riderId;
    orderCopy.riderAddress = _riderAddress;

    emit RiderAssigned(_orderId, _riderId);
  }

  function cancelRider(uint _orderId)
  public
  orderExists(_orderId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    uint riderId = orders[_orderId].riderId;
    orders[_orderId].riderId = 0;
    orders[_orderId].riderAddress = address(0);

    emit RiderCancelled(_orderId, riderId);
  }

  function orderStateChanger(uint _orderId, uint _state)
  public
  orderExists(_orderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    Order storage orderCopy = orders[_orderId];
    orderCopy.stateTime[_state] = now;
    orderCopy.state = OrderState(_state);

    if(OrderState(_state) == OrderState.COMPLETED) {
      address RIDERDATA = Admins(admin).getDataAddress(uint(DataType.RIDER));
      DeliveryRider(RIDERDATA).addCompletedOrder(orders[_orderId].riderId, _orderId);
    }

    emit OrderStateChanged(_orderId, now, OrderState(_state));
  }

  function getOrderBuyer(uint _orderId)
  public view
  orderExists(_orderId)
  returns(address) { return orders[_orderId].buyer; }

  function getOrderShopOwner(uint _orderId)
  public view
  orderExists(_orderId)
  returns(address) { return orders[_orderId].shopOwner; }

  function getOrderRiderAddress(uint _orderId)
  public view
  orderExists(_orderId)
  returns(address) { return orders[_orderId].riderAddress; }

  function getOrderShopId(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint) { return orders[_orderId].shopId; }

  function getOrderRiderId(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint) { return orders[_orderId].riderId; }

  function getOrderTotalPrice(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint) { return orders[_orderId].totalPrice; }

  function getOrderGoodsIds(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint[] memory) { return orders[_orderId].goodsIds; }

  function getOrderStateTimes(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint[] memory) { return orders[_orderId].stateTime; }

  function getOrderState(uint _orderId)
  public view
  orderExists(_orderId)
  returns(uint) { return uint(orders[_orderId].state); }

  function getNextOrderId()
  public view
  returns(uint) { return nextOrderId; }

  function orderExistsTrigger(uint _orderId)
  public view
  orderExists(_orderId)
  returns(bool) { return true; }
}