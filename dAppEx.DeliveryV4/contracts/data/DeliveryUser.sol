// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryReputation.sol";

/**
 * It manages overall user informations
 */
contract DeliveryUser is CommonData {
  using SafeMath for uint;

  struct UserInfo {
    address userAddress;
    uint userId;
    uint phoneNumber;
    uint riderId;
    uint[] shopIds;
    uint[] reviews;
    uint[] uploadedReviews;
    uint[] orders;
    string homeAddress;
    string publicKey;
  }

  mapping(address => UserInfo) users_byAddress;
  mapping(uint => address) users_byId;
  uint nextUserId = 1;

  constructor() public { owner = msg.sender; }

  modifier userExists(address _userAddress) {
    require(users_byAddress[_userAddress].userAddress != address(0),
      "user does not exist");
    _;
  }

  modifier isNotEmptyString(string memory _string) {
    bytes memory tempString = bytes(_string);
    require(tempString.length != 0,
      "string cannot be empty");
    _;
  }

  /**
  * @notice Register new user and rank user by reputation
  * @dev this only can be excuted by approvedApps. It is mangaged by contract Admins.
  *      function onlyApprovedAppTrigger will check its authorization.
  * @param _userAddress User's address
  * @param _phoneNumber User's phone number, its validation is pre-checked outside source
  * @param _homeAddress User home address for taking delivery
  * @param _userPublicKey User's publickey is used for encrypting message.
  */
  function registerUser(
    address _userAddress,
    uint _phoneNumber,
    string memory _homeAddress,
    string memory _userPublicKey)
  public
  isNotEmptyString(_homeAddress) isNotEmptyString(_userPublicKey) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    require(users_byAddress[_userAddress].userAddress == address(0),
      "user already exists");

    users_byAddress[_userAddress] = UserInfo({
      userAddress: _userAddress,
      userId: nextUserId,
      phoneNumber: _phoneNumber,
      riderId: 0,
      shopIds: new uint[](0),
      reviews: new uint[](0),
      uploadedReviews: new uint[](0),
      orders: new uint[](0),
      homeAddress: _homeAddress,
      publicKey: _userPublicKey });
    users_byId[nextUserId] = _userAddress;

    // Initializing User Reputation Info
    address REPDATA = Admins(admin).getDataAddress(uint(DataType.REPUTATION));
    DeliveryReputation(REPDATA).initializeUserRep(_userAddress);

    nextUserId = nextUserId.add(1);
  }

  /**
   * @notice change user information part
   *         User is only allowed to change phoneNumber and homeAddress
   * @param _phoneNumber is checked its validation from external source
   */
  function changePhoneNumber(address _userAddress, uint _phoneNumber)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    users_byAddress[_userAddress].phoneNumber = _phoneNumber;
  }

  function changeHomeAddress(address _userAddress, string memory _homeAddress)
  public
  isNotEmptyString(_homeAddress) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    users_byAddress[_userAddress].homeAddress = _homeAddress;
  }

  /**
   * @dev User can have shops more than one
   *      It is called from register shop in shop data contract
   */
  function setShopId(address _userAddress, uint _shopId)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    users_byAddress[_userAddress].shopIds.push(_shopId);
  }

  /**
   * @dev It is called only once user assigned rider id
   *      It is called from register rider in rider data contract
   */
  function setRiderId(address _userAddress, uint _riderId)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);
    require(users_byAddress[_userAddress].riderId == 0,
      "user has already been assigned riderId");

    users_byAddress[_userAddress].riderId = _riderId;
  }

  /**
   * @dev It is called from place order in order data contract
   */
  function addOrder(address _userAddress, uint _orderId)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    users_byAddress[_userAddress].orders.push(_orderId);
  }

  /**
   * @dev It is called from upload review in review data contract
   */
  function addReview(address _userAddress, uint _reviewId)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    users_byAddress[_userAddress].reviews.push(_reviewId);
  }

  function addUploadedReviews(address _userAddress, uint _reviewId)
  public
  userExists(_userAddress) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    users_byAddress[_userAddress].uploadedReviews.push(_reviewId);
  }

  /**
   * @notice Each function returns user's informations
   */
  function getUserAddress(uint _userId)
  public view
  returns(address) {
    require(users_byId[_userId] != address(0),
      "user does not exist");

    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    return users_byId[_userId];
  }

  function getUserId(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    return users_byAddress[_userAddress].userId;
  }

  function getUserPhoneNumber(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    return users_byAddress[_userAddress].phoneNumber;
  }

  function getUserRiderId(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    return users_byAddress[_userAddress].riderId;
  }

  function getUserShopIds(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint[] memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    return users_byAddress[_userAddress].shopIds;
  }

  /**
   * @notice It returns review Ids
   */
  function getUserReviews(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint[] memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    return users_byAddress[_userAddress].reviews;
  }

  function getUserUploadedReviews(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint[] memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    return users_byAddress[_userAddress].uploadedReviews;
  }

  function getUserOrders(address _userAddress)
  public view
  userExists(_userAddress)
  returns(uint[] memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    return users_byAddress[_userAddress].orders;
  }

  function getUserHomeAddress(address _userAddress)
  public view
  userExists(_userAddress)
  returns(string memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    return users_byAddress[_userAddress].homeAddress;
  }

  function getUserPublicKey(address _userAddress)
  public view
  userExists(_userAddress)
  returns(string memory) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    return users_byAddress[_userAddress].publicKey;
  }

  function userExistsTrigger(address _userAddress)
  public view
  userExists(_userAddress)
  returns(bool) { return true; }
}