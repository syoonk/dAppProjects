// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryUser.sol";

/**
 *
 */
contract DeliveryShop is CommonData {
  using SafeMath for uint;

  struct ShopInfo {
    address shopOwner;
    uint shopId;
    uint shopPhoneNumber;
    uint[] reviews_shop;  // review ids received as shop
    uint[] orders;
    string shopAddress;
    bool activate;
  }

  mapping(uint => ShopInfo) shops;
  uint public nextShopId = 1;

  constructor() public { owner = msg.sender; }

  modifier shopExists(uint _shopId) {
    require(shops[_shopId].shopOwner != address(0),
      "shop does not exist");
    _;
  }

  modifier shopActivated(uint _shopId) {
    require(shops[_shopId].activate,
      "shop is not activated");
    _;
  }

  /**
   * @dev User existance is checked on setShopId()
   */
  function registerShop(
    address _shopOwner,
    uint _shopPhoneNumber,
    string memory _shopAddress)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    shops[nextShopId] = ShopInfo(
      _shopOwner,
      nextShopId,
      _shopPhoneNumber,
      new uint[](0),
      new uint[](0),
      _shopAddress,
      true);

    address USERDATA = Admins(admin).getDataAddress(uint(DataType.USER));
    DeliveryUser(USERDATA).setShopId(_shopOwner, nextShopId);

    nextShopId = nextShopId.add(1);
  }

  function changeShopPhoneNumber(uint _shopId, uint _phoneNumber)
  public
  shopExists(_shopId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    shops[_shopId].shopPhoneNumber = _phoneNumber;
  }

  function changeShopAddress(uint _shopId, string memory _shopAddress)
  public
  shopExists(_shopId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    shops[_shopId].shopAddress = _shopAddress;
  }

  function activateShop(uint _shopId)
  public
  shopExists(_shopId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    shops[_shopId].activate = true;
  }

  function deactivateShop(uint _shopId)
  public
  shopExists(_shopId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    shops[_shopId].activate = false;
  }

  function addOrder(uint _shopId, uint _orderId)
  public
  shopActivated(_shopId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    shops[_shopId].orders.push(_orderId);
  }

  function addReview(uint _shopId, uint _reviewId)
  public
  shopExists(_shopId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    shops[_shopId].reviews_shop.push(_reviewId);
  }

  function getShopOwner(uint _shopId)
  public view
  shopExists(_shopId)
  returns(address) { return shops[_shopId].shopOwner; }

  function getShopPhoneNumber(uint _shopId)
  public view
  shopExists(_shopId)
  returns(uint) { return shops[_shopId].shopPhoneNumber; }

  function getShopReviews(uint _shopId)
  public view
  shopExists(_shopId)
  returns(uint[] memory) { return shops[_shopId].reviews_shop; }

  function getShopOrders(uint _shopId)
  public view
  shopExists(_shopId)
  returns(uint[] memory) { return shops[_shopId].orders; }

  function getShopAddress(uint _shopId)
  public view
  shopExists(_shopId)
  returns(string memory) { return shops[_shopId].shopAddress; }

  function getShopActivation(uint _shopId)
  public view
  shopExists(_shopId)
  returns(bool) { return shops[_shopId].activate; }

  function shopExistsTrigger(uint _shopId)
  public view
  shopExists(_shopId)
  returns(bool) { return true; }
}