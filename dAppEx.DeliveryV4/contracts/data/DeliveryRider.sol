// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryUser.sol";
import "./DeliveryOrder.sol";

/**
 *
 */
 contract DeliveryRider is CommonData {
   using SafeMath for uint;

  struct Rider {
    address riderAddress;
    uint riderId;
    uint riderPhoneNumber;
    uint currentOrderId;
    uint nextOrderId;
    uint[] completedOrders;
    uint[] reviews_rider;   // review ids recevied as rider
    string area;
    bool activate;
  }

  mapping(uint => Rider) riders;
  mapping(address => uint) riderIds_byUserAddress;
  uint nextRiderId = 1;

  constructor() public { owner = msg.sender; }

  modifier riderExists(uint _riderId) {
    require(riders[_riderId].riderAddress != address(0),
      "rider does not exist");
    _;
  }

  modifier riderActivated(uint _riderId) {
    require(riders[_riderId].activate,
     "rider is not activated");
    _;
  }

  function registerRider(
    address _userAddress,
    uint _riderPhoneNumber,
    string memory _deliveryArea)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    address USERDATA = Admins(admin).getDataAddress(uint(DataType.USER));
    uint userRiderId = DeliveryUser(USERDATA).getUserRiderId(_userAddress);
    require(userRiderId == 0,
      "user is already assigned riderId");

    riders[nextRiderId] = Rider(
      _userAddress,
      nextRiderId,
      _riderPhoneNumber,
      0,
      0,
      new uint[](0),
      new uint[](0),
      _deliveryArea,
      true);
    riderIds_byUserAddress[_userAddress] = nextRiderId;

    DeliveryUser(USERDATA).setRiderId(_userAddress, nextRiderId);

    nextRiderId = nextRiderId.add(1);
  }

  function changeRiderPhoneNumber(uint _riderId, uint _phoneNumber)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    riders[_riderId].riderPhoneNumber = _phoneNumber;
  }

  function changeRiderDeliveryArea(uint _riderId, string memory _deliveryArea)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    riders[_riderId].area = _deliveryArea;
  }

  function activateRider(uint _riderId)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    riders[_riderId].activate = true; }

  function deactivateRider(uint _riderId)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    require(riders[_riderId].currentOrderId == 0 && riders[_riderId].nextOrderId == 0,
      "rider cannot be deactivated with order assigned");

    riders[_riderId].activate = false;
  }

  function takeOrder(uint _riderId, uint _orderId)
  public
  riderActivated(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    address ORDERDATA = Admins(admin).getDataAddress(uint(DataType.ORDER));
    DeliveryOrder(ORDERDATA).orderExistsTrigger(_orderId);
    Rider storage riderCopy = riders[_riderId];
    require(riderCopy.currentOrderId == 0 || riderCopy.nextOrderId == 0,
      "rider is already assigned 2 orders");

    if(riderCopy.nextOrderId == 0 && riderCopy.currentOrderId != 0) {
      riderCopy.nextOrderId = _orderId;
    }
    if(riderCopy.currentOrderId == 0) {
      riderCopy.currentOrderId = _orderId;
    }
    DeliveryOrder(ORDERDATA).assignRider(_orderId, _riderId, riders[_riderId].riderAddress);
  }

  function unassignOrder(uint _riderId, uint _orderId)
  public
  riderActivated(_riderId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    address ORDERDATA = Admins(admin).getDataAddress(uint(DataType.ORDER));
    DeliveryOrder(ORDERDATA).orderExistsTrigger(_orderId);
    Rider storage riderCopy = riders[_riderId];
    require(riderCopy.currentOrderId == _orderId || riderCopy.nextOrderId == _orderId,
      "rider is not assigned this order");

    if(riderCopy.currentOrderId == _orderId) {
      riderCopy.currentOrderId = riderCopy.nextOrderId;
    }
    riderCopy.nextOrderId = 0;
  }

  function cancelAssignedOrder(uint _riderId, uint _orderId)
  public
  riderActivated(_riderId) {
    unassignOrder(_riderId, _orderId);

    address ORDERDATA = Admins(admin).getDataAddress(uint(DataType.ORDER));
    DeliveryOrder(ORDERDATA).orderExistsTrigger(_orderId);

    DeliveryOrder(ORDERDATA).cancelRider(_orderId);
  }

  function addReview(uint _riderId, uint _reviewId)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    riders[_riderId].reviews_rider.push(_reviewId);
  }

  /**
   * @dev It is executed by Order Data Contract
   */
  function addCompletedOrder(uint _riderId, uint _orderId)
  public
  riderExists(_riderId) {
    Admins(admin).onlyApprovedDataTrigger(msg.sender);

    riders[_riderId].completedOrders.push(_orderId);
  }

  function getRiderAddress(uint _riderId)
  public view
  riderExists(_riderId)
  returns(address) { return riders[_riderId].riderAddress; }

  function getRiderId(address _userAddress)
  public view
  returns(uint) {
    require(riderIds_byUserAddress[_userAddress] != 0,
      "this user is not assigned riderId");

    return riderIds_byUserAddress[_userAddress];
  }

  function getRiderPhoneNumber(uint _riderId)
  public view
  riderExists(_riderId)
  returns(uint) { return riders[_riderId].riderPhoneNumber; }

  function getRiderCurrentOrderId(uint _riderId)
  public view
  riderExists(_riderId)
  returns(uint) { return riders[_riderId].currentOrderId; }

  function getRiderNextOrderId(uint _riderId)
  public view
  riderExists(_riderId)
  returns(uint) { return riders[_riderId].nextOrderId; }

  function getRiderCompletedOrders(uint _riderId)
  public view
  riderExists(_riderId)
  returns(uint[] memory) { return riders[_riderId].completedOrders; }

  function getRiderReviews(uint _riderId)
  public view
  riderExists(_riderId)
  returns(uint[] memory) { return riders[_riderId].reviews_rider; }

  function getRiderArea(uint _riderId)
  public view
  riderExists(_riderId)
  returns(string memory) { return riders[_riderId].area; }

  function getRiderActivation(uint _riderId)
  public view
  riderExists(_riderId)
  returns(bool) { return riders[_riderId].activate; }

  function riderExistsTrigger(uint _riderId)
  public view
  riderExists(_riderId)
  returns(bool) { return true; }
}