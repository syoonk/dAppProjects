// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "../Admins.sol";
import "./CommonData.sol";
import "./DeliveryUser.sol";
import "./DeliveryOrder.sol";
import "./DeliveryRider.sol";
import "./DeliveryShop.sol";
import "./DeliveryReputation.sol";

contract DeliveryReview is CommonData {
  using SafeMath for uint;

  enum UserType {
    BUYER,
    RIDER,
    SHOP
  }

  struct Review {
    UserType from;
    UserType to;
    uint reviewId;
    uint orderId;
    uint stars;
    uint[] repIds;
    bool cancelled;
  }

  mapping(uint =>  Review) reviews;
  mapping(uint => mapping(UserType => mapping(UserType => bool))) reviewed;
  uint nextReviewId = 1;

  constructor() public { owner = msg.sender; }

  modifier reviewExists(uint _reviewId) {
    require(reviews[_reviewId].reviewId != 0,
      "review does not exist");
    _;
  }

  /**
   * @dev Verification of _from and _to will be done from app part
   * @param _from    User type. BUYER: 0, RIDER: 1, SHOP: 2
   * @param _to      User type. BUYER: 0, RIDER: 1, SHOP: 2
   * @param _orderId OrderId of target order
   * @param _stars   Score for review target. It can be given 1 to 10
   */
  function uploadReview(
    uint _from,
    uint _to,
    uint _orderId,
    uint _stars)
  public
  returns(uint) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    address ORDERDATA = Admins(admin).getDataAddress(uint(DataType.ORDER));
    uint state = DeliveryOrder(ORDERDATA).getOrderState(_orderId);
    require(state == 4,
      "order should be in completed state");
    UserType from = UserType(_from); UserType to = UserType(_to);
    require(_from != _to,
      "not allow self review");
    require(reviewed[_orderId][from][to] == false,
      "allow review only once");
    require(_stars <= 10 && _stars >= 1,
      "stars should be <= 10, >= 1");

    address USERDATA = Admins(admin).getDataAddress(uint(DataType.USER));
    DeliveryUser userInst = DeliveryUser(USERDATA);
    DeliveryOrder orderInst = DeliveryOrder(ORDERDATA);

    reviews[nextReviewId] = Review({
      from: from,
      to: to,
      reviewId: nextReviewId,
      orderId: _orderId,
      stars: _stars,
      repIds: new uint[](0),
      cancelled: false });

    if(to == UserType.SHOP) {
      uint shopId = orderInst.getOrderShopId(_orderId);
      address shopOwner = orderInst.getOrderShopOwner(_orderId);
      address SHOPDATA = Admins(admin).getDataAddress(uint(DataType.SHOP));

      DeliveryShop(SHOPDATA).addReview(shopId, nextReviewId);
      userInst.addReview(shopOwner, nextReviewId);
    }
    if(to == UserType.RIDER) {
      uint riderId = orderInst.getOrderRiderId(_orderId);
      address riderAddress = orderInst.getOrderRiderAddress(_orderId);
      address RIDERDATA = Admins(admin).getDataAddress(uint(DataType.RIDER));

      DeliveryRider(RIDERDATA).addReview(riderId, nextReviewId);
      userInst.addReview(riderAddress, nextReviewId);
    }
    if(to == UserType.BUYER) {
      address buyer = orderInst.getOrderBuyer(_orderId);

      userInst.addReview(buyer, nextReviewId);
    }

    address fromUserAddress = getToUserAddress(_orderId, _from);
    userInst.addUploadedReviews(fromUserAddress, nextReviewId);

    reviewed[_orderId][from][to] = true;
    nextReviewId = nextReviewId.add(1);

    return nextReviewId.sub(1);
  }

  function setReviewRepIds(uint _reviewId, uint[] memory _repIds)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    reviews[_reviewId].repIds = _repIds;
  }

  function cancelReview(uint _reviewId)
  public
  reviewExists(_reviewId) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    Review storage reviewCopy = reviews[_reviewId];
    require(reviewCopy.cancelled == false,
      "review is already cancelled");

    reviewCopy.cancelled = true;

    if(reviewCopy.repIds.length > 0) {
      address REPUDATA = Admins(admin).getDataAddress(uint(DataType.REPUTATION));
      for(uint id = 0; id < reviewCopy.repIds.length; id++) {
        DeliveryReputation(REPUDATA).revertReputation(reviewCopy.repIds[id]);
      }
    }
  }

  /**
   * @notice it returns address from user type
   * @dev function name is not well specified with its functioning
   */
  function getToUserAddress(uint _orderId, uint _userType)
  public view
  returns(address) {
    address ORDERDATA = Admins(admin).getDataAddress(uint(DataType.ORDER));

    if(_userType == 0) {
      return DeliveryOrder(ORDERDATA).getOrderBuyer(_orderId);
    }
    if(_userType == 1) {
      return DeliveryOrder(ORDERDATA).getOrderRiderAddress(_orderId);
    }
    if(_userType == 2) {
      return DeliveryOrder(ORDERDATA).getOrderShopOwner(_orderId);
    }
  }

  function getReviewFromUserType(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(UserType) { return reviews[_reviewId].from; }

  function getReviewToUserType(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(UserType) { return reviews[_reviewId].to; }

  function getReviewOrderId(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(uint) { return reviews[_reviewId].orderId; }

  function getReviewStars(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(uint) { return reviews[_reviewId].stars; }

  function getReviewRepIds(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(uint[] memory) { return reviews[_reviewId].repIds; }

  function getReviewCancelled(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(bool) { return reviews[_reviewId].cancelled; }

  function getReviewed(uint _orderId, uint _from, uint _to)
  public view
  returns(bool) { return reviewed[_orderId][UserType(_from)][UserType(_to)]; }

  function reviewExistsTrigger(uint _reviewId)
  public view
  reviewExists(_reviewId)
  returns(bool) { return true; }
}