// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

/**
 * This Contract collects the elements commonly used
 */
contract CommonData {
  enum DataType {
    USER,         // 0
    SHOP,         // 1
    RIDER,        // 2
    ORDER,        // 3
    REVIEW,       // 4
    REPUTATION }  // 5

  address owner;
  address admin;

  modifier onlyOwner() {
    require(msg.sender == owner,
      "only owner");
    _;
  }

  /**
  * @notice setting admin contract
  * @param _adminAddress of the admin contract address
  */
  function setAdmin(address _adminAddress)
  external
  onlyOwner() { admin = _adminAddress; }

  function getOwner()
  external
  returns(address) { return owner;}
}