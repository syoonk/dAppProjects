// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./CommonApp.sol";

/**
 * This Contract using on frontend side. User executes the function.
 */
contract DeliveryAppUser is CommonApp {
  constructor() CommonApp() public {}

  function registerUser(
    uint _phoneNumber,
    string memory _homeAddress,
    string memory _userPublicKey)
  external {
    userDataInst.registerUser(
      msg.sender,
      _phoneNumber,
      _homeAddress,
      _userPublicKey);
  }

  function changePhoneNumber(uint _phoneNumber)
  external { userDataInst.changePhoneNumber(msg.sender, _phoneNumber); }

  function changeHomeAddress(string calldata _homeAddress)
  external { userDataInst.changeHomeAddress(msg.sender, _homeAddress); }

  /**
   * Get Information Part
   */
  function getUserId(address _user)
  external view
  returns(uint) { return userDataInst.getUserId(_user); }

  function getUserPhoneNumber(address _user)
  external view
  returns(uint) { return userDataInst.getUserPhoneNumber(_user); }

  function getUserRiderId(address _user)
  external view
  returns(uint) { return userDataInst.getUserRiderId(_user); }

  function getUserShopIds(address _user)
  external view
  returns(uint[] memory) { return userDataInst.getUserShopIds(_user); }

  function getUserReviews(address _user)
  external view
  returns(uint[] memory) { return userDataInst.getUserReviews(_user); }

  function getUserUploadedReviews(address _user)
  external view
  returns(uint[] memory) { return userDataInst.getUserUploadedReviews(_user); }

  function getUserOrders(address _user)
  external view
  returns(uint[] memory) { return userDataInst.getUserOrders(_user); }

  function getUserHomeAddress(address _user)
  external view
  returns(string memory) { return userDataInst.getUserHomeAddress(_user); }

  function getUserPublicKey(address _user)
  external view
  returns(string memory) { return userDataInst.getUserPublicKey(_user); }

  function getUserReputation(address _user)
  external view
  returns(uint) { return repuDataInst.getUserReputation(_user); }

  function getUserRank(address _user)
  external view
  returns(uint) { return repuDataInst.getUserRank(_user); }

  function getUserByRank(uint _rank)
  external view
  returns(address) { return repuDataInst.getUserByRank(_rank); }

  function getRanks()
  external view
  returns(address[] memory) { return repuDataInst.getRanks(); }

  function getMaxRank()
  external view
  returns(uint) { return repuDataInst.getMaxRank(); }
}