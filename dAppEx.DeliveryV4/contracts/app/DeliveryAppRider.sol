// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./CommonApp.sol";

contract DeliveryAppRider is CommonApp {
  constructor() CommonApp() public {}

  function registerRider(uint _riderPhoneNumber, string calldata _deliveryArea)
  external { riderDataInst.registerRider(msg.sender, _riderPhoneNumber, _deliveryArea); }

  function changeRiderPhoneNumber(uint _phoneNumber)
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.changeRiderPhoneNumber(userRiderId, _phoneNumber);
  }

  function changeRiderDeliveryArea(string calldata _deliveryArea)
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.changeRiderDeliveryArea(userRiderId, _deliveryArea);
  }

  function activateRider()
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.activateRider(userRiderId);
  }

  function deactivateRider()
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.deactivateRider(userRiderId);
  }

  function takeOrder(uint _orderId)
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.takeOrder(userRiderId, _orderId);
  }

  function cancelAssignedOrder(uint _orderId)
  external {
    uint userRiderId = riderDataInst.getRiderId(msg.sender);
    riderDataInst.cancelAssignedOrder(userRiderId, _orderId);
  }

  /**
   * Get Information functions.
   */
  function getRiderAddress(uint _riderId)
  external view
  returns(address) { return riderDataInst.getRiderAddress(_riderId); }

  function getRiderId(address _userAddress)
  external view
  returns(uint) { return riderDataInst.getRiderId(_userAddress); }

  function getRiderPhoneNumber(uint _riderId)
  external view
  returns(uint) { return riderDataInst.getRiderPhoneNumber(_riderId); }

  function getRiderCurrentOrderId(uint _riderId)
  external view
  returns(uint) { return riderDataInst.getRiderCurrentOrderId(_riderId); }

  function getRiderNextOrderId(uint _riderId)
  external view
  returns(uint) { return riderDataInst.getRiderNextOrderId(_riderId); }

  function getRiderCompletedOrders(uint _riderId)
  external view
  returns(uint[] memory) { return riderDataInst.getRiderCompletedOrders(_riderId); }

  function getRiderReviews(uint _riderId)
  external view
  returns(uint[] memory) { return riderDataInst.getRiderReviews(_riderId); }

  function getRiderArea(uint _riderId)
  external view
  returns(string memory) { return riderDataInst.getRiderArea(_riderId); }

  function getRiderActivation(uint _riderId)
  external view
  returns(bool) { return riderDataInst.getRiderActivation(_riderId); }
}