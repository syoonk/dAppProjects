// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./AppDatas.sol";
import "../data/DeliveryUser.sol";
import "../data/DeliveryRider.sol";
import "../data/DeliveryShop.sol";
import "../data/DeliveryOrder.sol";
import "../data/DeliveryReview.sol";
import "../data/DeliveryReputation.sol";
import "../finance/Wallet.sol";

contract CommonApp {
  AppDatas appDatasInst;
  DeliveryUser userDataInst;
  DeliveryRider riderDataInst;
  DeliveryShop shopDataInst;
  DeliveryOrder orderDataInst;
  DeliveryReview reviewDataInst;
  DeliveryReputation repuDataInst;
  Wallet walletInst;

  uint constant USER       = 0;
  uint constant RIDER      = 1;
  uint constant SHOP       = 2;
  uint constant ORDER      = 3;
  uint constant REVIEW     = 4;
  uint constant REPUTATION = 5;

  address owner;

  constructor() public { owner = msg.sender; }

  modifier onlyOwner() {
    require(msg.sender == owner,
      "only owner");
    _;
  }

  function setAppDatas(address _appDatasAddress)
  external
  onlyOwner() {
    appDatasInst = AppDatas(_appDatasAddress);
    _setDataInstances();
  }

  function setWallet(address _walletAddress)
  external
  onlyOwner() { walletInst = Wallet(_walletAddress); }

  function _setDataInstances()
  internal
  onlyOwner() {
    for(uint dataType = 0; dataType < 6; dataType++) {
      address dataAddress = appDatasInst.getDataAddress(dataType);
      require(dataAddress != address(0),
        "this type of data address is not set in appDatas contract");

      if(dataType == USER) userDataInst = DeliveryUser(dataAddress);
      if(dataType == RIDER) riderDataInst = DeliveryRider(dataAddress);
      if(dataType == SHOP) shopDataInst = DeliveryShop(dataAddress);
      if(dataType == ORDER) orderDataInst = DeliveryOrder(dataAddress);
      if(dataType == REVIEW) reviewDataInst = DeliveryReview(dataAddress);
      if(dataType == REPUTATION) repuDataInst = DeliveryReputation(dataAddress);
    }
  }

  function getOwner()
  external
  returns(address) { return owner;}
}