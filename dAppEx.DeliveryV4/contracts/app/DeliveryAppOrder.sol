// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./CommonApp.sol";

contract DeliveryAppOrder is CommonApp {
  constructor() CommonApp() public {}

  uint constant ORDERED = 0;
  uint constant PREPARING = 1;
  uint constant DELIVERING = 2;
  uint constant DELIVERED = 3;
  uint constant COMPLETED = 4;
  uint constant CANCELLED = 5;

  enum UserType {
    BUYER,
    SHOP
  }

  mapping(uint => mapping(UserType => bool)) cancelSign;

  uint RIDERFEE = 3000;
  // reputation score for order completion
  uint REPUTATIONSCORE = 1000;
  uint KNGAMOUNT = 1000;

  function setRiderFee(uint _riderFee)
  external
  onlyOwner() { RIDERFEE = _riderFee; }

  function setRepuScore(uint _repuScore)
  external
  onlyOwner() { REPUTATIONSCORE = _repuScore; }

  function placeOrder(
    uint _shopId,
    uint[] calldata _goodsIds,
    uint _totalPrice)
  external {
    walletInst.holdKrw(msg.sender, _totalPrice);

    orderDataInst.placeOrder(
      msg.sender,
      _shopId,
      _goodsIds,
      _totalPrice);
  }

  function signOrder(uint _orderId)
  external {
    uint currentOrderState = orderDataInst.getOrderState(_orderId);
    require(currentOrderState != CANCELLED,
      "order has been cancelled");

    address shopOwner = orderDataInst.getOrderShopOwner(_orderId);
    address rider = orderDataInst.getOrderRiderAddress(_orderId);
    address buyer = orderDataInst.getOrderBuyer(_orderId);
    require(msg.sender == buyer ||
      msg.sender == shopOwner ||
      msg.sender == rider,
      "this user is not allowed to sign this order");

    if(currentOrderState == ORDERED) {
      require(msg.sender == shopOwner,
        "only shop owner can sign this state");
    }
    if(currentOrderState == PREPARING) {
      require(msg.sender == rider,
        "only rider can sign this state");

      uint totalPrice = orderDataInst.getOrderTotalPrice(_orderId);
      walletInst.releaseKRW(buyer, shopOwner, totalPrice - RIDERFEE);
      walletInst.releaseKRW(buyer, rider, RIDERFEE / 2);

      walletInst.faucetKngToken(shopOwner, KNGAMOUNT);
    }
    if(currentOrderState == DELIVERING) {
      require(msg.sender == rider,
        "only rider can sign this state");

      uint riderId = orderDataInst.getOrderRiderId(_orderId);
      riderDataInst.unassignOrder(riderId, _orderId);

      walletInst.releaseKRW(buyer, rider, RIDERFEE / 2);

      walletInst.faucetKngToken(rider, KNGAMOUNT);
    }
    if(currentOrderState == DELIVERED) {
      require(msg.sender == buyer,
        "only buyer can sign this state");

      repuDataInst.adjustedReputationChanger(buyer, REPUTATIONSCORE, true);
      repuDataInst.adjustedReputationChanger(shopOwner, REPUTATIONSCORE, true);
      repuDataInst.adjustedReputationChanger(rider, REPUTATIONSCORE, true);

      walletInst.faucetKngToken(buyer, KNGAMOUNT);
    }

    orderDataInst.orderStateChanger(_orderId, currentOrderState + 1);
  }

  function cancelOrderSign(uint _orderId)
  external {
    address buyer = orderDataInst.getOrderBuyer(_orderId);
    address shopOwner = orderDataInst.getOrderShopOwner(_orderId);
    uint currentOrderState = orderDataInst.getOrderState(_orderId);
    require(msg.sender == buyer || msg.sender == shopOwner,
      "only buyer, shop owner can sign to cancel");
    require(currentOrderState == ORDERED || currentOrderState == PREPARING,
      "only ORDERED or PREPARING state can be cancelled");

    if(msg.sender == buyer) {
      cancelSign[_orderId][UserType.BUYER] = true;

      // it can be directly cancelled by buyer if it is ORDERED state
      if(currentOrderState == ORDERED) {
        orderDataInst.orderStateChanger(_orderId, CANCELLED);

        uint totalPrice = orderDataInst.getOrderTotalPrice(_orderId);
        walletInst.releaseKRW(buyer, buyer, totalPrice);

        // Cancel assigned rider
        uint riderId = orderDataInst.getOrderRiderId(_orderId);
        if(riderId != 0) {
          riderDataInst.unassignOrder(riderId, _orderId);
        }

        return ;
      }
    }
    if(msg.sender == shopOwner) {
      cancelSign[_orderId][UserType.SHOP] = true;
    }

    if(cancelSign[_orderId][UserType.BUYER] && cancelSign[_orderId][UserType.SHOP]) {
      orderDataInst.orderStateChanger(_orderId, CANCELLED);

      uint totalPrice = orderDataInst.getOrderTotalPrice(_orderId);
      walletInst.releaseKRW(buyer, buyer, totalPrice);

      // Cancel assigned rider
      uint riderId = orderDataInst.getOrderRiderId(_orderId);
      if(riderId != 0) {
        riderDataInst.unassignOrder(riderId, _orderId);
      }
    }
  }

  function getOrderBuyer(uint _orderId)
  external view
  returns(address) { return orderDataInst.getOrderBuyer(_orderId); }

  function getOrderShopOwner(uint _orderId)
  external view
  returns(address) { return orderDataInst.getOrderShopOwner(_orderId); }

  function getOrderRiderAddress(uint _orderId)
  external view
  returns(address) { return orderDataInst.getOrderRiderAddress(_orderId); }

  function getOrderShopId(uint _orderId)
  external view
  returns(uint) { return orderDataInst.getOrderShopId(_orderId); }

  function getOrderRiderId(uint _orderId)
  external view
  returns(uint) { return orderDataInst.getOrderRiderId(_orderId); }

  function getOrderTotalPrice(uint _orderId)
  external view
  returns(uint) { return orderDataInst.getOrderTotalPrice(_orderId); }

  function getOrderGoodsIds(uint _orderId)
  external view
  returns(uint[] memory) { return orderDataInst.getOrderGoodsIds(_orderId); }

  function getOrderStateTimes(uint _orderId)
  external view
  returns(uint[] memory) { return orderDataInst.getOrderStateTimes(_orderId); }

  function getOrderState(uint _orderId)
  external view
  returns(uint) { return orderDataInst.getOrderState(_orderId); }

  function getOrderCancelSign(uint _orderId)
  external view
  returns(bool[] memory) {
    orderDataInst.orderExistsTrigger(_orderId);

    bool[] memory signs = new bool[](2);
    signs[0] = cancelSign[_orderId][UserType.BUYER];
    signs[1] = cancelSign[_orderId][UserType.SHOP];

    return signs;
  }

  function getNextOrderId()
  external view
  returns(uint) { return orderDataInst.getNextOrderId(); }

  function getOrderRiderFee()
  external view
  returns(uint) { return RIDERFEE; }
}