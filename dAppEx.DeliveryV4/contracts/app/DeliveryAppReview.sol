// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./CommonApp.sol";
import "../data/DeliveryReview.sol";
import "../data/DeliveryReputation.sol";

contract DeliveryAppReview is CommonApp {
  constructor() CommonApp() public {}

  // using for revert reputation of cancel review
  uint UPLOADREVIEWCOMP = 500;
  uint STARSCORERANGE = 2000;

  function setReviewCompensation(uint _repuComp)
  external
  onlyOwner() { UPLOADREVIEWCOMP = _repuComp; }

  function setStarScoreRange(uint _maxScore)
  external
  onlyOwner() { STARSCORERANGE = _maxScore; }

  function uploadReview(
    uint _to,
    uint _orderId,
    uint _stars)
  external {
    address buyer = orderDataInst.getOrderBuyer(_orderId);
    address shopOwner = orderDataInst.getOrderShopOwner(_orderId);
    address rider = orderDataInst.getOrderRiderAddress(_orderId);
    uint from;
    if(msg.sender == buyer) {from = 0;}
    else if(msg.sender == rider) {from = 1;}
    else if(msg.sender == shopOwner) {from = 2;}
    else {revert("uploader does not relevant to order");}
    require(_to == 0 || _to == 1 || _to == 2,
      "target user is not available");

    uint reviewId = reviewDataInst.uploadReview(
      from,
      _to,
      _orderId,
      _stars);

    uint[] memory repIds = new uint[](2);

    repIds[0] = repuDataInst.adjustedReputationChanger(msg.sender, UPLOADREVIEWCOMP, true);

    address toAddress = reviewDataInst.getToUserAddress(_orderId, _to);
    if(_stars < 5) {
      uint score = STARSCORERANGE * (5 - _stars) / 4;
      repIds[1] = repuDataInst.adjustedReputationChanger(toAddress, score, false);
    }
    if(_stars > 5) {
      uint score = STARSCORERANGE * (3 * _stars - 14) / 16;
      repIds[1] = repuDataInst.adjustedReputationChanger(toAddress, score, true);
    }

    reviewDataInst.setReviewRepIds(reviewId, repIds);
  }

  function cancelReview(uint _reviewId)
  external {
    uint orderId = reviewDataInst.getReviewOrderId(_reviewId);
    uint fromType = uint(reviewDataInst.getReviewFromUserType(_reviewId));
    address fromUserAddress = reviewDataInst.getToUserAddress(orderId, fromType);
    require(msg.sender == fromUserAddress,
      "only creator can cancel the review");

    reviewDataInst.cancelReview(_reviewId);
  }

  function getReviewFromUserType(uint _reviewId)
  external view
  returns(uint) { return uint(reviewDataInst.getReviewFromUserType(_reviewId)); }

  function getReviewToUserType(uint _reviewId)
  external view
  returns(uint) { return uint(reviewDataInst.getReviewToUserType(_reviewId)); }

  function getReviewOrderId(uint _reviewId)
  external view
  returns(uint) { return reviewDataInst.getReviewOrderId(_reviewId); }

  function getReviewStars(uint _reviewId)
  external view
  returns(uint) { return reviewDataInst.getReviewStars(_reviewId); }

  function getReviewCancelled(uint _reviewId)
  external view
  returns(bool) { return reviewDataInst.getReviewCancelled(_reviewId); }

  function getReviewed(uint _orderId, uint _from, uint _to)
  external view
  returns(bool) { return reviewDataInst.getReviewed(_orderId, _from, _to); }

  function getReviewUploadComp()
  external view
  onlyOwner()
  returns(uint) { return UPLOADREVIEWCOMP; }

  function getStarScoreRange()
  external view
  onlyOwner()
  returns(uint) { return STARSCORERANGE; }
}