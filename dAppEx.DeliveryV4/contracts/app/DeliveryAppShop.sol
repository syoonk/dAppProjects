// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "./CommonApp.sol";

contract DeliveryAppShop is CommonApp {
  constructor() CommonApp() public {}

  modifier isShopOwner(uint _shopId) {
    address shopOwner = shopDataInst.getShopOwner(_shopId);
    require(msg.sender == shopOwner,
      "user is not owner");
    _;
  }

  function registerShop(uint _shopPhoneNumber, string calldata _shopAddress)
  external { shopDataInst.registerShop(msg.sender, _shopPhoneNumber, _shopAddress); }

  function changeShopPhoneNumber(uint _shopId, uint _phoneNumber)
  external
  isShopOwner(_shopId) { shopDataInst.changeShopPhoneNumber(_shopId, _phoneNumber); }

  function changeShopAddress(uint _shopId, string calldata _shopAddress)
  external
  isShopOwner(_shopId) { shopDataInst.changeShopAddress(_shopId, _shopAddress); }

  function activateShop(uint _shopId)
  external
  isShopOwner(_shopId) { shopDataInst.activateShop(_shopId); }

  function deactivateShop(uint _shopId)
  external
  isShopOwner(_shopId) { shopDataInst.deactivateShop(_shopId); }

  /**
   * Get Informtaion Part
   */
  function getShopOwner(uint _shopId)
  external view
  returns(address) { return shopDataInst.getShopOwner(_shopId); }

  function getShopPhoneNumber(uint _shopId)
  external view
  returns(uint) { return shopDataInst.getShopPhoneNumber(_shopId); }

  function getShopReviews(uint _shopId)
  external view
  returns(uint[] memory) { return shopDataInst.getShopReviews(_shopId); }

  function getShopOrders(uint _shopId)
  external view
  returns(uint[] memory) { return shopDataInst.getShopOrders(_shopId); }

  function getShopAddress(uint _shopId)
  external view
  returns(string memory) { return shopDataInst.getShopAddress(_shopId); }

  function getShopActivation(uint _shopId)
  external view
  returns(bool) { return shopDataInst.getShopActivation(_shopId); }

  function getNextShopId()
  external view
  returns(uint) { return shopDataInst.nextShopId(); }
}