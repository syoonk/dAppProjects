// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "./CommonApp.sol";

contract DeliveryAppVoting is CommonApp {
  constructor() CommonApp() public {}

  using SafeMath for uint;

  enum VoteType {
    REVIEW,
    PENALTY,
    PRAISE
  }

  enum VoteState {
    VOTING,
    CONFIRMED,
    CANCELLED
  }

  struct Vote {
    VoteType voteType;
    address creator;
    address targetUser; // if type is CANCELREVIEW, it is 0
    uint reviewId;      // if type is not CANCELREVIEW, it is 0
    uint voteId;
    uint up;
    uint down;
    uint start;
    uint end;
    bool voteResult;
    VoteState state;
  }

  struct Voter {
    bool voted;
    bool voteTo;
  }

  mapping(uint => Vote) votes;
  mapping(uint => mapping(address => Voter)) voters;
  mapping(uint => address[]) votersList;
  uint nextVoteId = 1;

  uint VOTEFEE = 100;
  uint PENALTYSCORE = 1000;
  uint PRAISESCORE = 1000;

  modifier voteExists(uint _voteId) {
    require(votes[_voteId].creator != address(0),
      "vote does not exist");
    _;
  }

  modifier onlyConfirmed(uint _voteId) {
    require(votes[_voteId].state == VoteState.CONFIRMED,
      "only CONFIRMED state");
    _;
  }

  function setVoteFee(uint _voteFee)
  external
  onlyOwner() { VOTEFEE = _voteFee; }

  function setPenaltyScore(uint _penaltyScore)
  external
  onlyOwner() { PENALTYSCORE = _penaltyScore; }

  function setPraiseScore(uint _praiseScore)
  external
  onlyOwner() { PRAISESCORE = _praiseScore; }

  function startVote(
    VoteType _voteType,
    address _targetUser,
    uint _reviewId)
  external {
    votes[nextVoteId] = Vote(
      _voteType,
      msg.sender,
      _targetUser,
      _reviewId,
      nextVoteId,
      0,
      0,
      now,
      now + 86400 * 7, // for 7 days
      false,
      VoteState.VOTING);

    if(_voteType == VoteType.REVIEW) {
      reviewDataInst.reviewExistsTrigger(_reviewId);

      bool reviewCancelled = reviewDataInst.getReviewCancelled(_reviewId);
      require(!reviewCancelled,
        "review has already been cancelled");
    } else {
      userDataInst.userExistsTrigger(_targetUser);
      require(msg.sender != _targetUser,
        "creator cannot be target");
    }

    nextVoteId = nextVoteId.add(1);
  }

  function vote(uint _voteId, bool _voteTo)
  external
  voteExists(_voteId) {
    require(now <= votes[_voteId].end,
      "vote time is expired");
    require(votes[_voteId].state == VoteState.VOTING,
      "can vote only in VOTING state");
    require(msg.sender != votes[_voteId].creator,
      "creator cannot vote");
    require(msg.sender != votes[_voteId].targetUser,
      "target user cannot vote");
    require(!voters[_voteId][msg.sender].voted,
      "user already voted");

    voters[_voteId][msg.sender].voted = true;
    voters[_voteId][msg.sender].voteTo = _voteTo;
    votersList[_voteId].push(msg.sender);

    if(_voteTo) {
      votes[_voteId].up = votes[_voteId].up.add(1);
    }
    if(!_voteTo) {
      votes[_voteId].down = votes[_voteId].down.add(1);
    }

    repuDataInst.adjustedReputationChanger(msg.sender, VOTEFEE, true);
  }

  function cancelVote(uint _voteId)
  external
  voteExists(_voteId) {
    require(msg.sender == votes[_voteId].creator,
      "only creator can cancel vote");
    require(votes[_voteId].state == VoteState.VOTING,
      "cancel only in VOTING state");

    votes[_voteId].state = VoteState.CANCELLED;
  }

  function confirmVote(uint _voteId)
  external
  voteExists(_voteId) {
    Vote storage voteCopy = votes[_voteId];
    require(msg.sender == voteCopy.creator || msg.sender == owner,
      "not allow to confirm");
    require(now > voteCopy.end,
      "vote is not finished yet");
    require(voteCopy.state == VoteState.VOTING,
      "only confirm in VOTING state");

    // vote got majority agreements
    if(voteCopy.up > voteCopy.down) {
      voteCopy.voteResult = true;
      if(voteCopy.voteType == VoteType.REVIEW) {
        reviewDataInst.cancelReview(voteCopy.reviewId);
      }
      if(voteCopy.voteType == VoteType.PENALTY) {
        repuDataInst.adjustedReputationChanger(voteCopy.targetUser, PENALTYSCORE, false);
      }
      if(voteCopy.voteType == VoteType.PRAISE) {
        repuDataInst.adjustedReputationChanger(voteCopy.targetUser, PRAISESCORE, true);
      }
    // vote got rejected, nothing happens
    } else {
      voteCopy.voteResult = false;
    }

    voteCopy.state = VoteState.CONFIRMED;

    // compensate who voted to majority
    for(uint i = 0; i < votersList[_voteId].length; i++) {
      address voter = votersList[_voteId][i];
      if(voters[_voteId][voter].voteTo == voteCopy.voteResult) {
        repuDataInst.adjustedReputationChanger(voter, VOTEFEE, true);
      }
    }
  }

  function getVoteType(uint _voteId)
  external view
  returns(VoteType) { return votes[_voteId].voteType; }

  function getVoteCreator(uint _voteId)
  external view
  returns(address) { return votes[_voteId].creator; }

  function getVoteTargetUser(uint _voteId)
  external view
  returns(address) { return votes[_voteId].targetUser; }

  function getVoteReviewId(uint _voteId)
  external view
  returns(uint) { return votes[_voteId].reviewId; }

  function getVoteUps(uint _voteId)
  external view
  onlyConfirmed(_voteId)
  returns(uint) { return votes[_voteId].up; }

  function getVoteDowns(uint _voteId)
  external view
  onlyConfirmed(_voteId)
  returns(uint) { return votes[_voteId].down; }

  function getVoteStart(uint _voteId)
  external view
  returns(uint) { return votes[_voteId].start; }

  function getVoteEnd(uint _voteId)
  external view
  returns(uint) { return votes[_voteId].end; }

  function getVoteResult(uint _voteId)
  external view
  onlyConfirmed(_voteId)
  returns(bool) { return votes[_voteId].voteResult; }

  function getVoteState(uint _voteId)
  external view
  returns(VoteState) { return votes[_voteId].state; }

  function getVoterVoted(uint _voteId, address _userAddress)
  external view
  returns(bool) { return voters[_voteId][_userAddress].voted; }

  function getVoterVoteTo(uint _voteId, address _userAddress)
  external view
  returns(bool) { return voters[_voteId][_userAddress].voteTo; }

  function getAllVotes()
  external view
  returns(Vote[] memory) {
    Vote[] memory votesTemp = new Vote[](nextVoteId - 1);
    for(uint i = 1; i < nextVoteId; i++) {
      votesTemp[i - 1] = votes[i];
    }
    return votesTemp;
  }
}
