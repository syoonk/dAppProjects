// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

/**
 * This contract manages data addresses.
 */

  // uint constant USER       = 0;
  // uint constant RIDER      = 1;
  // uint constant SHOP       = 2;
  // uint constant ORDER      = 3;
  // uint constant REVIEW     = 4;
  // uint constant REPUTATION = 5;
  // uint constant WALLET     = 6;

contract AppDatas {
  address owner;

  mapping(uint => address) dataAddresses;
  mapping(address => bool) apps;

  constructor() public { owner = msg.sender; }

  modifier onlyOwner() {
    require(msg.sender == owner,
      "only owner");
    _;
  }

  function setDataAddresses(uint _dataType, address _dataAddress)
  external
  onlyOwner() { dataAddresses[_dataType] = _dataAddress; }

  function setApps(address _appAddress)
  external
  onlyOwner() { apps[_appAddress] = true; }

  function disableApps(address _appAddress)
  external
  onlyOwner() { apps[_appAddress] = false; }

  function getDataAddress(uint _dataType)
  external view
  returns(address) { return dataAddresses[_dataType]; }
}