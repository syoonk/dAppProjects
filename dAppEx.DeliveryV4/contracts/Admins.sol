// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

/**
* It manages app admins and app approving
*/
contract Admins {
  enum DataType {
    USER,         // 0
    SHOP,         // 1
    RIDER,        // 2
    ORDER,        // 3
    REVIEW,       // 4
    REPUTATION }  // 5

  mapping(address => bool) admins;
  mapping(address => bool) dataApproved;
  mapping(uint => address) dataAddresses;
  mapping(address => bool) appApproved;
  mapping(address => bool) utilApproved;
  address[] adminsList;
  address[] appsList;
  address[] utilsList;

  constructor() public {
    admins[msg.sender] = true;
  }

  modifier onlyAdmin(address _user) {
    require(admins[_user],
      "only admin");
    _;
  }

  modifier onlyApprovedData(address _dataAddress) {
    require(dataApproved[_dataAddress],
      "only approved data");
    _;
  }

  modifier onlyApprovedApp(address _appAddress) {
    require(appApproved[_appAddress] || dataApproved[_appAddress],
      "only approved app");
    _;
  }

  modifier onlyApprovedUtil(address _utilAddress) {
    require(utilApproved[_utilAddress],
      "only approved util");
    _;
  }

  function addAdmin(address _user)
  external
  onlyAdmin(msg.sender) {
    admins[_user] = true;
    adminsList.push(_user);
  }

  function removeAdmin(address _user)
  external
  onlyAdmin(msg.sender) {
    admins[_user] = false;

    /*

      Remove from the list

    */
  }

  function approveData(address _dataAddress, DataType _dataType)
  external
  onlyAdmin(msg.sender) {
    dataApproved[_dataAddress] = true;
    dataAddresses[uint(_dataType)] = _dataAddress;
  }

  function disapproveData(address _dataAddress, DataType _dataType)
  external
  onlyAdmin(msg.sender) {
    dataApproved[_dataAddress] = false;
    delete dataAddresses[uint(_dataType)];

    /*

      Remove from the list

    */
  }

  function approveApp(address _appAddress)
  external
  onlyAdmin(msg.sender) {
    appApproved[_appAddress] = true;
    appsList.push(_appAddress);
  }

  function disapproveApp(address _appAddress)
  external
  onlyAdmin(msg.sender) {
    appApproved[_appAddress] = false;

    /*

      Remove from the list

    */
  }

  function approveUtil(address _utilAddress)
  external
  onlyAdmin(msg.sender) {
    utilApproved[_utilAddress] = true;
    utilsList.push(_utilAddress);
  }

  function disapproveUtil(address _utilAddress)
  external
  onlyAdmin(msg.sender) {
    utilApproved[_utilAddress] = false;

    /*

      Remove from the list

    */
  }

  function getDataAddress(uint _type)
  public view
  returns(address) {
    require(dataAddresses[_type] != address(0),
      "this data type is not registered");

    return dataAddresses[_type];
  }

  function onlyAdminTrigger(address _user)
  public view
  onlyAdmin(_user)
  returns(bool) { return true; }

  function onlyApprovedDataTrigger(address _dataAddress)
  public view
  onlyApprovedData(_dataAddress)
  returns(bool) { return true; }

  function onlyApprovedAppTrigger(address _appAddress)
  public view
  onlyApprovedApp(_appAddress)
  returns(bool) { return true; }

  function onlyApprovedUtilTrigger(address _utilAddress)
  public view
  onlyApprovedUtil(_utilAddress)
  returns(bool) { return true; }
}