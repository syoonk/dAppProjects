// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.6;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Krw is ERC20 {
    constructor() ERC20('KRW', 'Coin equal to Korea Currency')
    public {}
    
    function faucet(address _to, uint _amount)
    external {
        _mint(_to, _amount);
    }
}