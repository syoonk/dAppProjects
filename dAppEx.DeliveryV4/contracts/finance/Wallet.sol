// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "@openzeppelin/contracts/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../Admins.sol";
import "../utils/KRWCommission.sol";
import "../utils/KNGScoring.sol";
import "./mocks/Kng.sol";

/**
* It manages finances of users and communicates with coin contracts
*/

contract Wallet {
  using SafeMath for uint;

  struct Token {
    bytes32 ticker;
    address tokenAddress;
  }

  mapping(bytes32 => Token) tokens;
  mapping(address => mapping(bytes32 => uint)) userBalances;
  mapping(address => uint) pendingBalances;
  bytes32[] tokenList;

  address owner;
  address admin;
  address krwUtil;
  address kngUtil;

  bytes32 constant KRW = bytes32("KRW");
  bytes32 constant KNG = bytes32("KNG");

  constructor()
  public { owner = msg.sender; }

  modifier onlyOwner() {
    require(msg.sender == owner,
      "only owner");
    _;
  }

  modifier tokenExists(bytes32 _ticker) {
    require(tokens[_ticker].tokenAddress != address(0),
      "token does not exist");
    _;
  }

  modifier enoughBalance(
    address _user,
    uint _amount,
    bytes32 _ticker) {
    require(userBalances[_user][_ticker] >= _amount,
      "not enough balance");
    _;
  }

  /**
  * @param _adminAddress of the admin contract address
  */
  function setAdmin(address _adminAddress)
  external
  onlyOwner() { admin = _adminAddress; }

  /**
   * @notice Set calculating KRW Commission Utility Address
   *         utilAddress should pre-approved by admin
   * @param _utilAddress contract address of calculating KRW commission
   */
  function setKrwCommissionUtil(address _utilAddress)
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);
    Admins(admin).onlyApprovedUtilTrigger(_utilAddress);

    krwUtil = _utilAddress;
  }

  function setKngScoringUtil(address _utilAddress)
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);
    Admins(admin).onlyApprovedUtilTrigger(_utilAddress);

    kngUtil = _utilAddress;
  }

  function addToken(bytes32 _ticker, address _address)
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);

    tokens[_ticker] = Token(_ticker, _address);
    tokenList.push(_ticker);
  }

  /**
   * @dev Wallet should be pre=approved(ERC20 Token) by user to transfer. It would be done by outsie source.
   *      User should register first. Or it would be reverted from Calculatin Commission.
   */
  function deposit(uint _amount, bytes32 _ticker)
  external
  tokenExists(_ticker) {
    require(krwUtil != address(0),
      "util is not set");

    IERC20(tokens[_ticker].tokenAddress).transferFrom(msg.sender, address(this), _amount);
    userBalances[address(this)][_ticker] = userBalances[address(this)][_ticker].add(_amount);

    if(_ticker == KRW) {
      KRWCommission krwInst = KRWCommission(krwUtil);
      uint commission = krwInst.calculateCommission(msg.sender);
      uint feeAmount = _amount.mul(commission).div(100000);
      uint depositAmount = _amount.sub(feeAmount);

      userBalances[address(this)][KRW] = userBalances[address(this)][KRW].sub(depositAmount);
      userBalances[msg.sender][KRW] = userBalances[msg.sender][KRW].add(depositAmount);

      return;
    }

    userBalances[address(this)][_ticker] = userBalances[address(this)][_ticker].sub(_amount);
    userBalances[msg.sender][_ticker] = userBalances[msg.sender][_ticker].add(_amount);
  }

  function withdraw(uint _amount, bytes32 _ticker)
  external
  tokenExists(_ticker) enoughBalance(msg.sender, _amount, _ticker) {
    userBalances[msg.sender][_ticker] = userBalances[msg.sender][_ticker].sub(_amount);
    IERC20(tokens[_ticker].tokenAddress).transfer(msg.sender, _amount);
  }

  /**
   *
   */
  function transfer(
    address _sender,
    address _receiver,
    uint _amount,
    bytes32 _ticker)
  external
  tokenExists(_ticker) enoughBalance(_sender, _amount, _ticker) {
    userBalances[_sender][_ticker] = userBalances[_sender][_ticker].sub(_amount);
    userBalances[_receiver][_ticker] = userBalances[_receiver][_ticker].add(_amount);
  }

  /**
   * @notice Hold the balance until the order finished
   */
  function holdKrw(address _sender, uint _amount)
  public
  enoughBalance(_sender, _amount, KRW) {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);

    userBalances[_sender][KRW] = userBalances[_sender][KRW].sub(_amount);
    pendingBalances[_sender] = pendingBalances[_sender].add(_amount);
  }

  /**
   * @notice Release the pended KRW
   */
  function releaseKRW(
    address _sender,
    address _receiver,
    uint _amount)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    require(pendingBalances[_sender] >= _amount,
      "not enough pending balance");

    pendingBalances[_sender] = pendingBalances[_sender].sub(_amount);
    userBalances[_receiver][KRW] = userBalances[_receiver][KRW].add(_amount);
  }

  /**
   * @dev it has security issue. Anybody can see user balance.
   */
  function getBalance(address _user, bytes32 _ticker)
  external view
  returns(uint) { return userBalances[_user][_ticker]; }

  function getPendingKrwBalance(address _user)
  external view
  returns(uint) { return pendingBalances[_user]; }

  /**
   * @dev Minting coins, it needs more security.
   */
  function faucetKngToken(address _userAddress, uint _amount)
  public {
    Admins(admin).onlyApprovedAppTrigger(msg.sender);
    require(kngUtil != address(0),
      "util is not set");

    uint score = KNGScoring(kngUtil).calculateFaucetAmount(_userAddress, _amount);

    Kng(tokens[KNG].tokenAddress).faucet(_userAddress, score);
  }

  function getOwner()
  external
  returns(address) { return owner;}
}