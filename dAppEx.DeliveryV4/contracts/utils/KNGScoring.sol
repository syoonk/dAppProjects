// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "../Admins.sol";
import "../data/DeliveryReputation.sol";

contract KNGScoring {
  address owner;
  address admin;

  constructor() public { owner = msg.sender; }

  function setAdmin(address _adminAddress)
  external {
    require(msg.sender == owner,
      "only owner");

    admin = _adminAddress;
  }

  function calculateFaucetAmount(address _userAddress, uint _amount)
  public view
  returns(uint) {
    address REPUDATA = Admins(admin).getDataAddress(5);
    uint MAXRANK = DeliveryReputation(REPUDATA).getMaxRank();
    uint userRank = DeliveryReputation(REPUDATA).getUserRank(_userAddress);

    uint score = _amount * ((3 * MAXRANK / 2) - userRank) / MAXRANK;

    return score;
  }
}