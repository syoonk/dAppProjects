// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "../Admins.sol";
import "../data/DeliveryReputation.sol";

contract AdjustingScore {
  address admin;
  address owner;

  constructor() public { owner = msg.sender; }

  function setAdmin(address _adminAddress)
  external {
    require(msg.sender == owner,
      "only owner");

    admin = _adminAddress;
  }

  function adjustScore(address _userAddress, uint _score)
  public view
  returns(uint) {
    address REPUDATA = Admins(admin).getDataAddress(5);
    DeliveryReputation repuInst = DeliveryReputation(REPUDATA);

    uint MAXREP = repuInst.MAXREP();
    uint userRepu = repuInst.getUserReputation(_userAddress);

    uint adjusted = 2 * (MAXREP - userRepu) * _score / MAXREP;

    return adjusted;
  }
}