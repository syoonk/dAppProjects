// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.9;

import "../Admins.sol";
import "../data/DeliveryReputation.sol";

contract KRWCommission {
  address admin;
  address owner;

  uint COMMISSIONBASE = 5000;

  constructor() public { owner = msg.sender; }

  function setAdmin(address _adminAddress)
  external {
    require(msg.sender == owner,
      "only owner");

    admin = _adminAddress;
  }

  function setCommissionBase(uint _newCom)
  external {
    Admins(admin).onlyAdminTrigger(msg.sender);
    require(_newCom >= 0 && _newCom <= 100000,
      "commission should be >= 0, <= 100000");

    COMMISSIONBASE = _newCom;
  }

  function calculateCommission(address _user)
  public view
  returns(uint) {
    address REPUDATA = Admins(admin).getDataAddress(5);
    DeliveryReputation repuInst = DeliveryReputation(REPUDATA);
    uint MAXRANK = repuInst.getMaxRank();
    uint userRank = repuInst.getUserRank(_user);

    uint commission = COMMISSIONBASE * (MAXRANK + 2 * userRank) / (2 * MAXRANK);

    return commission;
  }

  function getBaseCommission()
  public view
  returns(uint) { return COMMISSIONBASE; }
}