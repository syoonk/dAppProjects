# About Dex Application
Sample Food Delivery Application. Users can buy food from shop and rider delivers food to buyer. They use KRW token as primary currency and earn some score(reputation) and KNG Token.  
Frontend is now available. Please refer this [README](https://gitlab.com/syoonk/dAppProjects/-/blob/master/README.md).  
**Page:** http://syoonk.gitlab.io/delivery

## Used Tools
Solidity, Truffle, NodeJS, React, Metamask

----
## How To Run
It uses metamask.

| EndPoints ||
---|---|
Truffle | http://localhost:9545  
Ganache | http://localhost:8545

### Deploy Contract  
`./npm install`  
`./truffle develop`  
`truffle> migrate --reset`  

### Run Frontend
`./client/npm install`  
`./client/npm start`

----
## Features
### 1. Separated Data and App
It has Data Part and App Part. Data stores all infos and transacted values. App will use those value. Data app is not supposed to be used by users. Only approved App Contract can access the value. Approvement is managed from Admin Contract. Only admin user can approve app smart contracts.
App can be any form if it is once approved. Multiple apps can exist using same data. 

### 2. Currency
Coins are based on OpenZeppelin ERC20.
- KRW  
This coin is primary currency.

- KNG  
This coin is specific coin minted by app. It can be traded or be used to buy some specific goods app selling.(It is not built yet)


### 3. User Types
Any user can buy, sell and deliver food.

  - Buyer  
  - Shop  
    User can register their own shop. One user can have multiple shops.

  - Rider  
    User can be rider as well. One user is able to be assigned only one rider ID.

----
### 4. Reputation

Each user has own reputation score. It can be raised by good deeds.
#### Used For :
- KRW Deposit fee

  Higher score may reduce deposit fee. Lower score may higher the fee.

- KNG Coin Reward  
  Higher score may get more KNG coins.

- Matching (Not implemented yet)  
  High score user will possibly be matched with other high score users. For example, if rider has relatively low score, rider only can take order of low score users.

----
### 5. Voting
Each user can requests vote for following reason. Every users can vote.
  
- Cancel Review  
  If users think they got review unfairly, they can start vote for cancelling review. They will upload the evidences on the board.(it is not implemented yet)
  
- Praise  
  If user wants to praise another user, they can start vote. If vote is agreed, target user will get additional reputation score.
  
- Penalty  
  If user wants to give penalty someone, they can start vote. If vote is agreed, target user will lose reputation score.

Except target user and creator, every user can vote and will get few amount of reputation. If users vote to majority, they will get additional reputation reward.

----
## Pages
### Register User
If user is not registered, user only can see the registering page.  
<img src="./images/registerUser.png" width="50%" height="50%"></img>

### Home
This is main page. It shows users rank. Rank is sorted by reputation score.
<img src="./images/home.png" width="50%" height="50%"></img>  

### My Page
Users can see the review they uploaded and received. And buyers also sign their orders.
<img src="./images/myPage.png" width="50%" height="50%"></img>  

### Order
It shows shops list and buyers can order from here.  
<img src="./images/order.png" width="50%" height="50%"></img>  

### Shop
Shop owner manages the shop from this page. They check shop orders here.  
<img src="./images/registerShop.png" width="50%" height="50%"></img>  
<img src="./images/shop.png" width="50%" heigth="50%"></img>  

### Rider
Rider can take orders to deliver from thie page. If user didn't register as a rider before, they would see the registering page.
<img src="./images/registerRider.png" width="50%" height="50%"></img>
<img src="./images/rider.png" width="50%" heigth="50%"></img>

### Wallet
Users deposit, withdraw and transfer their coins from here. Wallet balance is used in delivery app.  
<img src="./images/wallet.png" width="50%" heigth="50%"></img>

----
## To Do List
* Add ERC721 Shop that can be bought by KNG tokens
* User matching system by reputation score