import React, { useEffect, useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const OrderState = ["ORDERED", "PREPARING", "DELIVERING", "DELIVERED", "COMPLETED", "CANCELLED"];
const UserType = ["BUYER", "RIDER", "SHOP"];

function Rider({contracts, accounts}) {
  const [registered, setRegistered] = useState(false);
  const [riderInfo, setRiderInfo] = useState({
    id: 0,
    phoneNumber: 0,
    currentOrderId: 0,
    nextOrderId: 0,
    completedOrders: [],
    reviews: [],
    area: "",
    activation: false
  });
  const [orderList, setOrderList] = useState([]);
  const [completedOrderList, setCompletedOrderList] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [openOrderDetail, setOpenOrderDetail] = useState({open: false, orderId: 0});
  const [assignedOrderDetail, setAssignedOrderDetail] = useState({});
  const [openUploadReview, setOpenUploadReview] = useState({
    open: false, 
    orderId: 0,
    reviewedToBuyer: false,
    reviewedToShop: false });

  const getRiderInfo = async (_account) => {
    let id, phoneNumber, currentOrderId, nextOrderId, completedOrders, reviews, area, activation;
    try {
      id = await contracts.rider.methods
        .getRiderId(_account)
        .call();
    } catch {
      setRegistered(false);
      return;
    }

    phoneNumber = await contracts.rider.methods
      .getRiderPhoneNumber(id)
      .call();
    currentOrderId = await contracts.rider.methods
      .getRiderCurrentOrderId(id)
      .call();
    nextOrderId = await contracts.rider.methods
      .getRiderNextOrderId(id)
      .call();
    completedOrders = await contracts.rider.methods
      .getRiderCompletedOrders(id)
      .call();
    reviews = await contracts.rider.methods
      .getRiderReviews(id)
      .call();
    area = await contracts.rider.methods
      .getRiderArea(id)
      .call();
    activation = await contracts.rider.methods
      .getRiderActivation(id)
      .call();
      
    setRiderInfo({
      phoneNumber,
      currentOrderId,
      nextOrderId,
      completedOrders,
      reviews,
      area,
      activation });
    
    setRegistered(true);
    
    await getReviews(reviews);
  }

  const getOrderList = async () => {
    const nextOrderId = await contracts.order.methods
      .getNextOrderId()
      .call();

    let orderListTemp = [];
    for(let i = 1; i < nextOrderId; i++) {
      const orderState = Number(await contracts.order.methods
        .getOrderState(i)
        .call());
      const assignedRider = Number(await contracts.order.methods
        .getOrderRiderId(i)
        .call());
      
      if(orderState > 1 || assignedRider !== 0) continue;
      
      const shopId = await contracts.order.methods
        .getOrderShopId(i)
        .call();
      const buyer = await contracts.order.methods
        .getOrderBuyer(i)
        .call();
      const shopOwner = await contracts.order.methods
        .getOrderShopOwner(i)
        .call();
      const buyerAddress = await contracts.user.methods
        .getUserHomeAddress(buyer)
        .call();
      const shopAddress = await contracts.shop.methods
        .getShopAddress(shopId)
        .call();
      const buyerReputation = await contracts.user.methods
        .getUserReputation(buyer)
        .call();
      const shopReputation = await contracts.user.methods
        .getUserReputation(shopOwner)
        .call();
      const averageRepu = (Number(buyerReputation) + Number(shopReputation)) / 2;
      
      orderListTemp.push({
        orderId: i,
        shopId,
        shopAddress,
        buyerAddress,
        averageRepu });
      
      for(let j = orderListTemp.length - 1; j > 0; j--) {
        if(orderListTemp[j - 1].averageRepu >= orderListTemp[j].averageRepu) break;

        const dummy = orderListTemp[j - 1];
        orderListTemp[j - 1] = orderListTemp[j];
        orderListTemp[j] = dummy;
      }
    }

    setOrderList(orderListTemp);
  }

  const getCompletedOrders = async () => {
    const riderId = await contracts.rider.methods
      .getRiderId(accounts[0])
      .call();
    const completedOrderIds = await contracts.rider.methods
      .getRiderCompletedOrders(riderId)
      .call();

    let completedOrderListTemp = [];
    for(let orderId of completedOrderIds) {
      const buyer = await contracts.order.methods
        .getOrderBuyer(orderId)
        .call();
      const shopId = await contracts.order.methods
        .getOrderShopId(orderId)
        .call();
      const stateTimes = await contracts.order.methods
        .getOrderStateTimes(orderId)
        .call();
      
      completedOrderListTemp.push({
        orderId,
        buyer,
        shopId,
        stateTimes });
    }

    setCompletedOrderList(completedOrderListTemp);
  }

  const getReviews = async (_reviewIds) => {
    let reviewsTemp = [];
    for(let i = 0; i < _reviewIds.length; i++) {
      const uploaderType = await contracts.review.methods
        .getReviewFromUserType(_reviewIds[i])
        .call();
      const orderId = await contracts.review.methods
        .getReviewOrderId(_reviewIds[i])
        .call();
      const stars = await contracts.review.methods
        .getReviewStars(_reviewIds[i])
        .call();
      const cancelled = await contracts.review.methods
        .getReviewCancelled(_reviewIds[i])
        .call();

      reviewsTemp.push({
        uploaderType,
        orderId,
        stars,
        cancelled });
    }

    setReviews(reviewsTemp);
  }

  const submitRegisterRiderHandle = async (_e) => {
    _e.preventDefault();

    const phoneNumber = document.getElementById("registerPhoneNumberInput").value;
    const address = document.getElementById("registerAddressSelect").value;
    if(address !== "area1" && address !== "area2") {
      alert("Not valid address");
      return;
    }
    
    await contracts.rider.methods
      .registerRider(phoneNumber, address)
      .send({from: accounts[0]});

    setRegistered(true);

    getRiderInfo(accounts[0]);
  }

  const clickOpenOrderDetailHandle = async (_orderId) => {
    const orderState = Number(await contracts.order.methods
      .getOrderState(_orderId)
      .call());    
    const shopId = await contracts.order.methods
      .getOrderShopId(_orderId)
      .call();
    const buyer = await contracts.order.methods
      .getOrderBuyer(_orderId)
      .call();
    const shopOwner = await contracts.order.methods
      .getOrderShopOwner(_orderId)
      .call();
    const buyerAddress = await contracts.user.methods
      .getUserHomeAddress(buyer)
      .call();
    const shopAddress = await contracts.shop.methods
      .getShopAddress(shopId)
      .call();
    const buyerReputation = await contracts.user.methods
      .getUserReputation(buyer)
      .call();
    const shopReputation = await contracts.user.methods
      .getUserReputation(shopOwner)
      .call();
    const averageRepu = (Number(buyerReputation) + Number(shopReputation)) / 2;

    setAssignedOrderDetail({
      orderId: _orderId, 
      shopId, 
      shopAddress, 
      buyerAddress, 
      orderState, 
      averageRepu});
    setOpenOrderDetail({open: true, orderId: _orderId});
  }

  const clickSignHandle = async (_orderId) => {
    try {
      await contracts.order.methods
        .signOrder(_orderId)
        .send({from: accounts[0]});

      const orderState = Number(await contracts.order.methods
        .getOrderState(_orderId)
        .call());    
      setAssignedOrderDetail({...assignedOrderDetail, orderState});
    } catch(e) {
      alert(e);
    }
  }

  const clickCloseOrderDetailHandle = () => {
    setOpenOrderDetail({open: false, orderId: 0});
    setAssignedOrderDetail({});
  }

  const clickUnassignHandle = async () => {
    try {
      await contracts.rider.methods
        .cancelAssignedOrder(openOrderDetail.orderId)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    setOpenOrderDetail({open: false, orderId: 0});

    await getRiderInfo(accounts[0]);
    await getOrderList();
  }

  const clickTakeOrderHandle = async (_orderId) => {
    try{
      await contracts.rider.methods
        .takeOrder(_orderId)
        .send({from: accounts[0]});

      await getRiderInfo(accounts[0]);
      await getOrderList();
    } catch(e) {
      alert(e);
    }
  }

  const clickOpenUploadReviewHandle = async (_orderId) => {
    const reviewedToBuyer = await contracts.review.methods
      .getReviewed(_orderId, 1, 0)
      .call();
    const reviewedToShop = await contracts.review.methods
      .getReviewed(_orderId, 1, 2)
      .call();
    
    setOpenUploadReview({
      open: true,
      orderId: _orderId,
      reviewedToBuyer,
      reviewedToShop });
  }

  const clickUploadReviewHandle = async () => {
    const target = document.getElementById("selectReviewTarget").value;
    const score = document.getElementById("selectReviewScore").value;

    try {
      if(score < 1 || score > 10 || target < 0 || target > 2) throw "invalid value";

      await contracts.review.methods
        .uploadReview(target, openUploadReview.orderId, score)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getCompletedOrders();

    setOpenUploadReview({
      open: false,
      orderId: 0,
      reviewedToBuyer: false,
      reviewedToShop: false });
  }

  const clickCloseUploadReviewHandle = async () => {
    setOpenUploadReview({
      open: false,
      orderId: 0,
      reviewedToBuyer: false,
      reviewedToShop: false });
  }

  useEffect(() => {
    const init = async () => {
      await getRiderInfo(accounts[0]);
      await getOrderList();
      await getCompletedOrders();
    }
    init();
  }, [accounts]);
  
  return (
    <>
      { !registered ? (
        <div className="container">
          <form className="center-block" onSubmit={e => submitRegisterRiderHandle(e)}>
            <fieldset>
              <legend>Register Rider</legend>
              <div className="form-group">
                <label>Phone Number</label>
                <input
                  id="registerPhoneNumberInput"
                  type="number"
                  className="form-control"
                  placeholder="phone number"
                ></input>
              </div>
              <div className="form-group">
                <label>Home Address</label>
                <select className="form-control" id="registerAddressSelect">
                  <option style={{fontWeight: "bold"}} value="">Choose Address</option>
                  <option value="area1">Area1</option>
                  <option value="area2">Area2</option>
                </select>
              </div>
              <button className="btn btn-primary">register</button>
            </fieldset>
          </form>
        </div>
      ) : (
        <main className="container-fluid row mt-md-3">
          <div className="card border-light" style={{maxWidth: "30rem"}}>
            <div className="card-header">Rider Info</div>
            <div>
              <table className="table" style={{textAlign: "center"}}>
                <tbody>
                  <tr>
                    <th scope="row">Phone Number</th>
                    <td>{riderInfo.phoneNumber}</td>
                  </tr>
                  <tr 
                    style={{cursor: "pointer"}} 
                    onClick={() => clickOpenOrderDetailHandle(riderInfo.currentOrderId)} 
                    data-toggle="tooltip" 
                    data-placement="top" 
                    title="Click to Detail">
                    <th scope="row">Current OrderID</th>
                    <td>{riderInfo.currentOrderId}</td>
                  </tr>
                  <tr 
                    style={{cursor: "pointer"}} 
                    onClick={() => clickOpenOrderDetailHandle(riderInfo.nextOrderId)}
                    data-toggle="tooltip" 
                    data-placement="top" 
                    title="Click to Detail">
                    <th scope="row">Next OrderID</th>
                    <td>{riderInfo.nextOrderId}</td>
                  </tr>
                  <tr>
                    <th scope="row">Area</th>
                    <td>{riderInfo.area}</td>
                  </tr>
                  <tr>
                    <th scope="row">Activation</th>
                    <td>{String(riderInfo.activation)}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <Modal isOpen={openOrderDetail.open}>
              <ModalHeader>Order Detail</ModalHeader>
              <ModalBody>
                <table className="table">
                  <tbody>
                    <tr>
                      <th>Order ID</th>
                      <td>{assignedOrderDetail.orderId}</td>
                    </tr>
                    <tr>
                      <th>Shop Id</th>
                      <td>{assignedOrderDetail.shopId}</td>
                    </tr>
                    <tr>
                      <th>Shop Location</th>
                      <td>{assignedOrderDetail.shopAddress}</td>
                    </tr>
                    <tr>
                      <th>Buyer Location</th>
                      <td>{assignedOrderDetail.buyerAddress}</td>
                    </tr>
                    <tr>
                      <th>Current State</th>
                      <td>{OrderState[assignedOrderDetail.orderState]}</td>
                    </tr>
                    <tr>
                      <th>Sign</th>
                      <td><button 
                        className="btn btn-primary btn-sm"
                        disabled={assignedOrderDetail.orderState == 1 || assignedOrderDetail.orderState == 2 ? false : true}
                        onClick={() => clickSignHandle(assignedOrderDetail.orderId)}
                      >Sign</button></td>
                    </tr>
                    <tr>
                      <th>Unassign Order</th>
                      <td><button 
                        className="btn btn-primary btn-sm" 
                        onClick={clickUnassignHandle}
                        disabled={assignedOrderDetail.orderState == 0 || assignedOrderDetail.orderState == 1 ? false : true}>Cancel Assignment</button></td>
                    </tr>
                  </tbody>
                </table>
              </ModalBody>
              <ModalFooter>
                <button className="btn btn-secondary" onClick={clickCloseOrderDetailHandle}>Close</button>
              </ModalFooter>
            </Modal>
          </div>

          <div className="col-md-8" style={{textAlign: "center"}}>
            <div>
              <legend>Orders</legend>
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">OrderID</th>
                    <th scope="col">Shop ID</th>
                    <th scope="col">Shop Location</th>
                    <th scope="col">Buyer Location</th>
                    <th scope="col">Average Repu</th>
                  </tr>
                </thead>
                <tbody>
                  {
                  orderList.map((order, idx) => (
                    <tr key={idx}>
                      <td>{order.orderId}</td>
                      <td>{order.shopId}</td>
                      <td>{order.shopAddress}</td>
                      <td>{order.buyerAddress}</td>
                      <td>{order.averageRepu}</td>
                      <td><button 
                        className="btn btn-primary btn-sm" 
                        onClick={() => clickTakeOrderHandle(order.orderId)}
                        disabled={riderInfo.currentOrderId !== '0' && riderInfo.nextOrderId !== '0' ? true : false}  
                      >take</button></td>
                    </tr>
                  ))
                  }
                </tbody>
              </table>
            </div>
            <div>
              <legend>Completed Orders</legend>
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Order ID</th>
                    <th scope="col">Buyer</th>
                    <th scope="col">Shop ID</th>
                    <th scope="col">Times</th>
                  </tr>
                </thead>
                <tbody>
                {
                completedOrderList.map((order, idx) => (
                  <tr key={idx}>
                    <td>{order.orderId}</td>
                    <td>{order.buyer.substr(0, 10)}...</td>
                    <td>{order.shopId}</td>
                    <td>{new Date(order.stateTimes[0] * 1000).toLocaleString()}</td>
                    <td><button className="btn btn-primary btn-sm" onClick={() => clickOpenUploadReviewHandle(order.orderId)}>Upload Review</button></td>
                  </tr>
                ))
                }
                </tbody>
              </table>
              <Modal isOpen={openUploadReview.open}>
                <ModalHeader>Upload Review</ModalHeader>
                <ModalBody>
                  <div>
                    <legend>To</legend>
                    <select className="form-control" id="selectReviewTarget">
                      <option style={{fontWeight: "bold"}} value={-1}>Choose Target User</option>
                      <option value={0} disabled={openUploadReview.reviewedToBuyer}>Buyer</option>
                      <option value={2} disabled={openUploadReview.reviewedToShop}>Shop</option>
                    </select>
                  </div>
                  <div>
                    <legend>Score</legend>
                    <select className="form-control" id="selectReviewScore">
                      <option style={{fontWeight: "bold"}} value={-1}>Choose Score</option>
                      {
                      [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((score, idx) => (<option key={idx} value={score}>{score}</option>))
                      }
                    </select>
                  </div>
                </ModalBody>
                <ModalFooter>
                {
                  openUploadReview.reviewedToBuyer && openUploadReview.reviewedToShop ? (<>You already uploaded Reviews</>) : (
                    <button className="btn btn-primary" onClick={() => clickUploadReviewHandle()}>Upload</button> )
                }
                  <button className="btn btn-secondary" onClick={() => clickCloseUploadReviewHandle()}>Close</button>
                </ModalFooter>
              </Modal>
            </div>
            <div>
              <legend>Reviews</legend>
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Reviewer</th>
                    <th scope="col">Order Id</th>
                    <th scope="col">Score</th>
                  </tr>
                </thead>
                <tbody>
                  {
                  reviews.map((review, idx) => (
                    <tr key={idx}>  
                      <td>{UserType[review.uploaderType]}</td>
                      <td>{review.orderId}</td>
                      <td>{review.stars}</td>
                      <td><button className="btn btn-primary">Details</button></td>
                    </tr>
                  ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </main>
      )
      }
    </>
  )
}

export default Rider;
