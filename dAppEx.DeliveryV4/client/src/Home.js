import React, { useEffect, useState } from "react";

function Home({contracts, userInfo, accounts}) {
  const [ranks, setRanks] = useState([]);

  const getRanks = async () => {
    const rankAddresses = await contracts.user.methods
      .getRanks()
      .call();
    
    const ranksTemp = Array(rankAddresses.length);
    for(let i = 0; i < ranksTemp.length; i++) {
      const userId = await contracts.user.methods
        .getUserId(rankAddresses[i])
        .call();
      const reputation = await contracts.user.methods
        .getUserReputation(rankAddresses[i])
        .call();
      
      ranksTemp[i] = {
        rank: i, 
        id: userId, 
        address: rankAddresses[i],
        reputation: reputation};
    }
    
    setRanks(ranksTemp);
  }

  useEffect(() => {
    const init = async () => {
      await getRanks();
    }
    init();
  }, []);

  return(
    <main className="container-fluid row mt-md-3">
      
      <div className="col-sm-3 first-col">
        <div className="card border-light" style={{maxWidth: "30rem"}}>
          <div className="card-header">User Info</div>
          <div>
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Address</th>
                  <td>{String(accounts[0]).substr(0, 10)}...</td>
                </tr>
                <tr>
                  <th scope="row">ID</th>
                  <td>{userInfo.infos.id}</td>
                </tr>
                <tr>
                  <th scope="row">Home</th>
                  <td>{userInfo.infos.homeAddress}</td>
                </tr>
                <tr>
                  <th scope="row">Reputation</th>
                  <td>{userInfo.infos.reputation}</td>
                </tr>
                <tr>
                  <th scope="row">Rank</th>
                  <td>{userInfo.infos.rank}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="card border-light" style={{maxWidth: "20rem"}}>
            <div className="card-header">Wallet Balances</div>
            <div>
              <table className="table">
                <tbody>
                  <tr>
                    <th scope="row">KRW</th>
                    <td>{userInfo.balances.KRW}</td>
                  </tr>
                  <tr>
                    <th scope="row">KNG</th>
                    <td>{userInfo.balances.KNG}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>          
        </div>
      </div>

      <div className="col-md-8" style={{textAlign: "center"}}>
        <legend>Rank</legend>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Rank</th>
              <th scope="col">ID</th>
              <th scope="col">User Address</th>
              <th scope="col">Reputation</th>
            </tr>
          </thead>
          <tbody>
            {ranks.map(_rank => (
              <tr key={_rank.id}>
                <td>{_rank.rank}</td>
                <td>{_rank.id}</td>
                <td>{_rank.address.substr(0, 10)}...</td>
                <td>{_rank.reputation}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>

    </main>
  )
}

export default Home;