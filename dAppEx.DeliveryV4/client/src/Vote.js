import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const VoteType = ["REVIEW", "PENALTY", "PRAISE"];
const VoteState = ["VOTING", "CONFIRMED", "CANCELLED"];

function Vote({contracts, accounts, web3}) {
  const [ongoingVotes, setOngoingVotes] = useState([]);
  const [confirmedVotes, setConfirmedVotes] = useState([]);
  const [myVotes, setMyVotes] = useState([]);
  const [openCreateVote, setOpenCreateVote] = useState(false);

  const getVotes = async () => {
    const voteList = await contracts.vote.methods
      .getAllVotes()
      .call();

    let myVotesTemp = [];
    let confirmedVotesTemp = [];
    let ongoingVotesTemp = [];    
    for(let i = 0; i < voteList.length; i++) {           
      if(voteList[i].creator.toLowerCase() === accounts[0].toLowerCase()) myVotesTemp.push(voteList[i]);     

      if(voteList[i].state == 0) {
        const voted = await contracts.vote.methods
          .getVoterVoted(voteList[i].voteId, accounts[0])
          .call();

        let currentTime = new Date().getTime()/ 1000;
        if(!voted && 
          currentTime < Number(voteList[i].end) &&
          voteList[i].creator.toLowerCase() !== accounts[0].toLowerCase() &&
          voteList[i].targetUser.toLowerCase() !== accounts[0].toLowerCase()) ongoingVotesTemp.push(voteList[i]);
      }
      else if(voteList[i].voteState > 0) confirmedVotesTemp.push(voteList[i]);
    }

    setOngoingVotes(ongoingVotesTemp);
    setConfirmedVotes(confirmedVotesTemp);
    setMyVotes(myVotesTemp);
  }

  const clickOpenCreatorHandle = () => { setOpenCreateVote(true); }

  const clickCloseCreatorHandle = () => { setOpenCreateVote(false); }

  const clickCreateVoteHandle = async () => {
    try{
      let targetUser, reviewId;
      const voteType = Number(document.getElementById("selectVoteType").value);
  
      if(voteType !== 0 && voteType !== 1 && voteType !== 2) throw "Invalid Vote Type";
      
      if(voteType === 0) {
        targetUser = "0x0000000000000000000000000000000000000000";
        reviewId = Number(document.getElementById("inputVoteTargetReview").value);

        if(reviewId <= 0 || typeof reviewId !== 'number') throw "Invalid Review Id";
      }
      else if(voteType > 0) {
        targetUser = document.getElementById("inputVoteTargetUser").value;
        reviewId = 0;

        if(!web3.utils.isAddress(targetUser)) throw "Invalid User Address";
      }

      await contracts.vote.methods
        .startVote(voteType, targetUser, reviewId)
        .send({from: accounts[0]});

      await getVotes();
    } catch(e) {
      alert(e);
    }

    setOpenCreateVote(false);
  }

  const clickCancelVoteHandle = async (_id) => {
    try {
      await contracts.vote.methods
        .cancelVote(_id)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getVotes();
  }

  const clickVoteHandle = async (_id, _vote) => {
    try{
      await contracts.vote.methods
        .vote(_id, _vote)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    await getVotes();
  }

  useEffect(() => {
    const init = async () => {
      await getVotes();
    }
    init();    
  }, [accounts]);
  
  return (
    <main className="container mt-md-3">
      <div>
        <div className="row form-group">
          <div>
            <legend>My Votes</legend>
          </div>
          <div>
            <button className="btn btn-primary ml-md-5" onClick={() => clickOpenCreatorHandle()}>Create Vote</button>
            <Modal isOpen={openCreateVote}>
              <ModalHeader>Create Vote</ModalHeader>
              <ModalBody>
                <div className="form-group">
                  <label>Vote Type</label>
                  <select className="form-control" id="selectVoteType">
                    <option style={{fontWeight: "bold"}} value={-1}>Choose Vote Type</option>
                    <option value={0}>Review</option>
                    <option value={1}>Penalty</option>
                    <option value={2}>Praise</option>
                  </select>
                </div>
                <div className="form-group">
                  <label>Target User</label>
                  <input
                    id="inputVoteTargetUser"
                    type="string"
                    className="form-control"
                    placeholder="user address"
                  ></input>
                  <small>If type is review, leave this part empty</small>
                </div>
                <div className="form-group">
                  <label>Review Id</label>
                  <input
                    id="inputVoteTargetReview"
                    type="number"
                    className="form-control"
                    placeholder="review id"
                  ></input>
                  <small>If type is not review, leave this part empty</small>
                </div>
              </ModalBody>
              <ModalFooter>
                <button className="btn btn-primary" onClick={() => clickCreateVoteHandle()}>Register</button> 
                <button className="btn btn-secondary" onClick={() => clickCloseCreatorHandle()}>Close</button>
              </ModalFooter>
            </Modal>
          </div>
        </div>
        <table className="table table-hover" style={{textAlign: "center"}}>
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">VoteType</th>
              <th scope="col">Vote End</th>
            </tr>
          </thead>
          <tbody>
            {
            myVotes.map((vote, idx) => (
              <tr key={idx}>
                <td>{vote.voteId}</td>
                <td>{VoteType[vote.voteType]}</td>
                <td>{new Date(vote.end * 1000).toLocaleString()}</td>
                <td>{vote.state == 2 ? <b>Cancelled</b> : <button className="btn btn-danger btn-sm" onClick={() => clickCancelVoteHandle(vote.voteId)}>Cancel</button>}</td>
                <td><button className="btn btn-primary btn-sm" disabled={(new Date().getDate() / 1000) < Number(vote.end) ? true : false}>confirm</button></td>
              </tr>
            ))
            }
          </tbody>
        </table>
      </div>

      <div>
        <legend>Ongoing Votes</legend>
        <table className="table table-hover" style={{textAlign: "center"}}>
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">VoteType</th>
              <th scope="col">Vote End</th>
              <th scope="col">Vote</th>
            </tr>
          </thead>
          <tbody>
            {
            ongoingVotes.map((vote, idx) => (
              <tr key={idx}>
                <td>{vote.voteId}</td>
                <td>{VoteType[vote.voteType]}</td>
                <td>{new Date(vote.end * 1000).toLocaleString()}</td>
                <td><button
                  className="btn btn-primary btn-sm"
                  onClick={() => clickVoteHandle(vote.voteId, true)}
                  disabled={vote.creator.toLowerCase() === accounts[0].toLowerCase() || vote.targetUser.toLowerCase() === accounts[0].toLowerCase() ? true : false}
                >Up</button>
                <button 
                  className="btn btn-secondary btn-sm" 
                  onClick={() => clickVoteHandle(vote.voteId, false)}
                  disabled={vote.creator.toLowerCase() === accounts[0].toLowerCase() || vote.targetUser.toLowerCase() === accounts[0].toLowerCase() ? true : false}
                >Down</button></td>
              </tr>
            ))
            }
          </tbody>
        </table>
      </div>
      <div>
        <legend>Ended Votes</legend>
        <table className="table table-hover" style={{textAlign: "center"}}>
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">VoteType</th>
              <th scope="col">Vote End</th>
              <th scope="col">Confirmed</th>
            </tr>
          </thead>
          <tbody>
            {
            confirmedVotes.map((vote, idx) => (
              <tr key={idx}>
                <td>{vote.voteId}</td>
                <td>{VoteType[vote.voteType]}</td>
                <td>{new Date(vote.end * 1000).toLocaleString()}</td>
                <td>{Number(vote.state) === 1 ? "Confirmed" : "Not Confirmed"}</td>
              </tr>
            ))
            }
          </tbody>
        </table>

      </div>
    </main>
  )
}

export default Vote;