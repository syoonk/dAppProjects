import React from 'react';

function RegisterUser({contracts, accounts}) {
  const submitRegisterUserHandle = async (_e) => {
    _e.preventDefault();
    
    const phoneNumber = parseInt(_e.target.elements[1].value);
    const homeAddress = String(_e.target.elements[2].value);
    const publicKey = _e.target.elements[3].value;

    console.log(phoneNumber, homeAddress, publicKey);

    if(!homeAddress) {
      alert("home address cannot be empty");

      return ;
    }

    try {
      await contracts.user.methods
        .registerUser(phoneNumber, homeAddress, publicKey)
        .send({from: accounts[0]});
      
      window.location.reload();
    } catch {
      alert("Somethings wrong!!");
    }
  }
  
  return (
    <div className="container">
      <form className="center-block" onSubmit={e => submitRegisterUserHandle(e)}>
        <fieldset>
          <legend>Register User</legend>
          <div className="form-group">
            <label>Phone Number</label>
            <input
              id="inputPhoneNumber"
              type="number"
              className="form-control"
              placeholder="phone number"
            ></input>
          </div>
          <div className="form-group">
            <label>Home Address</label>
            <select className="form-control">
              <option style={{fontWeight: "bold"}} value="">Choose Address</option>
              <option value="area1">Area1</option>
              <option value="area2">Area2</option>
            </select>
          </div>
          <div className="form-group">
            <label>Public Key</label>
            <input
              id="inputPublicKey"
              type="string"
              className="form-control"
              placeholder="public key"
            ></input>
          </div>
          <button className="btn btn-outline-primary" type="submit">register</button>
        </fieldset>
      </form>
    </div> 
  )
}

export default RegisterUser;