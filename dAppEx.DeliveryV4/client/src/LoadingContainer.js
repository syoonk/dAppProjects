import React, { useEffect, useState } from 'react';
import Web3 from 'web3';

import App from "./App.js";
import Admins from "./contracts/Admins.json";
import Kng from "./contracts/Kng.json";
import Krw from "./contracts/Krw.json";
import Wallet from "./contracts/Wallet.json";
import AppDatas from "./contracts/AppDatas.json";
import DeliveryAppOrder from "./contracts/DeliveryAppOrder.json";
import DeliveryAppRider from "./contracts/DeliveryAppRider.json";
import DeliveryAppReview from "./contracts/DeliveryAppReview.json";
import DeliveryAppShop from "./contracts/DeliveryAppShop.json";
import DeliveryAppUser from "./contracts/DeliveryAppUser.json";
import DeliveryAppVoting from "./contracts/DeliveryAppVoting.json";


function LoadingContainer() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState([]);
  const [contracts, setContracts] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      // Searching for modern dapp browser
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } 
      // Legacy dapp browser
      else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } 
      // Develop console host
      else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
    
      const admins = new web3.eth.Contract(Admins.abi, Admins.networks[networkId] && Admins.networks[networkId].address);
      const kng = new web3.eth.Contract(Kng.abi, Kng.networks[networkId] && Kng.networks[networkId].address);
      const krw = new web3.eth.Contract(Krw.abi, Krw.networks[networkId] && Krw.networks[networkId].address);
      const wallet = new web3.eth.Contract(Wallet.abi, Wallet.networks[networkId] && Wallet.networks[networkId].address); 
      const appdatas = new web3.eth.Contract(AppDatas.abi, AppDatas.networks[networkId] && AppDatas.networks[networkId].address);
      const order = new web3.eth.Contract(DeliveryAppOrder.abi, DeliveryAppOrder.networks[networkId] && DeliveryAppOrder.networks[networkId].address);
      const rider = new web3.eth.Contract(DeliveryAppRider.abi, DeliveryAppRider.networks[networkId] && DeliveryAppRider.networks[networkId].address);
      const review = new web3.eth.Contract(DeliveryAppReview.abi, DeliveryAppReview.networks[networkId] && DeliveryAppReview.networks[networkId].address);
      const shop = new web3.eth.Contract(DeliveryAppShop.abi, DeliveryAppShop.networks[networkId] && DeliveryAppShop.networks[networkId].address);
      const user = new web3.eth.Contract(DeliveryAppUser.abi, DeliveryAppUser.networks[networkId] && DeliveryAppUser.networks[networkId].address);
      const vote = new web3.eth.Contract(DeliveryAppVoting.abi, DeliveryAppVoting.networks[networkId] && DeliveryAppVoting.networks[networkId].address);
      
      const contracts = { 
        admins,
        kng, krw, wallet,
        appdatas, order, rider, review, shop, user, vote }

      setWeb3(web3);
      setContracts(contracts);
    }
    init();
    
    window.ethereum.on('accountsChanged', accounts => {
      setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof web3 !== 'undefined' 
      && typeof contracts !== 'undefined'
      && accounts.length > 0
    );
  }

  if (!isReady()) {
    return <div>Loading...</div>;
  }

  return (
    <App
      web3={web3}
      accounts={accounts}
      contracts={contracts}
    />
  );

}

export default LoadingContainer;