import React, { useEffect, useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const UserType = ["BUYER", "RIDER", "SHOP"];
// Sample Menu. (Menu Id, Price)
const SampleMenu = [
  [1, 10000],
  [2, 8000],
  [3, 12000],
  [4, 15000]
]

function Order({contracts, userInfo, accounts}) {
  const [shopList, setShopList] = useState([]);
  const [openReview, setOpenReview] = useState({open: false, shopId: 0, reviews: []});
  const [openMenu, setOpenMenu] = useState({open: false, shopId: 0});
  const [reviewDetails, setReviewDetails] = useState([]);

  const getShopList = async () => {
    const nextShopId = await contracts.shop.methods
      .getNextShopId()
      .call();
    
    let shopListTemp = [];
    for(let i = 1; i < nextShopId; i++) {
      const activation = await contracts.shop.methods
      .getShopActivation(i)
      .call();
      if(!activation) continue;
      const shopOwner = await contracts.shop.methods
      .getShopOwner(i)
      .call();
      if(shopOwner.toLowerCase() === accounts[0].toLowerCase()) continue;

      const phoneNumber = await contracts.shop.methods
        .getShopPhoneNumber(i)
        .call();
      const reviews = await contracts.shop.methods
        .getShopReviews(i)
        .call();
      const shopAddress = await contracts.shop.methods
        .getShopAddress(i)
        .call();
      
      shopListTemp.push({id: i, phoneNumber, reviews, shopAddress});
    }

    setShopList(shopListTemp);
  }

  const clickOpenReviewHandle = async (_id, _reviews) => { 
    let reviewsTemp = [];
    for(let i = 0; i < _reviews.length; i++) {
      const reviewer = await contracts.review.methods
        .getReviewFromUserType(_reviews[i])
        .call();
      const stars = await contracts.review.methods
        .getReviewStars(_reviews[i])
        .call();
      
      reviewsTemp.push({reviewer, stars});
    }

    setOpenReview({open: true, shopId: _id, reviews: reviewsTemp});
  }

  const clickCloseReviewHandle = () => { setOpenReview({open: false, shopId: 0, reviews: []}); }

  const clickOpenMenuHandle = (_id) => { setOpenMenu({open: true, shopId: _id}); }

  const clickCloseMenuHandle = () => { setOpenMenu({open: false, shopId: 0}); }

  const clickOrderHandle = async () => {
    let amounts = [
      Number(document.getElementById("inputAmount1").value),
      Number(document.getElementById("inputAmount2").value),
      Number(document.getElementById("inputAmount3").value),
      Number(document.getElementById("inputAmount4").value) ]
    if(amounts[0] < 0) amounts[0] = 0;
    if(amounts[1] < 0) amounts[1] = 0;
    if(amounts[2] < 0) amounts[2] = 0;
    if(amounts[3] < 0) amounts[3] = 0;

    let goodsIds = [];
    for(let i = 0; i < amounts[0]; i++) { goodsIds.push(1); }
    for(let i = 0; i < amounts[1]; i++) { goodsIds.push(2); }
    for(let i = 0; i < amounts[2]; i++) { goodsIds.push(3); }
    for(let i = 0; i < amounts[3]; i++) { goodsIds.push(4); }
    
    const totalPrice = amounts.reduce((total, amount, idx) => {
      return total + amount * SampleMenu[idx][1];
    }, 0);

    console.log(openMenu.shopId, amounts[0], amounts[1], amounts[2], amounts[3], goodsIds, totalPrice);

    try {
      await contracts.order.methods
        .placeOrder(openMenu.shopId, goodsIds, totalPrice)
        .send({from: accounts[0]});
    } catch(e) {
      alert(e);
    }

    clickCloseMenuHandle();
  }

  useEffect(() => {
    const init = async () => {
      await getShopList();
    }

    init();
  }, [accounts])
  
  return (
    <main className="container-fluid row mt-md-3">

      <div className="col-sm-3 first-col">
        <div className="card border-light" style={{maxWidth: "30rem"}}>
          <div className="card-header">User Info</div>
          <div>
            <table className="table">
              <tbody>
                <tr>
                  <th scope="row">Address</th>
                  <td>{String(accounts[0]).substr(0, 10)}...</td>
                </tr>
                <tr>
                  <th scope="row">ID</th>
                  <td>{userInfo.infos.id}</td>
                </tr>
                <tr>
                  <th scope="row">Home</th>
                  <td>{userInfo.infos.homeAddress}</td>
                </tr>
                <tr>
                  <th scope="row">Reputation</th>
                  <td>{userInfo.infos.reputation}</td>
                </tr>
                <tr>
                  <th scope="row">Rank</th>
                  <td>{userInfo.infos.rank}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="card border-light" style={{maxWidth: "20rem"}}>
            <div className="card-header">Wallet Balances</div>
            <div>
              <table className="table">
                <tbody>
                  <tr>
                    <th scope="row">KRW</th>
                    <td>{userInfo.balances.KRW}</td>
                  </tr>
                  <tr>
                    <th scope="row">KNG</th>
                    <td>{userInfo.balances.KNG}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div className="col-md-8" style={{textAlign: "center"}}>
        <legend>Shop List</legend>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Shop ID</th>
              <th scope="col">Phone Number</th>
              <th scope="col">Shop Location</th>
            </tr>
          </thead>
          <tbody>
            {
            shopList.map((shop, idx) => (
              <tr key={idx}>
                <td>{shop.id}</td>
                <td>{shop.phoneNumber}</td>
                <td>{shop.shopAddress}</td>
                <td><button className="btn btn-primary btn-sm" onClick={() => clickOpenReviewHandle(shop.id, shop.reviews)}>See Review</button></td>
                <td><button className="btn btn-primary btn-sm" onClick={() => clickOpenMenuHandle(shop.id)}>Menu</button></td>
              </tr> ))
            }
          </tbody>
        </table>
        <Modal isOpen={openReview.open}>
          <ModalHeader>Reviews</ModalHeader>
          <ModalBody>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Reviewer</th>
                  <th scope="col">Score</th> 
                </tr>
              </thead>
              <tbody>
                {
                  openReview.reviews.map((review, idx) => (
                  <tr key={idx}>
                    <td>{UserType[review.reviewer]}</td>
                    <td>{review.stars}</td>
                    <td><button className="btn btn-primary btn-sm">Click to Detail</button></td> 
                  </tr> ))
                }
              </tbody>
            </table>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-secondary" onClick={clickCloseReviewHandle}>Close</button>
          </ModalFooter>
        </Modal>
        <Modal isOpen={openMenu.open}>
          <ModalHeader>Menu</ModalHeader>
          <ModalBody>
            <table className="table table-hover" style={{textAlign: "center"}}>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Price</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Food1</td>
                  <td>{SampleMenu[0][1]}</td>
                  <td><input 
                    id="inputAmount1"
                    type="number" 
                    className="form-control"
                    min="0"
                    placeholder="amount"
                  ></input></td>
                </tr>
                <tr>
                  <td>Food2</td>
                  <td>{SampleMenu[1][1]}</td>
                  <td><input 
                    id="inputAmount2"
                    type="number" 
                    className="form-control"
                    min="0"
                    placeholder="amount"
                  ></input></td>
                </tr>
                <tr>
                  <td>Food3</td>
                  <td>{SampleMenu[2][1]}</td>
                  <td><input 
                    id="inputAmount3"
                    type="number" 
                    className="form-control"
                    min="0"
                    placeholder="amount"
                  ></input></td>
                </tr>
                <tr>
                  <td>Food4</td>
                  <td>{SampleMenu[3][1]}</td>
                  <td><input 
                    id="inputAmount4"
                    type="number" 
                    className="form-control"
                    min="0"
                    placeholder="amount"
                  ></input></td>
                </tr>
              </tbody>
            </table>
          </ModalBody>
          <ModalFooter>
            <button className="btn btn-primary" onClick={clickOrderHandle}>Order</button>
            <button className="btn btn-secondary" onClick={clickCloseMenuHandle}>Close</button>
          </ModalFooter>
        </Modal>
      </div>
      
    </main>
  )
}

export default Order;
