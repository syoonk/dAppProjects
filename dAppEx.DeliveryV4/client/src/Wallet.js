import React, { useEffect, useState } from 'react';

function Wallet({accounts, contracts, web3, setBalance}) {
  const [balances, setBalances] = useState({
    coinKrw: 0,
    coinKng: 0,
    walletKrw: 0,
    walletKng: 0});
  const [owner, setOwner] = useState({address: undefined, walletKrwBalance: 0});

  const getBalances = async (_account) => {
    const krwCoinBalance = await contracts.krw.methods
      .balanceOf(_account)
      .call();
    const kngCoinBalance = await contracts.kng.methods
      .balanceOf(_account)
      .call();
    const krwWalletBalance = await contracts.wallet.methods
      .getBalance(_account, web3.utils.fromAscii("KRW"))
      .call();
    const kngWalletBalance = await contracts.wallet.methods
      .getBalance(_account, web3.utils.fromAscii("KNG"))
      .call();

    return {krwCoinBalance, kngCoinBalance, krwWalletBalance, kngWalletBalance};
  }

  const getOwner = async () => {
    const ownerAddress = await contracts.wallet.methods
      .getOwner()
      .call();
    const walletKrwBalance = await contracts.wallet.methods
      .getBalance(contracts.wallet.options.address, web3.utils.fromAscii("KRW"))
      .call();
    
    setOwner({address: ownerAddress.toLowerCase(), walletKrwBalance});
  }

  const submitDepositHandle = async (_e) => {
    _e.preventDefault();
    
    const ticker = web3.utils.fromAscii(_e.target.elements[1].value);
    const amount = parseInt(_e.target.elements[2].value);

    await contracts.wallet.methods
      .deposit(amount, ticker)
      .send({from: accounts[0]});
    
    const balancesCopy = await getBalances(accounts[0]);
    
    setBalances({
      coinKrw: balancesCopy.krwCoinBalance,
      coinKng: balancesCopy.kngCoinBalance,
      walletKrw: balancesCopy.krwWalletBalance,
      walletKng: balancesCopy.kngWalletBalance });
    
    setBalance(accounts[0]);

    const walletKrwBalance = await contracts.wallet.methods
    .getBalance(contracts.wallet.options.address, web3.utils.fromAscii("KRW"))
    .call();
  
    setOwner({...owner, walletKrwBalance});
  }

  const submitWithdrawHandle = async (_e) => {
    _e.preventDefault();

    const ticker = web3.utils.fromAscii(_e.target.elements[1].value);
    const amount = parseInt(_e.target.elements[2].value);

    await contracts.wallet.methods
      .withdraw(amount, ticker)
      .send({from: accounts[0]});

    const balancesCopy = await getBalances(accounts[0]);
    
    setBalances({
      coinKrw: balancesCopy.krwCoinBalance,
      coinKng: balancesCopy.kngCoinBalance,
      walletKrw: balancesCopy.krwWalletBalance,
      walletKng: balancesCopy.kngWalletBalance });

    setBalance(accounts[0]);
  }

  useEffect(() => {
    const init = async () => {
      const balancesCopy = await getBalances(accounts[0]);
    
      setBalances({
        coinKrw: balancesCopy.krwCoinBalance,
        coinKng: balancesCopy.kngCoinBalance,
        walletKrw: balancesCopy.krwWalletBalance,
        walletKng: balancesCopy.kngWalletBalance });
        
      await getOwner();
    }
    init();
  },[accounts]);

  
  return (
    <main className="containter-fluid row mt-md-3">
      
      <div className="col-sm-3 first-col">
        <div className="card border-light" style={{maxWidth: "20rem"}}>
          <div className="card-header">Wallet</div>
          <div className="card-body">
            <ul>
              <li>
                KRW: {balances.walletKrw}
              </li>
              <li>
                KNG: {balances.walletKng}
              </li>
            </ul>
          </div>  
        </div>
        <div className="card border-light" style={{maxWidth: "20rem"}}>
          <div className="card-header">Coin</div>
          <div className="card-body">
            <ul>
              <li>
                KRW: {balances.coinKrw}
              </li>
              <li>
                KNG: {balances.coinKng}
              </li>
            </ul>
          </div>
        </div>
        {
        owner.address == accounts[0].toLowerCase() ? (
          <div className="card border-light" style={{maxWidth: "20rem"}}>
            <div className="card-header">Contract KRW (Only Owner)</div>
            <div className="card-body">
              <ul>
                <li>
                  KRW: {owner.walletKrwBalance}
                </li>
              </ul>
            </div>
          </div>          
        ) : (null)
        }
      </div>

      <div className="ml-md-3">
        <form className="mb-md-5" onSubmit={e => submitDepositHandle(e)}>
          <fieldset>
            <legend>Deposit</legend>
            <div className="form-group">
              <label>Token</label>
              <select id="tickerDeposit" className="form-control" name="Ticker">
                <option style={{fontWeight: "bold"}} value="">Ticker</option>
                <option value="KRW">KRW</option>
                <option value="kNG">KNG</option>
              </select>
            </div>
            <div className="form-group">
              <label>Amount</label>
              <input
                id="amountDeposit"
                type="number"
                className="form-control"
                placeholder="Amount"
              ></input>
            </div>
            <button className="btn btn-outline-primary">Submit</button>
          </fieldset>
        </form>

        <form className="mb-md-5" onSubmit={e => submitWithdrawHandle(e)}>
          <fieldset>
            <legend>Withdraw</legend>
            <div className="form-group">
              <label>Tokne</label>
              <select className="form-control" name="Ticker">
                <option style={{fontWeight: "bold"}} value="">Ticker</option>
                <option value="KRW">KRW</option>
                <option value="KNG">KNG</option>
              </select>
            </div>
            <div className="form-group">
              <label>Amount</label>
              <input 
                type="number"
                className="form-control"
                placeholder="Amount"
              ></input>
            </div>
            <button className="btn btn-outline-primary">Submit</button>
          </fieldset>
        </form>

        <form>
          <fieldset>
            <legend>Transfer</legend>
            <div className="form-group">
              <label>Token</label>
              <select className="form-control" style={{width: "20rem"}} name="Ticker">
                <option style={{fontWeight: "bold"}} value="">Ticker</option>
                <option value="KRW">KRW</option>
                <option value="KNG">KNG</option>
              </select>
            </div>
            <div className="form-group">
              <label>To</label>
              <input
                type="string"
                className="form-control"
                placeholder="Address"
              ></input>
            </div>
            <div className="form-group">
              <label>Amount</label>
              <input 
                type="number"
                className="form-control"
                placeholder="Amount"
              ></input>
            </div>
            <button className="btn btn-outline-primary">Submit</button>
          </fieldset>
        </form>
      </div>
    </main>
  )
}

export default Wallet;