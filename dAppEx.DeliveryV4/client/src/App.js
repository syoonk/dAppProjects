import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Link, NavLink } from 'react-router-dom';

import RegisterUser from "./RegisterUser.js";
import Home from "./Home.js";
import MyPage from "./MyPage.js";
import Order from "./Order.js";
import Shop from "./Shop.js";
import Rider from "./Rider.js";
import Vote from "./Vote.js";
import Wallet from "./Wallet.js";

function App({web3, accounts, contracts}) {
  const [registered, setRegistered] = useState(false);
  const [user, setUser] = useState({
    accounts: undefined,
    balances: {
      KRW: 0,
      KNG: 0 },
    infos: {
      id: 0,
      homeAddress: "",
      reputation: 0,
      rank: undefined      
    }
  });

  const getBalance = async (_accounts) => {
    const krwWalletBalance = await contracts.wallet.methods
      .getBalance(_accounts, web3.utils.fromAscii("KRW"))
      .call();
    const kngWalletBalance = await contracts.wallet.methods
      .getBalance(_accounts, web3.utils.fromAscii("KNG"))
      .call();
    
    return { KRW: krwWalletBalance, KNG: kngWalletBalance };
  }

  /**
   * @notice This function is used by Wallet for refreshing wallet balance in userInfo
   */
  const setBalance = async (_accounts) => {
    const balances = await getBalance(_accounts);
    setUser({...user, balances});
  }
    
  useEffect(() => {
    const init = async () => {
      const balances = await getBalance(accounts[0]);
      let userId;
      try {
        userId = await contracts.user.methods
          .getUserId(accounts[0])
          .call({from: accounts[0]});
        
        if(userId != 0) setRegistered(true);
      } catch {
        setRegistered(false);

        return;
      }
      const userHome = await contracts.user.methods
        .getUserHomeAddress(accounts[0])
        .call({from: accounts[0]});
      const userRepu = await contracts.user.methods
        .getUserReputation(accounts[0])
        .call({from: accounts[0]});
      const userRank = await contracts.user.methods
        .getUserRank(accounts[0])
        .call({from: accounts[0]});
      const infos = {
        id: userId,
        homeAddress: userHome, 
        reputation: userRepu,
        rank: userRank};

      setUser({accounts: accounts[0], balances, infos});
    }
    init();
  }, [accounts]);
  
  return (
    <div>
      {
      !registered ? (
        <RegisterUser
          contracts={{user: contracts.user}}
          accounts={accounts}
        ></RegisterUser> 
      ) : (
      <BrowserRouter>
      <div style={{paddingBottom: "50px"}}>
        <nav className="navbar navbar-expand-lg navbar-light fixed-top bg-light">
          <Link className="navbar-brand" to="/">Home</Link>
          <div className="collapse navbar-collapse" id="navbarColor03">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/mypage">My Page</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/Order">Order</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/Shop">Shop</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/Rider">Rider</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/Vote">Vote</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" activeClassName="nav-link active" to="/Wallet">Wallet</NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <Route
        path="/"
        exact
        render={() => <Home 
          contracts={{user: contracts.user}} 
          userInfo={user}
          accounts={accounts}></Home>}
      ></Route>
      <Route
        path="/mypage"
        exact
        render={() => <MyPage
          contracts={{
            user: contracts.user, 
            order: contracts.order,
            shop: contracts.shop,
            review: contracts.review}} 
          userInfo={user}
          accounts={accounts}></MyPage>}
      ></Route>
      <Route
        path="/order"
        exact
        render={() => <Order
          contracts={{
            shop: contracts.shop,
            order: contracts.order,
            user: contracts.user,
            review: contracts.review }}
          accounts={accounts}
          userInfo={user}></Order>}
      ></Route>
      <Route
        path="/shop"
        exact
        render={() => <Shop 
          contracts={{
            shop: contracts.shop, 
            order: contracts.order,
            user: contracts.user,
            review: contracts.review }}
          accounts={accounts}></Shop>}
      ></Route>
      <Route
        path="/rider"
        exact
        render={() => <Rider
          contracts={{
            rider: contracts.rider,
            order: contracts.order,
            user: contracts.user,
            review: contracts.review,
            shop: contracts.shop }}
          accounts={accounts}></Rider>}
      ></Route>
      <Route
        path="/vote"
        exact
        render={() => <Vote
          contracts={{vote: contracts.vote}}
          accounts={accounts}
          web3={web3}
        ></Vote>}
      ></Route>
      <Route
        path="/wallet"
        exact
        render={() => (
          <Wallet
            accounts={accounts}
            contracts={{
              kng: contracts.kng,
              krw: contracts.krw,
              wallet: contracts.wallet }}
            web3={web3}
            setBalance={setBalance}
          ></Wallet>
        )}
      ></Route>
    </BrowserRouter> )
    }
  </div>
  );
}

export default App;
