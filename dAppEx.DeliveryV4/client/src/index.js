import React from 'react';
import ReactDOM from 'react-dom';
import LoadingContainer from './LoadingContainer.js';
import 'bootswatch/dist/minty/bootstrap.min.css';

ReactDOM.render(
  <LoadingContainer></LoadingContainer>,
  document.getElementById('root')
);