import React, { useEffect, useState } from "react";
import EventContract from "./contracts/EventContract.json";
import Web3 from "web3";

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [contract, setContract] = useState(undefined);
  const [events, setEvents] = useState([]);
  const [userTickets, setUserTickets] = useState([]);

  useEffect(() => {
    const init = async () => {
      let web3;
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = EventContract.networks[networkId];
      const contract = new web3.eth.Contract(
        EventContract.abi,
        deployedNetwork && deployedNetwork.address
      );

      setWeb3(web3);
      setContract(contract);
    };
    init();

    // window.ethereum.on("accountsChanged", accounts => {
    //   setAccounts(accounts);
    // });
  }, []);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined"
    );
  };

  useEffect(() => {
    if (isReady()) {
      updateEvents();
      updateUserTickets();
    }
  }, [accounts, contract, web3]);

  async function updateEvents() {
    const nextId = await contract.methods.nextId().call();
    const eventsCopy = [];
    for (let i = 0; i < nextId; i++) {
      eventsCopy.push(await contract.methods.events(i).call());
    }
    setEvents(eventsCopy);
  }

  async function updateUserTickets() {
    const nextId = await contract.methods.nextId().call();
    const ticketsCopy = [];
    for (let i = 0; i < nextId; i++) {
      ticketsCopy.push(await contract.methods.tickets(accounts[0], i).call());
    }

    setUserTickets(ticketsCopy);
  }

  async function submitTransferTicketHandle(e) {
    e.preventDefault();
    const id = e.target.elements[0].value;
    const quantity = e.target.elements[1].value;
    const to = e.target.elements[2].value;

    await contract.methods
      .transferTicket(id, quantity, to)
      .send({ from: accounts[0] });

    updateUserTickets();

    window.location.reload();
  }

  async function submitBuyTicketHandle(e, event) {
    e.preventDefault();
    const quantity = Number(e.target.elements[0].value);
    const price = Number(event.price);
    const id = Number(event.id);
    console.log("eventId", event.id);
    console.log("ididid", id);
    const totalPrice = price * quantity;

    await contract.methods
      .buyTicket(id, quantity)
      .send({ from: accounts[0], value: totalPrice });

    updateEvents();
    updateUserTickets();

    window.location.reload();
  }

  async function submitCreateEventHandle(e) {
    e.preventDefault();
    const name = e.target.elements[0].value;
    const date = Math.floor(
      new Date(e.target.elements[1].value).getTime() / 1000
    );
    const price = e.target.elements[2].value;
    const ticketCounts = e.target.elements[3].value;

    await contract.methods
      .createEvent(name, date, price, ticketCounts)
      .send({ from: accounts[0] });

    updateEvents();
    updateUserTickets();

    window.location.reload();
  }

  function isFinished(event) {
    const now = new Date().getTime();
    const eventEnd = new Date(parseInt(event.date) * 1000).getTime();
    return eventEnd > now ? false : true;
  }

  if (!isReady()) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">Event Organization</h1>

      <p> Current User: {accounts[0]} </p>
      <p> Tickets Owned : </p>

      <ul>
        {events.map(e => (
          <li>
            ID: {e.id} , Quantity: {userTickets[e.id]}
          </li>
        ))}
      </ul>

      <div className="row">
        <div className="col-sm-12">
          <h2>Create event</h2>
          <form onSubmit={e => submitCreateEventHandle(e)}>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input type="text" className="form-control" id="name" />
            </div>
            <div className="form-group">
              <label htmlFor="date">Date</label>
              <input type="date" className="form-control" id="date" />
            </div>
            <div className="form-group">
              <label htmlFor="price">Price</label>
              <input type="text" className="form-control" id="price" />
            </div>
            <div className="form-group">
              <label htmlFor="ticketCount">Ticket count</label>
              <input type="text" className="form-control" id="ticketCount" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Transfer tickets</h2>
          <form onSubmit={e => submitTransferTicketHandle(e)}>
            <div className="form-group">
              <label htmlFor="eventId">Event Id</label>
              <input type="text" className="form-control" id="eventId" />
            </div>
            <div className="form-group">
              <label htmlFor="amount">Amount</label>
              <input type="text" className="form-control" id="amount" />
            </div>
            <div className="form-group">
              <label htmlFor="to">To</label>
              <input type="text" className="form-control" id="to" />
            </div>
            <button type="submit" className="btn btn-primary">
              Submit
            </button>
          </form>
        </div>
      </div>

      <hr />

      <div className="row">
        <div className="col-sm-12">
          <h2>Events</h2>
          <table className="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>Admin</th>
                <th>Name</th>
                <th>Date</th>
                <th>Price</th>
                <th>Ticket remaining</th>
                <th>Total tickets</th>
                <th>Buy</th>
              </tr>
            </thead>
            <tbody>
              {events.map(event => (
                <tr key={event.id + event.date}>
                  <td>{event.id}</td>
                  <td>{event.admin}</td>
                  <td>{event.name}</td>
                  <td>
                    {new Date(parseInt(event.date) * 1000).toLocaleString()}
                  </td>
                  <td>{event.price}</td>
                  <td>{event.ticketRemaining}</td>
                  <td>{event.ticketCount}</td>
                  <td>
                    {isFinished(event) ? (
                      "Event finished"
                    ) : (
                      <form onSubmit={e => submitBuyTicketHandle(e, event)}>
                        <div className="form-group">
                          <label htmlFor="amount">Amount</label>
                          <input
                            type="text"
                            className="form-control"
                            id="amount"
                          />
                        </div>
                        <button type="submit" className="btn btn-primary">
                          Submit
                        </button>
                      </form>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default App;
