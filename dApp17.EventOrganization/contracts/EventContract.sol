pragma solidity >= 0.5.0 < 0.7.0;

contract EventContract {
    struct Event {
      uint id;
        address admin;
        string name;
        uint date;
        uint price;
        uint ticketCount;
        uint ticketRemaining;
    }
    
    mapping(uint => Event) public events;  // Event id to Event
     // address has this num of tickets(uint) of this event(uint)
    mapping(address => mapping(uint => uint)) public tickets;
    uint public nextId;
    
    function createEvent(
        string calldata _name,
        uint _date,
        uint _price,
        uint _ticketCount
        ) external {
        require(_date > now, "event can only be organized in the future");
        require(_ticketCount > 0, "can only create event at least 1 ticket");
        events[nextId] = Event(
            nextId,
            msg.sender,
            _name,
            _date,
            _price,
            _ticketCount,
            _ticketCount
        );
        nextId++;
    }
        
    function buyTicket(uint _id, uint _quantity) 
    payable external eventExist(_id) eventActive(_id) {
        Event storage e = events[_id];
        require(msg.value == (e.price * _quantity), "Not exact amount of ether sent");
        require(e.ticketRemaining >= _quantity, "Not enough tickets remained");
        e.ticketRemaining -= _quantity;
        tickets[msg.sender][_id] += _quantity;
    }
        
    function transferTicket(uint _id, uint _quantity, address _to) 
    external eventExist(_id) eventActive(_id) {
        require(tickets[msg.sender][_id] >= _quantity, "Not enough tickets");
        tickets[msg.sender][_id] -= _quantity;
        tickets[_to][_id] += _quantity;
    }
        
    modifier eventExist(uint _id) {
        require(events[_id].date != 0, "This event is not exist");
        _;
    }
        
    modifier eventActive(uint _id) {
        require(events[_id].date > now, "This event is not active anymore");
        _;
    }
}