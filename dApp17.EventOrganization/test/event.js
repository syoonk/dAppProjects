const { expectRevert, time } = require("@openzeppelin/test-helpers");
const EventContract = artifacts.require("EventContract");

contract("EventContract", (accounts) => {
  let eventContract = null;
  before(async () => {
    eventContract = await EventContract.deployed();
  });

  it("Should NOT create an event if date is before now", async () => {
    const date = (await time.latest()).sub(time.duration.seconds(1000));

    await expectRevert(
      eventContract.createEvent("expiredEvent", date, 100, 10),
      "event can only be organized in the future"
    );
  });

  it("Should NOT create an event if less than 1 ticket", async () => {
    const date = (await time.latest()).add(time.duration.seconds(1000));

    await expectRevert(
      eventContract.createEvent("noTicketEvent", date, 100, 0),
      "can only create event at least 1 ticket"
    );
  });

  it("Should create an event", async () => {
    const date = (await time.latest()).add(time.duration.seconds(1000));
    // event id 0
    await eventContract.createEvent("event0", date, 100, 10);
  });

  it("Should NOT buy a ticket if event does not exist", async () => {
    await expectRevert(
      eventContract.buyTicket(1, 10),
      "This event is not exist"
    );
  });

  context("event created", () => {
    beforeEach(async () => {
      const date = (await time.latest()).add(time.duration.seconds(1000));
      await eventContract.createEvent("event1", date, 5, 2);
    });
    // event id 1
    it("Should NOT buy a ticket if wrong amount of ether sent", async () => {
      await expectRevert(
        eventContract.buyTicket(1, 2, { from: accounts[2], value: 5 }),
        "Not exact amount of ether sent"
      );
    });
    // event id 2
    it("Should NOT buy a ticket if not enough ticket left", async () => {
      await expectRevert(
        eventContract.buyTicket(2, 3, { from: accounts[2], value: 15 }),
        "Not enough tickets remained"
      );
    });
    // event id 3
    it("Should buy tickets", async () => {
      await eventContract.buyTicket(3, 2, { from: accounts[2], value: 10 });
    });
    // event id 4
    it("Should NOT transfer ticket if not enough tickets", async () => {
      await eventContract.buyTicket(4, 2, { from: accounts[3], value: 10 });

      await expectRevert(
        eventContract.transferTicket(4, 3, accounts[4], { from: accounts[3] }),
        "Not enough tickets"
      );
    });
    // event id 5
    it("Should transfer ticket", async () => {
      await eventContract.buyTicket(5, 2, { from: accounts[3], value: 10 });

      await eventContract.transferTicket(5, 2, accounts[4], {
        from: accounts[3]
      });
    });
    // event id 6
    it("Should NOT buy a ticket if event has expired", async () => {
      await time.increase(10e+6 + 1);

      await expectRevert(
        eventContract.buyTicket(6, 2, { from: accounts[3], value: 10 }),
        "This event is not active anymore"
      );
    });
  });
});
