const SplitPayment = artifacts.require("SplitPayment");

contract ("SplitPayment", (accounts) => {
    let splitPayment = null;
    before(async() => {
        splitPayment = await SplitPayment.deployed();
    });

    it("should split payment", async() => {
        const recipients = [accounts[1], accounts[2], accounts[3]];
        const amounts = [40, 50, 60];
        const initialBalances = await Promise.all(recipients.map(recipient => {
            return web3.eth.getBalance(recipient);
        }));

        await splitPayment.send(recipients, amounts, {from: accounts[0], value: 150});

        const finalBalances = await Promise.all(recipients.map(recipient => {
            return web3.eth.getBalance(recipient);
        }));

        recipients.forEach((_item, i) => {
            const initialBalance = web3.utils.toBN(initialBalances[i]);
            const finalBalance = web3.utils.toBN(finalBalances[i]);

            assert(finalBalance.sub(initialBalance).toNumber() === amounts[i]);
        });
    });

    it("should NOT split payment if array lenght not match", async() => {
        const recipients = [accounts[1], accounts[2], accounts[3]];
        const amounts = [40, 50];

        try {
            await splitPayment.send(recipients, amounts, { from: accounts[0], value: 150 });
        } catch(e) {
            assert(e.message.includes('# of address and # of amount are not match'));
            return;
        };

        assert(false);
    });

    it("should NOT spilt payment if caller is not owner", async () => {
        const recipients = [accounts[1], accounts[2], accounts[3]];
        const amounts = [40, 50, 60];

        try {
            await splitPayment.send(recipients, amounts, { from: accounts[6], value: 150 });
        } catch (e) {
            assert(e.message.includes('only owner can call this function'));
            return;
        };

        assert(false);        
    });
});