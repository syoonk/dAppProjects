pragma solidity ^0.5.0;

contract SplitPayment {
    address public owner;
    constructor(address _owner) public {
        owner = _owner;
    }

    function send(address payable[] memory to, uint[] memory amount) public payable onlyOwner() {
        require(to.length == amount.length, "# of address and # of amount are not match");

        for(uint i = 0; i < to.length; i++) {
            to[i].transfer(amount[i]);
        }

    }
    modifier onlyOwner() {
        require(msg.sender == owner, "only owner can call this function");
        _;
    }
}