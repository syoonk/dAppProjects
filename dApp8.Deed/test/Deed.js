const Deed = artifacts.require("Deed");

contract("Deed", (accounts) => {
    let deed = null;
    before(async() => {
        deed = await Deed.deployed();
    });

    it("should withdraw", async() => {
        const initialBalance = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));
        const initialDeposit = web3.utils.toBN(await web3.eth.getBalance(deed.address));
        await new Promise(resolve => setTimeout(resolve, 5000));  // no need reject since it would be always worked.
        await deed.withdraw({from: accounts[0]});
        const finalBalance = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));
    
        assert(finalBalance.sub(initialBalance).toNumber() === initialDeposit.toNumber());
    });

    it("should NOT withdraw if it is too early", async() => {
        // it already awaited 5s above so we need fresh contract here.
        const deed = await Deed.new(accounts[0], accounts[1], 5, {value: 100});
        
        try {
            await deed.withdraw({ from: accounts[0] });
        } catch(e) {
            assert(e.message.includes("too early"));
            return;
        }
        assert(false, e.message);
    });

    it('should NOT withdraw if caller is not lawyer', async() => {
        const deed = await Deed.new(accounts[0], accounts[1], 5, { value: 100 });

        try {
            await new Promise(resolve => setTimeout(resolve, 5000));
            await deed.withdraw({from: accounts[3]});
        } catch(e) {
            assert(e.message.includes("lawyer only"));
            return;
        }
        assert(false, e.message);
    })
})