const Deed = artifacts.require("Deed");

module.exports = function(deployer, _network, _accounts) {
    // accounts[0]: lawyer, accounts[1]: beneficiary, 5: fromNow, value: deposit amount wei upon deploymnet 
    deployer.deploy(Deed, _accounts[0], _accounts[1], 5, {value: 100});
}