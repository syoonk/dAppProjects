var contractABI = [{
    "constant": true,
    "inputs": [],
    "name": "hello",
    "outputs": [
        {
            "internalType": "string",
            "name": "",
            "type": "string"
        }
    ],
    "payable": false,
    "stateMutability": "pure",
    "type": "function"
}];
var contractAddress = "0xEaffD30D652Bdd49d1f094CBfA861e260D26dFd8";

var web3 = new Web3('http://localhost:9545');
var helloWorld = new web3.eth.Contract(contractABI, contractAddress);

// Browser execute code rightaway though, 
// possibly html page(DOM) has not finished loading 
// and We have to finish until html page is fully loaded
// So we wrap up the code with callback 
document.addEventListener('DOMContentLoaded', () => { 
    helloWorld.methods.hello().call()
        .then(result => {
           document.getElementById('hello').innerHTML = result;
        });
})