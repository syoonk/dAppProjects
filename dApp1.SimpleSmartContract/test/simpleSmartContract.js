const SimpleSmartContract = artifacts.require("./SimpleSmartContract.sol");

contract('SimpleSmartContract', () => {
	it('should deploy smart contract properly', async () => {
		const simpleSmartContract = await SimpleSmartContract.deployed();
		assert(simpleSmartContract.address != '');	
	});
});
