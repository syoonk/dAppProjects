const Strings = artifacts.require("Strings");

contract("Strings", () => {
  let strings = null;
  before(async () => {
    strings = await Strings.deployed();
  });

  it("should return the length of a string", async () => {
    const length = await strings.length(`abc`);
    assert.equal(length.toNumber(), 3);
  });

  it("should concatenate two strings", async () => {
    const concatStrings = await strings.concatenate(`abc`, `def`);
    assert.equal(concatStrings, `abcdef`);
  });

});
