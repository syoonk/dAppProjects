pragma solidity ^0.5.0;

contract DeedMultiPayout {
    address public lawyer;
    address payable public beneficiary;
    uint public earliest;
    uint public amount;
    uint constant public PAYOUTS = 4;
    uint constant public INTERVAL = 2;
    uint public paidPayouts;

    constructor(address _lawyer, address payable _beneficiary, uint _fromNow) public payable {
        lawyer = _lawyer;
        beneficiary = _beneficiary;
        earliest = now + _fromNow;
        amount = msg.value / PAYOUTS;
    }

    function withdraw() public {
        require(msg.sender == lawyer, "lawyer only");
        require(now >= earliest, "too early");
        require(paidPayouts < PAYOUTS, 'no payouts left');

        uint eligiblePayouts = (now - earliest) / INTERVAL;
        uint duePayouts = eligiblePayouts - paidPayouts;
        duePayouts = duePayouts + paidPayouts > PAYOUTS ? PAYOUTS - paidPayouts : duePayouts;
        paidPayouts += duePayouts;
        beneficiary.transfer(duePayouts * amount);
    }
}