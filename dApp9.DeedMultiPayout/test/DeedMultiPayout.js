/* 

This test needs to be modified for actural test

*/

const DeedMultiPayout = artifacts.require("DeedMultiPayout");

contract("DeedMultiPayout", (accounts) => {
    let deedMultiPayout = null;
    before(async () => {
        deedMultiPayout = await DeedMultiPayout.deployed();
    });

    it('should withdraw for all payouts (1)', async() => {
        const deedMultiPayout = await DeedMultiPayout.new(accounts[0], accounts[1], 1,  { from: accounts[0], value: 100 });
        
        for(let i = 0; i < 4; i++) {
            let balanceTest = web3.utils.toBN(await web3.eth.getBalance(accounts[0]));
            let balanceBefore = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));
            await new Promise(resolve => setTimeout(resolve, 1000));
            await deedMultiPayout.withdraw({ from: accounts[1] });
            let balanceAfter = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));

            // await new Promise(resolve => console.log(resolve, `before: ${balanceBefore}`));
            // await new Promise(resolve => console.log(resolve, `After: ${balanceAfter}`));

            new Promise(() => console.log(`i::: ${i}, test: ${balanceTest}`));
            new Promise(() => console.log(`before: ${balanceBefore}`));
            new Promise(() => console.log(`After: ${balanceAfter}`));

            assert.equal(balanceAfter.sub(balanceBefore).toNumber(), 25, `sibal`);
        }
    });

    
    it('should withdraw for all payouts (2)', async () => {
        const deedMultiPayout = await DeedMultiPayout.new(accounts[0], accounts[1], 1, {value: 100});
        
        for (let i = 0; i < 2; i++) {
            const balanceBefore = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));
            await new Promise(resolve => setTimeout(resolve, 2000));
            await deedMultiPayout.withdraw({ from: accounts[1] });
            const balanceAfter = web3.utils.toBN(await web3.eth.getBalance(accounts[1]));
 
            assert(balanceAfter.sub(balanceBefore).toNumber() === 50);
        }
    }); 

    it("should NOT withdraw if no payout left", async () => {
        try {
            await deedMultiPayout.withdraw({ from: accounts[1] });
        } catch(e) {
            assert(e.message.includes('no payouts left'));
            assert(e.message);
            return;
        }

        assert(false);
    });

    it("should NOT withdraw if it is too early", async () => {
        // it already awaited 5s above so we need fresh contract here.
        const deedMultiPayout = await DeedMultiPayout.new(accounts[0], accounts[1], 5, { value: 100 });

        try {
            await deedMultiPayout.withdraw({ from: accounts[1] });
        } catch (e) {
            assert(e.message.includes("too early"));
            return;
        }
        assert(false);
    });

    it('should NOT withdraw if caller is not beneficiary', async () => {
        const deedMultiPayout = await DeedMultiPayout.new(accounts[0], accounts[1], 5, { value: 100 });

        try {
            await new Promise(resolve => setTimeout(resolve, 5000));
            await deedMultiPayout.withdraw({ from: accounts[3] });
        } catch (e) {
            assert(e.message.includes("beneficiary only"));
            return;
        }
        assert(false);
    })
})