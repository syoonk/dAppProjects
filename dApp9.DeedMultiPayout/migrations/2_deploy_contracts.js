const DeedMultiPayout = artifacts.require("DeedMultiPayout");

module.exports = (deployer, _network, _accounts) => {
    deployer.deploy(DeedMultiPayout, _accounts[0], _accounts[1], 100, {value: 100});
}