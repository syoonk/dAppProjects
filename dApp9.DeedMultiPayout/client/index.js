import Web3 from 'web3';
import DeedMultiPayout from '../build/contracts/DeedMultiPayout.json';

let web3;
let deedMultiPayout;

const initWeb3 = () => {
  return new Promise((resolve, reject) => {
    if(typeof window.ethereum !== 'undefined') {
      const web3 = new Web3(window.ethereum);
      window.ethereum.enable()
        .then(() => {
          resolve(
            new Web3(window.ethereum)
          );
        })
        .catch(e => {
          reject(e);
        });
      return;
    }
    if(typeof window.web3 !== 'undefined') {
      return resolve(
        new Web3(window.web3.currentProvider)
      );
    }
    resolve(new Web3('http://localhost:9545'));
  });
};

const initContract = async () => {
  const networkId = await web3.eth.net.getId();
  return new web3.eth.Contract(
    DeedMultiPayout.abi, 
    DeedMultiPayout
      .networks[networkId]
      .address
  );
};

const initApp = () => {
  let accounts = [];
  web3.eth.getAccounts()
  .then(_accounts => {
    accounts = _accounts;
  });

  const $lawyer = document.getElementById(`lawyer`);
  $lawyer.innerHTML = `lawyer :: ${deedMultiPayout.methods.lawyer().call()}`;
  const $beneficiary = document.getElementById(`beneficiary`);
  $beneficiary.innerHTML = `beneficiary :: ${deedMultiPayout.methods.beneficiary().call()}`;
  const $accounts0 = document.getElementById(`accounts0`);
  $accounts0.innerHTML = `accounts[0] :: ${accounts[0]}`;
  const $accounts1 = document.getElementById(`accounts1`);
  $accounts1.innerHTML = `accounts[1] :: ${accounts[1]}`;

  const $balance = document.getElementById(`balance`);
  const showBalance = () => {
    web3.eth.getBalance(deedMultiPayout.options.address)
    .then(_balance => {
      $balance.innerHTML = _balance;
    });
  }

  const $paidPayouts = document.getElementById(`paid-payouts`);
  const showPaidPayouts = () => {
    deedMultiPayout.methods.paidPayouts().call()
    .then(_payouts => {
      $paidPayouts.innerHTML = `${_payouts}`;
    }).catch(e => {
      $paidPayouts.innerHTML = `Shit.. Error`;
    });
  }  

  const $earliest = document.getElementById(`earliest`);
  const showNextPayout = () => {
    deedMultiPayout.methods.earliest().call()
    .then(_earliest => {
      const payouts = parseInt(deedMultiPayout.methods.paidPayouts().call());
      const _date = parseInt(_earliest) * 1000 + parseInt(payouts) * 10000;
      $earliest.innerHTML = `${_date},,,${new Date(parseInt(_date)).toLocaleString()}`;
    }).catch(e => {
      $earliest.innerHTML = `error dafadf`;
    });
  }

  const $withdraw = document.getElementById(`withdraw`);
  const $withdrawResult = document.getElementById(`withdraw-result`);

  showBalance();
  showPaidPayouts();
  showNextPayout();

  $withdraw.addEventListener('submit', e => {
    e.preventDefault();
    deedMultiPayout.methods.withdraw().send({ from: accounts[1] })
    .then(() => {
      $withdrawResult.innerHTML = `Yeah successfully transffered`;

      showBalance();
      showPaidPayouts();
      showNextPayout();
    }).catch(e => {
      $withdrawResult.innerHTML = `Error As Usual ,, ${e.message}`;
    });
  });

  showBalance();
  showPaidPayouts();
  showNextPayout();
};

document.addEventListener('DOMContentLoaded', () => {
  initWeb3()
    .then(_web3 => {
      web3 = _web3;
      return initContract();
    })
    .then(_deedMultiPayout => {
      deedMultiPayout = _deedMultiPayout;
      initApp(); 
    })
    .catch(e => console.log(e.message));
});
