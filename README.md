# Dapp Projects
## Tools
**Backend**: Solidity, Truffle  
**Frontend**: React, Web3, NodeJS  

Every App uses Metamask for wallet.
You need to install firefox/chrome Metamask first.

----
## 'Dex' and 'Delivery' Sample page is now available.  
Contracts have been deployed on free gas Private Blockchain built of hyperledger besu.

**Dex**: https://syoonk.gitlab.io/dex  
**Delivery**: https://syoonk.gitlab.io/delivery

Make sure that you are using Metamask.  
1. Set the endpoint  
http://52.231.163.36/jsonrpc

2. Use pre-allocated addresses.  
(0) 3833e91e92c9d57e6ac4b1f295f9a4d01145d5f29852a1363147b93db2335755  
(1) 27688b88b5b1a379aede85e5084e6b0be6ddb7049359db477a23645d4fb1ca76  
(2) 46cd8dbd7fe1522d8d30e2307be849e364edbe624b082f49d6c3c1559f4f746a  
(3) eaccef8c167600e62013d133d7e8ca8fb76c2d2755373e948065fe7ec7143ded  
(4) 0c4803c5510568f8ba32c479ac9233d02b85535fb9fc61fec3f5c8139129d52a  
(5) c432932f724de18547e9a0a1a0d95f6a14cda9859b036588a458f19674fe9295  
(6) e7684c9cf265fe237bdbd529e14c7ef76aa6e6ebfd6ec8d79f8e55bbb8012a35  
(7) 33cd09c253306311273820b08b4f8fb0be09cdbf293b026b25b8ffada12f50a0  
(8) 66728bace03ecfcc93f6fd2af865ae8c7675768c7f19b4f0521710b9c5b98891  
(9) dd8ff6cb8e1f9a509fe3ec17c1d5832d1edfedf1957a3cc5e56d06c12faed2d8  