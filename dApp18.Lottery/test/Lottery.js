const { expectRevert } = require("@openzeppelin/test-helpers");
const Lottery = artifacts.require("Lottery");

const balances = async (addresses) => {
  const balancesToBN = await Promise.all(
    addresses.map(address => 
      web3.eth.getBalance(address)
    )
  );
  return balancesToBN.map(balance => web3.utils.toBN(balance));
};

contract("Lottery", accounts => {
  const admin = accounts[0];
  const gambler1 = accounts[1];
  const gambler2 = accounts[2];
  const gambler3 = accounts[3];
  let lottery = null;
  
  beforeEach(async () => {
    lottery = await Lottery.new(10);
  });

  it("Should NOT create bet if not admin", async () => {
    await expectRevert(
      lottery.createBet(3, 10, { from: gambler3 }),
      "only admin"
    );
  });

  it("Should create a bet", async () => {
    await lottery.createBet(3, 10, { from: admin });
  });

  it("Should NOT create bet if state not idle", async () => {
    await lottery.createBet(3, 10, { from: admin });

    await expectRevert(
      lottery.createBet(3, 10, { from: admin }),
      "current state doesn't allow this"
    );
  });

  it("Should NOT bet if not in state BETTING", async () => {
    await expectRevert(
      lottery.bet({ from: gambler1, value: 10 }),
      "current state doesn't allow this"
    );
  });

  it("Should NOT bet if not sending exact bet amount", async () => {
    await lottery.createBet(3, 10, { from: admin });

    await expectRevert(
      lottery.bet({ from: gambler1, value: 5 }),
      "can only bet exact betsize"
    );
  });

  it("Should bet", async () => {
    const gamblers = [gambler1, gambler2, gambler3];
    const beforeBalances = await balances(gamblers);
    await lottery.createBet(3, web3.utils.toWei("2", "ether"), {
      from: admin
    });
    const betReceipt = await Promise.all(
      gamblers.map(gambler => 
        lottery.bet({
          from: gambler,
          value: web3.utils.toWei("2", "ether"),
          gasPrice: 1
        })
      )
    );
    const afterBalances = await balances(gamblers);
    const expectedPrice = web3.utils.toBN(web3.utils.toWei('3.4', 'ether'));
    const result = gamblers.some((gambler, i) => {
      const gasUsed = web3.utils.toBN(betReceipt[i].receipt.gasUsed);
      return afterBalances[i].sub(beforeBalances[i]).add(gasUsed).eq(expectedPrice);
    });

    assert.equal(true, result);
  });

  it("Should NOT cancel if state is not betting", async () => {
    await expectRevert(
      lottery.cancel({ from: admin }),
      "current state doesn't allow this"
    );
  });

  it("Should NOT cancel if not admin", async () => {
    await lottery.createBet(3, 10, { from: admin });

    await expectRevert(lottery.cancel({ from: gambler2 }), "only admin");
  });

  it("Should cancel", async () => {
    await lottery.createBet(3, 10, {from: admin});

    await lottery.cancel({ from: admin });
  });
});
