import React, { useEffect, useState } from "react";
import Lottery from "./contracts/Lottery.json";
import Web3 from 'web3';

const states = ["IDLE", "BETTING"];

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [contract, setContract] = useState(undefined);
  const [bet, setBet] = useState(undefined);
  const [players, setPlayers] = useState([]);
  const [houseFee, setHouseFee] = useState(undefined);
  const [admin, setAdmin] = useState(undefined);

  useEffect(() => {
    const init = async () => {
      let web3;
      if (window.ethereum) {
        web3 = new Web3(window.ethereum);
        try {
          await window.ethereum.enable();
          const accounts = await window.ethereum.enable();
          setAccounts(accounts);
        } catch (error) {
          throw error;
        }
      } else if (window.web3) {
        web3 = window.web3;
        console.log("Injected web3 detected.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      } else {
        const provider = new Web3.providers.HttpProvider(
          "http://localhost:9545"
        );
        web3 = new Web3(provider);
        console.log("No web3 instance injected, using Local web3.");
        const accounts = await web3.eth.getAccounts();
        setAccounts(accounts);
      }

      const networkId = await web3.eth.net.getId();
      const deployedNetwork = Lottery.networks[networkId];
      const contract = new web3.eth.Contract(
        Lottery.abi,
        deployedNetwork && deployedNetwork.address
      );

      setWeb3(web3);

      const [_houseFee, _state] = await Promise.all([
        contract.methods.houseFee().call(),
        contract.methods.currentState().call()
      ]);

      const adminCopy = await contract.methods.admin().call();

      setAdmin(adminCopy);
      setContract(contract);
      setHouseFee(_houseFee);
      setBet({ state: _state });
    };
    init();

    window.ethereum.on('accountsChanged', accounts => {
    setAccounts(accounts);
    });
  }, []);

  const isReady = () => {
    return (
      typeof contract !== "undefined" &&
      typeof web3 !== "undefined" &&
      typeof accounts !== "undefined" &&
      typeof houseFee !== "undefined"
    );
  };

  const isAdmin = () => {
    return admin.toLowerCase() === accounts[0].toLowerCase();
  }

  useEffect(() => {
    if (isReady()) {
      updateBet();
      updatePlayers();
    }
  }, [accounts, contract, web3]);

  async function updateBet() {
    const _size = await contract.methods.betSize().call();
    const _count = await contract.methods.betCount().call();
    const _state = await contract.methods.currentState().call();
    const betCopy = {size: _size, count: _count, state: _state}; 
   
    setBet(betCopy);
  }

  /////// NEEDS GETTER??? /////
  async function updatePlayers() {
    const playersCopy = await contract.methods.getPlayers().call();
    
    setPlayers(playersCopy);
  }

  async function submitCreateBetHandle(e) {
    e.preventDefault();
    const count = e.target.elements[0].value;
    const size = e.target.elements[1].value;

    await contract.methods.createBet(count, size)
    .send({from: accounts[0]});

    updateBet();
  }

  async function clickCancelHandle() {
    await contract.methods.cancel()
    .send({from: accounts[0]});

    updateBet();
    updatePlayers();
  }

  async function clickBetHandle() {
    await contract.methods.bet()
    .send({
      from: accounts[0],
      value: bet.size
    });

    updatePlayers();
  }

  if (!bet || typeof bet.state === "undefined") {
    return <div>Loading...</div>;
  }

  return (
    <div className="container">
      <h1 className="text-center">Lottery</h1>

      <p>Current User: {accounts[0]}</p>
      <p>Admin: {admin} </p>
      <p>House Fee: {houseFee} </p>
      <p>State: {states[bet.state]}</p>

      { bet.state === 1 ? <>
        <p>Bet size: {bet.size}</p>
        <p>Bet count: {bet.count}</p>
        <div>
          <h2>Players</h2>
          <ul>
            {players.map((player, i) => <li id = {i}> player{i + 1}: {player}</li>)}
          </ul>
      </div> </> : null }

      { !isAdmin() ? (null) : ( bet.state == 0 ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Create bet</h2>
          <form onSubmit={e => submitCreateBetHandle(e)}>
            <div className="form-group">
              <label htmlFor="count">Count</label>
              <input type="text" className="form-control" id="count" />
            </div>
            <div className="form-group">
              <label htmlFor="size">Size</label>
              <input type="text" className="form-control" id="size" />
            </div>
            <button type="submit" className="btn btn-primary">
              Create Bet
            </button>
          </form>
        </div>
      </div> ) : (<p> Betting is on going... </p>)
      )}

      { !isAdmin() ? (null) : ( bet.state == 1 ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Cancel bet</h2>
          <button
            onClick={e => clickCancelHandle()}
            type="submit"
            className="btn btn-primary"
          >
            Cancel
          </button>
        </div>
      </div> ) : ( null )
      )}

      { isAdmin() ? ( null ) : ( bet.state == 1 ? (
      <div className="row">
        <div className="col-sm-12">
          <h2>Bet</h2>
          <button
            onClick={e => clickBetHandle()}
            type="submit"
            className="btn btn-primary"
          >
            Bet
          </button>
        </div>
      </div> ) : (<p> Game is not started yet </p>)
      )}

    </div>
  );
}

export default App;
