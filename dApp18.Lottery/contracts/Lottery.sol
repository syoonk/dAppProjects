pragma solidity >= 0.5.0 < 0.7.0;

contract Lottery {
    enum State {
        IDLE,
        BETTING
    }
    
    State public currentState = State.IDLE;
    address payable[] public players;
    uint public betCount;
    uint public betSize;
    uint public houseFee;   // percentage
    address payable public admin;
    
    constructor(uint _houseFee) 
    public {
        require( _houseFee >= 1 && _houseFee <= 99, "fee should be 1 to 99");
        houseFee = _houseFee;
        admin = msg.sender;
    }

    function getPlayers()
    external view
    returns (address[] memory) {
        address[] memory _players = new address[](players.length);
        for(uint i = 0; i < players.length; i++) {
            _players[i] = players[i];
        }
      
        return _players;
    }

    function createBet(uint _count, uint _size)
    external payable 
    inState(State.IDLE) onlyAdmin() {
        betCount = _count;
        betSize = _size;
        currentState = State.BETTING;
    }
    
    function bet()
    external payable 
    inState(State.BETTING) {
        require(msg.value == betSize, "can only bet exact betsize");
        players.push(msg.sender);
        if(players.length == betCount) {
            uint winner = _randomModulo(betCount);
            players[winner].transfer(betSize * betCount * (100 - houseFee) / 100);
            admin.transfer(address(this).balance);
            currentState = State.IDLE;
            delete players;
        }
    }
    
    function cancel()
    external
    inState(State.BETTING) onlyAdmin() {
        for(uint i = 0; i < players.length; i++) {
            players[i].transfer(betSize);
            delete players[i];
        }
    }
    
    function _randomModulo(uint _modulo)
    internal view
    returns (uint) {
        return uint(keccak256(abi.encodePacked(block.timestamp, block.difficulty))) % _modulo;
        
    }
    
    modifier inState(State _state) {
        require(currentState == _state, "current state doesn't allow this");
        _;
    }
    
    modifier onlyAdmin() {
        require(msg.sender == admin, "only admin");
        _;
    }
}