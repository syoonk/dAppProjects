const EtherWallet = artifacts.require("EtherWallet");

contract('EtherWallet', (accounts) => {
    let etherWallet = null;
    before(async () => {
        etherWallet = await EtherWallet.deployed();
    });

    it('should set accounts[0] as owner', async() => {
        const owner = await etherWallet.owner();
        assert(owner === accounts[0]);      
    });

    it('should deposit ether to etherWallet', async() => {
        await etherWallet.deposit({from: accounts[0], value: 100});
        const balance = await web3.eth.getBalance(etherWallet.address);
        assert(parseInt(balance) == 100);
    });

    it('should return balance of contract', async() => {
        const balance = await etherWallet.balanceOf();
        assert(parseInt(balance) == 100);
    });

    it('should transfer ether to another adderss', async() => {
        const balanceRecipientBefore = await web3.eth.getBalance(accounts[1]);
        const firstBalance = await web3.utils.toBN(balanceRecipientBefore);
        await etherWallet.send(accounts[1], 50, {from: accounts[0]});
        const balanceWallet = await web3.eth.getBalance(etherWallet.address);
        assert(parseInt(balanceWallet) == 50);

        const balanceRecipientAfter = await web3.eth.getBalance(accounts[1]);
        const finalBalance = await web3.utils.toBN(balanceRecipientAfter);
        assert(finalBalance.sub(firstBalance).toNumber() === 50);
    });

    it('should NOT transfer ether if sender is not owner', async() => {
        try {
            await etherWallet.send(accounts[0], 20, {from: accounts[1]});
        } catch(e) {
            assert(e.message.includes('sender is not owner'));
            return ;
        }

        assert(false);
    });
});