import Web3 from 'web3';
import EtherWallet from '../build/contracts/EtherWallet.json';

let web3;
let etherWallet;

const initWeb3 = () => {
  return new Promise((resolve, reject) => {
    if(typeof window.ethereum !== 'undefined') {
      const web3 = new Web3(window.ethereum);
      window.ethereum.enable()
        .then(() => {
          resolve(
            new Web3(window.ethereum)
          );
        })
        .catch(e => {
          reject(e);
        });
      return;
    }
    if(typeof window.web3 !== 'undefined') {
      return resolve(
        new Web3(window.web3.currentProvider)
      );
    }
    resolve(new Web3('http://localhost:9545'));
  });
};


const initContract = async () => {
  const networkId = await web3.eth.net.getId();
  return new web3.eth.Contract(
    EtherWallet.abi, 
    EtherWallet
      .networks[networkId]
      .address
  );
};

/*
const initContract = () => {
  const deploymentKey = Object.keys(EtherWallet.networks)[0];
  return new web3.eth.Contract(
    EtherWallet.abi,
    EtherWallet
      .networks[deploymentKey]
      .address
  );
};*/

const initApp = () => {
  let accounts = [];
  const $deposit = document.getElementById("deposit");
  const $depositResult = document.getElementById("deposit-result");
  const $balance = document.getElementById("balance");
  const $send = document.getElementById("send");
  const $sendResult = document.getElementById("send-result");

  web3.eth.getAccounts()
  .then(_accounts => {
    accounts = _accounts;
  });

  const showBalance = () => {
    etherWallet.methods.balanceOf().call()
    .then((result) => {
      $balance.innerHTML = `${result} wei`;
    }).catch (() => {
      $balance.innerHTML = `WOW!! Error while trying to get the balance amount`;
    });
  };

  showBalance();

  $deposit.addEventListener('submit', e => {
    e.preventDefault();
    const amount = e.target.elements[0].value;
    const toSend = web3.utils.toBN(amount);
    etherWallet.methods.deposit().send({from: accounts[0], value: toSend})
    .then(() => {
      $depositResult.innerHTML = `${toSend} was succesfully deposited!`;
      showBalance();
    }).catch(() => {
      $depositResult.innerHTML = `Ooops... there was an error while tying to deposit`;
    });
  });

  $send.addEventListener('submit', e => {
    e.preventDefault();
    const toAddress = e.target.elements[0].value;
    const amount = e.target.elements[1].value;
    etherWallet.methods.send(toAddress, amount).send({from: accounts[0]})
    .then(() => {
      $sendResult.innerHTML = `${amount} wei was succefully transferred to ${toAddress} wei`;
      showBalance();
    }).catch(() => {
      $sendResult.innerHTML = `Error... while trying to send ether`;
    });
  });
};

document.addEventListener('DOMContentLoaded', () => {
  initWeb3()
    .then(_web3 => {
      web3 = _web3;
      return initContract();
    })
    .then(_etherWallet => {
      etherWallet = _etherWallet;
      initApp(); 
    })
    .catch(e => console.log(e.message));
});
