pragma solidity ^0.5.0;

contract EtherWallet {
    address public owner;

    constructor(address _owner) public {
        owner = _owner;
    }

    function deposit() public payable {
    }

    function send(address payable _to, uint _amount) public {
        require(msg.sender == owner, 'sender is not owner');
        _to.transfer(_amount);
    }

    function balanceOf() public view returns(uint) {
        return address(this).balance;
    }
}